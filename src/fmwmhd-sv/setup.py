from setuptools import setup

setup(name="fmwnssv",
      version="0.0.1",
      description="Solvers for the stationary incompressible Navier-Stokes equations",
      packages = ["fmwnssv"])
