from firedrake import *
from firedrake.petsc import *

from fmwnssv.supg import *
from fmwnssv.element import *
from fmwnssv.utilities import coarsen
from fmwnssv.transfer import *
from fmwnssv.bary import BaryMeshHierarchy, bary

import pprint
import sys
from datetime import datetime

def scross(x, y):
    return x[0]*y[1] - x[1]*y[0]

def vcross(x,y):
    return as_vector([x[1]*y,-x[0]*y])

def scurl(x):
    return x[1].dx(0) - x[0].dx(1)

def vcurl(x):
    return as_vector([x.dx(1), -x.dx(0)])

def acurl(x):
    return as_vector([
                     x[2].dx(1),
                     -x[2].dx(0),
                     x[1].dx(0) - x[0].dx(1)
                     ])

class DGMassInv(PCBase):

    def initialize(self, pc):
        _, P = pc.getOperators()
        appctx = self.get_appctx(pc)
        V = dmhooks.get_function_space(pc.getDM())
        # get function spaces
        u = TrialFunction(V)
        v = TestFunction(V)
        massinv = assemble(Tensor(inner(u, v)*dx).inv)
        #massinv.force_evaluation()
        self.massinv = massinv.petscmat
        self.nu = appctx["nu"]
        self.gamma = appctx["gamma"]

    def update(self, pc):
        pass

    def apply(self, pc, x, y):
        self.massinv.mult(x, y)
        scaling = float(self.nu) + float(self.gamma)
        y.scale(-scaling)

    def applyTranspose(self, pc, x, y):
        raise NotImplementedError("Sorry!")


class NavierStokesSolver(object):

    def __init__(self, problem, nref=1, solver_type="almg",
                 stabilisation_type="none",
                 supg_method="shakib", supg_magic=9.0, gamma=10000, nref_viz=1,
                 k=5, patch="star", hierarchy="bary", use_mkl=False,
                 supg_weight=None, Rem=1, S=1, gamma2=1):
        self.problem = problem
        self.nref = nref
        self.solver_type = solver_type
        self.stabilisation_type = stabilisation_type
        assert solver_type in {"almg", "allu", "lu"}, "Invalid solver type %s" % solver_type
        assert stabilisation_type in {"none", "gls", "supg", "burman"}, "Invalid stabilisation type %s" % stabilisation_type

        self.patch = patch
        baseMesh = problem.mesh(problem.distribution_parameters)
        self.parallel = baseMesh.comm.size > 1
        self.tdim = baseMesh.topological_dimension()
        self.use_mkl = use_mkl

        if hierarchy != "bary" and patch == "macro":
            raise ValueError("macro patch only makes sense with a BaryHierarchy")

        if hierarchy == "bary":
            def before(dm, i):
                for p in range(*dm.getHeightStratum(1)):
                    dm.setLabelValue("prolongation", p, i+1)

            def after(dm, i):
                for p in range(*dm.getHeightStratum(1)):
                    dm.setLabelValue("prolongation", p, i+2)

            mh = BaryMeshHierarchy(baseMesh, nref + nref_viz, callbacks=(before, after), reorder=True, distribution_parameters=problem.distribution_parameters)

        elif hierarchy == "uniformbary":
            bmesh = Mesh(bary(baseMesh._plex), distribution_parameters={"partition": False})
            mh = MeshHierarchy(bmesh, nref + nref_viz, reorder=True, distribution_parameters=problem.distribution_parameters)
        elif hierarchy == "uniform":
            bmesh = baseMesh
            mh = MeshHierarchy(bmesh, nref + nref_viz, reorder=True, distribution_parameters=problem.distribution_parameters)
        else:
            raise NotImplementedError("Only know bary, uniformbary and uniform for the hierarchy.")
        self.area = assemble(Constant(1, domain=mh[0])*dx)
        nu = Constant(1.0)
        self.nu = nu
        self.char_L = problem.char_length()
        self.char_U = problem.char_velocity()
        Re = self.char_L*self.char_U / nu
        self.Re = Re
        if not isinstance(gamma, Constant):
            gamma = Constant(gamma)
        self.gamma = gamma
        self.advect = Constant(0)
        self.Rem = Rem
        self.S = S
        self.gamma2 = gamma2

        mesh = mh[-1 - nref_viz]
        uvizs = []
        for i in range(nref_viz):
            V = FunctionSpace(mh[-1-nref_viz+i+1], velocity_element(mh[-1-nref_viz+i+1], k))
            uvizs.append(Function(V))

        def viz_prolong(u):
            if nref_viz == 0:
                return u
            uc = u
            for i in range(nref_viz):
                prolong(uc, uvizs[i])
                uc = uvizs[i]
            return uvizs[-1]

        self.viz_prolong = viz_prolong

        self.mesh = mesh
        self.message(GREEN % "In parallel Mesh OVERLAP IS TOO BIG")
        Uel = velocity_element(mesh, k)
        Pel = pressure_element(mesh, k)
        Bel = magnetic_element(mesh, k)
        Sigmael = sigma_element(mesh, k)
        
        V = FunctionSpace(mesh, Uel)
        Q = FunctionSpace(mesh, Pel)
        W = FunctionSpace(mesh, Bel)
        R = FunctionSpace(mesh, Sigmael)
        Zel = MixedElement([Uel, Pel, Bel, Sigmael])
        Z = FunctionSpace(mesh, Zel)
        self.Z = Z
        self.V = V
        self.Q = Q
        self.W = W
        self.R = R

        Zdim = Z.dim(); size = mesh.mpi_comm().size
        if mesh.mpi_comm().rank == 0:
            print("Number of degrees of freedom: %s (avg %.2f per core)" % (Zdim, Zdim / size))
        z = Function(Z, name="Solution")
        self.z = z
        (u, p, B, sigma) = split(z)
        z.split()[0].rename("Velocity")
        z.split()[1].rename("Pressure")
        z.split()[2].rename("MageneticField")
        z.split()[3].rename("Sigma")
        (v, q, C, tau) = split(TestFunction(Z))

        nsp = problem.nullspace(Z)
        self.nsp = nsp
        bcs = problem.bcs(Z)

        params = self.get_parameters()
        if mesh.mpi_comm().rank == 0:
            pprint.pprint(params)
            sys.stdout.flush()

        if supg_weight is None:
            supg_weight = Constant(0.1) if self.tdim == 3 else Constant(1)
        self.z_last = z.copy(deepcopy=True)
        supg_wind = split(self.z_last)[0]
        if supg_method == "turek":
            SUPG = TurekSUPG(Re, V, state=supg_wind, h=problem.mesh_size(u), magic=supg_magic, weight=supg_weight)
        elif supg_method == "shakib":
            SUPG = ShakibHughesZohanSUPG(1.0/nu, V, state=supg_wind, h=problem.mesh_size(u), magic=supg_magic, weight=supg_weight)
        else:
            raise NotImplementedError
        
        k = V.ufl_element().degree()
        rhs = problem.rhs(Z, Re, S)
        ###########Does this change for MHD system?######################
        Lu = -nu * div(2*sym(grad(u))) + dot(grad(u), u) + grad(p) - Constant(1)*gamma**0.5 * grad(div(u))
        Lv = -nu * div(2*sym(grad(v))) + dot(grad(v), supg_wind) + grad(q) - Constant(1)*gamma**0.5 * grad(div(v))

        if rhs is not None:
            Lu -= rhs[0]
        self.SUPG = SUPG

        F_old = (
            nu * inner(2*sym(grad(u)), grad(v))*dx
            + gamma * inner(div(u), div(v))*dx
            + self.advect * inner(dot(grad(u), u), v)*dx
            - p * div(v) * dx
            - div(u) * q * dx  
        )

        F = (
            nu * inner(2*sym(grad(u)), grad(v))*dx
            + self.advect * inner(dot(grad(u), u), v) * dx 
            + gamma * inner(div(u), div(v)) * dx
            + S * inner(scross(v, B), scurl(B)) * dx
            - p * div(v) * dx
            - div(u) * q * dx
            + S/Rem * inner(scurl(B), scurl(C)) * dx
            - S * inner(scross(u, B), scurl(C)) * dx
            + gamma2 * inner(grad(sigma), C) * dx
            - inner(sigma, tau) * dx
            + inner(B, grad(tau)) * dx
        )


        if self.stabilisation_type == "gls":
            stabilisation = SUPG.form_gls(Lu, Lv, dx(degree=2*k))
        elif self.stabilisation_type == "supg":
            stabilisation = SUPG.form(Lu, v, dx(degree=2*k))
        elif self.stabilisation_type == "burman":
            n = FacetNormal(mesh)
            h = FacetArea(mesh)
            if self.tdim == 3:
                h = h**0.5 # go from area to length
            beta = Constant(1)  # Should be an advecting velocity in some norm.
            beta = sqrt(avg(facet_avg(dot(supg_wind, n)**2)))
            gamma1 = Constant(4e-3) # as chosen in doi:10.1016/j.apnum.2007.11.001
            stabilisation = 0.5 * gamma1 * h**2 * beta * dot(jump(grad(u), n), jump(grad(v), n))*dS
        else:
            stabilisation = 0

        if stabilisation != 0:
            F += (self.advect * stabilisation)

        if rhs is not None:
            F -= inner(rhs[0], v) * dx + inner(rhs[1], q) * dx

        appctx = {"nu": self.nu, "gamma": self.gamma}
        problem = NonlinearVariationalProblem(F, z, bcs=bcs)
        solver = NonlinearVariationalSolver(problem, solver_parameters=params,
                                            nullspace=nsp, options_prefix="ns_",
                                            appctx=appctx)
        self.solver = solver

        if hierarchy == "bary":
            prolongation = SchoeberlProlongation((Re, gamma), self.tdim)
            self.prolongation = prolongation

            if self.stabilisation_type in ["burman", "none"]:
                injection = NullTransfer()
            elif self.stabilisation_type in ["gls", "supg"]:
                injection = DGTransfer()
            else:
                raise ValueError("Unknown stabilisation")

            self.injection = injection

            solver.set_transfer_operators(dmhooks.transfer_operators(V, prolong=prolongation.prolong),
                                          dmhooks.transfer_operators(Q, inject=injection.inject))

        self.check_nograddiv_residual = False

        if self.check_nograddiv_residual:
            self.message(GREEN % "Checking residual without grad-div term")
            self.F_nograddiv = replace(F, {gamma: 0})
            self.F = F
            self.bcs = bcs

    def solve(self, re):
        self.z_last.assign(self.z)
        self.message(GREEN % ("Solving for Re = %s" % re))
        if re == 0:
            self.message(GREEN % ("Solving Stokes"))
            self.advect.assign(0)
            re = 1
        else:
            self.advect.assign(1)

        self.nu.assign(self.char_L*self.char_U/re)
        self.SUPG.update(self.z.split()[0])
        start = datetime.now()
        self.solver.solve()
        end = datetime.now()

        if self.nsp is not None:
            # Hardcode that pressure is constant
            (u, p, B, sigma) = self.z.split()
            pintegral = assemble(p*dx)
            p.assign(p - Constant(pintegral/self.area))

        if self.check_nograddiv_residual:
            F_ngd = assemble(self.F_nograddiv)
            for bc in self.bcs:
                bc.zero(F_ngd)
            F = assemble(self.F)
            for bc in self.bcs:
                bc.zero(F)
            with F_ngd.dat.vec_ro as v_ngd, F.dat.vec_ro as v:
                self.message(BLUE % ("Residual without grad-div term: %.14e" % v_ngd.norm()))
                self.message(BLUE % ("Residual with grad-div term:    %.14e" % v.norm()))
        Re_linear_its = self.solver.snes.getLinearSolveIterations()
        Re_nonlinear_its = self.solver.snes.getIterationNumber()
        Re_time = (end-start).total_seconds() / 60
        self.message(GREEN % ("Time taken: %.2f min in %d iterations (%.2f Krylov iters per Newton step)" % (Re_time, Re_linear_its, Re_linear_its/float(Re_nonlinear_its))))
        info_dict = {
            "Re": re,
            "Rem": Rem,
            "nu": self.nu.values()[0],
            "linear_iter": Re_linear_its,
            "nonlinear_iter": Re_nonlinear_its,
            "time": Re_time,
        }
        return (self.z, info_dict)

    def get_parameters(self):
        multiplicative = self.problem.relaxation_direction() is not None
        patchlu3d = "mkl_pardiso" if self.use_mkl else "umfpack"
        patchlu2d = "petsc"
        mg_levels_solver = {
            "ksp_type": "fgmres",
            "ksp_norm_type": "unpreconditioned",
            "ksp_max_it": 3 if self.tdim > 2 else 3,
            "ksp_convergence_test": "skip",
            "pc_type": "python",
            "pc_python_type": "firedrake.PatchPC",
            "patch_pc_patch_save_operators": True,
            "patch_pc_patch_partition_of_unity": False,
            "patch_pc_patch_sub_mat_type": "seqaij" if self.tdim > 2 else "seqdense",
            "patch_pc_patch_sub_mat_type": "aij",
            "patch_pc_patch_local_type": "multiplicative" if multiplicative else "additive",
            "patch_pc_patch_statistics": False,
            "patch_pc_patch_symmetrise_sweep": multiplicative,
            "patch_pc_patch_precompute_element_tensors": True,
            "patch_sub_ksp_type": "preonly",
            "patch_sub_pc_type": "lu",
            "patch_sub_pc_factor_mat_solver_type": patchlu3d if self.tdim > 2 else patchlu2d
        }

        if self.patch == "star":
            if multiplicative:
                mg_levels_solver["patch_pc_patch_construct_type"] = "python"
                mg_levels_solver["patch_pc_patch_construct_python_type"] = "fmwnssv.Star"
                mg_levels_solver["patch_pc_patch_construction_Star_sort_order"] = self.problem.relaxation_direction()
            else:
                mg_levels_solver["patch_pc_patch_construct_type"] = "star"
                mg_levels_solver["patch_pc_patch_construct_dim"] = 0
        elif self.patch == "macro":
            mg_levels_solver["patch_pc_patch_construct_type"] = "python"
            mg_levels_solver["patch_pc_patch_construct_python_type"] = "fmwnssv.MacroStar"
            mg_levels_solver["patch_pc_patch_construction_MacroStar_sort_order"] = self.problem.relaxation_direction()
        else:
            raise NotImplementedError("Unknown patch type %s" % self.patch)

        fieldsplit_0_lu = {
            "ksp_type": "preonly",
            "ksp_max_it": 1,
            "pc_type": "lu",
            "pc_factor_mat_solver_type": "mkl_pardiso" if self.use_mkl else "mumps",
        }

        fieldsplit_0_mg = {
            "ksp_type": "richardson",
            "ksp_richardson_self_scale": False,
            "ksp_max_it": 2,
            "ksp_norm_type": "unpreconditioned",
            "ksp_convergence_test": "skip",
            "pc_type": "mg",
            "pc_mg_type": "full",
            "mg_levels": mg_levels_solver,
            "mg_coarse_pc_type": "python",
            "mg_coarse_pc_python_type": "firedrake.AssembledPC",
            "mg_coarse_assembled_pc_type": "lu",
            "mg_coarse_assembled_pc_factor_mat_solver_type": "mumps",
        }

        fieldsplit_1 = {
            "ksp_type": "preonly",
            "pc_type": "python",
            "pc_python_type": "fmwnssv.solver.DGMassInv"
        }

        use_mg = self.solver_type == "almg"
        outer = {
            "snes_type": "newtonls",
            "snes_linesearch_type": "basic",
            "snes_linesearch_maxstep": 1.0,
            "snes_monitor": None,
            "snes_linesearch_monitor": None,
            "snes_converged_reason": None,
            "snes_rtol": 1.0e-9,
            "snes_atol": 1.0e-8,
            "snes_stol": 1.0e-6,
            #"mat_type": "matfree" if use_mg else "aij",
            "mat_type": "nest",
            "ksp_type": "fgmres",
            "ksp_rtol": 1.0e-9,
            "ksp_atol": 1.0e-10,
            "ksp_max_it": 500,
            "ksp_monitor_true_residual": None,
            "ksp_converged_reason": None,
            "pc_type": "fieldsplit",
            "pc_fieldsplit_type": "schur",
            "pc_fieldsplit_schur_factorization_type": "full",
            "pc_fieldsplit_schur_precondition": "user",
            "fieldsplit_0": fieldsplit_0_mg if use_mg else fieldsplit_0_lu,
            "fieldsplit_1": fieldsplit_1,
        }

        outer_lu = {
            "snes_type": "newtonls",
            "snes_linesearch_type": "l2",
            "snes_linesearch_maxstep": 1.0,
            "snes_monitor": None,
            "snes_linesearch_monitor": None,
            "snes_rtol": 1.0e-8,
            "snes_atol": 1.0e-7,
            "snes_stol": 0.0,
            "mat_type": "aij",
            "ksp_type": "fgmres",
            "ksp_rtol": 1.0e-9,
            "ksp_atol": 1.0e-10,
            "ksp_max_it": 1,
            "ksp_convergence_test": "skip",
            "ksp_monitor_true_residual": None,
            "ksp_converged_reason": None,
            "pc_type": "lu",
            "pc_factor_mat_solver_type": "mumps",
            "mat_mumps_icntl_14": 200,
            # "mat_mumps_icntl_24": 1,
            # "mat_mumps_icntl_25": 1,
        }

        parameters["default_sub_matrix_type"] = "aij" if self.use_mkl else "baij"

        if self.tdim > 2:
            outer["ksp_atol"] = 1.0e-8
            outer["ksp_rtol"] = 1.0e-8
            outer["snes_atol"] = outer["ksp_atol"]
            outer["snes_rtol"] = outer["ksp_rtol"]

        if self.solver_type == "lu":
            return outer_lu
        else:
            return outer

    def message(self, msg):
        if self.mesh.comm.rank == 0:
            warning(msg)
