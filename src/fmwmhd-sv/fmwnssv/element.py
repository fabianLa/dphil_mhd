from firedrake import *

def velocity_element(mesh, k):
    ele = VectorElement("Lagrange", mesh.ufl_cell(), k)
    return ele

def pressure_element(mesh, k):
    return FiniteElement("Discontinuous Lagrange", mesh.ufl_cell(), k-1)

def magnetic_element(mesh, k):
    return FiniteElement("N1curl", mesh.ufl_cell(), k)

def sigma_element(mesh, k):
    return FiniteElement("CG", mesh.ufl_cell(), k)
