from functools import singledispatch
from firedrake.slate.slac.compiler import PETSC_DIR
from firedrake import TensorFunctionSpace, Jacobian, interpolate, Function, op2
from firedrake.mg.ufl_utils import coarsen as default_coarsen


def cell_metric(domain, metric=None):
    V = TensorFunctionSpace(domain, "DG", 0)
    J = interpolate(Jacobian(domain), V)
    metric = metric or Function(V, name="CellMetric")
    kernel = op2.Kernel("""
#include <Eigen/Dense>
void polar(double A_[9], const double * B_) {
   Eigen::Map<Eigen::Matrix<double, 3, 3, Eigen::RowMajor> > A((double *)A_);
   Eigen::Map<Eigen::Matrix<double, 3, 3, Eigen::RowMajor> > B((double *)B_);
   Eigen::JacobiSVD<Eigen::Matrix<double, 3, 3, Eigen::RowMajor> > svd(B, Eigen::ComputeFullV);
   A += svd.matrixV() * svd.singularValues().asDiagonal() * svd.matrixV().transpose();
}""", "polar", cpp=True, include_dirs=["%s/include/eigen3" % d for d in PETSC_DIR])
    op2.par_loop(kernel, V.node_set, metric.dat(op2.INC), J.dat(op2.READ))
    return metric



@singledispatch
def coarsen(expr, callback, coefficient_mapping=None):
    return default_coarsen(expr, callback, coefficient_mapping=coefficient_mapping)


@coarsen.register(Function)
def coarsen_function(expr, callback, coefficient_mapping=None):
    if coefficient_mapping is None:
        coefficient_mapping = {}
    new = coefficient_mapping.get(expr)
    if new is None:
        if expr.name() == "CellMetric":
            domain = callback(expr.ufl_domain(), callback)
            new = cell_metric(domain)
        else:
            new = default_coarsen(expr, callback, coefficient_mapping=coefficient_mapping)
    return new
