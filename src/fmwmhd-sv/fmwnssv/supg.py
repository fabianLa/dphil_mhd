from firedrake import *
from firedrake.dmhooks import get_appctx, get_function_space
from firedrake.mg.utils import get_level


class SUPG(object):
    def __init__(self, Re, V, state=None, h=None, magic=1.0, weight=1.0):
        self.V = V
        self.Re = Re
        self.mesh = V.mesh()
        self.magic = magic
        self.weight = weight
        if state is None:
            self.wind = Function(V, name="Wind")
            self.separate_wind = True
        else:
            self.wind = state
            self.separate_wind = False
        self.h = CellSize(mesh) if h is None else h

    def update(self, w):
        if not self.separate_wind:
            return

        if isinstance(w, Function):
            self.wind.assign(w)
        else:
            with self.wind.dat.vec_wo as wind_v:
                w.copy(wind_v)

        # Need to update your coarse grid mates, too.
        # There's probably a better way to do this.
        dm = self.V.dm
        wind = self.wind

        while get_level(get_function_space(dm).mesh())[1] > 0:
            cdm = dm.coarsen()

            cctx = get_appctx(cdm)
            if cctx is None:
                break
            cwind = cctx.F._cache['coefficient_mapping'][wind]
            inject(wind, cwind)

            dm = cdm
            wind = cwind

    def coefficient(self):
        raise NotImplementedError

    def form(self, Lu, v, dx):
        beta = self.coefficient()
        w = self.wind
        return self.weight * beta * inner(Lu, dot(grad(v), w)) * dx

    def form_gls(self, Lu, Lv, dx):
        beta = self.coefficient()
        w = self.wind
        return self.weight * beta * inner(Lu, Lv) * dx


class ShakibHughesZohanSUPG(SUPG):
    """
    Stabilisation described by (3.58) in

    @article{shakib1991,
        doi = {10.1016/0045-7825(91)90041-4},
        year = 1991,
        volume = {89},
        number = {1-3},
        pages = {141--219},
        author = {F. Shakib and T. J. R. Hughes and Z. Johan},
        title = {A new finite element formulation for computational fluid dynamics: {X}. {T}he compressible {E}uler and {N}avier-{S}tokes equations},
        journal = {Computer Methods in Applied Mechanics and Engineering}
    }
    """

    def coefficient(self):
        mesh = self.mesh
        w = self.wind
        nu = 1.0/self.Re

        h = self.h
        beta = ((4.0*dot(w, w)/(h**2)) + self.magic*(4.0*nu/h**2)**2 )**(-0.5)
        return beta


class TurekSUPG(SUPG):

    """
    Stabilisation described in

    @book{turek1999,
      Author = {S. Turek},
      Title = {Efficient Solvers for Incompressible Flow Problems: An Algorithmic and Computational Approach},
      series = {Lecture Notes in Computational Science and Engineering},
      Publisher = {Springer},
      Year = {2013},
      ISBN = {3642635733},
    }
    """

    def __init__(self, *args, **kwargs):
        SUPG.__init__(self, *args, **kwargs)
        self.w_avg = Constant(0.0)
        self.domain_measure = assemble(Constant(1.0)*dx(domain=self.mesh))

    def update(self, w):
        SUPG.update(self, w)
        nrm = assemble(sqrt(inner(self.wind, self.wind))*dx)
        self.w_avg.assign(nrm/self.domain_measure)

    def coefficient(self):
        Re = self.Re
        w = self.wind
        h = self.h

        Re_tau = cell_avg(sqrt(inner(w, w))) * h * Re
        w_avg = self.w_avg
        beta = self.magic * h * 2. * Re_tau / ((w_avg) * (1. + Re_tau))
        return beta
