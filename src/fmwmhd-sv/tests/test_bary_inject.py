from firedrake import *
from firedrake.petsc import PETSc
from fmwnssv import *

def test_bary_inject():
    d = 2
    if d == 2:
        mesh = UnitTriangleMesh()
        # mesh = Mesh("triangle3.msh")
        # mesh = UnitSquareMesh(4, 4)
    else:
        mesh = UnitCubeMesh(1, 1, 1)

    mh = BaryMeshHierarchy(mesh, 1)
    # mh = MeshHierarchy(mesh, 1)

    coarse = mh[-2]
    fine = mh[-1]

    Qe = FiniteElement("DG", mesh.ufl_cell(), 2)

    Vc = FunctionSpace(coarse, Qe)
    Vf = FunctionSpace(fine, Qe)
    print(assemble(TestFunction(Vc) * dx).vector()[:])

    dginjector = DGInjection()
    uf = Function(Vf, name="uf")
    uc = Function(Vc, name="uc")
    Xf = SpatialCoordinate(fine)
    Xc = SpatialCoordinate(coarse)
    def uex(X):
        return X[0] * (1-X[0])
        # return Constant(1.)
    uf.interpolate(uex(Xf))
    dginjector.inject(uf, uc)


    errnorm = errornorm(uex(Xc), uc)
    print(errnorm)
    File("output/fine.pvd").write(uf)
    File("output/coarse.pvd").write(uc)

    assert errnorm < 1e-10

def test_bary_prolong():
    d = 2
    mesh = UnitTriangleMesh()
    # mesh = UnitSquareMesh(1, 1)

    mh = BaryMeshHierarchy(mesh, 1)
    # mh = MeshHierarchy(mesh, 1)

    coarse = mh[-2]
    fine = mh[-1]

    Qe = FiniteElement("DG", mesh.ufl_cell(), 1)

    Vc = FunctionSpace(coarse, Qe)
    Vf = FunctionSpace(fine, Qe)

    uf = Function(Vf, name="uf")
    uc = Function(Vc, name="uc")
    ucvec = uc.vector()
    for i in range(len(ucvec)):
        ucvec[i] = i

    # uc.interpolate(Expression("x[0]*(1-x[0])"))
    print(norm(uc))
    prolong(uc, uf)
    print(norm(uf))

    print(uc.vector()[:])
    # print(onec.vector()[:])



    File("output/fine.pvd").write(uf)
    File("output/coarse.pvd").write(uc)

    # assert errnorm < 1e-10

if __name__ == "__main__":
    test_bary_inject()
    # test_bary_prolong()
