from firedrake import *
from firedrake.petsc import PETSc
from fmwnssv import *

def test_bary_prolong():
    d = 3
    if d == 2:
        mesh = UnitTriangleMesh()
        # mesh = Mesh("triangle3.msh")
        mesh = UnitSquareMesh(1, 1)
    else:
        mesh = UnitCubeMesh(1, 1, 1)

    def before(dm, i):
        for p in range(*dm.getHeightStratum(1)):
            dm.setLabelValue("prolongation", p, i+1)

    def after(dm, i):
        for p in range(*dm.getHeightStratum(1)):
            dm.setLabelValue("prolongation", p, i+2)
    mh = BaryMeshHierarchy(mesh, 2, callbacks=(before, after))

    coarse = mh[-2]
    fine = mh[-1]

    Ve = VectorElement("CG", mesh.ufl_cell(), d)
    Qe = FiniteElement("DG", mesh.ufl_cell(), d-1)
    Ze = MixedElement([Ve, Qe])

    Zc = FunctionSpace(coarse, Ze)
    Zf = FunctionSpace(fine, Ze)

    zc = Function(Zc)
    (uc, pc) = split(zc)
    (vc, qc) = split(TestFunction(Zc))

    x = SpatialCoordinate(coarse)
    if d == 2:
        bcsc = DirichletBC(Zc.sub(0), (x[1]*x[1], x[0]*x[0]), "on_boundary")
        f = as_vector([x[1]**(d+1), x[0]**(d+1)])
    else:
        bcsc = DirichletBC(Zc.sub(0), (x[2]*x[2], x[0]*x[0], x[1]*x[1]), "on_boundary")
        f = as_vector([x[2]**(d+1), x[0]**(d+1), x[1]**(d+1)])

    F = inner(grad(uc), grad(vc))*dx - inner(div(vc), pc)*dx - inner(f, vc)*dx - inner(div(uc), qc)*dx
    nspc = MixedVectorSpaceBasis(Zc, [Zc.sub(0), VectorSpaceBasis(constant=True)])
    solve(F == 0, zc, bcsc, nullspace=nspc, 
          solver_parameters={
              "ksp_type": "gmres",
              "pc_type": "lu",
              "pc_factor_mat_solver_type": "mumps",
              "mat_type": "aij",
              "ksp_monitor": True,
              "ksp_converged_reason": True,
              "mat_mumps_icntl_14": 300,
          })

    uc = zc.split()[0]

    divucnorm = norm(div(uc))
    print("Divergence on coarse grid", divucnorm)
    assert divucnorm < 1.0e-9

    uf = Function(FunctionSpace(fine, Ve))
    File("/tmp/coarse.pvd").write(uc)

    Re = 1e4
    gamma = 1e4
    tdim = fine.topological_dimension()
    sp = SchoeberlProlongation(Re, gamma, tdim)

    prolong(uc, uf)
    divufnorm = norm(div(uf))
    print("Divergence on fine grid (before correction)", divufnorm)
    assert divufnorm > 1e-4

    sp.prolong(uc, uf)
    divuf = project(div(uf), FunctionSpace(fine, Qe))
    File("/tmp/finediv.pvd").write(divuf)

    File("/tmp/fine.pvd").write(uf)
    divufnorm = norm(div(uf))

    print("Divergence on fine grid (after correction)", divufnorm)
    assert divufnorm < 1e-8

    ipuc = Function(FunctionSpace(coarse, Ve))
    inject(uf, ipuc)
    diff = assemble(inner(uc - ipuc, uc - ipuc)*dx)
    print(diff)

if __name__ == "__main__":
    test_bary_prolong()
