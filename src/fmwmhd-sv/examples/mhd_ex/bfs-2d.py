from firedrake import *
from fmwnssv import *

import os
import shutil

def scross(x, y):
    return x[0]*y[1] - x[1]*y[0]

def vcross(x,y):
    return as_vector([x[1]*y,-x[0]*y])

def scurl(x):
    return x[1].dx(0) - x[0].dx(1)

def vcurl(x):
    return as_vector([x.dx(1), -x.dx(0)])

def acurl(x):
    return as_vector([
                     x[2].dx(1),
                     -x[2].dx(0),
                     x[1].dx(0) - x[0].dx(1)
                     ])

class TwoDimBackwardsFacingStepProblem(NavierStokesProblem):
    def __init__(self):
        super().__init__()

    def mesh(self, distribution_parameters):
        return Mesh("backwards-facing-step.msh", distribution_parameters=distribution_parameters)

    def bcs(self, Z):
        (x, y) = SpatialCoordinate(Z.mesh())
        u_ex = as_vector([(cosh(1.0)-cosh(y))/(sinh(1.0)), Constant(0, domain=Z.mesh())])
        p_ex = -x
        sigma_ex = 0.0
        B_ex = as_vector([(sinh(y)-y*sinh(1))/(cosh(1.0)),Constant(0, domain=Z.mesh())])
        bcs = [DirichletBC(Z.sub(0), u_ex, "on_boundary"),
               DirichletBC(Z.sub(2), B_ex , "on_boundary"),
               DirichletBC(Z.sub(3), sigma_ex , "on_boundary")]
        return bcs

    def has_nullspace(self):
        return False
    
    def rhs(self, Z, Re, S):
        (x, y) = SpatialCoordinate(Z.mesh())
        u_ex = as_vector([(cosh(1.0)-cosh(y))/(sinh(1.0)), Constant(0, domain=Z.mesh())])
        p_ex = -x
        sigma_ex = 0.0
        B_ex = as_vector([(sinh(y)-y*sinh(1))/(cosh(1.0)),Constant(0, domain=Z.mesh())])
        f = -1/Re * div(sym(grad(u_ex))) + dot(grad(u_ex),u_ex) + grad(p_ex) + S * vcross(B_ex, scurl(B_ex))
        return None #as_vector([f, Constant(0, domain=Z.mesh())])

    # def relaxation_direction(self): return "0+:1-"


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument("--nref", type=int, default=1)
    parser.add_argument("--patch", type=str, default="star")
    parser.add_argument("--k", type=int, default=5)
    parser.add_argument("--clear", dest="clear", default=False, action="store_true")
    parser.add_argument("--time", dest="time", default=False, action="store_true")
    parser.add_argument("--mkl", dest="mkl", default=False, action="store_true")
    parser.add_argument("--mh", type=str, default="bary")
    parser.add_argument("--solver-type", type=str, default="almg")
    parser.add_argument("--stabilisation-type", type=str, default="none")
    parser.add_argument("--gamma", type=float, default=10000)
    parser.add_argument("--Rem", type=float, default=1)
    parser.add_argument("--S", type=float, default=1)
    parser.add_argument("--gamma2", type=float, default=1)
    args, _ = parser.parse_known_args()

    if args.clear:
        shutil.rmtree("checkpoint", ignore_errors=True)
    if args.time:
        PETSc.Log.begin()
    # user has to create mesh first by running make first
    problem = TwoDimBackwardsFacingStepProblem()
    solver = NavierStokesSolver(problem, solver_type=args.solver_type,
                                stabilisation_type=args.stabilisation_type,
                                nref=args.nref, k=args.k, gamma=args.gamma, nref_viz=0, patch=args.patch, use_mkl=args.mkl, supg_method="shakib", supg_weight=1., hierarchy=args.mh, Rem=args.Rem, S=args.S, gamma2=args.gamma2)
    comm = solver.mesh.mpi_comm()

    pvd_u = File("output/velocity.pvd")
    pvd_p = File("output/pressure.pvd")
    os.makedirs("checkpoint", exist_ok=True)
    start = 200
    end = 10000
    step = 100
    res = [0, 1, 10, 100] + list(range(start, end+step, step))
    res = [10]
    results = []
    for re in res:
        try:
            with DumbCheckpoint("checkpoint/nssolution-dofs-%s-Re-%s" % (solver.Z.dim(), re), mode=FILE_READ) as checkpoint:
                checkpoint.load(solver.z, name="up_%i" % re)
            pvd_u.write(solver.viz_prolong(solver.z.split()[0]))
            continue
        except:
            pass
        (z, info_dict) = solver.solve(re)
        results.append(info_dict)
        with DumbCheckpoint("checkpoint/nssolution-dofs-%s-Re-%s" % (solver.Z.dim(), re), mode=FILE_UPDATE) as checkpoint:
            checkpoint.store(solver.z, name="up_%i" % re)
        pvd_u.write(solver.viz_prolong(solver.z.split()[0]), time=re)
        pvd_p.write(solver.z.split()[1], time=re)

    if comm.rank == 0:
        for info_dict in results:
            print(info_dict)
