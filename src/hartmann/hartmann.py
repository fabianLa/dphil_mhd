# -*- coding: utf-8 -*-
from firedrake import *
from datetime import datetime
import alfi
import argparse
import numpy
from pyop2.datatypes import IntType
import ufl.algorithms

distribution_parameters={"partition": True, "overlap_type": (DistributedMeshOverlapType.VERTEX, 2)}

def scross(x, y):
    return x[0]*y[1] - x[1]*y[0]

def vcross(x,y):
    return as_vector([x[1]*y,-x[0]*y])

def scurl(x):
    return x[1].dx(0) - x[0].dx(1)

def vcurl(x):
    return as_vector([x.dx(1), -x.dx(0)])

def acurl(x):
    return as_vector([
                     x[2].dx(1),
                     -x[2].dx(0),
                     x[1].dx(0) - x[0].dx(1)
                     ])

class InnerSchurPC(AuxiliaryOperatorPC):
    def form(self, pc, v, u):
        mesh = v.function_space().mesh()
        n = FacetNormal(mesh)
        h = CellDiameter(mesh)
        h_avg = (h('+') + h('-'))/2
        alpha = Constant(4)

        state = self.get_appctx(pc)['state']
        [u_n, B_n, p_n, sigma_n] = split(state)

        K =  S * gamma2 *(
              +  inner(grad(u), grad(v))*dx
              +  alpha/h_avg*inner(jump(v, n), jump(u, n))*dS
            )
        
        if not picard:
             K = K  - S * inner(scross(u_n, u), scurl(v))*dx

        bcs = [DirichletBC(v.function_space(), 0, "on_boundary")]

        return (K, bcs)

class SchurPC(AuxiliaryOperatorPC):
    def form(self, pc, V, U):
        mesh = V.function_space().mesh()
        [u, p] = split(U)
        [v, q] = split(V)
        h = CellDiameter(mesh)

        state = self.get_appctx(pc)['state']
        [u_n, B_n, p_n, sigma_n] = split(state)

        eps = lambda x: sym(grad(x))

        A =  (
                2 * nu * inner(eps(u), eps(v))*dx
              + advect * inner(dot(grad(u_n), u), v) * dx
              + advect * inner(dot(grad(u), u_n), v) * dx
              + gamma * inner(div(u), div(v)) * dx
              - inner(p, div(v)) * dx
              - inner(div(u), q) * dx
                        )

        #norm_u = sqrt(assemble(inner(u_ex, u_ex)*dx)) # h = 1/40/4/2
        #lambda_star = 1 / (1 + Rem * h * norm_u )
        lambda_star = lambda_

        K1 = lambda_star * S * Rem * inner(scross(B_n, u), scross(B_n, v)) * dx
        
        if picard: 
            lambda_star = 1
            K = lambda_star * S * Rem * inner(vcross(B_n, scross(u, B_n)), v) * dx
        else:
            K2 = lambda_star * S * inner(vcross(B_n, scross(u_n, vcurl(scross(u, B_n)))), v) * dx
            K = K1

        A = A + K
        
        
        bcs = [DirichletBC(V.function_space().sub(0), 0, "on_boundary"),
               PressureFixBC(V.function_space().sub(1), 0, 1),
        ]

        return (A, bcs)

class XuPC(AuxiliaryOperatorPC):
    def form(self, pc, V, U):
        mesh = V.function_space().mesh()
        [u, B, p, sigma] = split(U)
        [v, C, q, tau] = split(V)

        state = self.get_appctx(pc)['state']
        [u_n, B_n, p_n, sigma_n] = split(state)



        eps = lambda x: sym(grad(x))

        A =  (
                2 * nu * inner(eps(u), eps(v)) * dx
              + gamma * inner(div(u), div(v)) * dx
              #+ inner(div(u), q) * dx
              + inner(p, q) * dx
              + 1/Rem * inner(scurl(B), scurl(C)) * dx
              + inner(B, C) * dx
              #- inner(scross(u, B_n), scurl(C)) * dx
              + inner(B, grad(tau)) * dx
              #+ inner(sigma, tau) * dx
              - inner(grad(sigma), grad(tau)) * dx
                        )            
             
        bcs = [DirichletBC(V.function_space().sub(0), 0, "on_boundary"),
               DirichletBC(V.function_space().sub(1), 0, "on_boundary"),
               DirichletBC(V.function_space().sub(3), 0, "on_boundary"),
               PressureFixBC(V.function_space().sub(2), 0, 1),
        ]

        return (A, bcs)

class PressureFixBC(DirichletBC):
    def __init__(self, V, val, subdomain, method="topological"):
        super().__init__(V, val, subdomain, method)
        sec = V.dm.getDefaultSection()
        dm = V.mesh()._plex

        coordsSection = dm.getCoordinateSection()
        coordsDM = dm.getCoordinateDM()
        dim = dm.getCoordinateDim()
        coordsVec = dm.getCoordinatesLocal()

        (vStart, vEnd) = dm.getDepthStratum(0)
        indices = []
        for pt in range(vStart, vEnd):
            x = dm.getVecClosure(coordsSection, coordsVec, pt).reshape(-1, dim).mean(axis=0)
            if x.dot(x) == 0.0: # fix [0, 0] in original mesh coordinates (bottom left corner)
                if dm.getLabelValue("pyop2_ghost", pt) == -1:
                    indices = [pt]
                break

        nodes = []
        for i in indices:
            if sec.getDof(i) > 0:
                nodes.append(sec.getOffset(i))

        if V.mesh().comm.rank == 0:
            nodes = [0]
        else:
            nodes = []
        self.nodes = numpy.asarray(nodes, dtype=IntType)

        if len(self.nodes) > 0:
            print("Fixing nodes %s" % self.nodes)
        else:
            print("Not fixing any nodes")
        import sys; sys.stdout.flush()

lu = {
     "snes_type": "newtonls",
     "snes_monitor": None,
     "snes_atol": 1.0e-7,
     "snes_rtol": 1.0e-8,
     "snes_linesearch_type": "l2",
     #"snes_linesearch_monitor": None,
     "snes_linesearch_maxstep": 1,
     "snes_converged_reason": None,
     #"snes_max_it": 1000,
     #"snes_convergence_test": "skip",
     "mat_type": "aij",
     "ksp_type": "preonly",
     "pc_type": "lu",
     "pc_factor_mat_solver_type": "mumps",
     "mat_mumps_icntl_14": 200,
         }

mfslu = {
   "ksp_type": "gmres",
   "ksp_max_it": 1,
   #"ksp_monitor_true_residual": None,
   "ksp_converged_reason": None,
   "pc_type": "fieldsplit",
   "pc_fieldsplit_type": "schur",
   "pc_fieldsplit_schur_fact_type": "full",
   "pc_fieldsplit_0_fields": "1",
   "pc_fieldsplit_1_fields": "0",
   "fieldsplit_0_ksp_type": "preonly",
   "fieldsplit_0_pc_type": "lu",
   "fieldsplit_0_pc_factor_mat_solver_type": "mumps",
   "fieldsplit_0_mat_mumps_icntl_14": 200,
   #"fieldsplit_1_ksp_type": "richardson",
   "fieldsplit_1_ksp_type": "preonly",
   #"fieldsplit_1_ksp_type": "fgmres",
   "fieldsplit_1_ksp_max_it": 2,
   "fieldsplit_1_ksp_norm_type": "unpreconditioned",
   #"fieldsplit_1_ksp_monitor_true_residual": None,
   "fieldsplit_1_pc_type": "python",
   "fieldsplit_1_pc_python_type": "__main__.InnerSchurPC",
   "fieldsplit_1_aux_pc_type": "lu",
   "fieldsplit_1_aux_pc_factor_mat_solver_type": "mumps",
   "fieldsplit_1_aux_mat_mumps_icntl_14": 200,
}

mfsstar = {
   "ksp_type": "fgmres",
   "ksp_monitor_true_residual": None,
   "ksp_max_it": 2,
   "ksp_monitor_true_residual": None,
   "pc_type": "fieldsplit",
   "pc_fieldsplit_type": "schur",
   "pc_fieldsplit_schur_fact_type": "full",
   "fieldsplit_0_ksp_type": "chebyshev",
   "fieldsplit_0_pc_type": "jacobi",
   #"fieldsplit_1_ksp_type": "richardson",
   "fieldsplit_1_ksp_type": "preonly",
   #"fieldsplit_1_ksp_type": "fgmres",
   "fieldsplit_1_ksp_max_it": 1,
   "fieldsplit_1_ksp_norm_type": "unpreconditioned",
   "fieldsplit_1_ksp_monitor_true_residual": None,
   "fieldsplit_1_pc_type": "python",
   "fieldsplit_1_pc_python_type": "__main__.InnerSchurPC",
   "fieldsplit_1_aux_pc_type": "mg",
   "fieldsplit_1_aux_pc_mg_cycle_type": "v",
   "fieldsplit_1_aux_pc_mg_type": "full",
   "fieldsplit_1_aux_mg_levels_ksp_type": "richardson",
   "fieldsplit_1_aux_mg_levels_ksp_richardson_scale": 1,
   "fieldsplit_1_aux_mg_levels_ksp_convergence_test": "skip",
   "fieldsplit_1_aux_mg_levels_ksp_max_it": 2,
   #"fieldsplit_1_aux_mg_levels_ksp_norm_type": "unpreconditioned",
   #"fieldsplit_1_aux_mg_levels_ksp_monitor_true_residual": None,
   "fieldsplit_1_aux_mg_levels_pc_type": "python",
   "fieldsplit_1_aux_mg_levels_pc_python_type": "firedrake.PatchPC",
   "fieldsplit_1_aux_mg_levels_patch_pc_patch_save_operators": True,
   "fieldsplit_1_aux_mg_levels_patch_pc_patch_partition_of_unity": True,
   "fieldsplit_1_aux_mg_levels_patch_pc_patch_sub_mat_type": "seqdense",
   "fieldsplit_1_aux_mg_levels_patch_pc_patch_construct_dim": 0,
   "fieldsplit_1_aux_mg_levels_patch_pc_patch_construct_type": "star",
   "fieldsplit_1_aux_mg_levels_patch_sub_ksp_type": "preonly",
   "fieldsplit_1_aux_mg_levels_patch_sub_pc_type": "lu",
   # "fieldsplit_1_aux_mg_levels_patch_sub_pc_factor_mat_solver_type": "mumps",
   "fieldsplit_1_aux_mg_coarse_ksp_type": "preonly",
   "fieldsplit_1_aux_mg_coarse_pc_type": "lu",
       }

nfslu = {
    "ksp_type": "gmres",
    "ksp_monitor_true_residual": None,
    "ksp_converged_reason": None,
    "mat_type": "aij",
    "ksp_max_it": 2,
    "pc_type": "fieldsplit",
    "pc_fieldsplit_type": "schur",
    # "pc_fieldsplit_schur_factorization_type": "upper",
    "pc_fieldsplit_schur_factorization_type": "full",
    "pc_fieldsplit_schur_precondition": "user",
    "fieldsplit_0": {
            "ksp_type": "preonly",
            "ksp_max_it": 1,
            "pc_type": "lu",
            "pc_factor_mat_solver_type": "mumps",
            "mat_mumps_icntl_14": 150,
    },
    "fieldsplit_1":{
            "ksp_type": "preonly",
            "pc_type": "python",
            "pc_python_type": "alfi.solver.DGMassInv"
        },
    }

jaclu = {
     "snes_type": "newtonls",
     "snes_max_it": 20,
     "snes_linesearch_type": "basic",
     "snes_linesearch_maxstep": 1.0,
     "snes_rtol": 1.0e-7,
     "snes_monitor": None,
     "snes_linesearch_monitor": None,
     "snes_converged_reason": None,
     "ksp_type": "fgmres",
     #"ksp_atol": 0,
     #"ksp_rtol": 1.0e-8,
     "ksp_monitor_true_residual": None,
     "ksp_converged_reason": None,
     "mat_type": "aij",
     "pc_type": "fieldsplit",
     "pc_fieldsplit_type": "additive",
     "pc_fieldsplit_0_fields": "0,2",
     "pc_fieldsplit_1_fields": "1,3",
     "fieldsplit_0_ksp_type": "preonly",
     "fieldsplit_0_pc_type": "lu",
     "fieldsplit_0_pc_factor_mat_solver_type": "mumps",
     "fieldsplit_0_mat_mumps_icntl_14": 200,
     "fieldsplit_1_ksp_type": "preonly",
     "fieldsplit_1_pc_type": "lu",
     "fieldsplit_1_pc_factor_mat_solver_type": "mumps",
     "fieldsplit_1_mat_mumps_icntl_14": 200,
    }

jacfs = {
     "snes_type": "newtonls",
     "snes_max_it": 20,
     "snes_linesearch_type": "basic",
     "snes_linesearch_maxstep": 1.0,
     "snes_monitor": None,
     "snes_linesearch_monitor": None,
     "snes_converged_reason": None,
     "ksp_type": "fgmres",
     "ksp_atol": 0,
     "ksp_rtol": 1.0e-8,
     "ksp_monitor_true_residual": None,
     "ksp_converged_reason": None,
     "mat_type": "aij",
     "pc_type": "fieldsplit",
     "pc_fieldsplit_type": "additive",
     "pc_fieldsplit_0_fields": "0,2",
     "pc_fieldsplit_1_fields": "1,3",
     "fieldsplit_0": nfslu,
     "fieldsplit_1": mfslu,
    }

mschur = {
   "ksp_type": "gmres",
   "ksp_max_it": 2,
   "ksp_monitor_true_residual": None,
   "ksp_converged_reason": None,
   "pc_type": "python",
   "pc_python_type": "__main__.SchurPC",
   #"aux_pc_type": nfslu,
   "aux_pc_type": "lu",
   "aux_pc_factor_mat_solver_type": "mumps",
   "aux_mat_mumps_icntl_14": 200,
   }


fs2by2 = {
     "snes_type": "newtonls",
     "snes_max_it": 30,
     "snes_linesearch_type": "l2",
     "snes_linesearch_maxstep": 1.0,
     "snes_rtol": 1.0e-8,
     "snes_monitor": None,
     #"snes_linesearch_monitor": None,
     "snes_converged_reason": None,
     "ksp_type": "fgmres",
     #"ksp_atol": 0,
     #"ksp_rtol": 1.0e-8,
     "ksp_monitor_true_residual": None,
     "ksp_converged_reason": None,
     "mat_type": "aij",
     "pc_type": "fieldsplit",
     "pc_fieldsplit_type": "schur",
     "pc_fieldsplit_0_fields": "1,3",
     "pc_fieldsplit_1_fields": "0,2",
     #"fieldsplit_0": mfslu,
     "fieldsplit_0_ksp_type": "preonly",
     "fieldsplit_0_pc_type": "lu",
     "fieldsplit_0_pc_factor_mat_solver_type": "mumps",
     "fieldsplit_0_mat_mumps_icntl_14": 200,
     "fieldsplit_1": mschur,
    }
    
jacmfs = {
     "snes_type": "newtonls",
     "snes_max_it": 20,
     "snes_linesearch_type": "basic",
     "snes_linesearch_maxstep": 1.0,
     "snes_rtol": 1.0e-7,
     "snes_monitor": None,
     "snes_linesearch_monitor": None,
     "snes_converged_reason": None,
     "ksp_type": "fgmres",
     "ksp_monitor_true_residual": None,
     "ksp_converged_reason": None,
     "mat_type": "aij",
     "pc_type": "fieldsplit",
     "pc_fieldsplit_type": "additive",
     "pc_fieldsplit_0_fields": "0,2",
     "pc_fieldsplit_1_fields": "1,3",
     "fieldsplit_0_ksp_type": "preonly",
     "fieldsplit_0_pc_type": "lu",
     "fieldsplit_0_pc_factor_mat_solver_type": "mumps",
     "fieldsplit_0_mat_mumps_icntl_14": 200,
     "fieldsplit_1": mfslu,
    }

jacnfs = {
     "snes_type": "newtonls",
     "snes_max_it": 20,
     "snes_linesearch_type": "basic",
     "snes_linesearch_maxstep": 1.0,
     "snes_rtol": 1.0e-7,
     "snes_monitor": None,
     "snes_linesearch_monitor": None,
     "snes_converged_reason": None,
     "ksp_type": "fgmres",
     "ksp_monitor_true_residual": None,
     "ksp_converged_reason": None,
     "mat_type": "aij",
     "pc_type": "fieldsplit",
     "pc_fieldsplit_type": "additive",
     "pc_fieldsplit_0_fields": "0,2",
     "pc_fieldsplit_1_fields": "1,3",
     "fieldsplit_0": nfslu,
     "fieldsplit_1_ksp_type": "preonly",
     "fieldsplit_1_pc_type": "lu",
     "fieldsplit_1_pc_factor_mat_solver_type": "mumps",
     "fieldsplit_1_mat_mumps_icntl_14": 200,
    }

Xu = {
     "snes_type": "newtonls",
     "snes_max_it": 20,
     "snes_linesearch_type": "basic",
     "snes_linesearch_maxstep": 1.0,
     "snes_rtol": 1.0e-7,
     "snes_monitor": None,
     "snes_converged_reason": None,
     "ksp_type": "fgmres",
     "ksp_monitor_true_residual": None,
     "ksp_converged_reason": None,
     "mat_type": "aij",
     "pc_type": "python",
     "pc_python_type": "__main__.XuPC",
     "aux_pc_type": "lu",
     "aux_pc_factor_mat_solver_type": "mumps",
     "aux_mat_mumps_icntl_14": 200,
    }


solvers = {"lu": lu, "jaclu": jaclu, "jacfs": jacfs, "jacmfs": jacmfs, "jacnfs": jacnfs, "fs2by2": fs2by2, "Xu": Xu}

parser = argparse.ArgumentParser(add_help=False)
parser.add_argument("--baseN", type=int, default=10)
parser.add_argument("--k", type=int, default=3)
parser.add_argument("--nref", type=int, default=1)
parser.add_argument("--Re", type=float, default=1)
parser.add_argument("--Rem", type=float, default=1)
parser.add_argument("--gamma", type=float, default=10000)
parser.add_argument("--S", type=float, default=1)
parser.add_argument("--char_L", type=float, default=1)
parser.add_argument("--char_U", type=float, default=1)
parser.add_argument("--lambda_", type=float, default=1)
parser.add_argument("--hierarchy", choices=["bary", "uniform"], default="bary")
parser.add_argument("--stabilisation-type", choices=["none", "burman"], default="none")
parser.add_argument("--solver-type", choices=list(solvers.keys()), default="lu")
parser.add_argument("--testproblem", choices=["ldc", "hartmann", "Wathen"], default = "hartmann")
parser.add_argument("--picard", default=False, action="store_true")

args, _ = parser.parse_known_args()
baseN = args.baseN
k = args.k
nref = args.nref
Re = Constant(args.Re)
Rem = Constant(args.Rem)
gamma = Constant(args.gamma)
S = Constant(args.S)
hierarchy = args.hierarchy
stabilisation_type = args.stabilisation_type
solver_type = args.solver_type
testproblem = args.testproblem
gamma2 = 1/args.Rem
advect = Constant(1)  
char_L = Constant(args.char_L)
char_U = Constant(args.char_U)
lambda_= Constant(args.lambda_)
nu = Constant(1.0)
picard = args.picard

base = UnitSquareMesh(baseN, baseN, diagonal="crossed", distribution_parameters=distribution_parameters)

def before(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+1)

def after(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+2)
    #rebalance(dm, i)

if hierarchy == "bary":
    mh = alfi.BaryMeshHierarchy(base, nref, callbacks=(before, after))
elif hierarchy == "uniformbary":
    bmesh = Mesh(bary(base._plex), distribution_parameters={"partition": False})
    mh = MeshHierarchy(bmesh, nref, reorder=True, callbacks=(before, after),
                       distribution_parameters=distribution_parameters)
elif hierarchy == "uniform":
    mh = MeshHierarchy(base, nref, reorder=True, callbacks=(before, after),
             distribution_parameters=distribution_parameters)
else:
    raise NotImplementedError("Only know bary, uniformbary and uniform for the hierarchy.")
for m in mh:
    m.coordinates.dat.data[:, 0] -= 0.5
    m.coordinates.dat.data[:, 1] -= 0.5
mesh = mh[-1]

area = assemble(Constant(1, domain=mh[0])*dx)

def message(msg):
    if mesh.comm.rank == 0:
        warning(msg)

V = VectorFunctionSpace(mesh, "CG", k)
Q = FunctionSpace(mesh, "DG", k-1)
W = FunctionSpace(mesh, "N1curl", k)
R = FunctionSpace(mesh, "CG", k)
Z = MixedFunctionSpace([V, W, Q, R])

z = Function(Z)
(u, B, p, sigma) = split(z)
(v, C, q, tau)   = split(TestFunction(Z))
z_last = Function(Z)

eps = lambda x: sym(grad(x))
F = (
      2 * nu * inner(eps(u), eps(v))*dx
    + advect * inner(dot(grad(u), u), v) * dx
    + gamma * inner(div(u), div(v)) * dx
    + S * inner(scross(v, B), scurl(B)) * dx
    - inner(p, div(v)) * dx
    - inner(div(u), q) * dx
    + S/Rem * inner(scurl(B), scurl(C)) * dx
    - S * inner(scross(u, B), scurl(C)) * dx
    + gamma2 * inner(grad(sigma), C) * dx
    - inner(sigma, tau) * dx
    + inner(B, grad(tau)) * dx
    )

if stabilisation_type == "burman":
    n = FacetNormal(mesh)
    h = FacetArea(mesh)
    if mesh.topological_dimension() == 3:
        h = h**0.5 # go from area to length
    supg_wind = split(z_last)[0]
    beta = sqrt(avg(facet_avg(dot(supg_wind, n)**2)))
    gamma1 = Constant(4e-3) # as chosen in doi:10.1016/j.apnum.2007.11.001
    stabilisation = 0.5 * gamma1 * h**2 * beta * dot(jump(grad(u), n), jump(grad(v), n))*dS
    F += (advect * stabilisation)
elif stabilisation_type == "none":
    pass
else:
    raise NotImplementedError


(x, y) = SpatialCoordinate(Z.mesh())

def compute_rhs(u_ex, B_ex, p_ex):
    f1 = -2 * nu * div(eps(u_ex)) + advect * dot(grad(u_ex),u_ex) - gamma * grad(div(u_ex)) + grad(p_ex) + S * vcross(B_ex, scurl(B_ex))
    #print("||f1||_L^2 = %s" % sqrt(assemble(inner(f1, f1)*dx)))
    f3 = S/Rem * vcurl(scurl(B_ex)) - S * vcurl(scross(u_ex, B_ex))
    #print("||f3||_L^2 = %s" % sqrt(assemble(inner(f3, f3)*dx)))

    return (f1, f3)
    

if testproblem == "hartmann":
    Sc = Constant(1)
    Ha = sqrt(Re*Rem*Sc) 
    G = 2*Ha*sinh(Ha/2) / (Re * (cosh(Ha/2) - 1))

    b = G/(2*Sc) * (sinh(y*Ha)/sinh(Ha/2) - 2*y)
    u_ex = as_vector([(G*Re / (2*Ha*tanh(Ha/2))) * (1 - (cosh(y*Ha)/cosh(Ha/2))), 0])
    p_ex = -G*x - Sc/2 * b**2
    pintegral = assemble(p_ex*dx)
    p_ex = p_ex - Constant(pintegral/area)
    B_ex = as_vector([b, Constant(1, domain=mesh)])
    sigma_ex = Constant(0)

    (f1, f3) = compute_rhs(u_ex, B_ex, p_ex)
    rhs = True
    solution_known = True



elif testproblem == "ldc":
   #example taken from chapter 5.1 in doi.org/10.1137/16M1074084
   #u_ex = as_vector([(x-0.5)*(x-0.5)*(x+0.5)*(x+0.5)*(0.25*(y+0.5)*(y+0.5)), 0])
   u_ex = Constant((1, 0), domain=mesh)
   B_ex = Constant((-1, 0), domain=mesh)
   B_ex = project(B_ex, W)
   
   bcs = [DirichletBC(Z.sub(0), u_ex, 4),  #4 == upper boundary (y==1)
          DirichletBC(Z.sub(0), 0, (1,2,3)),
          DirichletBC(Z.sub(1), B_ex, "on_boundary"),
          DirichletBC(Z.sub(3), 0 , "on_boundary"),
          PressureFixBC(Z.sub(2), 0, 1)]
   rhs = None
   solution_known = False

elif testproblem == "Wathen":
    #example taken from chapter 6.2.1 in doi.org/a10.1137/16M1098991
    u_ex = as_vector([x*y*exp(x+y) + x*exp(x+y), -x*y*exp(x+y) - y*exp(x+y)])
    p_ex = exp(y) * sin(x)
    pintegral = assemble(p_ex*dx)
    p_ex = p_ex - Constant(pintegral/area)
    B_ex = as_vector([exp(x+y)*cos(x), exp(x+y)*sin(x) - exp(x+y)*cos(x)])
    sigma_ex = Constant(0, domain=mesh)

    (f1, f3) = compute_rhs(u_ex, B_ex, p_ex)
    rhs = True
    solution_known = True

#u_ex_ = project(u_ex, V)
#B_ex_ = project(B_ex, W)
#p_ex_ = project(p_ex, Q)
#z.sub(0).assign(u_ex_)
#z.sub(1).assign(B_ex_)
#z.sub(2).assign(p_ex_)
    
if rhs is not None:
    F -= inner(f1, v) * dx + inner(f3, C) * dx

    
if solution_known:
    bcs = [DirichletBC(Z.sub(0), u_ex, "on_boundary"),
           DirichletBC(Z.sub(1), B_ex , "on_boundary"),
           DirichletBC(Z.sub(3), sigma_ex , "on_boundary"),
           PressureFixBC(Z.sub(2), 0, 1)]
#           DirichletBC(Z.sub(2), 0, ((1,3), ), method="geometric")] # Doesn't work for DG pressures, does for CG

if picard:
    w = TrialFunction(Z)
    [w_u, w_B, w_p, w_sigma] = split(w)

    J_newton = ufl.algorithms.expand_derivatives(derivative(F, z, w))
    J_picard = (
          J_newton
        + S * inner(scross(u, w_B), scurl(C))*dx    # A tilde     S * inner(scross(u_n, B), scurl(C))*dx
        - S * inner(scross(v, w_B), scurl(B))*dx    # Z tilde     S * inner(scross(v, B),scurl(B_n))*dx
        #- S * inner(scross(v, B), scurl(w_B))*dx    # Z           S * inner(scross(v, B_n), scurl(B))*dx
        #+ S * inner(scross(w_u, B), scurl(C))*dx    # Z transpose S * inner(scross(u, B_n), scurl(C))*d
        
        #- advect * inner(dot(grad(u), w_u), v) * dx   # - advect * inner(dot(grad(u_n), u), v) * d
        #- advect * inner(dot(grad(w_u), u), v) * dx   # - advect * inner(dot(grad(u), u_n), v) * dx
        )
    problem = NonlinearVariationalProblem(F, z, bcs, J=J_picard)

    if False:
        print("S = %s" % S)
        print("Rem = %s" % Rem)
        print("Re = %s" % Re)
        print("gamma = %s" % gamma)
        print("advect = %s" % advect)
        print("u = %s" % u)
        print("B = %s" % B)
        print("p = %s" % p)
        print("sigma = %s" % sigma)
        print("v = %s" % v)
        print("C = %s" % C)
        print("q = %s" % q)
        print("tau = %s" % tau)
        print("w = %s" % w)
        print("w_u = %s" % w_u)
        print("w_B = %s" % w_B)
        print("J_newton = %s" % J_newton)
        print("J_picard = %s" % J_picard)
        exit(-1)
else: 
    problem = NonlinearVariationalProblem(F, z, bcs)
    
appctx = {"Re": char_L * char_U / nu, "gamma": gamma, "nu": nu}
params = solvers[args.solver_type]
#print("args.solver_type", args.solver_type)
#import pprint
#pprint.pprint(params)
solver = NonlinearVariationalSolver(problem, solver_parameters=params, appctx=appctx)

def get_transfers():
    if stabilisation_type in ["burman", "none"]:
        qtransfer = alfi.NullTransfer()
    elif stabilisation_type in ["gls", "supg"]:
        qtransfer = EmbeddedDGTransfer(V.ufl_element())
    else:
        raise ValueError("Unknown stabilisation")
    transfers = [dmhooks.transfer_operators(Q, inject=qtransfer.inject)]
    if hierarchy == "bary":
        vtransfer = alfi.SVSchoeberlTransfer((1/Re, gamma), 2, hierarchy)
        restriction = False
        if restriction:
            transfers.append(
                dmhooks.transfer_operators(V, prolong=vtransfer.prolong, restrict=vtransfer.restrict))
        else:
            transfers.append(
                dmhooks.transfer_operators(V, prolong=vtransfer.prolong))
    return transfers

solver.set_transfer_operators(*get_transfers())




Res = [Re]
for re_ in Res:
    if Re == 0:
        advect.assign(0)
        nu.assign(char_L * char_U)
        message(GREEN % ("Solving Stokes"))
    else:
        advect.assign(1)
        nu.assign(char_L * char_U / re_)
        message(GREEN % ("Solving for Re = %s, Rem = %s, nref=%s" % (float(re_), float(Rem), int(nref))))

    start = datetime.now()
    solver.solve()
    end = datetime.now()

    linear_its = solver.snes.getLinearSolveIterations()
    nonlinear_its = solver.snes.getIterationNumber()
    time = (end-start).total_seconds() / 60

    print(GREEN % ("Time taken: %.2f min in %d nonlinear iterations, %d linear iterations (%.2f Krylov iters per Newton step)" % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its))))
    print("%.2f @ %d @ %d @ %.2f" % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its)))

    (u, B, p, sigma) = z.split()

    pintegral = assemble(p*dx)
    p.assign(p - Constant(pintegral/area))

    print("||div(u)||_L^2 = %s" % sqrt(assemble(inner(div(u), div(u))*dx)))
    print("||div(B)||_L^2 = %s" % sqrt(assemble(inner(div(B), div(B))*dx)))

    B.rename("MagneticField")
    u.rename("VelocityField")
    p.rename("Pressure")
    sigma.rename("Sigma")

    if solution_known:
        B_ex_ = project(B_ex, B.function_space())
        u_ex_ = project(u_ex, u.function_space())
        p_ex_ = project(p_ex, p.function_space())
        B_ex_.rename("ExactSolutionB")
        u_ex_.rename("ExactSolutionu")
        p_ex_.rename("ExactSolutionp")

        error_u = errornorm(u_ex_, u, 'L2')
        error_B = errornorm(B_ex_, B, 'L2')
        error_sigma = errornorm(sigma_ex, sigma, 'L2')
        error_p = errornorm(p_ex_, p, 'L2')

        errors=[error_u,error_p,error_B]

        print("Error ||u_ex - u||_L^2 = %s" % error_u)
        print("Error ||p_ex - p||_L^2 = %s" % error_p)
        print("Error ||B_ex - B||_L^2 = %s" % error_B)
        print("Error ||σ_ex - σ||_L^2 = %s" % error_sigma)
        File("output/mhd.pvd").write(u, u_ex_, p, p_ex_, B, B_ex_, sigma, time=float(re_))
        print("%s,%s,%s" % (error_u,error_p,error_B))


        error_u = Function(u.function_space())
        error_u.assign(u_ex_ - u)
        error_u.rename("ErrorVelocity")
        error_B = Function(B.function_space())
        error_B.assign(B_ex_ - B)
        error_B.rename("ErrorMagneticField")
        error_p = Function(p.function_space())
        error_p.assign(p_ex_ - p)
        error_p.rename("ErrorPressure")
        File("output/errors.pvd").write(error_u, error_B, error_p, time=float(re_))
    else:
        #File("output/mhd.pvd").write(u, p, B, sigma, time=re_)
        File("output/mhd.pvd").write(u,  time=float(re_))
        
    z_last.assign(z)




    


