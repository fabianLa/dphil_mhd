from pprint import pprint
from alfi import *
import os
from firedrake import *
import numpy as np
import inflect
eng = inflect.engine()

lu = {"mat_type": "aij", "ksp_type": "preonly", "pc_type": "lu", "pc_factor_mat_solver_type": "mumps", "mat_mumps_icntl_14": 300}


class TwoDimLidDrivenCavityMMSProblem(NavierStokesProblem):
    def __init__(self, baseN, boundary_cond, Re=Constant(1), Repres=None):
        super().__init__()
        self.baseN = baseN
        self.Re = Re
        self.boundary_cond = boundary_cond

    def mesh(self, distribution_parameters):
        base = RectangleMesh(self.baseN, self.baseN, 2, 2,
                              distribution_parameters=distribution_parameters)        
        return base

    def bcs(self, Z):
        #bcs = [DirichletBC(Z.sub(0), self.actual_solution(Z)[0], [4]),
        #       DirichletBC(Z.sub(0), Constant((0., 0.)), [1, 2, 3])]
        u_ex = self.actual_solution(Z)[0]
        if self.boundary_cond == "Dirichlet":
            bcs = [DirichletBC(Z.sub(0), u_ex, "on_boundary")]
        elif self.boundary_cond == "normal":
            bcs = [
                  DirichletBC(Z.sub(0).sub(0), u_ex[0], 1),
                  DirichletBC(Z.sub(0).sub(0), u_ex[0], 2),
                  DirichletBC(Z.sub(0).sub(1), u_ex[1], 3),
                  DirichletBC(Z.sub(0).sub(1), u_ex[1], 4),
                ]
        elif self.boundary_cond == "tangential":
            bcs = [
                  DirichletBC(Z.sub(0).sub(1), u_ex[1], 1),
                  DirichletBC(Z.sub(0).sub(1), u_ex[1], 2),
                  DirichletBC(Z.sub(0).sub(0), u_ex[0], 3),
                  DirichletBC(Z.sub(0).sub(0), u_ex[0], 4),
                ]
        else:
           raise NotImplementedError
        return bcs

    def has_nullspace(self): return True

    def char_length(self): return 2.0

    def actual_solution(self, Z):
        (x, y) = SpatialCoordinate(Z.mesh())
        area = assemble(Constant(1, domain=Z.mesh())*dx)
        #u_ex = as_vector([exp(x+y)*cos(x), exp(x+y)*sin(x) - exp(x+y)*cos(x)])
        u_ex = as_vector([cos(y), sin(x)])
        #u_ex = as_vector([0.1*exp(-1/(1-y**2)), 0.1*exp(-1/(1-x**2))])
        p_ex = sin(x)
        p_ex = p_ex - Constant(assemble(p_ex*dx)/area)
        return (u_ex, p_ex)

    def rhs(self, Z):
        (u, p) = self.actual_solution(Z)
        nu = self.char_length() * self.char_velocity() / self.Re
        f1 = -nu * div(2*sym(grad(u))) + dot(grad(u), u) + grad(p)
        f2 = -div(u)
        return f1, f2

    def disturbed_exact_sol(self, Z):
        (x, y) = SpatialCoordinate(Z.mesh())
        u_ex = self.actual_solution(Z)[0]
        p_ex = self.actual_solution(Z)[1]

        fac = 0.05
        dis_vec = as_vector([fac * sin(y), fac * cos(x)])
        dis_scalar = fac * cos(y)

        u_ex_dis_ = project(u_ex, Z.sub(0), solver_parameters=lu) + project(dis_vec, Z.sub(0), solver_parameters=lu)
        p_ex_dis_ = project(p_ex, Z.sub(1), solver_parameters=lu) + project(dis_scalar, Z.sub(1), solver_parameters=lu)
        
        return (u_ex_dis_, p_ex_dis_)


if __name__ == "__main__":
    
     parser = get_default_parser()
     parser.add_argument("--boundary-cond", choices=["normal", "Dirichlet", "tangential"], required=True)
     args, _ = parser.parse_known_args()

     problem = TwoDimLidDrivenCavityMMSProblem(args.baseN, args.boundary_cond)

     #res = [1000, 2000, 5000]
     res = [1, 1000]
     results = {}

     solver = get_solver(args, problem)
     mesh = solver.mesh

     for re in res:
         problem.Re.assign(re)
         z = solver.z
         Z = z.function_space()
         (u, p) = z.split()
         
         (u_ex_dis_, p_ex_dis_) = problem.disturbed_exact_sol(Z)
         u.assign(u_ex_dis_)
         p.assign(p_ex_dis_)

         (z, info_dict) = solver.solve(re)
         results[re] = info_dict

         (u_, p_) = problem.actual_solution(Z)

         veldiv = norm(div(u))
         area = assemble(Constant(1, domain=Z.mesh())*dx)
         pressureintegral = assemble(p * dx)/area
         p = p - Constant(pressureintegral)
         uerr = norm(u_-u)
         ugraderr = norm(grad(u_-u))
         perr = norm(p_-p)

         #print("|div(u_h)| = ", veldiv)
         print("||u - u_ex|| = ", uerr)
         print("||p - p_ex|| = ", perr)


     for re in results:
                 print(results[re])
