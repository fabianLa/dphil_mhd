# -*- coding: utf-8 -*-
from firedrake import *
from datetime import datetime
import alfi
import argparse
import numpy
from pyop2.datatypes import IntType
import ufl.algorithms
from alfi.stabilisation import *
from alfi.transfer import *
from alfi import *

distribution_parameters={"partition": True, "overlap_type": (DistributedMeshOverlapType.VERTEX, 1)}

def scross(x, y):
    return x[0]*y[1] - x[1]*y[0]

def vcross(x,y):
    return as_vector([x[1]*y,-x[0]*y])

def scurl(x):
    return x[1].dx(0) - x[0].dx(1)

def vcurl(x):
    return as_vector([x.dx(1), -x.dx(0)])

def acurl(x):
    return as_vector([
                     x[2].dx(1),
                     -x[2].dx(0),
                     x[1].dx(0) - x[0].dx(1)
                     ])

def BurmanStab(B, C, supg_wind, mesh):
    n = FacetNormal(mesh)
    h = FacetArea(mesh)
    beta = sqrt(avg(facet_avg(dot(supg_wind, n)**2)))
    gamma1 = Constant(5e-3) # as chosen in doi:10.1016/j.apnum.2007.11.001
    stabilisation_form = 0.5 * gamma1 * h**2 * beta * dot(jump(grad(B), n), jump(grad(C), n))*dS
    return stabilisation_form

lu = {
     "snes_type": "newtonls",
     "snes_monitor": None,
     "snes_atol": 1.0e-7,
     "snes_rtol": 1.0e-8,
     "snes_linesearch_type": "l2",
     #"snes_linesearch_monitor": None,
     "snes_linesearch_maxstep": 1,
     #"snes_converged_reason": None,
     #"snes_max_it": 1000,
     #"snes_convergence_test": "skip",
     "mat_type": "aij",
     "ksp_type": "preonly",
     "pc_type": "lu",
     "pc_factor_mat_solver_type": "mumps",
     "mat_mumps_icntl_14": 300,
         }



mfsstar2 = {
   "snes_type": "ksponly",
   "snes_monitor": None,
   "ksp_type": "fgmres",
   "ksp_monitor_true_residual": None,
   "ksp_atol": 1.0e-10,
   "ksp_rtol": 1.0e-9,
   "pc_type": "mg",
   "mg_transfer_manager": __name__ + ".transfermanager",
   "pc_mg_cycle_type": "v",
   "pc_mg_type": "full",
   "mg_levels_ksp_type": "fgmres",
   "mg_levels_ksp_convergence_test": "skip",
   "mg_levels_ksp_max_it": 6,
   "mg_levels_ksp_norm_type": "unpreconditioned",
   #"mg_levels_kssp_monitor_true_residual": None,
   "mg_levels_pc_type": "python",
   "mg_levels_pc_python_type": "firedrake.PatchPC",
   "mg_levels_patch_pc_patch_save_operators": True,
   "mg_levels_patch_pc_patch_partition_of_unity": False,
   "mg_levels_patch_pc_patch_sub_mat_type": "seqaij",
   "mg_levels_patch_pc_patch_construct_dim": 0,
   "mg_levels_patch_pc_patch_construct_type": "python",
   "mg_levels_patch_pc_patch_construct_python_type": "alfi.MacroStar",
   "mg_levels_patch_sub_ksp_type": "preonly",
   "mg_levels_patch_sub_pc_type": "lu",
   "mg_levels_patch_sub_pc_factor_mat_solver_type": "petsc",
   "mg_coarse_pc_type": "python",
   "mg_coarse_pc_python_type": "firedrake.AssembledPC",
   "mg_coarse_assembled": {
         "mat_type": "aij",
         "pc_type": "telescope",
         "pc_telescope_reduction_factor": 1,
         "pc_telescope_subcomm_type": "contiguous",
         "telescope_pc_type": "lu",
         "telescope_pc_factor_mat_solver_type": "superlu_dist",
     }
       }




solvers = {"lu": lu,  "mfsstar2": mfsstar2}

parser = argparse.ArgumentParser(add_help=False)
parser.add_argument("--baseN", type=int, default=10)
parser.add_argument("--k", type=int, default=3)
parser.add_argument("--nref", type=int, default=1)
parser.add_argument("--Re", type=float, default=1)
parser.add_argument("--gamma", type=float, default=10000)
parser.add_argument("--delta", type=float, default=1)
parser.add_argument("--hierarchy", choices=["bary", "uniform"], default="bary")
parser.add_argument("--boundary-cond", choices=["normal", "Dirichlet"], required=True)
parser.add_argument("--char_L", type=float, default=1)
parser.add_argument("--char_U", type=float, default=1)
parser.add_argument("--solver-type", choices=list(solvers.keys()), default="lu")
parser.add_argument("--testproblem", choices=["ldc", "hartmann", "Wathen", "hartmann2", "hartmann3"], default = "hartmann")
parser.add_argument("--exing", default=False, action="store_true")
parser.add_argument("--stab", default=False, action="store_true")

args, _ = parser.parse_known_args()
baseN = args.baseN
k = args.k
nref = args.nref
Re = Constant(args.Re)
gamma = Constant(args.gamma)
hierarchy = args.hierarchy
stab = args.stab
solver_type = args.solver_type
testproblem = args.testproblem
advect = Constant(1)
delta = Constant(args.delta)
char_L = Constant(args.char_L)
char_U = Constant(args.char_U)
nu = Constant(1.0)
exact_initial_guess = args.exing
boundary_cond=args.boundary_cond

base = UnitSquareMesh(baseN, baseN, diagonal="crossed", distribution_parameters=distribution_parameters)

def before(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+1)

def after(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+2)
    #rebalance(dm, i)

if hierarchy == "bary":
    mh = alfi.BaryMeshHierarchy(base, nref, callbacks=(before, after))
elif hierarchy == "uniformbary":
    bmesh = Mesh(bary(base._plex), distribution_parameters={"partition": False})
    mh = MeshHierarchy(bmesh, nref, reorder=True, callbacks=(before, after),
                       distribution_parameters=distribution_parameters)
elif hierarchy == "uniform":
    mh = MeshHierarchy(base, nref, reorder=True, callbacks=(before, after),
             distribution_parameters=distribution_parameters)
else:
    raise NotImplementedError("Only know bary, uniformbary and uniform for the hierarchy.")
for m in mh:
    m.coordinates.dat.data[:, 0] -= 0.5
    m.coordinates.dat.data[:, 1] -= 0.5
mesh = mh[-1]

area = assemble(Constant(1, domain=mh[0])*dx)

def message(msg):
    if mesh.comm.rank == 0:
        warning(msg)

V = VectorFunctionSpace(mesh, "CG", k)  #u
Z = V

z = Function(Z)
u = z
v = TestFunction(Z)
z_last_u = Function(V)


(x, y) = SpatialCoordinate(Z.mesh())
n = FacetNormal(mesh)
t = as_vector([n[1], -n[0]])

#B_ex__=as_vector([exp(x+y)*cos(x), exp(x+y)*sin(x) - exp(x+y)*cos(x)])

eps = lambda x: sym(grad(x))

#example taken from chapter 6.2.1 in doi.org/a10.1137/16M1098991
u_ex = as_vector([x*y*exp(x+y) + x*exp(x+y), -x*y*exp(x+y) - y*exp(x+y)])
rhs = True


F = (
      2 * nu * inner(eps(u), eps(v))*dx
    + delta * inner(dot(grad(u), u), v) * dx
    + gamma * inner(div(u), div(v)) * dx 
    )

f1 = -2 * nu * div(eps(u_ex)) + delta * dot(grad(u_ex),u_ex) - gamma * grad(div(u_ex)) 
    
if rhs is not None:
    F -= inner(f1, v) * dx 

#if boundary_cond == "normal":
#    F = F + eta * inner(div(B_ex), dot(C, n)) * ds + inner(E_ex, dot(C,t)) * ds

if stab:
    z_last_u.assign(project(u_ex, V))
    stabilisation_form_u = BurmanStab(u, v, z_last_u, mesh)
    F += (delta * stabilisation_form_u)
    
if boundary_cond == "normal":
    bcs = [DirichletBC(Z.sub(0), u_ex[0], 1),
       DirichletBC(Z.sub(0), u_ex[0], 2),
       DirichletBC(Z.sub(1), u_ex[1], 3),
       DirichletBC(Z.sub(1), u_ex[1], 4),]

elif boundary_cond == "Dirichlet":
    bcs =  [DirichletBC(Z, u_ex, "on_boundary")]
else:
    raise NotImplementedError("Only know normal and Dirichlet.")


problem = NonlinearVariationalProblem(F, z, bcs)
    
appctx = {"Re": char_L * char_U / nu, "gamma": gamma, "nu": 1/Re}
params = solvers[args.solver_type]
#print("args.solver_type", args.solver_type)
#import pprint
#pprint.pprint(params)

solver = NonlinearVariationalSolver(problem, solver_parameters=params, options_prefix="", appctx=appctx)
qtransfer = NullTransfer()
Etransfer = NullTransfer()
vtransfer = SVSchoeberlTransfer((1/Re, gamma), 2, hierarchy)
transfers = {
                V.ufl_element(): (vtransfer.prolong, vtransfer.restrict, inject),

            }
transfermanager = TransferManager(native_transfers=transfers)
solver.set_transfer_manager(transfermanager)

results = {}
start = 250
end = 10000
step = 2000
rems = [1, 10, 100, 2000, 5000]
#rems = [1000]

fac = 0.05
u_ex_ = project(u_ex, V, solver_parameters=lu)
dis_vec = as_vector([fac * cos(y), fac * sin(x)])
dis_scalar = fac * sin(x)
dis_vec_u_ = project(dis_vec, V)

u_ex_dis_ = u_ex_ + dis_vec_u_

results = {}
start = 250
end = 10000
step = 2000
res = [1, 100, 2000, 5000, 10000]
#rems = [1000]

for re in res:
    u = z
    u.assign(u_ex_dis_)

    nu.assign(char_L * char_U / re)
    Re.assign(re)
    message(GREEN % ("Solving for Re = %s, gamma = %s ,nref=%s" % (float(re), float(gamma), int(nref))))

    start = datetime.now()
    solver.solve()
    end = datetime.now()

    linear_its = solver.snes.getLinearSolveIterations()
    nonlinear_its = solver.snes.getIterationNumber()
    time = (end-start).total_seconds() / 60

    print(GREEN % ("Time taken: %.2f min in %d nonlinear iterations, %d linear iterations (%.2f Krylov iters per Newton step)" % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its))))
    print("%.2f @ %d @ %d @ %.2f" % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its)))

    u = z


    print("||div(u)||_L^2 = %s" % sqrt(assemble(inner(div(u), div(u))*dx)))





    u_ex_ = project(u_ex, u.function_space())

    u_ex_.rename("ExactSolutionu")


    error_u = errornorm(u_ex_, u, 'L2')

    print("Error ||u_ex - u||_L^2 = %s" % error_u)

    info_dict = {
        "Re": re,
        "krylov/nonlin": linear_its/nonlinear_its,
        "error_u": error_u,

    }
    results[re] = info_dict


        

        
z_last_u.assign(u)

for rem in results:
        print(results[rem])


    


