#Rems=(1 10 100 500 1000 2000 3000 4000 5000 7000 10000)
#Res=(1 10 100 500 1000 1500 2000 2500 3000 3500 4000 4500 5000 5500 6000 6500 7000 7500 8000 8500 9000 9500 10000)
#Res=(1 10 100 500 1000 2000 3000 4000 5000 6000 7000 8000 9000 10000)
Rems=(1 10 100)
Res=(1 10 100)
testproblem=hartmann2

if [ $1 -eq 1 ]
then
  for linearisation in picard picardG newton 
    do for nref in 1
        do for Rem in ${Rems[@]} 
               do python hartmannEB_2d.py --baseN 10 --k 2 --nref $nref --Re ${Res[@]} --Rem $Rem --gamma 10000 --gamma2 0 --solver-type fs2by2 --testproblem $testproblem --linearisation $linearisation --stab &
        done &
    done
  done
else
    python print_results.py --Rems ${Rems[@]} --Res ${Res[@]} --testproblem $testproblem
fi
