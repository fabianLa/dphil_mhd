# -*- coding: utf-8 -*-
from firedrake import *
import argparse
from datetime import datetime
import numpy
import alfi
from alfi.stabilisation import *
from alfi.transfer import *
from alfi import *

distribution_parameters={"partition": True, "overlap_type": (DistributedMeshOverlapType.VERTEX, 2)}

def scross(x, y):
    return x[0]*y[1] - x[1]*y[0]

def scurl(x):
    return x[1].dx(0) - x[0].dx(1)

def vcurl(x):
    return as_vector([x.dx(1), -x.dx(0)])

def acurl(x):
    return as_vector([
                     x[2].dx(1),
                     -x[2].dx(0),
                     x[1].dx(0) - x[0].dx(1)
                     ])

def BurmanStab(B, C, supg_wind, mesh):
    n = FacetNormal(mesh)
    h = FacetArea(mesh)
    beta = sqrt(avg(facet_avg(dot(supg_wind, n)**2)))
    gamma1 = Constant(5e-3) # as chosen in doi:10.1016/j.apnum.2007.11.001
    stabilisation_form = 0.5 * gamma1 * h**2 * beta * dot(jump(grad(B), n), jump(grad(C), n))*dS
    return stabilisation_form

class SchurPC(AuxiliaryOperatorPC):
    def form(self, pc, test, trial):
        K = -gamma**(-1) * inner(test, trial)*dx
        bcs = [DirichletBC(test.function_space(), 0, "on_boundary")]

        return (K, bcs)

class InnerSchurPC(AuxiliaryOperatorPC):
    def form(self, pc, v, u):
        #K = gamma * inner(grad(test), grad(trial))*dx
        #K = gamma * inner(grad(test), grad(trial))*dx + gamma * inner(test, trial)*dx
        #K = gamma * inner(test, trial)*dx

        mesh = v.function_space().mesh()
        n = FacetNormal(mesh)
        t = as_vector([n[1], -n[0]])
        h = CellDiameter(mesh)
        h_avg = (h('+') + h('-'))/2
        alpha = Constant(4)
        gammaa = Constant(8.0)

        K = 1/Rem *(
              +  inner(grad(u), grad(v))*dx
              +  alpha/h_avg*inner(jump(v, t), jump(u, t))*dS
            )

        K = K  - delta * inner(scross(u_ex, u), scurl(v))*dx #+ (
            #  - delta * inner(avg(scross(u_ex, u)), jump(v, t)) * dS
            #- delta * inner(jump(scross(u_ex, u), n), avg(v)) * dS
            # + delta * inner(scross(u_ex, u), dot(v, t)) * ds
            #)
        K = K + gamma * inner(div(u), div(v)) * dx

        #K = K +  nu * (
        #     inner(grad(u), grad(v)) * dx
        #   #- inner(dot(grad(u_ex),n), v) * ds

        #   #+ dot(avg(grad(v[0])), jump(u[0], n))*dS
        #   #+ dot(avg(grad(v[1])), jump(u[1], n))*dS 
        #   - dot(jump(v[0], n), avg(grad(u[0])))*dS 
        #   - dot(jump(v[1], n), avg(grad(u[1])))*dS
        #   + alpha/h_avg *  dot(jump(u[0], n) , jump(v[0], n)) * dS
        #   + alpha/h_avg *  dot(jump(u[1], n) , jump(v[1], n)) * dS
        #   + dot(grad(v[0]), u[0]*n)*ds 
        #   + dot(grad(v[1]), u[1]*n)*ds
        #   - dot(v[0]*n, grad(u[0]))*ds 
        #   - dot(v[1]*n, grad(u[1]))*ds 
        #   #+ (gammaa/h)*dot(v[0], u[0])*ds
        #   #+ (gammaa/h)*dot(v[1], u[1])*ds
        #    )
        
        bcs = [DirichletBC(v.function_space(), 0, "on_boundary")]

        #bcs = [DirichletBC(v.function_space().sub(0), 0, 1),
        #   DirichletBC(v.function_space().sub(0), 0, 2),
        #   DirichletBC(v.function_space().sub(1), 0, 3),
        #   DirichletBC(v.function_space().sub(1), 0, 4)]



        return (K, bcs)


lu = {
   "mat_type": "aij",
   "snes_type": "newtonls",
   "snes_monitor": None,
   "ksp_type": "gmres",
   #"ksp_monitor_true_residual": None,
   "pc_type": "lu",
   "pc_factor_mat_solver_type": "mumps",
   "mat_mumps_icntl_14": 200,
     }

fslu = {
   "mat_type": "aij",
   "snes_type": "ksponly",
   "snes_monitor": None,
   "snes_atol": 0,
   "snes_rtol": 1.0e-9,
   "ksp_type": "fgmres",
   "ksp_atol": 0,
   "ksp_rtol": 1.0e-10,
   "ksp_monitor_true_residual": None,
   "pc_type": "fieldsplit",
   "pc_fieldsplit_type": "schur",
   "pc_fieldsplit_schur_fact_type": "full",
   "fieldsplit_0_ksp_type": "preonly",
   "fieldsplit_0_pc_type": "lu",
   "fieldsplit_0_pc_factor_mat_solver_type": "mumps",
   "fieldsplit_0_mat_mumps_icntl_14": 200,
   #"fieldsplit_1_ksp_type": "richardson",
   "fieldsplit_1_ksp_type": "preonly",
   #"fieldsplit_1_ksp_type": "fgmres",
   "fieldsplit_1_ksp_max_it": 1,
   "fieldsplit_1_ksp_norm_type": "unpreconditioned",
   "fieldsplit_1_ksp_monitor_true_residual": None,
   "fieldsplit_1_pc_type": "python",
   "fieldsplit_1_pc_python_type": "__main__.InnerSchurPC",
   "fieldsplit_1_aux_pc_type": "lu",
   "fieldsplit_1_aux_pc_factor_mat_solver_type": "mumps",
   "fieldsplit_1_aux_mat_mumps_icntl_14": 200,
       }

fsstar = {
   "mat_type": "aij",
   "snes_type": "ksponly",
   "snes_monitor": None,
   "snes_atol": 0,
   "snes_rtol": 1.0e-9,
   "ksp_type": "fgmres",
   "ksp_monitor_true_residual": None,
   "ksp_atol": 0,
   "ksp_rtol": 1.0e-10,   "pc_type": "fieldsplit",
   "pc_fieldsplit_type": "schur",
   "pc_fieldsplit_schur_fact_type": "full",
   "fieldsplit_0_ksp_type": "chebyshev",
   #"fieldsplit_0_ksp_max_it": 1,
   #"fieldsplit_0_ksp_convergence_test": "skip",
   "fieldsplit_0_pc_type": "jacobi",
   "fieldsplit_0_pc_factor_mat_solver_type": "mumps", 
   "fieldsplit_1_ksp_type": "gmres",
   "fieldsplit_1_ksp_max_it": 1,
   "fieldsplit_1_ksp_norm_type": "unpreconditioned",
   "fieldsplit_1_pc_type": "python",
   "fieldsplit_1_pc_python_type": "__main__.InnerSchurPC",
   "fieldsplit_1_aux_mg_transfer_manager": __name__ + ".transfermanager",
   "fieldsplit_1_aux_pc_type": "mg",
   "fieldsplit_1_aux_pc_mg_cycle_type": "v",
   "fieldsplit_1_aux_pc_mg_type": "full",
   "fieldsplit_1_aux_mg_levels_ksp_type": "fgmres",
   "fieldsplit_1_aux_mg_levels_ksp_convergence_test": "skip",
   "fieldsplit_1_aux_mg_levels_ksp_max_it": 6,
   "fieldsplit_1_aux_mg_levels_ksp_norm_type": "unpreconditioned",
   #"fieldsplit_1_aux_mg_levels_ksp_monitor_true_residual": None,
   "fieldsplit_1_aux_mg_levels_pc_type": "python",
   "fieldsplit_1_aux_mg_levels_pc_python_type": "firedrake.PatchPC",
   "fieldsplit_1_aux_mg_levels_patch_pc_patch_save_operators": True,
   "fieldsplit_1_aux_mg_levels_patch_pc_patch_partition_of_unity": False,
   "fieldsplit_1_aux_mg_levels_patch_pc_patch_sub_mat_type": "seqaij",
   "fieldsplit_1_aux_mg_levels_patch_pc_patch_construct_dim": 0,
   "fieldsplit_1_aux_mg_levels_patch_pc_patch_construct_type": "python",
   "fieldsplit_1_aux_mg_levels_patch_pc_patch_construct_python_type": "alfi.MacroStar",
   "fieldsplit_1_aux_mg_levels_patch_sub_ksp_type": "preonly",
   "fieldsplit_1_aux_mg_levels_patch_sub_pc_type": "lu",
   "fieldsplit_1_aux_mg_levels_patch_sub_pc_factor_mat_solver_type": "petsc",
   "fieldsplit_1_aux_mg_coarse_pc_type": "python",
   "fieldsplit_1_aux_mg_coarse_pc_python_type": "firedrake.AssembledPC",
   "fieldsplit_1_aux_mg_coarse_assembled": {
         "mat_type": "aij",
         "pc_type": "telescope",
         "pc_telescope_reduction_factor": 1,
         "pc_telescope_subcomm_type": "contiguous",
         "telescope_pc_type": "lu",
         "telescope_pc_factor_mat_solver_type": "superlu_dist",
     }
       }      

star = {
   "mat_type": "aij",
   "snes_type": "newtonls",
   "snes_monitor": None,
   "snes_atol": 1.0e-7,
   "snes_rtol": 0,
   "ksp_type": "fgmres",
   "ksp_norm_type": "unpreconditioned",
   "ksp_monitor_true_residual": None,
   "ksp_atol": 1.0e-10,
   "ksp_rtol": 1.0e-7,
   "pc_type": "mg",
   "pc_mg_cycle_type": "v",
   "pc_mg_type": "full",
   "mg_levels_ksp_type": "fgmres",
   #"fieldsplit_1_aux_mg_levels_ksp_richardson_scale": 1,
   "mg_levels_ksp_convergence_test": "skip",
   "mg_levels_ksp_max_it": 6,
   "mg_levels_ksp_norm_type": "unpreconditioned",
   "mg_levels_ksp_monitor_true_residual": None,
   "mg_levels_pc_type": "python",
   "mg_levels_pc_python_type": "firedrake.PatchPC",
   "mg_levels_patch_pc_patch_save_operators": True,
   "mg_levels_patch_pc_patch_partition_of_unity": False,
   "mg_levels_patch_pc_patch_sub_mat_type": "seqaij",
   "mg_levels_patch_pc_patch_construct_dim": 0,
   "mg_levels_patch_pc_patch_construct_type": "python",
   "mg_levels_patch_pc_patch_construct_python_type": "alfi.MacroStar",
   "mg_levels_patch_sub_ksp_type": "preonly",
   "mg_levels_patch_sub_pc_type": "lu",
   "mg_levels_patch_sub_pc_factor_mat_solver_type": "umfpack",
   #"mg_levels_patch_sub_mat_mumps_icntl_14": 200,
   "mg_coarse_pc_type": "python",
   "mg_coarse_pc_python_type": "firedrake.AssembledPC",
   "mg_coarse_assembled": {
         "mat_type": "aij",
         "pc_type": "telescope",
         "pc_telescope_reduction_factor": 1,
         "pc_telescope_subcomm_type": "contiguous",
         "telescope_pc_type": "lu",
         "telescope_pc_factor_mat_solver_type": "mumps",
         "telescope_pc_factor_mat_mumps_icntl_14": 200,
     }
       }

mg = {
   "mat_type": "aij",
   "snes_type": "newtonls",
   "snes_monitor": None,
   "snes_atol": 0,
   "snes_rtol": 1.0e-9,
   "ksp_type": "fgmres",
   "ksp_norm_type": "unpreconditioned",
   "ksp_monitor_true_residual": None,
   "ksp_atol": 1.0e-10,
   "ksp_rtol": 1.0e-7,
   "pc_type": "mg",
   }

solvers = {"lu": lu, "fslu": fslu, "fsstar": fsstar, "star": star, "mg": mg}

parser = argparse.ArgumentParser(add_help=False)
parser.add_argument("--k", type=int, default=2)
parser.add_argument("--nref", type=int, default=1)
parser.add_argument("--baseN", type=int, default=10)
parser.add_argument("--Rem", type=float, required=True)
parser.add_argument("--delta", type=float, default=1)
parser.add_argument("--gamma", type=float, default=1)
parser.add_argument("--solver-type", choices=list(solvers.keys()), required=True)
parser.add_argument("--testfunc", choices=["brenner", "afw", "hartmann", "costabel", "Wathen"], required=True)
parser.add_argument("--hierarchy", choices=["bary", "uniform"], default="uniform")
parser.add_argument("--stab", default=False, action="store_true")
args, _ = parser.parse_known_args()

Rem = Constant(args.Rem)
k = Constant(args.k)
gamma = Constant(args.gamma)
delta = Constant(args.delta)
hierarchy = args.hierarchy
Re = Constant(20)
Sc = Constant(1)
Ha = sqrt(Re*Re*Sc)
G = 2*Ha*sinh(Ha/2) / (Re * (cosh(Ha/2) - 1))
nref = args.nref
stab = args.stab

eta = Constant(1/Rem)

base = UnitSquareMesh(args.baseN, args.baseN, distribution_parameters=distribution_parameters)

def message(msg):
    if mesh.comm.rank == 0:
        warning(msg)

def before(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+1)

def after(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+2)
    #rebalance(dm, i)


   
if hierarchy == "bary":
    mh = alfi.BaryMeshHierarchy(base, nref, callbacks=(before, after))
elif hierarchy == "uniformbary":
    bmesh = Mesh(bary(base._plex), distribution_parameters={"partition": False})
    mh = MeshHierarchy(bmesh, nref, reorder=True, callbacks=(before, after),
                       distribution_parameters=distribution_parameters)
elif hierarchy == "uniform":
    mh = MeshHierarchy(base, nref, reorder=True, callbacks=(before, after),
             distribution_parameters=distribution_parameters)
else:
    raise NotImplementedError("Only know bary, uniformbary and uniform for the hierarchy.")
for m in mh:
    m.coordinates.dat.data[:, 0] -= 0.5
    m.coordinates.dat.data[:, 1] -= 0.5
mesh = mh[-1]

(x, y) = SpatialCoordinate(mesh)
E_ex = sin(x)
u_ex = as_vector([x*y*exp(x+y) + x*exp(x+y), -x*y*exp(x+y) - y*exp(x+y)])



if args.testfunc == "hartmann":
    Sc = Constant(1)
    Ha = sqrt(Re*Rem*Sc)
    G = 2*Ha*sinh(Ha/2) / (Re * (cosh(Ha/2) - 1))
    Re = Constant(1)

    b = G/(2*Sc) * (sinh(y*Ha)/sinh(Ha/2) - 2*y)
    u_ex = as_vector([(G*Re / (2*Ha*tanh(Ha/2))) * (1 - (cosh(y*Ha)/cosh(Ha/2))), 0])
    p_ex = -G*x - Sc/2 * b**2
    B_ex = as_vector([b, Constant(1, domain=mesh)])
    sigma_ex = sin(x) 

    f1 = + vcurl(sigma_ex) - gamma * grad(div(B_ex)) 
    f2 = -1/Rem * scurl(B_ex) + sigma_ex + delta * scross(u_ex, B_ex)
    delta_term = delta*vcurl(scross(u_ex, B_ex))
    print("||delta_term||_L^2 = %s" % sqrt(assemble(inner(delta_term, delta_term)*dx)))

elif args.testfunc == "Wathen":
    #u_ex = as_vector([x*y*exp(x+y) + x*exp(x+y), -x*y*exp(x+y) - y*exp(x+y)])
    B_ex = as_vector([exp(x+y)*cos(x), exp(x+y)*sin(x) - exp(x+y)*cos(x)])
    E_ex = sin(x) #(x-0.5)*(x+0.5)*(y-0.5)*(y+0.5)# sin(x)#Constant(0, domain=mesh)
    #f= 1/Rem * vcurl(scurl(B_ex))  - delta*vcurl(scross(u_ex, B_ex)) + beta*B_ex + gamma*grad(sigma_ex)
    f1 = + vcurl(E_ex) - gamma * grad(div(B_ex)) - eta * grad(div(B_ex))
    f2 = -1/Rem * scurl(B_ex) + E_ex + delta * scross(u_ex, B_ex)
    delta_term = delta*vcurl(scross(u_ex, B_ex))
    print("||delta_term||_L^2 = %s" % sqrt(assemble(inner(delta_term, delta_term)*dx)))
    n = FacetNormal(mesh)
    t = as_vector([n[1], -n[0]])
    R = FunctionSpace(mesh, "R", 0)
    zero = Function(R)
    zero.project(Constant(0))

    B_ = as_vector([B_ex[0], B_ex[1], zero])
    n_ = as_vector([n[0], n[1], zero])
    g_ = cross(acurl(B_), n_)
    g  = as_vector([g_[0], g_[1]])

else:
    raise ValueError("Testfunc does not exist")

W = FunctionSpace(mesh, "RT", args.k-1)    #B
#W = VectorFunctionSpace(mesh, "CG", args.k-1)    #B
R = FunctionSpace(mesh, "CG", args.k-1)    #E
Z = MixedFunctionSpace([R, W])



z = Function(Z)
(E, B) = split(z)
(Ff, C)   = split(TestFunction(Z))
F = (
      eta * inner(div(B), div(C)) * dx
    + gamma * inner(div(B), div(C)) * dx
    + inner(vcurl(E), C) * dx
    - 1/Rem * inner(B, vcurl(Ff)) * dx
    + delta * inner(scross(u_ex, B), Ff) * dx
    + inner(E, Ff) * dx
    -(inner(f1, C) * dx + inner(f2, Ff) * dx)
    )

if stab:
    stabilisation_form = BurmanStab(B, C, B_ex, mesh)
    F += (delta * stabilisation_form)

appctx = {"nu": 1/Rem, "gamma": gamma}

params = solvers[args.solver_type]

bcs = [DirichletBC(Z.sub(1), B_ex, "on_boundary"),
           DirichletBC(Z.sub(0), E_ex , "on_boundary"),
    ]

nvproblem = NonlinearVariationalProblem(F, z, bcs=bcs)
solver = NonlinearVariationalSolver(nvproblem, solver_parameters=params, options_prefix="", appctx=appctx)
qtransfer = NullTransfer()

dgtransfer = DGInjection()
rawtransfer = TransferManager(native_transfers={VectorElement("DG", mesh.ufl_cell(), args.k-1): (dgtransfer.prolong, restrict, dgtransfer.inject)})
class SVSchoeberlTransferDG(SVSchoeberlTransfer):
    def standard_transfer(self, source, target, mode):
        if mode == "prolong":
            rawtransfer.prolong(source, target)
        elif mode == "restrict":
            rawtransfer.restrict(source, target)
        else:
            raise NotImplementedError

vtransfer = SVSchoeberlTransferDG((1/Rem, gamma), 2, hierarchy)
transfers = {
                #W.ufl_element(): (vtransfer.prolong, vtransfer.restrict, rawtransfer.inject),
                R.ufl_element(): (prolong, restrict, qtransfer.inject),
                VectorElement("DG", mesh.ufl_cell(), args.k-1): (dgtransfer.prolong, restrict, dgtransfer.inject),
            }
transfermanager = TransferManager(native_transfers=transfers)
solver.set_transfer_manager(transfermanager)


fac = 0.5
B_ex_ = project(B_ex, W, solver_parameters=lu)
E_ex_ = project(E_ex, R, solver_parameters=lu)
dis_vec = as_vector([fac * cos(y), fac * sin(x)])
dis_scalar = fac * sin(x)
dis_vec_B_ = project(dis_vec, W)
dis_vec_E_ = project(dis_scalar, R)

B_ex_dis_ = B_ex_ + dis_vec_B_
E_ex_dis_ = E_ex_ + dis_vec_E_

results = {}
start = 250
end = 10000
step = 2000
rems = [args.Rem]
#rems = [1, 1000, 10000, 100000]

for rem in rems:
   Rem.assign(rem)

   (E, B) = z.split()
   B.assign(B_ex_dis_)
   E.assign(E_ex_dis_)
   

   message(GREEN % ("Solving for Rem = %s, gamma = %s ,nref=%s" % (float(rem), float(gamma), int(nref))))

   start = datetime.now()
   solver.solve()
   end = datetime.now()

   linear_its = solver.snes.getLinearSolveIterations()
   nonlinear_its = solver.snes.getIterationNumber()
   time = (end-start).total_seconds() / 60
   print(GREEN % ("Time taken: %.2f min in %d nonlinear iterations, %d linear iterations (%.2f Krylov iters per Newton step)" % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its))))
   print("%.2f @ %d @ %d @ %.2f" % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its)))

   (E, B) = z.split()
   print("||div(B)||_L^2 = %s" % sqrt(assemble(inner(div(B), div(B))*dx)))

   if args.testfunc == "brenner" or args.testfunc == "hartmann"  or args.testfunc == "costabel" or args.testfunc == "Wathen":
      error = errornorm(B_ex, B, 'L2')
      error_E = errornorm(E_ex, E, 'L2')
      B_ex_ = project(B_ex, B.function_space())
      E_ex_ = project(E_ex, E.function_space())
      B.rename("MagneticField")
      B_ex_.rename("ExactSolution")
      E.rename("E field")
      E_ex_.rename("E_ex")
      print("Error ||B_ex - B||_L^2 = %s" % error)
      print("Error ||E_ex - E||_L^2 = %s" % error_E)
      File("output/magnetic.pvd").write(B, B_ex_, E, E_ex_)
   else:
      B_ex_ = project(B_ex, B.function_space())
      B.rename("MagneticField")
      B_ex_.rename("ExactSolution")
      sigma.rename("sigma multiplier")
      File("output/magnetic.pvd").write(B, B_ex_, sigma)
   info_dict = {
        "Rem": rem,
        "krylov/nonlin": linear_its/nonlinear_its,
        "error_B": error,
        "error_E": error_E,

    }
   results[rem] = info_dict


for rem in results:
        print(results[rem])
