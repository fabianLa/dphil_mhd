# -*- coding: utf-8 -*-
from firedrake import *
from datetime import datetime
import alfi
import argparse
import numpy
from pyop2.datatypes import IntType
import ufl.algorithms
from alfi.stabilisation import *
from alfi.transfer import *
from alfi import *

distribution_parameters={"partition": True, "overlap_type": (DistributedMeshOverlapType.VERTEX, 1)}

def scross(x, y):
    return x[0]*y[1] - x[1]*y[0]

def vcross(x,y):
    return as_vector([x[1]*y,-x[0]*y])

def scurl(x):
    return x[1].dx(0) - x[0].dx(1)

def vcurl(x):
    return as_vector([x.dx(1), -x.dx(0)])

def acurl(x):
    return as_vector([
                     x[2].dx(1),
                     -x[2].dx(0),
                     x[1].dx(0) - x[0].dx(1)
                     ])

def BurmanStab(B, C, supg_wind, mesh):
    n = FacetNormal(mesh)
    h = FacetArea(mesh)
    beta = sqrt(avg(facet_avg(dot(supg_wind, n)**2)))
    gamma1 = Constant(5e-3) # as chosen in doi:10.1016/j.apnum.2007.11.001
    stabilisation_form = 0.5 * gamma1 * h**2 * beta * dot(jump(grad(B), n), jump(grad(C), n))*dS
    return stabilisation_form


class Mass(AuxiliaryOperatorPC):
    def form(self, pc, q, p):
        mesh = q.function_space().mesh()
       
        K = -1/(nu+gamma) * inner(p, q) * dx

        bcs = None#[DirichletBC(q.function_space(), 0, "on_boundary")]

        return (K, bcs)

class InnerSchurPC(AuxiliaryOperatorPC):
    def form(self, pc, C, B):
        mesh = C.function_space().mesh()
        n = FacetNormal(mesh)
        t = as_vector([n[1], -n[0]])
        h = CellDiameter(mesh)
        h_avg = (h('+') + h('-'))/2
        alpha = Constant(4)

        state = self.get_appctx(pc)['state']
        [u_n, p_n, B_n, E_n] = split(state)

        K = eta * (
              + inner(grad(B), grad(C))*dx
              #+ alpha/h_avg*inner(jump(C, t), jump(B, t))*dS
            )

        K = K  + gamma2 * inner(div(B), div(C)) * dx
        K = K - inner(scross(u_n, B), scurl(C))*dx

        if stab:
            stabilisation_form = BurmanStab(B, C, z_last_B, mesh)
            K = K + stabilisation_form

        #K = K- (+ inner(dot(grad(u_n), B), C) * dx - inner(dot(grad(B), u_n), C) * dx
        #        + inner(u_n*div(B), C) * dx - inner(B*div(u_n), C) * dx
        #        )

        if boundary_cond == "normal":
           bcs = [ DirichletBC(C.function_space().sub(0), 0, 1),
               DirichletBC(C.function_space().sub(0), 0, 2),
               DirichletBC(C.function_space().sub(1), 0, 3),
               DirichletBC(C.function_space().sub(1), 0, 4)]
        elif boundary_cond == "Dirichlet":
            bcs = [ DirichletBC(C.function_space(), 0, "on_boundary")]

        return (K, bcs)


class SchurPC(AuxiliaryOperatorPC):
    def form(self, pc, V, U):
        mesh = V.function_space().mesh()
        [u, p] = split(U)
        [v, q] = split(V)
        h = CellDiameter(mesh)

        state = self.get_appctx(pc)['state']
        [u_n, p_n, B_n, E_n] = split(state)

        eps = lambda x: sym(grad(x))

        A =  (
                2 * nu * inner(eps(u), eps(v))*dx
              + advect * inner(dot(grad(u_n), u), v) * dx
              + advect * inner(dot(grad(u), u_n), v) * dx
              + gamma * inner(div(u), div(v)) * dx
              - inner(p, div(v)) * dx
              - inner(div(u), q) * dx
              + S * inner(vcross(B_n, scross(u, B_n)), v) * dx
                        )
        if stab:
            stabilisation_form = BurmanStab(u, v, z_last_u, mesh)
            A = A + stabilisation_form
        
        bcs = [DirichletBC(V.function_space().sub(0), 0, "on_boundary"),
               PressureFixBC(V.function_space().sub(1), 0, 1),
        ]

        return (A, bcs)

class Schur(AuxiliaryOperatorPC):
    def form(self, pc, V, U):
        mesh = V.function_space().mesh()
        [B, E] = split(U)
        [C, Ff] = split(V)
        h = CellDiameter(mesh)

        state = self.get_appctx(pc)['state']
        [u_n, p_n, B_n, E_n] = split(state)

        eps = lambda x: sym(grad(x))

        A =  (
             + inner(E, Ff) * dx
             + inner(scross(u_n, B), Ff) * dx
             - 1/Rem * inner(scurl(B), Ff) * dx
             + inner(E, scurl(C)) * dx
             + eta * inner(div(B), div(C)) * dx
             + gamma2 * inner(div(B), div(C)) * dx
                        )
        if stab:
            stabilisation_form = BurmanStab(B, C, z_last_B, mesh)
            A = A + stabilisation_form
        
        bcs = [DirichletBC(V.function_space().sub(0), 0, "on_boundary"),
               PressureFixBC(V.function_space().sub(1), 0, 1),
        ]

        return (A, bcs)



class XuPC(AuxiliaryOperatorPC):
    def form(self, pc, V, U):
        mesh = V.function_space().mesh()
        [u, B, p, sigma] = split(U)
        [v, C, q, tau] = split(V)

        state = self.get_appctx(pc)['state']
        [u_n, B_n, p_n, sigma_n] = split(state)



        eps = lambda x: sym(grad(x))

        A =  (
                2 * nu * inner(eps(u), eps(v)) * dx
              + gamma * inner(div(u), div(v)) * dx
              #+ inner(div(u), q) * dx
              + inner(p, q) * dx
              + 1/Rem * inner(scurl(B), scurl(C)) * dx
              + inner(B, C) * dx
              #- inner(scross(u, B_n), scurl(C)) * dx
              + inner(B, grad(tau)) * dx
              #+ inner(sigma, tau) * dx
              - inner(grad(sigma), grad(tau)) * dx
                        )            
             
        bcs = [DirichletBC(V.function_space().sub(0), 0, "on_boundary"),
               DirichletBC(V.function_space().sub(1), 0, "on_boundary"),
               DirichletBC(V.function_space().sub(3), 0, "on_boundary"),
               PressureFixBC(V.function_space().sub(2), 0, 1),
        ]

        return (A, bcs)

class PressureFixBC(DirichletBC):
    def __init__(self, V, val, subdomain, method="topological"):
        super().__init__(V, val, subdomain, method)
        sec = V.dm.getDefaultSection()
        dm = V.mesh()._plex

        coordsSection = dm.getCoordinateSection()
        coordsDM = dm.getCoordinateDM()
        dim = dm.getCoordinateDim()
        coordsVec = dm.getCoordinatesLocal()

        (vStart, vEnd) = dm.getDepthStratum(0)
        indices = []
        for pt in range(vStart, vEnd):
            x = dm.getVecClosure(coordsSection, coordsVec, pt).reshape(-1, dim).mean(axis=0)
            if x.dot(x) == 0.0: # fix [0, 0] in original mesh coordinates (bottom left corner)
                if dm.getLabelValue("pyop2_ghost", pt) == -1:
                    indices = [pt]
                break

        nodes = []
        for i in indices:
            if sec.getDof(i) > 0:
                nodes.append(sec.getOffset(i))

        if V.mesh().comm.rank == 0:
            nodes = [0]
        else:
            nodes = []
        self.nodes = numpy.asarray(nodes, dtype=IntType)

        if len(self.nodes) > 0:
            print("Fixing nodes %s" % self.nodes)
        else:
            print("Not fixing any nodes")
        import sys; sys.stdout.flush()

lu = {
     "snes_type": "newtonls",
     "snes_monitor": None,
     "snes_atol": 1.0e-7,
     "snes_rtol": 1.0e-8,
     "snes_linesearch_type": "l2",
     #"snes_linesearch_monitor": None,
     "snes_linesearch_maxstep": 1,
     #"snes_converged_reason": None,
     #"snes_max_it": 1000,
     #"snes_convergence_test": "skip",
     "mat_type": "aij",
     "ksp_type": "preonly",
     "pc_type": "lu",
     "pc_factor_mat_solver_type": "mumps",
     "mat_mumps_icntl_14": 300,
         }

mfsstar2 = {
   "ksp_type": "gmres",
   #"ksp_monitor_true_residual": None,
   "ksp_max_it": 2,
   "pc_type": "fieldsplit",
   "pc_fieldsplit_type": "schur",
   "pc_fieldsplit_schur_fact_type": "full",
   "pc_fieldsplit_0_fields": "1",
   "pc_fieldsplit_1_fields": "0",
   "fieldsplit_0_ksp_type": "chebyshev",
   #"fieldsplit_0_ksp_max_it": 1,
   #"fieldsplit_0_ksp_convergence_test": "skip",
   "fieldsplit_0_pc_type": "jacobi",
   "fieldsplit_0_pc_factor_mat_solver_type": "mumps", 
   "fieldsplit_1_ksp_type": "gmres",
   "fieldsplit_1_ksp_max_it": 1,
   "fieldsplit_1_ksp_norm_type": "unpreconditioned",
   "fieldsplit_1_pc_type": "python",
   "fieldsplit_1_pc_python_type": "__main__.InnerSchurPC",
   "fieldsplit_1_aux_pc_type": "mg",
   "fieldsplit_1_aux_pc_mg_cycle_type": "v",
   "fieldsplit_1_aux_pc_mg_type": "full",
   "fieldsplit_1_aux_mg_levels_ksp_type": "fgmres",
   "fieldsplit_1_aux_mg_levels_ksp_convergence_test": "skip",
   "fieldsplit_1_aux_mg_levels_ksp_max_it": 6,
   "fieldsplit_1_aux_mg_levels_ksp_norm_type": "unpreconditioned",
   #"fieldsplit_1_aux_mg_levels_ksp_monitor_true_residual": None,
   "fieldsplit_1_aux_mg_levels_pc_type": "python",
   "fieldsplit_1_aux_mg_levels_pc_python_type": "firedrake.PatchPC",
   "fieldsplit_1_aux_mg_levels_patch_pc_patch_save_operators": True,
   "fieldsplit_1_aux_mg_levels_patch_pc_patch_partition_of_unity": False,
   "fieldsplit_1_aux_mg_levels_patch_pc_patch_sub_mat_type": "seqaij",
   "fieldsplit_1_aux_mg_levels_patch_pc_patch_construct_dim": 0,
   "fieldsplit_1_aux_mg_levels_patch_pc_patch_construct_type": "python",
   "fieldsplit_1_aux_mg_levels_patch_pc_patch_construct_python_type": "alfi.MacroStar",
   "fieldsplit_1_aux_mg_levels_patch_sub_ksp_type": "preonly",
   "fieldsplit_1_aux_mg_levels_patch_sub_pc_type": "lu",
   "fieldsplit_1_aux_mg_levels_patch_sub_pc_factor_mat_solver_type": "petsc",
   "fieldsplit_1_aux_mg_coarse_pc_type": "python",
   "fieldsplit_1_aux_mg_coarse_pc_python_type": "firedrake.AssembledPC",
   "fieldsplit_1_aux_mg_coarse_assembled": {
         "mat_type": "aij",
         "pc_type": "telescope",
         "pc_telescope_reduction_factor": 1,
         "pc_telescope_subcomm_type": "contiguous",
         "telescope_pc_type": "lu",
         "telescope_pc_factor_mat_solver_type": "superlu_dist",
     }
       }



nfslu2 = {
    "ksp_type": "gmres",
    "ksp_monitor_true_residual": None,
    "ksp_converged_reason": None,
    "mat_type": "aij",
    "ksp_max_it": 2,
    "pc_type": "fieldsplit",
    "pc_fieldsplit_type": "schur",
    # "pc_fieldsplit_schur_factorization_type": "upper",
    "pc_fieldsplit_schur_factorization_type": "full",
    "pc_fieldsplit_schur_precondition": "user",
    "fieldsplit_0": {
            "ksp_type": "preonly",
            "ksp_max_it": 1,
            "pc_type": "lu",
            "pc_factor_mat_solver_type": "mumps",
            "mat_mumps_icntl_14": 150,
    },
    "fieldsplit_1":{
            "ksp_type": "preonly",
            "pc_type": "python",
            "pc_python_type": "alfi.solver.DGMassInv"
        },
    }

nslu = {
   "ksp_type": "gmres",
   "ksp_max_it": 2,
   #"ksp_monitor_true_residual": None,
   #"ksp_converged_reason": None,
   "pc_type": "python",
   "pc_python_type": "__main__.SchurPC",
   #"aux_pc_type": "lu",
   #"aux_pc_factor_mat_solver_type": "mumps",
   #"aux_mat_mumps_icntl_14": 200,
   "aux_pc_type": "fieldsplit",
   "aux_pc_fieldsplit_type": "schur",
   "aux_pc_fieldsplit_schur_factorization_type": "full",
   "aux_pc_fieldsplit_schur_precondition": "user",
   "aux_fieldsplit_0": {
           "ksp_type": "preonly",
           "ksp_max_it": 1,
           "pc_type": "lu",
           "pc_factor_mat_solver_type": "mumps",
           "mat_mumps_icntl_14": 150,
       },
   "aux_fieldsplit_1":{
           "ksp_type": "preonly",
           "pc_type": "python",
           "pc_python_type": "alfi.solver.DGMassInv"
       },
   }

schurfsstar = {
   "ksp_type": "gmres",
   #"ksp_monitor_true_residual": None,
   "ksp_max_it": 2,
   "pc_type": "python",
   "pc_python_type": "__main__.Schur",
   "aux_mg_transfer_manager": __name__ + ".transfermanager",
   "aux_pc_type": "fieldsplit",
   "aux_pc_fieldsplit_type": "schur",
   "aux_pc_fieldsplit_schur_fact_type": "full",
   "aux_pc_fieldsplit_0_fields": "1",
   "aux_pc_fieldsplit_1_fields": "0",
   "aux_fieldsplit_0_ksp_type": "chebyshev",
   #"fieldsplit_0_ksp_max_it": 1,
   #"fieldsplit_0_ksp_convergence_test": "skip",
   "aux_fieldsplit_0_pc_type": "jacobi",
   "aux_fieldsplit_0_pc_factor_mat_solver_type": "mumps", 
   "aux_fieldsplit_1_ksp_type": "gmres",
   "aux_fieldsplit_1_ksp_max_it": 1,
   "aux_fieldsplit_1_ksp_norm_type": "unpreconditioned",
   "aux_fieldsplit_1_pc_type": "python",
   "aux_fieldsplit_1_pc_python_type": "__main__.InnerSchurPC",
   "aux_fieldsplit_1_aux_mg_transfer_manager": __name__ + ".transfermanager",
   "aux_fieldsplit_1_aux_pc_type": "mg",
   "aux_fieldsplit_1_aux_pc_mg_cycle_type": "v",
   "aux_fieldsplit_1_aux_pc_mg_type": "full",
   "aux_fieldsplit_1_aux_mg_levels_ksp_type": "fgmres",
   "aux_fieldsplit_1_aux_mg_levels_ksp_convergence_test": "skip",
   "aux_fieldsplit_1_aux_mg_levels_ksp_max_it": 6,
   "aux_fieldsplit_1_aux_mg_levels_ksp_norm_type": "unpreconditioned",
   "aux_fieldsplit_1_aux_mg_levels_ksp_monitor_true_residual": None,
   "aux_fieldsplit_1_aux_mg_levels_pc_type": "python",
   "aux_fieldsplit_1_aux_mg_levels_pc_python_type": "firedrake.PatchPC",
   "aux_fieldsplit_1_aux_mg_levels_patch_pc_patch_save_operators": True,
   "aux_fieldsplit_1_aux_mg_levels_patch_pc_patch_partition_of_unity": False,
   "aux_fieldsplit_1_aux_mg_levels_patch_pc_patch_sub_mat_type": "seqaij",
   "aux_fieldsplit_1_aux_mg_levels_patch_pc_patch_construct_dim": 0,
   "aux_fieldsplit_1_aux_mg_levels_patch_pc_patch_construct_type": "python",
   "aux_fieldsplit_1_aux_mg_levels_patch_pc_patch_construct_python_type": "alfi.MacroStar",
   "aux_fieldsplit_1_aux_mg_levels_patch_sub_ksp_type": "preonly",
   "aux_fieldsplit_1_aux_mg_levels_patch_sub_pc_type": "lu",
   "aux_fieldsplit_1_aux_mg_levels_patch_sub_pc_factor_mat_solver_type": "petsc",
   "aux_fieldsplit_1_aux_mg_coarse_pc_type": "python",
   "aux_fieldsplit_1_aux_mg_coarse_pc_python_type": "firedrake.AssembledPC",
   "aux_fieldsplit_1_aux_mg_coarse_assembled": {
         "mat_type": "aij",
         "pc_type": "telescope",
         "pc_telescope_reduction_factor": 1,
         "pc_telescope_subcomm_type": "contiguous",
         "telescope_pc_type": "lu",
         "telescope_pc_factor_mat_solver_type": "superlu_dist",
     }
    }


schurlu = {
   "ksp_type": "gmres",
   "ksp_max_it": 2,
   #"ksp_monitor_true_residual": None,
   #"ksp_converged_reason": None,
   "pc_type": "python",
   "pc_python_type": "__main__.Schur",
   "aux_pc_type": "lu",
   "aux_pc_factor_mat_solver_type": "mumps",
   "aux_mat_mumps_icntl_14": 200,
   }

nsfs = {
   "ksp_type": "fgmres",
   "ksp_atol": 1.0e-7,
   "ksp_rtol": 1.0e-7,
   #"ksp_monitor_true_residual": None,
   #"ksp_converged_reason": None,
   #"pc_type": "python",
   #"pc_python_type": "__main__.SchurPC",
   #"aux_pc_type": "lu",
   #"aux_pc_factor_mat_solver_type": "mumps",
   #"aux_mat_mumps_icntl_14": 200,
   "ksp_max_it": 2,
   "pc_type": "fieldsplit",
   "pc_fieldsplit_type": "schur",
   "pc_fieldsplit_schur_factorization_type": "full",
   "pc_fieldsplit_schur_precondition": "user",
   "fieldsplit_0": {
          "ksp_type": "gmres",
          "ksp_max_it": 1,
          "ksp_norm_type": "unpreconditioned",
          "pc_type": "mg",
          "pc_mg_cycle_type": "v",
          "pc_mg_type": "full",
          "mg_levels_ksp_type": "fgmres",
          "mg_levels_ksp_convergence_test": "skip",
          "mg_levels_ksp_max_it": 6,
          "mg_levels_ksp_norm_type": "unpreconditioned",
          #"mg_levels_ksp_monitor_true_residual": None,
          "mg_levels_pc_type": "python",
          "mg_levels_pc_python_type": "firedrake.PatchPC",
          "mg_levels_patch_pc_patch_save_operators": True,
          "mg_levels_patch_pc_patch_partition_of_unity": False,
          "mg_levels_patch_pc_patch_sub_mat_type": "seqaij",
          "mg_levels_patch_pc_patch_construct_dim": 0,
          "mg_levels_patch_pc_patch_construct_type": "python",
          "mg_levels_patch_pc_patch_construct_python_type": "alfi.MacroStar",
          "mg_levels_patch_sub_ksp_type": "preonly",
          "mg_levels_patch_sub_pc_type": "lu",
          "mg_levels_patch_sub_pc_factor_mat_solver_type": "petsc",
          "mg_coarse_pc_type": "python",
          "mg_coarse_pc_python_type": "firedrake.AssembledPC",
          "mg_coarse_assembled": {
                "mat_type": "aij",
                "pc_type": "telescope",
                "pc_telescope_reduction_factor": 1,
                "pc_telescope_subcomm_type": "contiguous",
                "telescope_pc_type": "lu",
                "telescope_pc_factor_mat_solver_type": "superlu_dist",
            }
       },
   "fieldsplit_1":{
           "ksp_type": "preonly",
           "pc_type": "python",
           "pc_python_type": "alfi.solver.DGMassInv"
       },
   }
       


fs2by2 = {
     "snes_type": "newtonls",
     "snes_max_it": 30,
     "snes_linesearch_type": "l2",
     "snes_linesearch_maxstep": 1.0,
     "snes_rtol": 1.0e-8,
     #"snes_convergence_test": "skip",
     "snes_monitor": None,
     #"snes_linesearch_monitor": None,
     "snes_converged_reason": None,
     "ksp_type": "fgmres",
     #"ksp_atol": 0,
     #"ksp_rtol": 1.0e-8,
     "ksp_monitor_true_residual": None,
     #"ksp_converged_reason": None,
     "mat_type": "aij",
     "pc_type": "fieldsplit",
     "pc_fieldsplit_type": "schur",
     "pc_fieldsplit_0_fields": "0,1",
     "pc_fieldsplit_1_fields": "2,3",
     "fieldsplit_0": nsfs,
     #"fieldsplit_0_ksp_type": "preonly",
     #"fieldsplit_0_pc_type": "lu",
     #"fieldsplit_0_pc_factor_mat_solver_type": "mumps",
     #"fieldsplit_0_mat_mumps_icntl_14": 200,
     "fieldsplit_1": schurlu,
    }
    



solvers = {"lu": lu, "fs2by2": fs2by2}

parser = argparse.ArgumentParser(add_help=False)
parser.add_argument("--baseN", type=int, default=10)
parser.add_argument("--k", type=int, default=3)
parser.add_argument("--nref", type=int, default=1)
parser.add_argument("--Re", type=float, default=1)
parser.add_argument("--Rem", type=float, default=1)
parser.add_argument("--gamma", type=float, default=10000)
parser.add_argument("--gamma2", type=float, default=1)
parser.add_argument("--beta", type=float, default=0)
parser.add_argument("--eta", type=float, default=0)
parser.add_argument("--S", type=float, default=1)
parser.add_argument("--char_L", type=float, default=1)
parser.add_argument("--char_U", type=float, default=1)
parser.add_argument("--lambda_", type=float, default=1)
parser.add_argument("--hierarchy", choices=["bary", "uniform"], default="bary")
parser.add_argument("--boundary-cond", choices=["normal", "Dirichlet"], required=True)
parser.add_argument("--solver-type", choices=list(solvers.keys()), default="lu")
parser.add_argument("--testproblem", choices=["ldc", "hartmann", "Wathen", "hartmann2", "hartmann3"], default = "hartmann")
parser.add_argument("--picard", default=False, action="store_true")
parser.add_argument("--exing", default=False, action="store_true")
parser.add_argument("--stab", default=False, action="store_true")

args, _ = parser.parse_known_args()
baseN = args.baseN
k = args.k
nref = args.nref
Re = Constant(args.Re)
Rem = Constant(args.Rem)
gamma = Constant(args.gamma)
S = Constant(args.S)
hierarchy = args.hierarchy
stab = args.stab
solver_type = args.solver_type
testproblem = args.testproblem
gamma2 = Constant(args.gamma2)
advect = Constant(1)  
char_L = Constant(args.char_L)
char_U = Constant(args.char_U)
lambda_= Constant(args.lambda_)
beta = Constant(args.beta)
eta = Constant(args.eta)
nu = Constant(1/Re)
picard = args.picard
exact_initial_guess = args.exing
boundary_cond=args.boundary_cond

eta = 1/Rem 

base = UnitSquareMesh(baseN, baseN, diagonal="crossed", distribution_parameters=distribution_parameters)

def before(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+1)

def after(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+2)
    #rebalance(dm, i)

if hierarchy == "bary":
    mh = alfi.BaryMeshHierarchy(base, nref, callbacks=(before, after))
elif hierarchy == "uniformbary":
    bmesh = Mesh(bary(base._plex), distribution_parameters={"partition": False})
    mh = MeshHierarchy(bmesh, nref, reorder=True, callbacks=(before, after),
                       distribution_parameters=distribution_parameters)
elif hierarchy == "uniform":
    mh = MeshHierarchy(base, nref, reorder=True, callbacks=(before, after),
             distribution_parameters=distribution_parameters)
else:
    raise NotImplementedError("Only know bary, uniformbary and uniform for the hierarchy.")
for m in mh:
    m.coordinates.dat.data[:, 0] -= 0.5
    m.coordinates.dat.data[:, 1] -= 0.5
mesh = mh[-1]

area = assemble(Constant(1, domain=mh[0])*dx)

def message(msg):
    if mesh.comm.rank == 0:
        warning(msg)

V = VectorFunctionSpace(mesh, "CG", k)  #u
Q = FunctionSpace(mesh, "DG", k-1)      #p
R = FunctionSpace(mesh, "DG", k-1)    #E
W = VectorFunctionSpace(mesh, "CG", k)        #B
Z = MixedFunctionSpace([V, Q, W, R])

z = Function(Z)
(u, p, B, E) = split(z)
(v, q, C, Ff)   = split(TestFunction(Z))
z_last_u = Function(V)
z_last_B = Function(W)


(x, y) = SpatialCoordinate(Z.mesh())
n = FacetNormal(mesh)
t = as_vector([n[1], -n[0]])

#B_ex__=as_vector([exp(x+y)*cos(x), exp(x+y)*sin(x) - exp(x+y)*cos(x)])

eps = lambda x: sym(grad(x))

def compute_rhs(u_ex, B_ex, p_ex, E_ex):
    f1 = -2 * nu * div(eps(u_ex)) + advect * dot(grad(u_ex),u_ex) - gamma * grad(div(u_ex)) + grad(p_ex) + S * vcross(B_ex, (E_ex + scross(u_ex, B_ex)))
    f2 = + vcurl(E_ex) - eta * grad(div(B_ex)) 
    f3 = -1/Rem * scurl(B_ex) + E_ex + scross(u_ex, B_ex)
    #print("||f1||_L^2 = %s" % sqrt(assemble(inner(f1, f1)*dx)))
    #print("||f2||_L^2 = %s" % sqrt(assemble(inner(f2, f2)*dx)))
    #print("||f3||_L^2 = %s" % sqrt(assemble(inner(f3, f3)*dx)))
    #exit(-1)
    return (f1, f2, f3)
    

if testproblem == "hartmann":
    u_ex = as_vector([cos(y), Constant(0, domain=mesh)])
    B_ex = as_vector([Constant(0, domain=mesh), cos(x)])
    p_ex = -x * cos(y)
    pintegral = assemble(p_ex*dx)
    p_ex = p_ex - Constant(pintegral/area)
    E_ex = sin(x)
    
    (f1, f2, f3) = compute_rhs(u_ex, B_ex, p_ex, E_ex)
    rhs = True
    solution_known = True

elif testproblem == "hartmann2":
    Sc = Constant(1)
    Ha = sqrt(Re*Rem*Sc) 
    G = 2*Ha*sinh(Ha/2) / (Re * (cosh(Ha/2) - 1))

    b = G/(2*Sc) * (sinh(y*Ha)/sinh(Ha/2) - 2*y)*Rem
    u_ex = as_vector([(G*Re / (2*Ha*tanh(Ha/2))) * (1 - (cosh(y*Ha)/cosh(Ha/2))), 0])
    p_ex = -G*x - Sc/2 * b**2
    pintegral = assemble(p_ex*dx)
    p_ex = p_ex - Constant(pintegral/area)
    B_ex = as_vector([b, Constant(1, domain=mesh)])
    E_ex = 1/Rem * scurl(B_ex) - scross(u_ex, B_ex)
    
    (f1, f2, f3) = compute_rhs(u_ex, B_ex, p_ex, E_ex)
    rhs = True
    solution_known = True

elif testproblem == "hartmann3":
    H = sqrt(Re*Rem)

    b = 1/(2*sinh(H/2)) * (sinh(y*H) - 2*y*sinh(H/2))
    u_1 = (Re) / (2*H*sinh(H/2))*(cosh(H/2) - cosh(H*y))
    u_ex = as_vector([u_1, 0])
    p_ex = -x -  b**2/2
    pintegral = assemble(p_ex*dx)
    p_ex = p_ex - Constant(pintegral/area)
    B_ex = as_vector([b, Constant(1, domain=mesh)])
    E_ex = (- 1 / (Rem*2*sinh(H/2))*(H*cosh(H*y)-2*sinh(H/2)) - 1*Re/(2*H*sinh(H/2))*(cosh(H/2)-cosh(H*y)))

    (f1, f2, f3) = compute_rhs(u_ex, B_ex, p_ex, E_ex)
    rhs = True
    solution_known = True

elif testproblem == "hartmann4":
    Ha = sqrt(Re*Rem)

    b = sinh(y*Ha)/(2*sinh(Ha/2)) - y
    u1 = 1/(2*tanh(Ha/2))*sqrt(Re/Rem)*(1-cosh(y*Ha)/cosh(Ha/2))
    u_ex = as_vector([u1, Constant(0)])
    p_ex = -x -0.5*b**2
    pintegral = assemble(p_ex*dx)
    p_ex = p_ex - Constant(pintegral/area)
    B_ex = as_vector([b, Constant(1, domain=mesh)])
    E_ex = scurl(B_ex) - scross(u_ex, B_ex)

    (f1, f2, f3) = compute_rhs(u_ex, B_ex, p_ex, E_ex)
    rhs = True
    solution_known = True


elif testproblem == "ldc":
   #example taken from chapter 5.1 in doi.org/10.1137/16M1074084
   #u_ex = as_vector([(x-0.5)*(x-0.5)*(x+0.5)*(x+0.5)*(0.25*(y+0.5)*(y+0.5)), 0])
   u_ex = Constant((1, 0), domain=mesh)
   B_ex = Constant((-1, 0), domain=mesh)
   B_ex = project(B_ex, W)
   
   bcs = [DirichletBC(Z.sub(0), u_ex, 4),  #4 == upper boundary (y==1)
          DirichletBC(Z.sub(0), 0, (1,2,3)),
          #DirichletBC(Z.sub(2), B_ex, "on_boundary"),
          DirichletBC(Z.sub(2).sub(0), B_ex[0], 1),
          DirichletBC(Z.sub(2).sub(0), B_ex[0], 2),
          DirichletBC(Z.sub(2).sub(1), B_ex[1], 3),
          DirichletBC(Z.sub(2).sub(1), B_ex[1], 4),
          DirichletBC(Z.sub(3), 0 , "on_boundary"),
          PressureFixBC(Z.sub(1), 0, 1)]
   rhs = None
   solution_known = False

elif testproblem == "Wathen":
    #example taken from chapter 6.2.1 in doi.org/a10.1137/16M1098991
    u_ex = as_vector([x*y*exp(x+y) + x*exp(x+y), -x*y*exp(x+y) - y*exp(x+y)])
    p_ex = exp(y) * sin(x)
    pintegral = assemble(p_ex*dx)
    p_ex = p_ex - Constant(pintegral/area)
    B_ex = as_vector([exp(x+y)*cos(x), exp(x+y)*sin(x) - exp(x+y)*cos(x)])
    E_ex = sin(x) #(x-0.5)*(x+0.5)*(y-0.5)*(y+0.5)#sin(x) # Constant(0, domain=mesh) # sin(x)

    (f1, f2, f3) = compute_rhs(u_ex, B_ex, p_ex, E_ex)
    rhs = True
    solution_known = True

if exact_initial_guess:    
    u_ex_ = project(u_ex, V, solver_parameters=lu)
    B_ex_ = project(B_ex, W, solver_parameters=lu)
    p_ex_ = project(p_ex, Q, solver_parameters=lu)
    E_ex_ = project(E_ex, R, solver_parameters=lu)
    z.sub(0).assign(u_ex_)
    z.sub(1).assign(p_ex_)
    z.sub(2).assign(B_ex_)
    z.sub(3).assign(E_ex_)

F = (
      2 * nu * inner(eps(u), eps(v))*dx
    + advect * inner(dot(grad(u), u), v) * dx
    + gamma * inner(div(u), div(v)) * dx
    + S * inner(vcross(B, E), v) * dx
    + S * inner(vcross(B, scross(u, B)), v) * dx
    - inner(p, div(v)) * dx
    - inner(div(u), q) * dx
    + inner(E, Ff) * dx
    + inner(scross(u, B), Ff) * dx
    - 1/Rem * inner(scurl(B), Ff) * dx
    + inner(E, scurl(C)) * dx
    + eta * inner(div(B), div(C)) * dx
    + gamma2 * inner(div(B), div(C)) * dx
    
    )
    
if rhs is not None:
    F -= inner(f1, v) * dx + inner(f3, Ff) * dx + inner(f2, C) * dx

if boundary_cond == "normal":
    F = F + eta * inner(div(B_ex), dot(C, n)) * ds + inner(E_ex, dot(C,t)) * ds

if stab:
    z_last_u.assign(project(u_ex, V))
    z_last_B.assign(project(B_ex, W))
    stabilisation_form_u = BurmanStab(u, v, z_last_u, mesh)
    stabilisation_form_B = BurmanStab(B, C, z_last_B, mesh)
    F += (advect * stabilisation_form_u)
    F += (advect * stabilisation_form_B)
    
if solution_known:
    if boundary_cond == "normal":
        bcs = [DirichletBC(Z.sub(0), u_ex, "on_boundary"),
           DirichletBC(Z.sub(2).sub(0), B_ex[0], 1),
           DirichletBC(Z.sub(2).sub(0), B_ex[0], 2),
           DirichletBC(Z.sub(2).sub(1), B_ex[1], 3),
           DirichletBC(Z.sub(2).sub(1), B_ex[1], 4),
           PressureFixBC(Z.sub(1), 0, 1),
           PressureFixBC(Z.sub(3), 0, 1)]
    
    elif boundary_cond == "Dirichlet":
        bcs =  [DirichletBC(Z.sub(0), u_ex, "on_boundary"),  
           DirichletBC(Z.sub(2), B_ex , "on_boundary"),
           PressureFixBC(Z.sub(1), 0, 1),
           PressureFixBC(Z.sub(3), 0, 1)]
    else:
        raise NotImplementedError("Only know normal and Dirichlet.")

if picard:
    w = TrialFunction(Z)
    [w_u, w_p, w_B, w_E] = split(w)

    J_newton = ufl.algorithms.expand_derivatives(derivative(F, z, w))
    J_picard = (
          J_newton
        #- S * inner(vcross(w_B, E), v) * dx               #J_tilde    #inner(B, E_n, v) * dx
        #- S * inner(vcross(w_B, scross(u, B)), v) * dx    #D_1_tilde  #inner(vcross(B, scross(u_n, B_n)), v) * dx
        #- S * inner(vcross(B, scross(u, w_B)), v) * dx    #D_2_tilde  #inner(vcross(B_n, scross(u_n, B)), v) * dx
        #- inner(scross(u, w_B), Ff) * dx                  #G_tilde    #inner(scross(u_n, B), Ff) * dx

        - inner(scross(w_u, B), Ff) * dx                   #G          #inner(scross(u, B_n), Ff) * dx
        #- S * inner(vcross(B, w_E), v) * dx               #J          #inner(vcross(B_n, E), v) * dx
    
        #- S * inner(vcross(B, scross(w_u, B)), v) * dx    #D        #inner(vcross(B_n, scross(u, B_n)), v) * dx
        #- advect * inner(dot(grad(u), w_u), v) * dx   # - advect * inner(dot(grad(u_n), u), v) * d
        #- advect * inner(dot(grad(w_u), u), v) * dx   # - advect * inner(dot(grad(u), u_n), v) * dx
        )
    problem = NonlinearVariationalProblem(F, z, bcs, J=J_picard)

    
    if False:
        print("S = %s" % S)
        print("Rem = %s" % Rem)
        print("Re = %s" % Re)
        print("gamma = %s" % gamma)
        print("advect = %s" % advect)
        print("u = %s" % u)
        print("B = %s" % B)
        print("p = %s" % p)
        print("sigma = %s" % sigma)
        print("v = %s" % v)
        print("C = %s" % C)
        print("q = %s" % q)
        print("tau = %s" % tau)
        print("w = %s" % w)
        print("w_u = %s" % w_u)
        print("w_B = %s" % w_B)
        print("J_newton = %s" % J_newton)
        print("J_picard = %s" % J_picard)
        exit(-1)
else: 
    problem = NonlinearVariationalProblem(F, z, bcs)
    
appctx = {"Re": char_L * char_U / nu, "gamma": gamma, "nu": 1/Re, "Rem": Rem, "gamma2": gamma2}
params = solvers[args.solver_type]
#print("args.solver_type", args.solver_type)
#import pprint
#pprint.pprint(params)

solver = NonlinearVariationalSolver(problem, solver_parameters=params, options_prefix="", appctx=appctx)
qtransfer = NullTransfer()
Etransfer = NullTransfer()
vtransfer = SVSchoeberlTransfer((1/Re, gamma), 2, hierarchy)
Btransfer = SVSchoeberlTransfer((1/Rem, gamma2), 2, hierarchy)
transfers = {
                V.ufl_element(): (vtransfer.prolong, vtransfer.restrict, inject),
                W.ufl_element(): (Btransfer.prolong, Btransfer.restrict, inject),
                Q.ufl_element(): (prolong, restrict, qtransfer.inject),
                R.ufl_element(): (prolong, restrict, Etransfer.inject),
            }
transfermanager = TransferManager(native_transfers=transfers)
solver.set_transfer_manager(transfermanager)

results = {}
start = 250
end = 10000
step = 2000
rems = [1, 10, 100, 2000, 5000]
#rems = [1000]

fac = 0.1
dis_vec = as_vector([fac * cos(y), fac * sin(x)])
dis_scalar = fac * sin(x)
dis_vec_u_ = project(dis_vec, V)
dis_vec_p_ = project(dis_scalar, Q)
dis_vec_B_ = project(dis_vec, W)
dis_vec_E_ = project(dis_scalar, R)

u_ex_dis_ = u_ex_ + dis_vec_u_
p_ex_dis_ = p_ex_ + dis_vec_p_
B_ex_dis_ = B_ex_ + dis_vec_B_
E_ex_dis_ = E_ex_ + dis_vec_E_

results = {}
start = 250
end = 10000
step = 2000
rems = [1, 10, 100, 2000, 5000, 10000]
#rems = [1000]

for rem in rems:
    (u, p, B, E) = z.split()
    u.assign(u_ex_dis_)
    p.assign(p_ex_dis_)
    B.assign(B_ex_dis_)
    E.assign(E_ex_dis_)
    
    advect.assign(1)
    nu.assign(char_L * char_U / Re)
    Rem.assign(rem)
    message(GREEN % ("Solving for Re = %s, Rem = %s, gamma = %s, gamma2 = %s ,nref=%s" % (float(Re), float(rem), float(gamma), float(gamma2), int(nref))))

    start = datetime.now()
    solver.solve()
    end = datetime.now()

    linear_its = solver.snes.getLinearSolveIterations()
    nonlinear_its = solver.snes.getIterationNumber()
    time = (end-start).total_seconds() / 60

    print(GREEN % ("Time taken: %.2f min in %d nonlinear iterations, %d linear iterations (%.2f Krylov iters per Newton step)" % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its))))
    print("%.2f @ %d @ %d @ %.2f" % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its)))

    (u, p, B, E) = z.split()

    pintegral = assemble(p*dx)
    p.assign(p - Constant(pintegral/area))

    print("||div(u)||_L^2 = %s" % sqrt(assemble(inner(div(u), div(u))*dx)))
    print("||div(B)||_L^2 = %s" % sqrt(assemble(inner(div(B), div(B))*dx)))

    B.rename("MagneticField")
    u.rename("VelocityField")
    p.rename("Pressure")
    E.rename("ElectricFieldf")

    if solution_known:
        B_ex_ = project(B_ex, B.function_space())
        u_ex_ = project(u_ex, u.function_space())
        p_ex_ = project(p_ex, p.function_space())
        E_ex_ = project(E_ex, E.function_space())
        B_ex_.rename("ExactSolutionB")
        u_ex_.rename("ExactSolutionu")
        p_ex_.rename("ExactSolutionp")
        E_ex_.rename("ExactSolutionE")

        error_u = errornorm(u_ex_, u, 'L2')
        error_B = errornorm(B_ex_, B, 'L2')
        error_E = errornorm(E_ex_, E, 'L2')
        error_p = errornorm(p_ex_, p, 'L2')

        errors=[error_u,error_p,error_B, error_E]

        print("Error ||u_ex - u||_L^2 = %s" % error_u)
        print("Error ||p_ex - p||_L^2 = %s" % error_p)
        print("Error ||B_ex - B||_L^2 = %s" % error_B)
        print("Error ||E_ex - E||_L^2 = %s" % error_E)
        File("output/mhd.pvd").write(u, u_ex_, p, p_ex_, B, B_ex_, E, E_ex_, time=float(rem))
        print("%s,%s,%s,%s" % (error_u,error_p,error_B, error_E))

        info_dict = {
            "Rem": rem,
            "krylov/nonlin": linear_its/nonlinear_its,
            "error_u": error_u,
            "error_p": error_p,
            "error_B": error_B,
            "error_E": error_E,
        }
        results[rem] = info_dict


        error_u = Function(u.function_space())
        error_u.assign(u_ex_ - u)
        error_u.rename("ErrorVelocity")
        error_B = Function(B.function_space())
        error_B.assign(B_ex_ - B)
        error_B.rename("ErrorMagneticField")
        error_p = Function(p.function_space())
        error_p.assign(p_ex_ - p)
        error_p.rename("ErrorPressure")
        #error_E.assign(E_ex_ - E)
        #error_E.rename("ErrorElectricField")
        File("output/errors.pvd").write(error_u, error_B, error_p, time=float(rem))
    else:
        #File("output/mhd.pvd").write(u, p, B, sigma, time=re_)
        File("output/mhd.pvd").write(u,  time=float(rem))

        
    z_last_u.assign(u)
    z_last_B.assign(B)

for rem in results:
        print(results[rem])


    


