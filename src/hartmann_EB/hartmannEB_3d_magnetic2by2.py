# -*- coding: utf-8 -*-
from firedrake import *
from datetime import datetime
import alfi
import argparse
import numpy
from pyop2.datatypes import IntType
import ufl.algorithms
from alfi.stabilisation import *
from alfi.transfer import *
from alfi import *
from  multiprocessing import *
import sys
import os

distribution_parameters={"partition": True, "overlap_type": (DistributedMeshOverlapType.VERTEX, 2)}

def scross(x, y):
    return x[0]*y[1] - x[1]*y[0]

def scurl(x):
    return x[1].dx(0) - x[0].dx(1)

def vcurl(x):
    return as_vector([x.dx(1), -x.dx(0)])


def BurmanStab(B, C, supg_wind, stab_weight, mesh):
    n = FacetNormal(mesh)
    h = FacetArea(mesh)
    beta = sqrt(avg(facet_avg(dot(supg_wind, n)**2)))
    gamma1 = stab_weight # as chosen in doi:10.1016/j.apnum.2007.11.001
    stabilisation_form = 0.5 * gamma1 * h**2 * beta * dot(jump(grad(B), n), jump(grad(C), n))*dS
    return stabilisation_form


class InnerSchurPC(AuxiliaryOperatorPC):
    def form(self, pc, C, B):
        mesh = C.function_space().mesh()
        n = FacetNormal(mesh)
        t = as_vector([n[1], -n[0]])
        h = CellDiameter(mesh)
        h_avg = (h('+') + h('-'))/2
        alpha = Constant(4)
        gammaa = Constant(8.0)
        
        state = self.get_appctx(pc)['state']
        [B_n, E_n] = split(state)

        K = eta * (
              + inner(grad(B), grad(C))*dx
        #      + alpha/h_avg*inner(jump(C, t), jump(B, t))*dS
             + alpha/h_avg * inner(cross(C('+'),n('+')) + cross(C('-'),n('-')),
                                   cross(B('+'),n('+')) + cross(B('-'),n('-'))) * dS
        #      + dot(avg(grad(C[0])), jump(B[0], n))*dS
        #      + dot(avg(grad(C[1])), jump(B[1], n))*dS
        #     + dot(avg(grad(C[2])), jump(B[2], n))*dS 
        #      - dot(jump(C[0], n), avg(grad(B[0])))*dS 
        #      - dot(jump(C[1], n), avg(grad(B[1])))*dS
        #      - dot(jump(C[2], n), avg(grad(B[2])))*dS
        #      + alpha/h_avg *  dot(jump(B[0], n) , jump(C[0], n)) * dS
        #      + alpha/h_avg *  dot(jump(B[1], n) , jump(C[1], n)) * dS
        #      + alpha/h_avg *  dot(jump(B[2], n) , jump(C[2], n)) * dS
        #      + dot(grad(C[0]), B[0]*n)*ds 
        #      + dot(grad(C[1]), B[1]*n)*ds
        #      + dot(grad(C[2]), B[2]*n)*ds
        #      - dot(C[0]*n, grad(B[0]))*ds 
        #      - dot(C[1]*n, grad(B[1]))*ds
        #      - dot(C[2]*n, grad(B[2]))*ds
        #      + (gammaa/h)*dot(C[0], B[0])*ds
        #      + (gammaa/h)*dot(C[1], B[1])*ds
        #      + (gammaa/h)*dot(C[2], B[2])*ds
            )


        #K = eta * (
        #   + inner(grad(C), grad(B))*dx
        #   - dot(avg(grad(C[0])), jump(B[0], n))*dS
        ##   - dot(avg(grad(C[1])), jump(B[1], n))*dS
        #   - dot(avg(grad(C[2])), jump(B[2], n))*dS
        #   - dot(jump(C[0], n), avg(grad(B[0])))*dS
        #   - dot(jump(C[1], n), avg(grad(B[1])))*dS
        #   - dot(jump(C[2], n), avg(grad(B[2])))*dS
        #   + alpha/h_avg * dot(jump(C[0], n), jump(B[0], n))*dS
        #   + alpha/h_avg * dot(jump(C[1], n), jump(B[1], n))*dS
        #   + alpha/h_avg * dot(jump(C[2], n), jump(B[2], n))*dS 
        #   - dot(grad(C[0]), B[0]*n)*ds
        #   - dot(grad(C[1]), B[1]*n)*ds
        #   - dot(grad(C[2]), B[2]*n)*ds 
        #   - dot(C[0]*n, grad(B[0]))*ds
        #   - dot(C[1]*n, grad(B[1]))*ds
        #   - dot(C[2]*n, grad(B[2]))*ds 
        #   + (gammaa/h)*dot(C[0], B[0])*ds
        #   + (gammaa/h)*dot(C[1], B[1])*ds
        #   + (gammaa/h)*dot(C[2], B[2])*ds#

 #           )

        #K = K - delta * inner(curl(cross(u_ex, B)), C)*dx

        K = K + gamma2 * inner(div(B), div(C)) * dx

        bcs = [DirichletBC(C.function_space(), 0, "on_boundary")]

        return (K, bcs)


lu = {
     "snes_type": "newtonls",
     "snes_monitor": None,
     "snes_atol": 1.0e-8,
     "snes_rtol": 1.0e-10,
     "snes_linesearch_type": "l2",
     #"snes_linesearch_monitor": None,
     "snes_linesearch_maxstep": 1,
     #"snes_converged_reason": None,
     "snes_max_it": 15,
     #"snes_convergence_test": "skip",
     "mat_type": "aij",
     "ksp_type": "preonly",
     #"ksp_monitor_true_residual": None,
     "pc_type": "lu",
     "pc_factor_mat_solver_type": "mumps",
     "mat_mumps_icntl_14": 300,
         }

fs = {
    "snes_type": "ksponly",
    "snes_monitor": None,
    "ksp_type": "fgmres",
    "ksp_max_it": 50,
    "ksp_atol": 1.0e-8,
    "ksp_rtol": 1.0e-9,
    "ksp_monitor_true_residual": None,#
    "ksp_converged_reason": None,
    "mat_type": "aij",
#    "ksp_type": "fgmres",
#    "ksp_max_it": 2,
    "pc_type": "mg",
    "pc_mg_cycle_type": "v",
    "pc_mg_type": "full",
    "mg_levels_ksp_type": "fgmres",
    #"fieldsplit_1_aux_mg_levels_ksp_richardson_scale": 1,
    "mg_levels_ksp_convergence_test": "skip",
    "mg_levels_ksp_max_it": 20,
    "mg_levels_ksp_norm_type": "unpreconditioned",
    #"mg_levels_ksp_monitor_true_residual": None,
    "mg_levels_pc_type": "python",
    "mg_levels_pc_python_type": "firedrake.PatchPC",
    "mg_levels_patch_pc_patch_save_operators": True,
    "mg_levels_patch_pc_patch_partition_of_unity": False,
    "mg_levels_patch_pc_patch_sub_mat_type": "seqaij",
    "mg_levels_patch_pc_patch_construct_dim": 0,
    "mg_levels_patch_pc_patch_construct_type": "python",
    "mg_levels_patch_pc_patch_construct_python_type": "alfi.MacroStar",
    "mg_levels_patch_sub_ksp_type": "preonly",
    "mg_levels_patch_sub_pc_type": "lu",
    "mg_levels_patch_sub_pc_factor_mat_solver_type": "umfpack",
    "mg_coarse_ksp_type": "richardson",
    "mg_coarse_ksp_max_it": 1,
    "mg_coarse_ksp_norm_type": "unpreconditioned",
    #"mg_coarse_ksp_monitor_true_residual": None,
    "mg_coarse_pc_type": "python",
    "mg_coarse_pc_python_type": "firedrake.AssembledPC",
    "mg_coarse_assembled": {
          "mat_type": "aij",
          "pc_type": "telescope",
          "pc_telescope_reduction_factor": 1,
          "pc_telescope_subcomm_type": "contiguous",
          "telescope_pc_type": "lu",
          "telescope_pc_factor_mat_solver_type": "mumps",
          "telescope_pc_factor_mat_mumps_icntl_14": 200,
      }
        }
vanka_add_gmres2 = {
   "mat_type": "matfree",
   "snes_type": "ksponly",
   #"ksp_view": None,
   "ksp_type": "fgmres",
   "ksp_pc_side": "right",
   "ksp_monitor_true_residual": None,
   "ksp_converged_reason": None,
   "ksp_atol": 1.0e-08,
   "ksp_rtol": 1.0e-09,
   "ksp_max_it": 400,
   "pc_type": "mg",
   "pc_mg_log": None,
   "pc_mg_cycle_type": "v",
   "pc_mg_type": "multiplicative",
   "mg_levels_ksp_type": "gmres",
   "mg_levels_ksp_max_it": 2,
   "mg_levels_ksp_convergence_test": "skip",
   "mg_levels_pc_type": "python",
   "mg_levels_pc_python_type": "firedrake.PatchPC",
   "mg_levels_patch_pc_patch_save_operators": True,
   "mg_levels_patch_pc_patch_partition_of_unity": False,
   "mg_levels_patch_pc_patch_sub_mat_type": "seqdense",
   "mg_levels_patch_pc_patch_construct_dim": 0,
   "mg_levels_patch_pc_patch_construct_type": "vanka",
   "mg_levels_patch_pc_patch_exclude_subspaces": "2,3",
   "mg_levels_patch_pc_patch_local_type": "additive",
   "mg_levels_patch_pc_patch_precompute_element_tensors": True,
   "mg_levels_patch_pc_patch_symmetrise_sweep": False,
   "mg_levels_patch_sub_ksp_type": "preonly",
   "mg_levels_patch_sub_pc_type": "lu",
   "mg_levels_patch_sub_pc_factor_shift_type": "nonzero",
   "mg_coarse_pc_type": "python",
   "mg_coarse_pc_python_type": "firedrake.AssembledPC",
   "mg_coarse_assembled_pc_type": "lu",
   "mg_coarse_assembled_pc_factor_mat_solver_type": "mumps",
   "mg_coarse_assembled_ mat_mumps_icntl_14": 200,
             }


vanka_mul_gmres2 = {
   "mat_type": "matfree",
   "snes_type": "ksponly",
   "snes_monitor": None,
   "snes_max_linear_solve_fail": 4,
   "ksp_type": "fgmres",
   "ksp_monitor_true_residual": None,
   "ksp_converged_reason": None,
   #"ksp_view": True,
   "ksp_atol": 1.0e-08,
   "ksp_rtol": 1.0e-09,
   "ksp_max_it": 400,
   "pc_type": "mg",
   "pc_mg_log": None,
   "pc_mg_cycle_type": "v",
   "pc_mg_type": "multiplicative",
   "mg_levels_ksp_type": "gmres",
   "mg_levels_ksp_convergence_test": "skip",
   "mg_levels_ksp_max_it": 2,
   "mg_levels_pc_type": "python",
   "mg_levels_pc_python_type": "firedrake.PatchPC",
   "mg_levels_patch_pc_patch_save_operators": True,
   "mg_levels_patch_pc_patch_partition_of_unity": True,
   "mg_levels_patch_pc_patch_sub_mat_type": "seqdense",
   "mg_levels_patch_pc_patch_construct_dim": 0,
   "mg_levels_patch_pc_patch_construct_type": "vanka",
   "mg_levels_patch_pc_patch_exclude_subspaces": "2,3",
   "mg_levels_patch_pc_patch_local_type": "multiplicative",
   "mg_levels_patch_pc_patch_precompute_element_tensors": True,
   "mg_levels_patch_pc_patch_symmetrise_sweep": False,
   "mg_levels_patch_sub_ksp_type": "preonly",
   "mg_levels_patch_sub_pc_type": "lu",
   "mg_levels_patch_sub_pc_factor_shift_type": "nonzero",
   "mg_coarse_pc_type": "python",
   "mg_coarse_pc_python_type": "firedrake.AssembledPC",
   "mg_coarse_assembled_pc_type": "lu",
   "mg_coarse_assembled_pc_factor_mat_solver_type": "mumps",
   "mg_coarse_assembled_mat_mumps_icntl_14": 200,
             }

mfsstar2 = {
   "snes_type": "ksponly",
   "snes_monitor": None,
   "ksp_type": "fgmres",
   "ksp_max_it": 150,
   "ksp_atol": 1.0e-8,
   "ksp_rtol": 1.0e-13,
   "ksp_monitor_true_residual": None,#
   "ksp_converged_reason": None,
   "mat_type": "aij",
#    "ksp_type": "fgmres",
#    "ksp_max_it": 2,
   "ksp_type": "fgmres",
   #"ksp_monitor_true_residual": None,
   "pc_type": "fieldsplit",
   "pc_fieldsplit_type": "schur",
   "pc_fieldsplit_schur_fact_type": "full",
   "pc_fieldsplit_0_fields": "1",
   "pc_fieldsplit_1_fields": "0",
   "fieldsplit_0_ksp_type": "gmres", 
   #"fieldsplit_0_ksp_max_it": 1,
   #"fieldsplit_0_ksp_convergence_test": "skip",
   "fieldsplit_0_pc_type": "lu", ###jacobi
   #"fieldsplit_0_pc_factor_mat_solver_type": "mumps", 
   "fieldsplit_1_ksp_type": "gmres",
   "fieldsplit_1_ksp_max_it": 1,
   "fieldsplit_1_ksp_norm_type": "unpreconditioned",
   "fieldsplit_1_pc_type": "python",
   "fieldsplit_1_pc_python_type": "__main__.InnerSchurPC",
   "fieldsplit_1_aux_pc_type": "lu", #### mg
   "fieldsplit_1_aux_mg_transfer_manager": __name__ + ".transfermanager",
   "fieldsplit_1_aux_pc_mg_cycle_type": "v",
   "fieldsplit_1_aux_pc_mg_type": "full",
   "fieldsplit_1_aux_mg_levels_ksp_type": "fgmres",
   "fieldsplit_1_aux_mg_levels_ksp_convergence_test": "skip",
   "fieldsplit_1_aux_mg_levels_ksp_max_it": 6,
   "fieldsplit_1_aux_mg_levels_ksp_norm_type": "unpreconditioned",
   #"fieldsplit_1_aux_mg_levels_ksp_monitor_true_residual": None,
   "fieldsplit_1_aux_mg_levels_pc_type": "python",
   "fieldsplit_1_aux_mg_levels_pc_python_type": "firedrake.PatchPC",
   "fieldsplit_1_aux_mg_levels_patch_pc_patch_save_operators": True,
   "fieldsplit_1_aux_mg_levels_patch_pc_patch_partition_of_unity": False,
   "fieldsplit_1_aux_mg_levels_patch_pc_patch_sub_mat_type": "seqaij",
   "fieldsplit_1_aux_mg_levels_patch_pc_patch_construct_dim": 0,
   "fieldsplit_1_aux_mg_levels_patch_pc_patch_construct_type": "python",
   "fieldsplit_1_aux_mg_levels_patch_pc_patch_construct_python_type": "alfi.MacroStar",
   "fieldsplit_1_aux_mg_levels_patch_sub_ksp_type": "preonly",
   "fieldsplit_1_aux_mg_levels_patch_sub_pc_type": "lu",
   "fieldsplit_1_aux_mg_levels_patch_sub_pc_factor_mat_solver_type": "petsc",
   "fieldsplit_1_aux_mg_coarse_pc_type": "python",
   "fieldsplit_1_aux_mg_coarse_pc_python_type": "firedrake.AssembledPC",
   "fieldsplit_1_aux_mg_coarse_assembled": {
         "mat_type": "aij",
         "pc_type": "telescope",
         "pc_telescope_reduction_factor": 1,
         "pc_telescope_subcomm_type": "contiguous",
         "telescope_pc_type": "lu",
         "telescope_pc_factor_mat_solver_type": "mumps",
         "telescope_pc_factor_mat_solver_type": "mumps", 
     }
       }

self = {
   "snes_type": "ksponly",
   "snes_monitor": None,
   "ksp_type": "fgmres",
   "ksp_max_it": 150,
   "ksp_atol": 1.0e-8,
   "ksp_rtol": 1.0e-9,
   "ksp_monitor_true_residual": None,#
   "ksp_converged_reason": None,
   "mat_type": "aij",
#    "ksp_type": "fgmres",
#    "ksp_max_it": 2,
   "ksp_type": "fgmres",
   #"ksp_monitor_true_residual": None,
   "pc_type": "fieldsplit",
   "pc_fieldsplit_type": "schur",
   "pc_fieldsplit_schur_fact_type": "full",
   "pc_fieldsplit_0_fields": "1",
   "pc_fieldsplit_1_fields": "0",
   "fieldsplit_0_ksp_type": "gmres", 
   #"fieldsplit_0_ksp_max_it": 1,
   #"fieldsplit_0_ksp_convergence_test": "skip",
   "fieldsplit_0_pc_type": "lu", ###jacobi
   #"fieldsplit_0_pc_factor_mat_solver_type": "mumps", 
   "fieldsplit_1_ksp_type": "gmres",
   "fieldsplit_1_ksp_max_it": 1,
   "fieldsplit_1_ksp_norm_type": "unpreconditioned",
   "pc_fieldsplit_schur_precondition": "selfp",
   #"fieldsplit_1_mat_schur_complement_ainv_type": "lump", 
   "fieldsplit_1_pc_type": "hypre"
       }

selfpp= {
    "snes_type": "ksponly",
    "snes_monitor": None,
    "ksp_type": "gmres",
    "ksp_max_it": 100,
    "ksp_monitor_true_residual": None,
    "ksp_rtol": 1e-8,
    "pc_type": "fieldsplit",
    "pc_fieldsplit_type": "schur",
    "pc_fieldsplit_schur_fact_type": "full",
    "pc_fieldsplit_0_fields": "1",
    "pc_fieldsplit_1_fields": "0",
    "fieldsplit_0_ksp_type": "preonly",
    "fieldsplit_0_pc_type": "lu",
    "fieldsplit_0_pc_factor_mat_solver_type": "mumps", 
    "fieldsplit_1_ksp_type": "preonly",
    "fieldsplit_1_ksp_max_it": 1,
    "pc_fieldsplit_schur_precondition": "selfp",
    "fieldsplit_1_pc_type": "lu",
    "fieldsplit_1_pc_factor_mat_solver_type": "mumps", 
}

selfp= {
    "snes_type": "ksponly",
    "snes_monitor": None,
    "ksp_type": "gmres",
    "ksp_max_it": 100,
    "ksp_monitor_true_residual": None,
    "ksp_rtol": 1e-8,
    "pc_type": "fieldsplit",
    "pc_fieldsplit_type": "schur",
    "pc_fieldsplit_schur_fact_type": "full",
    "pc_fieldsplit_0_fields": "1",
    "pc_fieldsplit_1_fields": "0",
    "fieldsplit_0_ksp_type": "preonly",
    "fieldsplit_0_pc_type": "lu",
    "fieldsplit_0_pc_factor_mat_solver_type": "mumps", 
    "fieldsplit_1_ksp_type": "preonly",
    "fieldsplit_1_ksp_max_it": 1,
    "pc_fieldsplit_schur_precondition": "selfp",
    "fieldsplit_1_pc_type": "mg",
    "fieldsplit_1_pc_mg_cycle_type": "v",
    "fieldsplit_1_pc_mg_type": "full",
    "fieldsplit_1_mg_levels_ksp_type": "fgmres",
    "fieldsplit_1_mg_levels_ksp_convergence_test": "skip",
    "fieldsplit_1_mg_levels_ksp_max_it": 6,
    "fieldsplit_1_mg_levels_ksp_norm_type": "unpreconditioned",
    #"fieldsplit_1_mg_levels_ksp_monitor_true_residual": None,
    "fieldsplit_1_mg_levels_pc_type": "python",
    "fieldsplit_1_mg_levels_pc_python_type": "firedrake.PatchPC",
    "fieldsplit_1_mg_levels_patch_pc_patch_save_operators": True,
    "fieldsplit_1_mg_levels_patch_pc_patch_partition_of_unity": False,
    "fieldsplit_1_mg_levels_patch_pc_patch_sub_mat_type": "seqaij",
    "fieldsplit_1_mg_levels_patch_pc_patch_construct_dim": 0,
    "fieldsplit_1_mg_levels_patch_pc_patch_construct_type": "python",
    "fieldsplit_1_mg_levels_patch_pc_patch_construct_python_type": "alfi.MacroStar",
    "fieldsplit_1_mg_levels_patch_sub_ksp_type": "preonly",
    "fieldsplit_1_mg_levels_patch_sub_pc_type": "lu",
    "fieldsplit_1_mg_levels_patch_sub_pc_factor_mat_solver_type": "umfpack",
    "fieldsplit_1_mg_coarse_pc_type": "python",
    "fieldsplit_1_mg_coarse_pc_python_type": "firedrake.AssembledPC",
    "fieldsplit_1_mg_coarse_assembled": {
          "mat_type": "aij",
          "pc_type": "telescope",
          "pc_telescope_reduction_factor": 1,
          "pc_telescope_subcomm_type": "contiguous",
          "telescope_pc_type": "lu",
          "telescope_pc_factor_mat_solver_type": "mumps",
      }
        }


class Block0(AuxiliaryOperatorPC):  #B - N1div
    def form(self, pc, C, B):
        K =  1/Rem * inner(B, C) * dx + 1/Rem *  inner(div(B), div(C))*dx 
        bcs = [DirichletBC(C.function_space(), 0, "on_boundary")]
        return (K, bcs)    

class Block1(AuxiliaryOperatorPC):  #E - N1curl
    def form(self, pc, Ff, E):
        K =   inner(E, Ff) * dx + 1/Rem * inner(curl(E), curl(Ff))*dx 
        bcs = [DirichletBC(Ff.function_space(), 0, "on_boundary")]
        return (K, bcs) 

addit = {
   "mat_type": "aij",
   "snes_type": "ksponly",
   "snes_monitor": None,
   "snes_atol": 0,
   "snes_rtol": 1.0e-9,
   "ksp_type": "gmres",
   "ksp_atol": 0,
   "ksp_rtol": 1.0e-10,
   "ksp_monitor_true_residual": None,
   "pc_type": "fieldsplit",
   "pc_fieldsplit_type": "additive",
   "fieldsplit_0_ksp_type": "preonly",
   "fieldsplit_0_pc_type": "python",
   "fieldsplit_0_pc_python_type": "__main__.Block0",
   "fieldsplit_0_aux_pc_type": "lu",
   "fieldsplit_1_ksp_type": "preonly",
   "fieldsplit_1_pc_type": "python",
   "fieldsplit_1_pc_python_type": "__main__.Block1",
   "fieldsplit_1_aux_pc_type": "lu",
}

solvers = {"lu": lu, "fs": fs, "vanka_add_gmres2": vanka_add_gmres2, "vanka_mul_gmres2": vanka_mul_gmres2, "mfsstar2": mfsstar2,
           "selfpp": selfpp, "addit": addit}

parser = argparse.ArgumentParser(add_help=False)
parser.add_argument("--baseN", type=int, default=10)
parser.add_argument("--k", type=int, default=3)
parser.add_argument("--dim", type=int, choices=[2,3], required=True)
parser.add_argument("--nref", type=int, default=1)
parser.add_argument("--Rem", nargs='+', type=float, default=[1])
parser.add_argument("--gamma2", type=float, default=0)
parser.add_argument("--eta", type=float, default=0)
parser.add_argument("--delta", type=float, default=1)
parser.add_argument("--hierarchy", choices=["bary", "uniform"], default="bary")
parser.add_argument("--solver-type", choices=list(solvers.keys()), default="lu")
parser.add_argument("--stab", default=False, action="store_true")

args, _ = parser.parse_known_args()
baseN = args.baseN
k = args.k
nref = args.nref
Rem = Constant(args.Rem[0])
gamma2 = Constant(args.gamma2)
hierarchy = args.hierarchy
solver_type = args.solver_type
gamma2 = Constant(args.gamma2)
eta = Constant(args.eta)
delta = Constant(args.delta)
stab = args.stab
dim = args.dim

stab_weight = Constant(5e-3)

eta = 1/Rem

#if k<3:
#    raise ValueError("Scott Vogelius is not stable for k<3")

if dim == 2:
    base = UnitSquareMesh(baseN, baseN, distribution_parameters=distribution_parameters)
elif dim == 3:
    base = UnitCubeMesh(baseN, baseN, baseN, distribution_parameters=distribution_parameters)

def before(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+1)

def after(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+2)
    #rebalance(dm, i)

if hierarchy == "bary":
    mh = alfi.BaryMeshHierarchy(base, nref, callbacks=(before, after))
elif hierarchy == "uniformbary":
    bmesh = Mesh(bary(base._plex), distribution_parameters={"partition": False})
    mh = MeshHierarchy(bmesh, nref, reorder=True, callbacks=(before, after),
                       distribution_parameters=distribution_parameters)
elif hierarchy == "uniform":
    mh = MeshHierarchy(base, nref, reorder=True, callbacks=(before, after),
             distribution_parameters=distribution_parameters)
else:
    raise NotImplementedError("Only know bary, uniformbary and uniform for the hierarchy.")
if dim == 2:
    for m in mh:
        m.coordinates.dat.data[:, 0] -= 0.5
        m.coordinates.dat.data[:, 1] -= 0.5
elif dim == 3:
    for m in mh:
        m.coordinates.dat.data[:, 0] -= 0.5
        m.coordinates.dat.data[:, 1] -= 0.5
        m.coordinates.dat.data[:, 2] -= 0.5
mesh = mh[-1]

area = assemble(Constant(1, domain=mh[0])*dx)

def message(msg):
    if mesh.comm.rank == 0:
        warning(msg)

if dim == 2:
    R = FunctionSpace(mesh, "CG", k-1)    #E
    W = FunctionSpace(mesh, "RT", k-1)        #B
elif dim == 3:
    R = FunctionSpace(mesh, "N1curl", k-1)    #E
    W = FunctionSpace(mesh, "N1div", k-1)        #B
    #R = VectorFunctionSpace(mesh, "DG", k-2)    #E
    #W = VectorFunctionSpace(mesh, "CG", k-1)        #B

Z = MixedFunctionSpace([W, R])

z = Function(Z)
(B, E) = split(z)
(C, Ff)   = split(TestFunction(Z))
z_last_B = Function(W)

if dim == 2:
    (x, y) = SpatialCoordinate(Z.mesh())
    u_ex = as_vector([sin(x)*y, x*x])
    B_ex = as_vector([sin(y), x])
    E_ex = sin(x) # as_vector([sin(x)*y, y*sin(zz), sin(x)])
    E_ex_ = interpolate(E_ex, R)
elif dim == 3:
    (x, y, zz) = SpatialCoordinate(Z.mesh())
    u_ex = as_vector([sin(zz)*y, x*zz, y*x])
    B_ex = as_vector([sin(y),zz,x])
    E_ex = grad(sin(x)*zz) # as_vector([sin(x)*y, y*sin(zz), sin(x)])
    E_ex_ = interpolate(E_ex, R)

eps = lambda x: sym(grad(x))

if dim == 2:
    F = (
        eta * inner(div(B), div(C)) * dx
        + gamma2 * inner(div(B), div(C)) * dx
        + inner(vcurl(E), C) * dx
        - 1/Rem * inner(B, vcurl(Ff)) * dx
        + delta * inner(scross(u_ex, B), Ff) * dx
        + inner(E, Ff) * dx
        )

    f1 = vcurl(E_ex) - eta * grad(div(B_ex)) - gamma2 * grad(div(B_ex))
    #f1 = interpolate(f1, W)
    f2 = -1/Rem * scurl(B_ex) + E_ex + delta * scross(u_ex, B_ex)

elif dim == 3:
    F = (
        + inner(E, Ff) * dx
        + delta * inner(cross(u_ex, B), Ff) * dx
        - 1/Rem * inner(B, curl(Ff)) * dx
        + inner(curl(E), (C)) * dx
        + eta * inner(div(B), div(C)) * dx
        + gamma2 * inner(div(B), div(C)) * dx
        )

    f1 = curl(E_ex) - eta  * grad(div(B_ex)) - gamma2 * grad(div(B_ex))
    #f1 = interpolate(f1, W)
    f2 = -1/Rem * curl(B_ex) + E_ex + delta * cross(u_ex, B_ex)

F -= inner(f1, C) * dx + inner(f2, Ff) * dx

if stab:
    stabilisation_form_B = BurmanStab(B, C, B_ex, stab_weight, mesh)
    F += stabilisation_form_B

                    
bcs = [DirichletBC(Z.sub(1), E_ex, "on_boundary"),
       DirichletBC(Z.sub(0), B_ex , "on_boundary")]
#           DirichletBC(Z.sub(2), 0, ((1,3), ), method="geometric")] # Doesn't work for DG pressures, does for CG

problem = NonlinearVariationalProblem(F, z, bcs)
 
appctx = {"Rem": Rem, "gamma2": gamma2}
params = solvers[args.solver_type]
#print("args.solver_type", args.solver_type)
#import pprint
#pprint.pprint(params)

solver = NonlinearVariationalSolver(problem, solver_parameters=params, options_prefix="", appctx=appctx)
qtransfer = NullTransfer()
Etransfer = NullTransfer()
V = VectorFunctionSpace(mesh, "CG", k)  #u
Q = FunctionSpace(mesh, "DG", k-1)
vtransfer = SVSchoeberlTransfer((1/Rem, gamma2), 2, hierarchy)
Btransfer = SVSchoeberlTransfer((1/Rem, gamma2), 2, hierarchy)
dgtransfer = DGInjection()
def traceinject(src, dest):
    out = inject(src, dest)
    with dest.dat.vec_ro as x:
        print("Injecting %s -> %s: result has norm %s" % (src, dest, x.norm()))
    import sys; sys.stdout.flush()
    return out

dg = {VectorElement("DG", mesh.ufl_cell(), args.k-1): (dgtransfer.prolong, restrict, dgtransfer.inject)}
secrettm = TransferManager(native_transfers=dg)
def myprolong(src, dest):
    out = secrettm.prolong(src, dest)

    family = src.function_space().ufl_element().family()
    if family == "Raviart-Thomas":
        op = div
        family = "RT"
    elif "Nedelec" in family:
        op = curl
        family = "ND"
    else:
        raise NotImplementedError
    energy = lambda u: sqrt(assemble(inner(u, u)*dx + inner(op(u), op(u))*dx))
    Esrc = energy(src)
    Edest = energy(dest)
    ratio = Edest/Esrc
    if mesh.comm.rank == 0:
        print("%s: E(src) = %s | E(dest) = %s | ratio = %s" % (family, Esrc, Edest, ratio), flush=True)
    # Calculate energy norm before and after here
    return out


transfers = {
                #V.ufl_element(): (vtransfer.prolong, vtransfer.restrict, inject),
                W.ufl_element(): (myprolong, secrettm.restrict, secrettm.inject),
                #Q.ufl_element(): (prolong, restrict, qtransfer.inject),
                R.ufl_element(): (myprolong, secrettm.restrict, secrettm.inject),
                VectorElement("DG", mesh.ufl_cell(), args.k-1): (dgtransfer.prolong, restrict, dgtransfer.inject),
            }
transfermanager = TransferManager(native_transfers=transfers)
solver.set_transfer_manager(transfermanager)

for rem in args.Rem:
    Rem.assign(rem)
    (B, E) = z.split()
    B.assign(project(Constant([0,0,0]), B.function_space()))
    E.assign(project(Constant([0,0,0]), E.function_space()))

    message(GREEN % ("Solving for #dofs = %s , Rem = %s, gamma2 = %s ,nref=%s, baseN=%s"
                     % (Z.dim(), float(Rem), float(gamma2), int(nref), int(baseN))))

    start = datetime.now()
    solver.solve()
    end = datetime.now()


    linear_its = solver.snes.getLinearSolveIterations()
    nonlinear_its = solver.snes.getIterationNumber()
    time = (end-start).total_seconds() / 60

    if mesh.comm.rank == 0:
        print(GREEN % ("Time taken: %.2f min in %d nonlinear iterations, %d linear iterations (%.2f Krylov iters per Newton step)"
                       % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its))))
        print("%.2f @ %d @ %d @ %.2f" % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its)))

    (B, E) = z.split()

    B.rename("MagneticField")
    E.rename("ElectricFieldf")

    error_B = errornorm(B_ex, B, 'L2')
    error_E = errornorm(E_ex, E, 'L2')

    norm_div_B = sqrt(assemble(inner(div(B), div(B))*dx))

    if mesh.comm.rank == 0:
        print("||div(B)||_L^2 = %s" % norm_div_B)
        print("Error ||B_ex - B||_L^2 = %s" % error_B)
        print("Error ||E_ex - E||_L^2 = %s" % error_E)

    info_dict = {
        "Rem": float(Rem),
        "krylov/nonlin": linear_its/nonlinear_its,
        "nonlinear_iter": nonlinear_its,
        "error_B": error_B,
        "error_E": error_E,
    }

    message(BLUE % info_dict)
