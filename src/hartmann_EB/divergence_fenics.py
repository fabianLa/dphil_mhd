from dolfin import *
import argparse
from datetime import datetime
import numpy

def scross(x, y):
    return x[0]*y[1] - x[1]*y[0]

def scurl(x):
    return x[1].dx(0) - x[0].dx(1)

def vcurl(x):
    return as_vector([x.dx(1), -x.dx(0)])



parser = argparse.ArgumentParser(add_help=False)
parser.add_argument("--k", type=int, default=2)
parser.add_argument("--dim", type=int, required=True)
parser.add_argument("--nref", type=int, default=0)
parser.add_argument("--baseN", type=int, default=10)
parser.add_argument("--Rem", type=float, default=1)
parser.add_argument("--delta", type=float, default=0)
args, _ = parser.parse_known_args()

Rem = Constant(args.Rem)
k = Constant(args.k)
Re = Constant(1)
nref = args.nref
dim = args.dim
delta = Constant(args.delta)

eta = 1/Rem

assert dim in [2, 3]

if dim == 2:
    mesh = UnitSquareMesh(args.baseN*2**nref, args.baseN*2**nref)
else:
    mesh = UnitCubeMesh(args.baseN*2**nref, args.baseN*2**nref, args.baseN*2**nref)
   
if dim == 2:
    (x, y) = SpatialCoordinate(mesh)
    u_ex = Expression(("sin(x[1])","cos(x[0])"), degree=4, domain=mesh)
else:
    (x, y, zz) = SpatialCoordinate(mesh)
    u_ex = Expression(("sin(x[2])","cos(x[0])","exp(x[1])"), degree=4, domain=mesh)

if dim == 2:
    W = FiniteElement("RT", mesh.ufl_cell(), args.k)    #B
    R = FiniteElement("CG", mesh.ufl_cell(), args.k)    #E
else:
    W = FiniteElement("N2div", mesh.ufl_cell(), args.k)    #B
    R = FiniteElement("N2curl", mesh.ufl_cell(), args.k)    #E

Z = R * W
Z = FunctionSpace(mesh, Z)


if dim == 2:
   B_ex = Expression(("cos(x[1])","sin(x[0])"), degree=4, domain=mesh)
   #E_ex = Expression("sin(x[0])", degree=4, domain=mesh)
   E_ex = Expression("0 * x[0]", degree=4, domain=mesh)

   f1 = vcurl(E_ex) - eta * grad(div(B_ex))
   #f1 = Expression(("0","-cos(x[0])"), degree=4, domain=mesh)
   f1 = project(f1, FunctionSpace(mesh, W))
   f2 = -1/Rem * scurl(B_ex) + E_ex + delta * scross(u_ex, B_ex)
   
else:
   B_ex = Expression(("sin(x[2])","sin(x[0])", "sin(x[1])"), degree=2, domain=mesh)
   #E_ex = Expression(("exp(x[2])","cos(x[0])", "cos(x[1])"), degree=4, domain=mesh)
   #E_pot = Expression("sin(x[1])*cos(x[0]))", degree=4, domain=mesh)
   E_ex = Expression(("sin(x[0])","cos(x[1])", "sin(x[2])"), degree=2, domain=mesh)
   #E_ex = Expression(("cos(x[2])","sin(x[0])", "sin(x[1])"), degree=4, domain=mesh)
   

   f1 = curl(E_ex) - eta * grad(div(B_ex))
   f2 = -1/Rem * curl(B_ex) + E_ex + delta * cross(u_ex, B_ex)
   
(E, B) = TrialFunctions(Z)
(Ff, C) = TestFunctions(Z)


if dim == 2:
    F = (
      eta * inner(div(B), div(C)) * dx
    + inner(vcurl(E), C) * dx
    - 1/Rem * inner(B, vcurl(Ff)) * dx
    + delta * inner(scross(u_ex, B), Ff) * dx
    + inner(E, Ff) * dx
    )
else:
    F = (
      eta * inner(div(B), div(C)) * dx
    + inner(curl(E), C) * dx
    - 1/Rem * inner(B, curl(Ff)) * dx
    + delta * inner(cross(u_ex, B), Ff) * dx
    + inner(E, Ff) * dx
    )

L = (
    inner(f1, C) * dx
  + inner(f2, Ff) * dx
    )


bcs = [DirichletBC(Z.sub(1), B_ex, "on_boundary"),
           DirichletBC(Z.sub(0), E_ex, "on_boundary"),
    ]

z = Function(Z)

solve( F == L, z, bcs)

(E, B) = z.split()

if dim == 2:
   curl_E= vcurl(E)
   curl_E_ex = vcurl(E_ex)
else:
   curl_E= curl(E)
   curl_E_ex = curl(E_ex)


print("||div(B)||_L^2 = %s" % sqrt(assemble(inner(div(B), div(B))*dx)))
print("||div(B_ex)||_L^2 = %s" % sqrt(assemble(inner(div(B_ex), div(B_ex))*dx)))
print("||curl(E)||_L^2 = %s" % sqrt(assemble(inner(curl_E, curl_E)*dx)))
print("||curl(E_ex)||_L^2 = %s" % sqrt(assemble(inner(curl_E_ex, curl_E_ex)*dx)))

print("")
error_B_l2 = errornorm(B_ex, B, 'L2')
error_E_l2 = errornorm(E_ex, E, 'L2')
error_B_hdiv = errornorm(B_ex, B, 'hdiv')
error_E_hcurl = errornorm(E_ex, E, 'hcurl')

print("Error ||B_ex - B||_L^2 = %s" % error_B_l2)
print("Error ||B_ex - B||_Hdiv = %s" % error_B_hdiv)
print("Error ||E_ex - E||_L^2 = %s" % error_E_l2)
print("Error ||E_ex - E||_Hcurl = %s" % error_E_hcurl)
   



