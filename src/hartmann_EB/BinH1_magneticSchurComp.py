# -*- coding: utf-8 -*-
from firedrake import *
from datetime import datetime
import alfi
import argparse
import numpy
from pyop2.datatypes import IntType
import ufl.algorithms

distribution_parameters={"partition": True, "overlap_type": (DistributedMeshOverlapType.VERTEX, 1)}

def scross(x, y):
    return x[0]*y[1] - x[1]*y[0]

def vcross(x,y):
    return as_vector([x[1]*y,-x[0]*y])

def scurl(x):
    return x[1].dx(0) - x[0].dx(1)

def vcurl(x):
    return as_vector([x.dx(1), -x.dx(0)])

def acurl(x):
    return as_vector([
                     x[2].dx(1),
                     -x[2].dx(0),
                     x[1].dx(0) - x[0].dx(1)
                     ])



lu = {
   "mat_type": "aij",
   "snes_type": "newtonls",
   "snes_monitor": None,
   "ksp_type": "gmres",
   "ksp_monitor_true_residual": None,
   "pc_type": "lu",
   "pc_factor_mat_solver_type": "mumps",
   "mat_mumps_icntl_14": 200,
     }

mg = {
   "mat_type": "aij",
   "snes_type": "ksponly",
   "snes_monitor": None,
   "snes_atol": 0,
   "snes_rtol": 1.0e-9,
   "ksp_type": "gmres",
   "ksp_norm_type": "unpreconditioned",
   "ksp_monitor_true_residual": None,
   "ksp_atol": 0,
   "ksp_rtol": 1.0e-10,
   "pc_type": "mg",
   "pc_mg_type": "full",
   "mg_levels_ksp_type": "chebyshev",
   "mg_levels_ksp_max_it": 2,
   "mg_levels_pc_type": "jacobi"
}

star = {
   "mat_type": "aij",
   "snes_type": "ksponly",
   "snes_monitor": None,
   "snes_atol": 0,
   "snes_rtol": 1.0e-9,
   "ksp_type": "richardson",
   "ksp_norm_type": "unpreconditioned",
   "ksp_monitor_true_residual": None,
   "ksp_atol": 0,
   "ksp_rtol": 1.0e-10,
   "pc_type": "mg",
   "pc_mg_cycle_type": "v",
   "pc_mg_type": "full",
   "mg_levels_ksp_type": "richardson",
   "mg_levels_ksp_richardson_scale": 1,
   "mg_levels_ksp_convergence_test": "skip",
   "mg_levels_ksp_max_it": 1,
   "mg_levels_pc_type": "python",
   "mg_levels_pc_python_type": "firedrake.PatchPC",
   "mg_levels_patch_pc_patch_save_operators": True,
   "mg_levels_patch_pc_patch_partition_of_unity": True,
   "mg_levels_patch_pc_patch_sub_mat_type": "seqdense",
   "mg_levels_patch_pc_patch_construct_dim": 0,
   "mg_levels_patch_pc_patch_construct_type": "star",
   "mg_levels_patch_sub_ksp_type": "preonly",
   "mg_levels_patch_sub_pc_type": "lu",
       }


solvers = {"lu": lu, "star": star, "mg": mg}

parser = argparse.ArgumentParser(add_help=False)
parser.add_argument("--baseN", type=int, default=10)
parser.add_argument("--k", type=int, default=2)
parser.add_argument("--nref", type=int, default=1)
parser.add_argument("--Rem", type=float, default=1)
parser.add_argument("--Re", type=float, default=1)
parser.add_argument("--delta", type=float, default=1)
parser.add_argument("--hierarchy", choices=["bary", "uniform"], default="bary")
parser.add_argument("--boundary-cond", choices=["normal", "Dirichlet"], default="Dirichlet")
parser.add_argument("--solver-type", choices=list(solvers.keys()), default="lu")
parser.add_argument("--testproblem", choices=["ldc", "hartmann", "Wathen", "hartmann2", "hartmann3"], default = "hartmann")

args, _ = parser.parse_known_args()
baseN = args.baseN
k = args.k
nref = args.nref
Rem = Constant(args.Rem)
Re = Constant(args.Re)
hierarchy = args.hierarchy
solver_type = args.solver_type
testproblem = args.testproblem
gamma2 = 1/args.Rem 
delta = Constant(args.delta)
boundary_cond=args.boundary_cond

eta = 1/args.Rem

base = UnitSquareMesh(baseN, baseN, diagonal="crossed", distribution_parameters=distribution_parameters)

def before(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+1)

def after(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+2)
    #rebalance(dm, i)

if hierarchy == "bary":
    mh = alfi.BaryMeshHierarchy(base, nref, callbacks=(before, after))
elif hierarchy == "uniformbary":
    bmesh = Mesh(bary(base._plex), distribution_parameters={"partition": False})
    mh = MeshHierarchy(bmesh, nref, reorder=True, callbacks=(before, after),
                       distribution_parameters=distribution_parameters)
elif hierarchy == "uniform":
    mh = MeshHierarchy(base, nref, reorder=True, callbacks=(before, after),
             distribution_parameters=distribution_parameters)
else:
    raise NotImplementedError("Only know bary, uniformbary and uniform for the hierarchy.")
for m in mh:
    m.coordinates.dat.data[:, 0] -= 0.5
    m.coordinates.dat.data[:, 1] -= 0.5
mesh = mh[-1]

area = assemble(Constant(1, domain=mh[0])*dx)

def message(msg):
    if mesh.comm.rank == 0:
        warning(msg)


W = VectorFunctionSpace(mesh, "CG", k)        #B
Z = W

B = Function(Z)
C = TestFunction(Z)

(x, y) = SpatialCoordinate(Z.mesh())
n = FacetNormal(mesh)
t = as_vector([n[1], -n[0]])


u_ex = as_vector([x*y*exp(x+y) + x*exp(x+y), -x*y*exp(x+y) - y*exp(x+y)])

eps = lambda x: sym(grad(x))
F = (
     1/Rem * inner(grad(B), grad(C))*dx
     - delta * inner(scross(u_ex, B), scurl(C))*dx
     #- delta * ( inner(dot(grad(u_ex), B), C) * dx - inner(dot(grad(B), u_ex), C) * dx)
    )


if testproblem == "Wathen":
    #example taken from chapter 6.2.1 in doi.org/a10.1137/16M1098991
    b= sin((y-0.5)/2/pi)*sin((y+0.5)/2/pi)
    #B_ex = as_vector([b, Constant(1, domain=mesh)])
    B_ex = as_vector([exp(x+y)*cos(x), exp(x+y)*sin(x) - exp(x+y)*cos(x)])
    rhs = True
    solution_known = True
    
if rhs is not None:
    f1 = -1/Rem * div(grad(B_ex)) - delta * vcurl(scross(u_ex, B_ex))
    F -= inner(f1, C) * dx

    
if solution_known:
    if boundary_cond == "normal":
        bcs = [DirichletBC(Z.sub(0), B_ex[0], 1),
           DirichletBC(Z.sub(0), B_ex[0], 2),
           DirichletBC(Z.sub(1), B_ex[1], 3),
               DirichletBC(Z.sub(1), B_ex[1], 4)]
    
    elif boundary_cond == "Dirichlet":
        bcs =  [DirichletBC(Z, B_ex , "on_boundary")]
    else:
        raise NotImplementedError("Only know normal and Dirichlet.")


problem = NonlinearVariationalProblem(F, B, bcs)
    

params = solvers[args.solver_type]
#print("args.solver_type", args.solver_type)
#import pprint
#pprint.pprint(params)
solver = NonlinearVariationalSolver(problem, solver_parameters=params)



start = datetime.now()
solver.solve()
end = datetime.now()

linear_its = solver.snes.getLinearSolveIterations()
nonlinear_its = solver.snes.getIterationNumber()
time = (end-start).total_seconds() / 60

print(GREEN % ("Time taken: %.2f min in %d nonlinear iterations, %d linear iterations (%.2f Krylov iters per Newton step)" % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its))))
print("%.2f @ %d @ %d @ %.2f" % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its)))

print("||div(B)||_L^2 = %s" % sqrt(assemble(inner(div(B), div(B))*dx)))

B.rename("MagneticField")


if solution_known:
    B_ex_ = project(B_ex, B.function_space())
    B_ex_.rename("ExactSolutionB")
    error_B = errornorm(B_ex_, B, 'L2')

    print("Error ||B_ex - B||_L^2 = %s" % error_B)
       



    


