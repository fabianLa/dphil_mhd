# -*- coding: utf-8 -*-
from firedrake import *
from datetime import datetime
import alfi
import argparse
import numpy
from pyop2.datatypes import IntType
import ufl.algorithms
from alfi.stabilisation import *
from alfi.transfer import *
from alfi import *
from  multiprocessing import *
import sys
import os

distribution_parameters={"partition": True, "overlap_type": (DistributedMeshOverlapType.VERTEX, 2)}

def scross(x, y):
    return x[0]*y[1] - x[1]*y[0]

def scurl(x):
    return x[1].dx(0) - x[0].dx(1)

def vcurl(x):
    return as_vector([x.dx(1), -x.dx(0)])


def BurmanStab(B, C, supg_wind, stab_weight, mesh):
    n = FacetNormal(mesh)
    h = FacetArea(mesh)
    beta = sqrt(avg(facet_avg(dot(supg_wind, n)**2)))
    gamma1 = stab_weight # as chosen in doi:10.1016/j.apnum.2007.11.001
    stabilisation_form = 0.5 * gamma1 * h**2 * beta * dot(jump(grad(B), n), jump(grad(C), n))*dS
    return stabilisation_form





lu = {
     "snes_type": "newtonls",
     "snes_monitor": None,
     "snes_atol": 1.0e-8,
     "snes_rtol": 1.0e-10,
     "snes_linesearch_type": "l2",
     #"snes_linesearch_monitor": None,
     "snes_linesearch_maxstep": 1,
     #"snes_converged_reason": None,
     "snes_max_it": 15,
     #"snes_convergence_test": "skip",
     "mat_type": "aij",
     "ksp_type": "preonly",
     #"ksp_monitor_true_residual": None,
     "pc_type": "lu",
     "pc_factor_mat_solver_type": "mumps",
     "mat_mumps_icntl_14": 300,
         }

fs = {
    "snes_type": "ksponly",
    "snes_monitor": None,
    "ksp_type": "gmres",
    #"ksp_richardson_scale": 0.1,
    "ksp_max_it": 150,
    "ksp_atol": 1.0e-6,
    "ksp_rtol": 1.0e-6,
    "ksp_monitor_true_residual": None,#
    "ksp_converged_reason": None,
    "mat_type": "aij",
#    "ksp_type": "fgmres",
#    "ksp_max_it": 2,
    #"fieldsplit_1_aux_mg_levels_ksp_richardson_scale": 1,
    "ksp_norm_type": "unpreconditioned",
    #"ksp_monitor_true_residual": None,
    "pc_type": "python",
    "pc_python_type": "firedrake.PatchPC",
    "patch_pc_patch_save_operators": True,
    "patch_pc_patch_partition_of_unity": False,
    "patch_pc_patch_sub_mat_type": "seqaij",
    "patch_pc_patch_construct_dim": 0,
    "patch_pc_patch_construct_type": "python",
    "patch_pc_patch_construct_python_type": "alfi.MacroStar",
    "patch_sub_ksp_type": "preonly",
    "patch_sub_pc_type": "lu",
    "patch_sub_pc_factor_mat_solver_type": "umfpack",
        }

mfsstar2 = {
   "snes_type": "ksponly",
   "snes_monitor": None,
   "ksp_type": "fgmres",
   "ksp_max_it": 150,
   "ksp_atol": 1.0e-8,
   "ksp_rtol": 1.0e-13,
   "ksp_monitor_true_residual": None,#
   "ksp_converged_reason": None,
   "mat_type": "aij",
#    "ksp_type": "fgmres",
#    "ksp_max_it": 2,
   "ksp_type": "fgmres",
   #"ksp_monitor_true_residual": None,
   "pc_type": "fieldsplit",
   "pc_fieldsplit_type": "schur",
   "pc_fieldsplit_schur_fact_type": "full",
   "pc_fieldsplit_0_fields": "1",
   "pc_fieldsplit_1_fields": "0",
   "fieldsplit_0_ksp_type": "gmres", 
   #"fieldsplit_0_ksp_max_it": 1,
   #"fieldsplit_0_ksp_convergence_test": "skip",
   "fieldsplit_0_pc_type": "jacobi", ###jacobi
   #"fieldsplit_0_pc_factor_mat_solver_type": "mumps", 
   "fieldsplit_1_ksp_type": "gmres",
   "fieldsplit_1_ksp_max_it": 1,
   "fieldsplit_1_ksp_norm_type": "unpreconditioned",
   "fieldsplit_1_pc_type": "python",
   "fieldsplit_1_pc_python_type": "__main__.InnerSchurPC",
   "fieldsplit_1_aux_pc_type": "mg", #### mg
   "fieldsplit_1_aux_mg_transfer_manager": __name__ + ".transfermanager",
   "fieldsplit_1_aux_pc_mg_cycle_type": "v",
   "fieldsplit_1_aux_pc_mg_type": "full",
   "fieldsplit_1_aux_mg_levels_ksp_type": "fgmres",
   "fieldsplit_1_aux_mg_levels_ksp_convergence_test": "skip",
   "fieldsplit_1_aux_mg_levels_ksp_max_it": 6,
   "fieldsplit_1_aux_mg_levels_ksp_norm_type": "unpreconditioned",
   #"fieldsplit_1_aux_mg_levels_ksp_monitor_true_residual": None,
   "fieldsplit_1_aux_mg_levels_pc_type": "python",
   "fieldsplit_1_aux_mg_levels_pc_python_type": "firedrake.PatchPC",
   "fieldsplit_1_aux_mg_levels_patch_pc_patch_save_operators": True,
   "fieldsplit_1_aux_mg_levels_patch_pc_patch_partition_of_unity": False,
   "fieldsplit_1_aux_mg_levels_patch_pc_patch_sub_mat_type": "seqaij",
   "fieldsplit_1_aux_mg_levels_patch_pc_patch_construct_dim": 0,
   "fieldsplit_1_aux_mg_levels_patch_pc_patch_construct_type": "python",
   "fieldsplit_1_aux_mg_levels_patch_pc_patch_construct_python_type": "alfi.MacroStar",
   "fieldsplit_1_aux_mg_levels_patch_sub_ksp_type": "preonly",
   "fieldsplit_1_aux_mg_levels_patch_sub_pc_type": "lu",
   "fieldsplit_1_aux_mg_levels_patch_sub_pc_factor_mat_solver_type": "umfpack",
   "fieldsplit_1_aux_mg_coarse_pc_type": "python",
   "fieldsplit_1_aux_mg_coarse_pc_python_type": "firedrake.AssembledPC",
   "fieldsplit_1_aux_mg_coarse_assembled": {
         "mat_type": "aij",
         "pc_type": "telescope",
         "pc_telescope_reduction_factor": 1,
         "pc_telescope_subcomm_type": "contiguous",
         "telescope_pc_type": "lu",
         "telescope_pc_factor_mat_solver_type": "mumps", 
     }
       }



solvers = {"lu": lu, "fs": fs }

parser = argparse.ArgumentParser(add_help=False)
parser.add_argument("--baseN", type=int, default=10)
parser.add_argument("--k", type=int, default=3)
parser.add_argument("--dim", type=int, choices=[2,3], required=True)
parser.add_argument("--nref", type=int, default=1)
parser.add_argument("--Rem", nargs='+', type=float, default=[1])
parser.add_argument("--gamma2", type=float, default=0)
parser.add_argument("--eta", type=float, default=0)
parser.add_argument("--delta", type=float, default=1)
parser.add_argument("--hierarchy", choices=["bary", "uniform"], default="bary")
parser.add_argument("--solver-type", choices=list(solvers.keys()), default="lu")
parser.add_argument("--boundary-cond", choices=["normal", "Dirichlet"], required=True)
parser.add_argument("--stab", default=False, action="store_true")

args, _ = parser.parse_known_args()
baseN = args.baseN
k = args.k
nref = args.nref
Rem = Constant(args.Rem[0])
gamma2 = Constant(args.gamma2)
hierarchy = args.hierarchy
solver_type = args.solver_type
gamma2 = Constant(args.gamma2)
eta = Constant(args.eta)
delta = Constant(args.delta)
boundary_cond=args.boundary_cond
stab = args.stab
dim = args.dim

stab_weight = Constant(5e-3)

eta = 1/Rem


if k<3:
    raise ValueError("Scott Vogelius is not stable for k<3")

if dim == 2:
    base = UnitSquareMesh(baseN, baseN, distribution_parameters=distribution_parameters)
elif dim == 3:
    base = UnitCubeMesh(baseN, baseN, baseN, distribution_parameters=distribution_parameters)

def before(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+1)

def after(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+2)
    #rebalance(dm, i)

if hierarchy == "bary":
    mh = alfi.BaryMeshHierarchy(base, nref, callbacks=(before, after))
elif hierarchy == "uniformbary":
    bmesh = Mesh(bary(base._plex), distribution_parameters={"partition": False})
    mh = MeshHierarchy(bmesh, nref, reorder=True, callbacks=(before, after),
                       distribution_parameters=distribution_parameters)
elif hierarchy == "uniform":
    mh = MeshHierarchy(base, nref, reorder=True, callbacks=(before, after),
             distribution_parameters=distribution_parameters)
else:
    raise NotImplementedError("Only know bary, uniformbary and uniform for the hierarchy.")
if dim == 2:
    for m in mh:
        m.coordinates.dat.data[:, 0] -= 0.5
        m.coordinates.dat.data[:, 1] -= 0.5
elif dim == 3:
    for m in mh:
        m.coordinates.dat.data[:, 0] -= 0.5
        m.coordinates.dat.data[:, 1] -= 0.5
        m.coordinates.dat.data[:, 2] -= 0.5
mesh = mh[-1]


def message(msg):
    if mesh.comm.rank == 0:
        warning(msg)
        
Z = VectorFunctionSpace(mesh, "CG", k-1)

B = Function(Z)
C  = TestFunction(Z)

if dim == 2:
    (x, y) = SpatialCoordinate(Z.mesh())
    u_ex = as_vector([sin(x)*y, x*x])
    B_ex = as_vector([sin(y), x])
elif dim == 3:
    (x, y, zz) = SpatialCoordinate(Z.mesh())
    u_ex = as_vector([sin(zz)*y, x*zz, y*x])
    B_ex = as_vector([sin(y),zz,x])
    B_ex = as_vector([1.0e-12*x, 1.0e-12*y, 1.0e-12*zz])

eps = lambda x: sym(grad(x))

if dim == 2:
    F = (
        1/Rem * inner(grad(B), grad(C)) * dx
        + gamma2 * inner(div(B), div(C)) * dx
        ) 

elif dim == 3:
    F = (
        + delta * inner(curl(cross(u_ex, B)), C) * dx
        + 1/Rem * inner(grad(B), grad(C)) * dx
        #+ 1/Rem * (inner(curl(B), curl(C)) * dx + inner(div(B), div(C)) * dx)
        + gamma2 * inner(div(B), div(C)) * dx
        )

    f1 =  1/Rem * (curl(curl(B_ex)) - grad(div(B_ex))) - gamma2 * grad(div(B_ex)) + delta * curl(cross(u_ex, B_ex))


#F -= inner(f1, C) * dx

n = FacetNormal(mesh)
if boundary_cond == "normal":
    #F = F - 1/Rem * inner(div(B_ex), dot(C, n)) * ds - gamma2 * inner(div(B_ex), dot(C, n)) * ds + 1/Rem * inner(curl(B_ex), cross(C,n)) * ds
    F = F - 1/Rem * (inner(dot(grad(B_ex), n), C) * ds)
if stab:
    stabilisation_form_B = BurmanStab(B, C, B_ex, stab_weight, mesh)
    F += stabilisation_form_B

                    
bcs = [DirichletBC(Z, 0 , "on_boundary")]
#           DirichletBC(Z.sub(2), 0, ((1,3), ), method="geometric")] # Doesn't work for DG pressures, does for CG

if boundary_cond == "normal" and dim == 3:
        bcs = [DirichletBC(Z.sub(0), 0, 1),
           DirichletBC(Z.sub(0), 0, 2),
           DirichletBC(Z.sub(1), 0, 3),
           DirichletBC(Z.sub(1), 0, 4),
           DirichletBC(Z.sub(2), 0, 5),
           DirichletBC(Z.sub(2), 0, 6)]

#Initialise randomly
with B.dat.vec_wo as w:
    w.setRandom()
bcs[0].apply(B)

problem = NonlinearVariationalProblem(F, B, bcs)
 
appctx = {"Rem": Rem, "gamma2": gamma2}
params = solvers[args.solver_type]
#print("args.solver_type", args.solver_type)
#import pprint
#pprint.pprint(params)

solver = NonlinearVariationalSolver(problem, solver_parameters=params, options_prefix="", appctx=appctx)
qtransfer = NullTransfer()
Etransfer = NullTransfer()
V = VectorFunctionSpace(mesh, "CG", k)  #u
Q = FunctionSpace(mesh, "DG", k-1)
vtransfer = SVSchoeberlTransfer((1/Rem, gamma2), 3, hierarchy)
Btransfer = SVSchoeberlTransfer((1/Rem, gamma2), 3, hierarchy)
dgtransfer = DGInjection()
def traceinject(src, dest):
    out = inject(src, dest)
    with dest.dat.vec_ro as x:
        print("Injecting %s -> %s: result has norm %s" % (src, dest, x.norm()))
    import sys; sys.stdout.flush()
    return out

def myprolong(src, dest):
    out = prolong(src, dest)
    with dest.dat.vec_ro as x:
        print("Prolong %s -> %s: result has norm %s" % (src, dest, x.norm()))
    import sys; sys.stdout.flush()
    return out

transfers = {
                #V.ufl_element(): (vtransfer.prolong, vtransfer.restrict, inject),
                Z.ufl_element(): (Btransfer.prolong, Btransfer.restrict, inject),
                Q.ufl_element(): (prolong, restrict, qtransfer.inject),
                #R.ufl_element(): (prolong, restrict, Etransfer.inject),
                #VectorElement("DG", mesh.ufl_cell(), args.k-1): (dgtransfer.prolong, restrict, dgtransfer.inject),
                #VectorElement("DG", mesh.ufl_cell(), args.k-2): (dgtransfer.prolong, restrict, dgtransfer.inject),
            }
transfermanager = TransferManager(native_transfers=transfers)
solver.set_transfer_manager(transfermanager)

for rem in args.Rem:
    Rem.assign(rem)

    message(GREEN % ("Solving for #dofs = %s , Rem = %s, gamma2 = %s ,nref=%s, baseN=%s"
                     % (Z.dim(), float(Rem), float(gamma2), int(nref), int(baseN))))



    pvd = File("output/relaxation/iterates.pvd")
    erroru = Function(Z)
    def mymonitor(ksp, it, rnorm):
        xbar = solver.snes.getSolution()
        #x = ksp.getSolution()
        x = ksp.buildSolution()
        with erroru.dat.vec_wo as y:
            xbar.copy(y)
            y.axpy(-1, x)
        pvd.write(erroru)
        import numpy as np
        #print("norm xbar %s" % np.linalg.norm(xbar))
        #print("norm x %s" % np.linalg.norm(x))
        #print("norm y %s" % np.linalg.norm(y))
        # iter = solver.snes.ksp.getIterationNumber()
        # plt.figure()
        # plot(erroru)
        # plt.savefig("output/iterate_%d.png"%iter)
    solver.snes.ksp.setMonitor(mymonitor)
    start = datetime.now()
    solver.solve()
    end = datetime.now()


    linear_its = solver.snes.getLinearSolveIterations()
    nonlinear_its = solver.snes.getIterationNumber()
    time = (end-start).total_seconds() / 60

    if mesh.comm.rank == 0:
        print(GREEN % ("Time taken: %.2f min in %d nonlinear iterations, %d linear iterations (%.2f Krylov iters per Newton step)"
                       % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its))))
        print("%.2f @ %d @ %d @ %.2f" % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its)))



    B.rename("MagneticField")

    error_B = errornorm(B_ex, B, 'L2')

    norm_div_B = sqrt(assemble(inner(div(B), div(B))*dx))

    if mesh.comm.rank == 0:
        print("||div(B)||_L^2 = %s" % norm_div_B)
        print("Error ||B_ex - B||_L^2 = %s" % error_B)

    info_dict = {
        "Rem": float(Rem),
        "krylov/nonlin": linear_its/nonlinear_its,
        "nonlinear_iter": nonlinear_its,
        "error_B": error_B,
    }
    message(BLUE % info_dict)
