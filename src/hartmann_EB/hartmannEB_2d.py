# -*- coding: utf-8 -*-
from firedrake import *
from datetime import datetime
import alfi
import argparse
import numpy
from pyop2.datatypes import IntType
import ufl.algorithms
from alfi.stabilisation import *
from alfi.transfer import *
from alfi import *
from  multiprocessing import *
import sys
import os

distribution_parameters={"partition": True, "overlap_type": (DistributedMeshOverlapType.VERTEX, 2)}

def scross(x, y):
    return x[0]*y[1] - x[1]*y[0]

def vcross(x,y):
    return as_vector([x[1]*y,-x[0]*y])

def scurl(x):
    return x[1].dx(0) - x[0].dx(1)

def vcurl(x):
    return as_vector([x.dx(1), -x.dx(0)])

def acurl(x):
    return as_vector([
                     x[2].dx(1),
                     -x[2].dx(0),
                     x[1].dx(0) - x[0].dx(1)
                     ])

def BurmanStab(B, C, supg_wind, stab_weight, mesh):
    n = FacetNormal(mesh)
    h = FacetArea(mesh)
    beta = sqrt(avg(facet_avg(dot(supg_wind, n)**2)))
    gamma1 = stab_weight # as chosen in doi:10.1016/j.apnum.2007.11.001
    stabilisation_form = 0.5 * gamma1 * h**2 * beta * dot(jump(grad(B), n), jump(grad(C), n))*dS
    return stabilisation_form


class InnerSchurPC(AuxiliaryOperatorPC):
    def form(self, pc, C, B):
        mesh = C.function_space().mesh()
        n = FacetNormal(mesh)
        t = as_vector([n[1], -n[0]])
        h = CellDiameter(mesh)
        h_avg = (h('+') + h('-'))/2
        alpha = Constant(4)

        state = self.get_appctx(pc)['state']
        [u_n, p_n, B_n, E_n] = split(state)

        K = eta * (
              + inner(grad(B), grad(C))*dx
              + alpha/h_avg*inner(jump(C, t), jump(B, t))*dS
            )

        K = K - inner(scross(u_n, B), scurl(C))*dx 

        bcs = [DirichletBC(C.function_space(), 0, "on_boundary")]

        return (K, bcs)


class SchurPC(AuxiliaryOperatorPC):
    def form(self, pc, V, U):
        mesh = V.function_space().mesh()
        [u, p] = split(U)
        [v, q] = split(V)
        h = CellDiameter(mesh)

        state = self.get_appctx(pc)['state']
        [u_n, p_n, B_n, E_n] = split(state)

        eps = lambda x: sym(grad(x))

        A =  (
                2 * nu * inner(eps(u), eps(v))*dx
              + advect * inner(dot(grad(u_n), u), v) * dx
              + advect * inner(dot(grad(u), u_n), v) * dx
              + gamma * inner(div(u), div(v)) * dx
              - inner(p, div(v)) * dx
              - inner(div(u), q) * dx
              + S * inner(vcross(B_n, scross(u, B_n)), v) * dx
                        )

        if stab:
            stabilisation_form = BurmanStab(u, v, z_last_u, stab_weight, mesh)
            A = A + stabilisation_form

        bcs = [DirichletBC(V.function_space().sub(0), 0, "on_boundary"),
               PressureFixBC(V.function_space().sub(1), 0, 1),
        ]

        return (A, bcs)
    

class HideousHackPC(AssembledPC):
            
    def update(self, pc):
        coeffs = [coeff for coeff in self.P.a.coefficients() if isinstance(coeff, Function)]
        assert len(coeffs) == 1

        state = self.get_appctx(pc)['state']
        #print("Assigning %s to %s %s" % (state, coeffs[0], coeffs[0].split()[0]))
        coeffs[0].split()[0].assign(state)
        AssembledPC.update(self, pc)
        import sys; sys.stdout.flush()

class Schur(AuxiliaryOperatorPC):
    def form(self, pc, V, U):
        mesh = V.function_space().mesh()
        [B, E] = split(U)
        [C, Ff] = split(V)
        h = CellDiameter(mesh)

        state = self.get_appctx(pc)['state']
        [u_n, p_n, B_n, E_n] = split(state)

        eps = lambda x: sym(grad(x))

        A =  (
             + inner(E, Ff) * dx
             + inner(scross(u_n, B), Ff) * dx
             - 1/Rem * inner(scurl(B), Ff) * dx
             + inner(E, scurl(C)) * dx
             + eta * inner(div(B), div(C)) * dx
             + gamma2 * inner(div(B), div(C)) * dx
                        )
        if stab:
            stabilisation_form = BurmanStab(B, C, z_last_B, stab_weight, mesh)
            A = A + stabilisation_form
        
        bcs = [DirichletBC(V.function_space().sub(0), 0, "on_boundary"),
               PressureFixBC(V.function_space().sub(1), 0, 1),
        ]

        return (A, bcs)

class PressureFixBC(DirichletBC):
    def __init__(self, V, val, subdomain, method="topological"):
        super().__init__(V, val, subdomain, method)
        sec = V.dm.getDefaultSection()
        dm = V.mesh()._plex

        coordsSection = dm.getCoordinateSection()
        coordsDM = dm.getCoordinateDM()
        dim = dm.getCoordinateDim()
        coordsVec = dm.getCoordinatesLocal()

        (vStart, vEnd) = dm.getDepthStratum(0)
        indices = []
        for pt in range(vStart, vEnd):
            x = dm.getVecClosure(coordsSection, coordsVec, pt).reshape(-1, dim).mean(axis=0)
            if x.dot(x) == 0.0: # fix [0, 0] in original mesh coordinates (bottom left corner)
                if dm.getLabelValue("pyop2_ghost", pt) == -1:
                    indices = [pt]
                break

        nodes = []
        for i in indices:
            if sec.getDof(i) > 0:
                nodes.append(sec.getOffset(i))

        if V.mesh().comm.rank == 0:
            nodes = [0]
        else:
            nodes = []
        self.nodes = numpy.asarray(nodes, dtype=IntType)

        if len(self.nodes) > 0:
            print("Fixing nodes %s" % self.nodes)
        import sys; sys.stdout.flush()

lu = {
     "snes_type": "newtonls",
     "snes_monitor": None,
     "snes_atol": 1.0e-8,
     "snes_rtol": 1.0e-10,
     "snes_linesearch_type": "l2",
     #"snes_linesearch_monitor": None,
     "snes_linesearch_maxstep": 1,
     #"snes_converged_reason": None,
     #"snes_max_it": 1000,
     #"snes_convergence_test": "skip",
     "mat_type": "aij",
     "ksp_type": "preonly",
     #"ksp_monitor_true_residual": None,
     "pc_type": "lu",
     "pc_factor_mat_solver_type": "mumps",
     "mat_mumps_icntl_14": 300,
         }

mstar = {
   "ksp_type": "fgmres",
   "ksp_max_it": 2,
   "pc_type": "mg",
   "pc_mg_cycle_type": "v",
   "pc_mg_type": "full",
   "mg_levels_ksp_type": "fgmres",
   #"fieldsplit_1_aux_mg_levels_ksp_richardson_scale": 1,
   "mg_levels_ksp_convergence_test": "skip",
   "mg_levels_ksp_max_it": 6,
   "mg_levels_ksp_norm_type": "unpreconditioned",
   #"_mg_levels_ksp_monitor_true_residual": None,
   "mg_levels_pc_type": "python",
   "mg_levels_pc_python_type": "firedrake.PatchPC",
   "mg_levels_patch_pc_patch_save_operators": True,
   "mg_levels_patch_pc_patch_partition_of_unity": False,
   "mg_levels_patch_pc_patch_sub_mat_type": "seqaij",
   "mg_levels_patch_pc_patch_construct_dim": 0,
   "mg_levels_patch_pc_patch_construct_type": "python",
   "mg_levels_patch_pc_patch_construct_python_type": "alfi.MacroStar",
   "mg_levels_patch_sub_ksp_type": "preonly",
   "mg_levels_patch_sub_pc_type": "lu",
   "mg_levels_patch_sub_pc_factor_mat_solver_type": "umfpack",
   "mg_coarse_ksp_type": "richardson",
   "mg_coarse_ksp_max_it": 1,
   "mg_coarse_ksp_norm_type": "unpreconditioned",
   #"mg_coarse_ksp_monitor_true_residual": None,
   "mg_coarse_pc_type": "python",
   "mg_coarse_pc_python_type": "firedrake.AssembledPC",
   "mg_coarse_assembled": {
         "mat_type": "aij",
         "pc_type": "telescope",
         "pc_telescope_reduction_factor": 1,
         "pc_telescope_subcomm_type": "contiguous",
         "telescope_pc_type": "lu",
         "telescope_pc_factor_mat_solver_type": "mumps",
         "telescope_pc_factor_mat_mumps_icntl_14": 200,
     }
       }

outerschurfs = {
   "ksp_type": "fgmres",
   "ksp_atol": 1.0e-7,
   "ksp_rtol": 1.0e-7,
   #"ksp_monitor_true_residual": None,
   #"ksp_converged_reason": None,
   "pc_type": "python",
   "pc_python_type": "__main__.SchurPC",
   "aux_mg_transfer_manager": __name__ + ".transfermanager",
   #"aux_pc_type": "lu",
   #"aux_pc_factor_mat_solver_type": "mumps",
   #"aux_mat_mumps_icntl_14": 200,
   "ksp_max_it": 2,
   "aux_mat_type": "nest",
   "aux_pc_type": "fieldsplit",
   "aux_pc_fieldsplit_type": "schur",
   "aux_pc_fieldsplit_schur_factorization_type": "full",
   "aux_pc_fieldsplit_schur_precondition": "user",
   "aux_fieldsplit_0": {
          "ksp_type": "gmres",
          "ksp_max_it": 1,
          "ksp_norm_type": "unpreconditioned",
          "pc_type": "mg",
          "pc_mg_cycle_type": "v",
          "pc_mg_type": "full",
          "mg_levels_ksp_type": "fgmres",
          "mg_levels_ksp_convergence_test": "skip",
          "mg_levels_ksp_max_it": 6,
          "mg_levels_ksp_norm_type": "unpreconditioned",
          #"mg_levels_ksp_monitor_true_residual": None,
          "mg_levels_pc_type": "python",
          "mg_levels_pc_python_type": "firedrake.PatchPC",
          "mg_levels_patch_pc_patch_save_operators": True,
          "mg_levels_patch_pc_patch_partition_of_unity": False,
          "mg_levels_patch_pc_patch_sub_mat_type": "seqaij",
          "mg_levels_patch_pc_patch_construct_dim": 0,
          "mg_levels_patch_pc_patch_construct_type": "python",
          "mg_levels_patch_pc_patch_construct_python_type": "alfi.MacroStar",
          "mg_levels_patch_sub_ksp_type": "preonly",
          "mg_levels_patch_sub_pc_type": "lu",
          "mg_levels_patch_sub_pc_factor_mat_solver_type": "umfpack",
          #"mg_levels_patch_sub_mat_mumps_icntl_14": 200,
          "mg_coarse_ksp_type": "richardson",
          "mg_coarse_ksp_max_it": 1,
          "mg_coarse_ksp_norm_type": "unpreconditioned",
          #"mg_coarse_ksp_monitor_true_residual": None,
          "mg_coarse_pc_type": "python",
          #"mg_coarse_pc_factor_mat_solver_type": "mumps",
          #"mg_coarse_pc_factor_mat_mumps_icntl_14": 200,
          "mg_coarse_pc_python_type": "firedrake.AssembledPC",
          "mg_coarse_assembled": {
                "mat_type": "aij",
                "pc_type": "telescope",
                "pc_telescope_reduction_factor": 1,
                "pc_telescope_subcomm_type": "contiguous",
                "telescope_pc_type": "lu",
                "telescope_pc_factor_mat_solver_type": "mumps",
                "telescope_pc_factor_mat_mumps_icntl_14": 200,
            }
       },
   "aux_fieldsplit_1":{
           "ksp_type": "preonly",
           "pc_type": "python",
           "pc_python_type": "alfi.solver.DGMassInv"
       },
   }

outerschurlu = {
   "ksp_type": "gmres",
   "ksp_max_it": 2,
   #"ksp_monitor_true_residual": None,
   #"ksp_converged_reason": None,
   "pc_type": "python",
   "pc_python_type": "__main__.SchurPC",
   "aux_pc_type": "lu",
   "aux_pc_factor_mat_solver_type": "mumps",
   "aux_mat_mumps_icntl_14": 200,
   #"aux_pc_type": "fieldsplit",
   #"aux_pc_fieldsplit_type": "schur",
   #"aux_pc_fieldsplit_schur_factorization_type": "full",
   #"aux_pc_fieldsplit_schur_precondition": "user",
   #"aux_fieldsplit_0": {
   #        "ksp_type": "preonly",
   #        "ksp_max_it": 1,
   #        "pc_type": "lu",
   #        "pc_factor_mat_solver_type": "mumps",
   #        "mat_mumps_icntl_14": 150,
   #    },
   #"aux_fieldsplit_1":{
   #         "ksp_type": "preonly",
   #        "pc_type": "python",
   #        "pc_python_type": "alfi.solver.DGMassInv"
   #    },
   }


fs2by2 = {
     "snes_type": "newtonls",
     "snes_max_it": 25,
     "snes_linesearch_type": "l2",
     "snes_linesearch_maxstep": 1.0,
     "snes_rtol": 1.0e-10,
     "snes_atol": 1.0e-6,
     #"snes_convergence_test": "skip",
     "snes_monitor": None,
     #"snes_linesearch_monitor": None,
     #"snes_converged_reason": None,
     "ksp_type": "fgmres",
     "ksp_max_it": 40,
     "ksp_atol": 1.0e-7,
     "ksp_rtol": 1.0e-7,
     "ksp_monitor_true_residual": None,
     "ksp_converged_reason": None,
     "mat_type": "aij",
     "pc_type": "fieldsplit",
     "pc_fieldsplit_type": "schur",
     "pc_fieldsplit_0_fields": "2,3",
     "pc_fieldsplit_1_fields": "0,1",
     "fieldsplit_0": mstar,
     "fieldsplit_1": outerschurfs,
    }

fs2by2mlu = {
     "snes_type": "newtonls",
     "snes_max_it": 30,
     "snes_linesearch_type": "l2",
     "snes_linesearch_maxstep": 1.0,
     "snes_rtol": 1.0e-10,
     "snes_atol": 1.0e-7,
     #"snes_convergence_test": "skip",
     "snes_monitor": None,
     #"snes_linesearch_monitor": None,
     #"snes_converged_reason": None,
     "ksp_type": "fgmres",
     "ksp_max_it": 50,
     "ksp_atol": 1.0e-8,
     "ksp_rtol": 1.0e-9,
     "ksp_monitor_true_residual": None,
     "ksp_converged_reason": None,
     "mat_type": "aij",
     "pc_type": "fieldsplit",
     "pc_fieldsplit_type": "schur",
     "pc_fieldsplit_0_fields": "2,3",
     "pc_fieldsplit_1_fields": "0,1",
     "fieldsplit_0_ksp_type": "preonly",
     "fieldsplit_0_pc_type": "lu",
     "fieldsplit_0_pc_factor_mat_solver_type": "mumps",
     "fieldsplit_0_mat_mumps_icntl_14": 200,
     "fieldsplit_1": outerschurfs,
    }

fs2by2slu = {
     "snes_type": "newtonls",
     "snes_max_it": 30,
     "snes_linesearch_type": "l2",
     "snes_linesearch_maxstep": 1.0,
     "snes_rtol": 1.0e-10,
     "snes_atol": 1.0e-7,
     #"snes_convergence_test": "skip",
     "snes_monitor": None,
     #"snes_linesearch_monitor": None,
     #"snes_converged_reason": None,
     "ksp_type": "fgmres",
     "ksp_max_it": 40,
     "ksp_atol": 1.0e-7,
     "ksp_rtol": 1.0e-7,
     "ksp_monitor_true_residual": None,
     "ksp_converged_reason": None,
     "mat_type": "aij",
     "pc_type": "fieldsplit",
     "pc_fieldsplit_type": "schur",
     "pc_fieldsplit_0_fields": "2,3",
     "pc_fieldsplit_1_fields": "0,1",
     "fieldsplit_0": mstar,
     "fieldsplit_1": outerschurlu,
    }
    


solvers = {"lu": lu, "fs2by2": fs2by2, "fs2by2mlu": fs2by2mlu, "fs2by2slu": fs2by2slu}

parser = argparse.ArgumentParser(add_help=False)
parser.add_argument("--baseN", type=int, default=10)
parser.add_argument("--k", type=int, default=3)
parser.add_argument("--nref", type=int, default=1)
parser.add_argument("--Re", nargs='+', type=float, default=[1])
parser.add_argument("--Rem", nargs='+', type=float, default=[1])
parser.add_argument("--gamma", type=float, default=10000)
parser.add_argument("--gamma2", type=float, default=0)
parser.add_argument("--beta", type=float, default=0)
parser.add_argument("--eta", type=float, default=0)
parser.add_argument("--S", type=float, default=1)
parser.add_argument("--char_L", type=float, default=1)
parser.add_argument("--char_U", type=float, default=1)
parser.add_argument("--hierarchy", choices=["bary", "uniform"], default="bary")
parser.add_argument("--solver-type", choices=list(solvers.keys()), default="lu")
parser.add_argument("--testproblem", choices=["ldc", "hartmann", "Wathen", "hartmann2"], default = "Wathen")
parser.add_argument("--linearisation", choices=["picard", "picardG", "newton"], required=True )
parser.add_argument("--multiprocessing", default=False, action="store_true")
parser.add_argument("--stab", default=False, action="store_true")
parser.add_argument("--exing", default=False, action="store_true")
parser.add_argument("--checkpoint", default=False, action="store_true")

args, _ = parser.parse_known_args()
baseN = args.baseN
k = args.k
nref = args.nref
Re = Constant(args.Re[0])
Rem = Constant(args.Rem[0])
gamma = Constant(args.gamma)
gamma2 = Constant(args.gamma2)
S = Constant(args.S)
hierarchy = args.hierarchy
solver_type = args.solver_type
testproblem = args.testproblem
gamma2 = Constant(args.gamma2)
advect = Constant(1)  
char_L = Constant(args.char_L)
char_U = Constant(args.char_U)
beta = Constant(args.beta)
eta = Constant(args.eta)
nu = Constant(0)
linearisation = args.linearisation
stab = args.stab
multiprocessing = args.multiprocessing
exing = args.exing
checkpoint = args.checkpoint

S2 = Constant(1)
stab_weight = Constant(5e-3)

eta = 1/Rem

if multiprocessing and testproblem == "ldc":
    raiseValueError("You should use continuation for ldc and not multiprocessing")

if exing and testproblem == "ldc":
    raise ValueError("Analytical solution for ldc is not known")

if checkpoint and exing:
    raise ValueError("It doesn't make sense to use checkpoint and exing at the same time")

base = UnitSquareMesh(baseN, baseN, diagonal="crossed", distribution_parameters=distribution_parameters)

def before(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+1)

def after(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+2)
    #rebalance(dm, i)

if hierarchy == "bary":
    mh = alfi.BaryMeshHierarchy(base, nref, callbacks=(before, after))
elif hierarchy == "uniformbary":
    bmesh = Mesh(bary(base._plex), distribution_parameters={"partition": False})
    mh = MeshHierarchy(bmesh, nref, reorder=True, callbacks=(before, after),
                       distribution_parameters=distribution_parameters)
elif hierarchy == "uniform":
    mh = MeshHierarchy(base, nref, reorder=True, callbacks=(before, after),
             distribution_parameters=distribution_parameters)
else:
    raise NotImplementedError("Only know bary, uniformbary and uniform for the hierarchy.")
for m in mh:
    m.coordinates.dat.data[:, 0] -= 0.5
    m.coordinates.dat.data[:, 1] -= 0.5
mesh = mh[-1]

area = assemble(Constant(1, domain=mh[0])*dx)

def message(msg):
    if mesh.comm.rank == 0:
        warning(msg)

V = VectorFunctionSpace(mesh, "CG", k)  #u
Q = FunctionSpace(mesh, "DG", k-1)      #p
R = FunctionSpace(mesh, "CG", k)    #E
W = FunctionSpace(mesh, "RT", k)        #B
Z = MixedFunctionSpace([V, Q, W, R])

z = Function(Z)
(u, p, B, E) = split(z)
(v, q, C, Ff)   = split(TestFunction(Z))
z_last_u = Function(V)
if float(Re) > 2 and checkpoint:
    chk = DumbCheckpoint("dump"+str(float(Rem))+str(linearisation), mode=FILE_READ)
    chk.load(z)
    (u_, p_, B_, E_) = z.split()
    z_last_u.assign(u_)

(x, y) = SpatialCoordinate(Z.mesh())

eps = lambda x: sym(grad(x))
F = (
      2 * nu * inner(eps(u), eps(v))*dx
    + advect * inner(dot(grad(u), u), v) * dx
    + gamma * inner(div(u), div(v)) * dx
    + S * inner(vcross(B, E), v) * dx
    + S * inner(vcross(B, scross(u, B)), v) * dx
    - inner(p, div(v)) * dx
    - inner(div(u), q) * dx
    + inner(E, Ff) * dx
    + S2 * inner(scross(u, B), Ff) * dx
    - 1/Rem * inner(B, vcurl(Ff)) * dx
    + inner(vcurl(E), C) * dx
    + eta * inner(div(B), div(C)) * dx
    + gamma2 * inner(div(B), div(C)) * dx
    )

def compute_rhs(u_ex, B_ex, p_ex, E_ex):
    E_ex_ = interpolate(E_ex, R)
    f1 = (-2 * nu * div(eps(u_ex)) + advect * dot(grad(u_ex),u_ex) - gamma * grad(div(u_ex))
          + grad(p_ex) + S * vcross(B_ex, (E_ex + scross(u_ex, B_ex))))
    f2 = + vcurl(E_ex_) - eta * grad(div(B_ex)) - gamma2 * grad(div(B_ex))
    f3 = -1/Rem * scurl(B_ex) + E_ex + S2 * scross(u_ex, B_ex)
    #print("||f1||_L^2 = %s" % sqrt(assemble(inner(f1, f1)*dx)))
    #print("||f2||_L^2 = %s" % sqrt(assemble(inner(f2, f2)*dx)))
    #print("||f3||_L^2 = %s" % sqrt(assemble(inner(f3, f3)*dx)))
    #exit(-1)
    return (f1, f2, f3)
    

if testproblem == "hartmann":
    u_ex = as_vector([cos(y), Constant(0, domain=mesh)])
    B_ex = as_vector([Constant(0, domain=mesh), cos(x)])
    p_ex = -x * cos(y)
    pintegral = assemble(p_ex*dx)
    p_ex = p_ex - Constant(pintegral/area)
    E_ex = x
    
    (f1, f2, f3) = compute_rhs(u_ex, B_ex, p_ex, E_ex)
    rhs = True
    solution_known = True
    bc_varying = False

elif testproblem == "hartmann2":
    Sc = Constant(1)
    Ha = sqrt(Re*Rem*Sc) 
    if float(Re) * float(Rem) > 5000:
        G = 2*Ha/Re
        approxsinh = exp(Ha*(y-1/2)) - exp(Ha*(-y-1/2))
        b = G/(2*Sc) * (approxsinh - 2*y)
        u_ex = as_vector([(G*Re / (2*Ha)) * (1 - ( exp(Ha*(y-1/2)) + exp(Ha*(-y-1/2)))), 0])
    else:
        G = 2*Ha*sinh(Ha/2) / (Re * (cosh(Ha/2) - 1))
        b = G/(2*Sc) * (sinh(y*Ha)/sinh(Ha/2) - 2*y)
        u_ex = as_vector([(G*Re / (2*Ha*tanh(Ha/2))) * (1 - (cosh(y*Ha)/cosh(Ha/2))), 0])
    
    p_ex = -G*x - Sc/2 * b**2
    pintegral = assemble(p_ex*dx)
    p_ex = p_ex - Constant(pintegral/area)
    B_ex = as_vector([b, Constant(1, domain=mesh)])
    E_ex = 1/Rem * scurl(B_ex) - scross(u_ex, B_ex)
    
    (f1, f2, f3) = compute_rhs(u_ex, B_ex, p_ex, E_ex)
    rhs = True
    solution_known = True
    bc_varying = True

elif testproblem == "ldc":
   #example taken from chapter 5.1 in doi.org/10.1137/16M1074084
   #u_ex = as_vector([1*(x-0.5)*(x-0.5)*(x+0.5)*(x+0.5)*(0.25*(y+0.5)*(y+0.5)), 0])
   u_ex = Constant((1, 0), domain=mesh)
   #B_ex = Constant((-1, 0), domain=mesh)
   B_ex = Constant((0, 1), domain=mesh)
   B_ex = project(B_ex, W)
   
   bcs = [DirichletBC(Z.sub(0), u_ex, 4),  #4 == upper boundary (y==1)
          DirichletBC(Z.sub(0), 0, (1,2,3)),
          DirichletBC(Z.sub(2), B_ex, "on_boundary"),
          DirichletBC(Z.sub(3), 0 , "on_boundary"),
          PressureFixBC(Z.sub(1), 0, 1)]
   rhs = None
   solution_known = False
   bc_varying = False

elif testproblem == "Wathen":
    #example taken from chapter 6.2.1 in doi.org/a10.1137/16M1098991
    u_ex = as_vector([x*y*exp(x+y) + x*exp(x+y), -x*y*exp(x+y) - y*exp(x+y)])
    p_ex = exp(y) * sin(x)
    pintegral = assemble(p_ex*dx)
    p_ex = p_ex - Constant(pintegral/area)
    B_ex = as_vector([exp(x+y)*cos(x), exp(x+y)*sin(x) - exp(x+y)*cos(x)])
    E_ex = sin(x)

    (f1, f2, f3) = compute_rhs(u_ex, B_ex, p_ex, E_ex)
    rhs = True
    solution_known = True
    bc_varying = False

if solution_known:  
    u_ex_ = project(u_ex, V, solver_parameters=lu)
    B_ex_ = project(B_ex, W, solver_parameters=lu)
    p_ex_ = project(p_ex, Q, solver_parameters=lu)
    E_ex_ = project(E_ex, R, solver_parameters=lu)

if stab:
    if solution_known:
       u_ex_ = project(u_ex, V, solver_parameters=lu)
       z_last_u.assign(u_ex_)
    stabilisation_form_u = BurmanStab(u, v, z_last_u, stab_weight, mesh)
    F += (advect * stabilisation_form_u)
          
if rhs is not None:
    F -= inner(f1, v) * dx + inner(f3, Ff) * dx + inner(f2, C) * dx

if solution_known:
    bcs = [DirichletBC(Z.sub(0), u_ex, "on_boundary"),
           DirichletBC(Z.sub(2), B_ex , "on_boundary"),
           DirichletBC(Z.sub(3), E_ex , "on_boundary"),
           PressureFixBC(Z.sub(1), 0, 1)]
#           DirichletBC(Z.sub(2), 0, ((1,3), ), method="geometric")] # Doesn't work for DG pressures, does for CG

w = TrialFunction(Z)
[w_u, w_p, w_B, w_E] = split(w)
    
J_newton = ufl.algorithms.expand_derivatives(derivative(F, z, w))

if linearisation == "newton":
    J = J_newton

elif linearisation == "picardG":
    J_picardG = (
          J_newton
        #- S * inner(vcross(w_B, E), v) * dx               #J_tilde    #inner(B, E_n, v) * dx
        #- S * inner(vcross(B, scross(w_u, B)), v) * dx    #D  #inner(vcross(B_n, scross(u, B_n)), v) * dx
        #- S * inner(vcross(w_B, scross(u, B)), v) * dx    #D_1_tilde  #inner(vcross(B, scross(u_n, B_n)), v) * dx
        #- S * inner(vcross(B, scross(u, w_B)), v) * dx    #D_2_tilde  #inner(vcross(B_n, scross(u_n, B)), v) * dx
        #- inner(scross(u, w_B), Ff) * dx              #G_tilde    #inner(scross(u_n, B), Ff) * dx
        - inner(scross(w_u, B), Ff) * dx              #G          #inner(scross(u, B_n), Ff) * dx
        #- S * inner(vcross(B, w_E), v) * dx               #J          #inner(vcross(B_n, E), v) * dx
        #- advect * inner(dot(grad(u), w_u), v) * dx   # - advect * inner(dot(grad(u_n), u), v) * dx
        #- advect * inner(dot(grad(w_u), u), v) * dx   # - advect * inner(dot(grad(u), u_n), v) * dx
    )
    J = J_picardG
    
elif linearisation == "picard":
    J_picard = (
          J_newton
        - S * inner(vcross(w_B, E), v) * dx               #J_tilde    #inner(B, E_n, v) * dx
        #- S * inner(vcross(B, scross(w_u, B)), v) * dx    #D  #inner(vcross(B_n, scross(u, B_n)), v) * dx
        - S * inner(vcross(w_B, scross(u, B)), v) * dx    #D_1_tilde  #inner(vcross(B, scross(u_n, B_n)), v) * dx
        - S * inner(vcross(B, scross(u, w_B)), v) * dx    #D_2_tilde  #inner(vcross(B_n, scross(u_n, B)), v) * dx
        - inner(scross(u, w_B), Ff) * dx              #G_tilde    #inner(scross(u_n, B), Ff) * dx
        #- inner(scross(w_u, B), Ff) * dx              #G          #inner(scross(u, B_n), Ff) * dx
        #- S * inner(vcross(B, w_E), v) * dx               #J          #inner(vcross(B_n, E), v) * dx
        #- advect * inner(dot(grad(u), w_u), v) * dx   # - advect * inner(dot(grad(u_n), u), v) * dx
        #- advect * inner(dot(grad(w_u), u), v) * dx   # - advect * inner(dot(grad(u), u_n), v) * dx
    )
    J = J_picard
    
else:
    raise ValueError("only know newton, picardG and picard as linearisation method")

problem = NonlinearVariationalProblem(F, z, bcs, J=J)
 
appctx = {"Re": char_L * char_U / nu, "gamma": gamma, "nu": 1/Re, "Rem": Rem, "gamma2": gamma2}
params = solvers[args.solver_type]
#print("args.solver_type", args.solver_type)
#import pprint
#pprint.pprint(params)

solver = NonlinearVariationalSolver(problem, solver_parameters=params, options_prefix="", appctx=appctx)
qtransfer = NullTransfer()
Etransfer = NullTransfer()
vtransfer = SVSchoeberlTransfer((1/Re, gamma), 2, hierarchy)
Btransfer = SVSchoeberlTransfer((1/Rem, gamma2), 2, hierarchy)
dgtransfer = DGInjection()
def traceinject(src, dest):
    out = inject(src, dest)
    with dest.dat.vec_ro as x:
        print("Injecting %s -> %s: result has norm %s" % (src, dest, x.norm()))
    import sys; sys.stdout.flush()
    return out

transfers = {
                V.ufl_element(): (vtransfer.prolong, vtransfer.restrict, inject),
                #W.ufl_element(): (Btransfer.prolong, Btransfer.restrict, inject),
                Q.ufl_element(): (prolong, restrict, qtransfer.inject),
                R.ufl_element(): (prolong, restrict, Etransfer.inject),
                VectorElement("DG", mesh.ufl_cell(), args.k): (dgtransfer.prolong, restrict, dgtransfer.inject),
            }
transfermanager = TransferManager(native_transfers=transfers)
solver.set_transfer_manager(transfermanager)

#import firedrake.dmhooks
#firedrake.dmhooks.push_appctx(W.dm, solver._ctx)

results = {}
#res = [1, 500, 1000, 2000, 5000]
#rems = [1, 1000, 5000, 10000]#, 100, 1000,  2000, 5000, 10000]
res =   args.Re #[1, 10, 100, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000]#[1, 100, 500, 1000, 2000, 3000, 5000, 7000, 10000]#[args.Re]#, 100, 500]#[args.Re]#, 300, 500, 700, 1000]#[args.Re]
rems =  args.Rem #[1, 10, 100, 1000, 2000, 3000, 5000, 7000, 10000]#[args.Rem]

if multiprocessing:
    res = [1, 100, 500, 1000, 2000, 3000, 5000, 7000, 10000]
    rems = [1, 100, 500, 1000, 2000, 3000, 5000, 7000, 10000]


def run(re, rem):
       (u, p, B, E) = z.split()
       nu.assign(char_L * char_U / re)
       Re.assign(re)
       Rem.assign(rem)

       if bc_varying:
           u_ex_ = project(u_ex, V, solver_parameters=lu)
           B_ex_ = project(B_ex, W, solver_parameters=lu)
           p_ex_ = project(p_ex, Q, solver_parameters=lu)
           E_ex_ = project(E_ex, R, solver_parameters=lu)

           if not solution_known:
               raise ValueError("Sorry, don't know how to reconstruct the BCs")
           else:
               # bcs[0] is u, bcs[1] is B, bcs[2] is E
               bcs[0].function_arg = u_ex_
               bcs[1].function_arg = B_ex_
               bcs[2].function_arg = E_ex_

       if solution_known and exing or testproblem == "hartmann2" and float(Re) == 1.0 :
           if re >= 1000 or rem >= 1000:
                fac = Constant(0.1)
           else:
                fac = Constant(0.5)
                
           dis_vec = as_vector([fac * cos(y), fac * sin(x)])
           dis_scalar = fac * sin(x)
           dis_vec_u_ = project(dis_vec, V)
           dis_vec_p_ = project(dis_scalar, Q)
           dis_vec_B_ = project(dis_vec, W)
           dis_vec_E_ = project(dis_scalar, R)

           u_ex_dis_ = project(u_ex, V, solver_parameters=lu) + dis_vec_u_
           p_ex_dis_ = project(p_ex, Q, solver_parameters=lu) + dis_vec_p_
           B_ex_dis_ = project(B_ex, W, solver_parameters=lu) + dis_vec_B_
           E_ex_dis_ = project(E_ex, R, solver_parameters=lu) + dis_vec_E_

           u.assign(u_ex_dis_)
           p.assign(p_ex_dis_)
           B.assign(B_ex_dis_)
           E.assign(E_ex_dis_)
           
       elif float(Re) > 2 and checkpoint:
          chk = DumbCheckpoint("dump"+str(float(Rem))+str(linearisation), mode=FILE_READ)
          chk.load(z)
       advect.assign(1)
       stab_weight.assign(5e-3)
       S.assign(1)
       S2.assign(1)

       message(GREEN % ("Solving for Re = %s, Rem = %s, gamma = %s, gamma2 = %s ,nref=%s"
                        % (float(re), float(rem), float(gamma), float(gamma2), int(nref))))

       start = datetime.now()
       try:
           solver.solve()
       except:
           f = open('results'+str(linearisation)+str(testproblem)+'/'+str(float(Re))+str(float(Rem))+'.txt','w+')
           f.write("({0:2.0f}){1:4.1f}".format(0, 0))
           #return {"Re": re, "Rem": rem, "krylov/nonlin": 0, "nonlinear_iter": 0}
       end = datetime.now()


       linear_its = solver.snes.getLinearSolveIterations()
       nonlinear_its = solver.snes.getIterationNumber()
       time = (end-start).total_seconds() / 60

       if mesh.comm.rank == 0:
           print(GREEN % ("Time taken: %.2f min in %d nonlinear iterations, %d linear iterations (%.2f Krylov iters per Newton step)"
                          % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its))))
           print("%.2f @ %d @ %d @ %.2f" % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its)))
   
       (u, p, B, E) = z.split()

       pintegral = assemble(p*dx)
       p.assign(p - Constant(pintegral/area))

       B.rename("MagneticField")
       u.rename("VelocityField")
       p.rename("Pressure")
       E.rename("ElectricFieldf")

       norm_div_u = sqrt(assemble(inner(div(u), div(u))*dx))
       norm_div_B = sqrt(assemble(inner(div(B), div(B))*dx))
           
       if mesh.comm.rank == 0:
           print("||div(u)||_L^2 = %s" % norm_div_u)
           print("||div(B)||_L^2 = %s" % norm_div_B)


       if solution_known:
           B_ex_ = project(B_ex, B.function_space())
           u_ex_ = project(u_ex, u.function_space())
           p_ex_ = project(p_ex, p.function_space())
           E_ex_ = project(E_ex, E.function_space())
           B_ex_.rename("ExactSolutionB")
           u_ex_.rename("ExactSolutionu")
           p_ex_.rename("ExactSolutionp")
           E_ex_.rename("ExactSolutionE")

           error_u = errornorm(u_ex_, u, 'L2')
           error_B = errornorm(B_ex_, B, 'L2')
           error_E = errornorm(E_ex_, E, 'L2')
           error_p = errornorm(p_ex_, p, 'L2')

           errors=[error_u,error_p,error_B, error_E]
           if mesh.comm.rank == 0:
               print("Error ||u_ex - u||_L^2 = %s" % error_u)
               print("Error ||p_ex - p||_L^2 = %s" % error_p)
               print("Error ||B_ex - B||_L^2 = %s" % error_B)
               print("Error ||E_ex - E||_L^2 = %s" % error_E)
           #File("output/mhd.pvd").write(u, u_ex_, p, p_ex_, B, B_ex_, E, E_ex_, time=float(rem))
           #print("%s,%s,%s,%s" % (error_u,error_p,error_B, error_E))
               
           sys.stdout.flush()
           info_dict = {
               "Re": re,
               "Rem": rem,
               "krylov/nonlin": linear_its/nonlinear_its,
               "nonlinear_iter": nonlinear_its,
               "error_u": error_u,
               "error_p": error_p,
               "error_B": error_B,
               "error_E": error_E,
           }
           results[(rem,re)] = info_dict

           #if mesh.comm.rank == 0:
           #     error_u = Function(u.function_space())
           #     error_u.assign(u_ex_ - u)
           #     error_u.rename("ErrorVelocity")
           #     error_B = Function(B.function_space())
           #     error_B.assign(B_ex_ - B)
           #     error_B.rename("ErrorMagneticField")
           #     error_p = Function(p.function_space())
           #     error_p.assign(p_ex_ - p)
           #     error_p.rename("ErrorPressure")
           #error_E.assign(E_ex_ - E)
           #error_E.rename("ErrorElectricField")
           #File("output/errors.pvd").write(error_u, error_B, error_p, time=float(rem))
       else:
           #File("output/mhd.pvd").write(u, p, B, sigma, time=re_)
           norm_div_u = sqrt(assemble(inner(div(u), div(u))*dx))
           norm_div_B = sqrt(assemble(inner(div(B), div(B))*dx))
           message("||div(u)||_L^2 = %s" % norm_div_u)
           message("||div(B)||_L^2 = %s" % norm_div_B)
           norm_B = sqrt(assemble(inner(B, B)*dx))
           message("||B||_L^2 = %s" % norm_B)
           norm_B_B_ex = sqrt(assemble(inner(B-B_ex, B-B_ex)*ds))
           message("||B-B_ex||_L^2(boundary) = %s" % norm_B_B_ex)
           u_ex_ = project(u_ex, u.function_space())
           File("output/mhd.pvd").write(B,  time=float(rem))
           info_dict = {
               "Re": re,
               "Rem": rem,
               "krylov/nonlin": linear_its/nonlinear_its,
               "nonlinear_iter": nonlinear_its,
           }
           results[(rem,re)] = info_dict

       message(BLUE % info_dict)
       if mesh.comm.rank == 0:
          dir = 'results'+str(linearisation)+str(testproblem)+'/'
          if not os.path.exists(dir):
              os.mkdir(dir)
          f = open(dir+str(float(Re))+str(float(Rem))+'.txt','w+')
          f.write("({0:2.0f}){1:4.1f}".format(float(info_dict["nonlinear_iter"]),float(info_dict["krylov/nonlin"])))
          
       z_last_u.assign(u)
       chk = DumbCheckpoint("dump"+str(float(Rem))+str(linearisation), mode=FILE_CREATE)
       chk.store(z)

       

def print_results(Res, Rems, res_list):
    temp = sys.stdout
    f = open('output_ldc.txt','a')
    f.write(str(sys.argv))
    sys.stdout = f
    print("")
    print("%s, k=%s, nref=%s, stab=%s, linearisation=%s, solver=%s" % (testproblem, k, nref, stab, linearisation, solver_type))
    print("  Rem\Re ", end = '')
    iterRems = iter(Rems)
    for re in Res:
        print("%8s" % re, end = ' ')
    for i, result in enumerate(res_list):
        if (i) % len(Res) == 0:
           print(" ")
           print("%8s" % next(iterRems) , end = ' ')
        print("({0:2.0f}){1:4.1f}".format(float(result["nonlinear_iter"]),float(result["krylov/nonlin"])) , end = ' ')
    print(" "); print("")
    sys.stdout = temp
    f.close()
    #with open('output_ldc.txt','r') as f:
    #    print(f.read())

if multiprocessing:
    rerem= []
    for rem in rems:
        for re in res:
            rerem.append([re,rem])#

    with Pool(processes=20) as pool:
        res_list = pool.starmap(run, rerem)
    #print_results(res, rems, res_list)
else:
    res_list = []
    for rem in rems:
        for re in res:
            run(re, rem)
    #print_results(res, rems, res_list)

#if mesh.comm.rank == 0:
#       for result in res_list:
#          print(BLUE % result)
#          with open('output_verbose_ldc.txt','a') as f:
#              f.write("%s, k=%s, nref=%s, stab=%s, linearisation=%s, solver=%s\n" % (testproblem, k, nref, stab, linearisation, solver_type))
#              f.write(str(result)+"\n"+""+"\n")

