from firedrake import *
from datetime import datetime
import argparse
import alfi

def scross(x, y):
    return x[0]*y[1] - x[1]*y[0]

def vcross(x,y):
    return as_vector([x[1]*y,-x[0]*y])

def scurl(x):
    return x[1].dx(0) - x[0].dx(1)

def vcurl(x):
    return as_vector([x.dx(1), -x.dx(0)])

def acurl(x):
    return as_vector([
                     x[2].dx(1),
                     -x[2].dx(0),
                     x[1].dx(0) - x[0].dx(1)
                     ])

distribution_parameters={"partition": True, "overlap_type": (DistributedMeshOverlapType.VERTEX, 1)}

lu = {
   "mat_type": "aij",
   "snes_type": "newtonls",
   "snes_monitor": None,
   "ksp_type": "gmres",
   "ksp_monitor_true_residual": None,
   "pc_type": "lu",
   "pc_factor_mat_solver_type": "mumps",
   "mat_mumps_icntl_14": 200,
     }

star = {
   "mat_type": "aij",
   "snes_type": "newtonls",
   "snes_monitor": None,
   "snes_atol": 0,
   "snes_rtol": 1.0e-9,
   "ksp_type": "fgmres",
   "ksp_norm_type": "unpreconditioned",
   "ksp_monitor_true_residual": None,
   "ksp_atol": 0,
   "ksp_rtol": 1.0e-10,
   "pc_type": "mg",
   "pc_mg_cycle_type": "v",
   "pc_mg_type": "full",
   "mg_levels_ksp_type": "gmres",
   "mg_levels_ksp_convergence_test": "skip",
   "mg_levels_ksp_max_it": 3,
   "mg_levels_pc_type": "python",
   "mg_levels_pc_python_type": "firedrake.PatchPC",
   "mg_levels_patch_pc_patch_save_operators": True,
   "mg_levels_patch_pc_patch_partition_of_unity": True,
   "mg_levels_patch_pc_patch_sub_mat_type": "seqdense",
   "mg_levels_patch_sub_ksp_type": "preonly",
   "mg_levels_patch_sub_pc_type": "lu",
       }

solvers = {"lu": lu, "star": star}

parser = argparse.ArgumentParser(add_help=False)
parser.add_argument("--baseN", type=int, default=10)
parser.add_argument("--k", type=int, default=2)
parser.add_argument("--nref", type=int, default=3)
parser.add_argument("--Re", type=float, default=1)

parser.add_argument("--solver-type", choices=list(solvers.keys()), default="lu")
parser.add_argument("--discr", choices=["pkp0", "sv"], default = "pkp0")

args, _ = parser.parse_known_args()
baseN = args.baseN
k = args.k
nref = args.nref
nu = Constant(1/args.Re)
Re = Constant(args.Re)
solver_type = args.solver_type
discr = args.discr
#hierarchy = args.hierarchy


hierarchy = "uniform"
star["mg_levels_patch_pc_patch_construct_type"] = "star"
star["mg_levels_patch_pc_patch_construct_dim"] = 0


def before(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+1)

def after(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+2)

base = UnitSquareMesh(baseN, baseN, diagonal="crossed", distribution_parameters=distribution_parameters)
        
if hierarchy == "bary":
    mh = alfi.BaryMeshHierarchy(base, nref, callbacks=(before, after))
elif hierarchy == "uniformbary":
    bmesh = Mesh(bary(base._plex), distribution_parameters={"partition": False})
    mh = MeshHierarchy(bmesh, nref, reorder=True, callbacks=(before, after),
                       distribution_parameters=distribution_parameters)
elif hierarchy == "uniform":
    mh = MeshHierarchy(base, nref, reorder=True, callbacks=(before, after),
             distribution_parameters=distribution_parameters)
else:
    raise NotImplementedError("Only know bary, uniformbary and uniform for the hierarchy.")

mesh = mh[-1]

def message(msg):
    if mesh.comm.rank == 0:
        warning(msg)

n = FacetNormal(mesh)
t = as_vector([n[1], -n[0]])
h = CellDiameter(mesh)
h_avg = (h('+') + h('-'))/2
alpha = Constant(4)
gammaa = Constant(8.0)
        
V = FunctionSpace(mesh, "RT", k)

u = Function(V)
v = TestFunction(V)

(x, y) = SpatialCoordinate(V.mesh())
u_ex = as_vector([sin(x+0.5)*cos(y+0.5), cos(3*x+0.1)*sin(x*y-0.3)])
#u_ex = as_vector([sin(y), cos(x)])
#u_ex = as_vector([100*x*(1-x)*y*(1-y), 1*x*(1-x)*y*(1-y)])
#u_ex = as_vector([y, y])

F = (
    nu * (
         inner(grad(u), grad(v)) * dx
       #- inner(dot(grad(u_ex),n), v) * ds

       + dot(avg(grad(v[0])), jump(u[0], n))*dS
       + dot(avg(grad(v[1])), jump(u[1], n))*dS 
       - dot(jump(v[0], n), avg(grad(u[0])))*dS 
       - dot(jump(v[1], n), avg(grad(u[1])))*dS
       + alpha/h_avg *  dot(jump(u[0], n) , jump(v[0], n)) * dS
       + alpha/h_avg *  dot(jump(u[1], n) , jump(v[1], n)) * dS
       + dot(grad(v[0]), u[0]*n)*ds 
       + dot(grad(v[1]), u[1]*n)*ds
       - dot(v[0]*n, grad(u[0]))*ds 
       - dot(v[1]*n, grad(u[1]))*ds 
       + (gammaa/h)*dot(v[0], u[0])*ds
       + (gammaa/h)*dot(v[1], u[1])*ds
        )
    )



f = -nu * div(grad(u_ex))
F -= inner(f, v) * dx  + (
                #+ dot(grad(v[0]), u_ex[0]*n)*ds
                #+ dot(grad(v[1]), u_ex[1]*n)*ds
                #- dot(v[0]*n, grad(u_ex[0]))*ds
                #- dot(v[1]*n, grad(u_ex[1]))*ds
                +  u_ex[0] * dot(grad(v[0]) , n) * ds
                +  u_ex[1] * dot(grad(v[1]) , n) * ds
                + (gammaa/h)*dot(v[0], u_ex[0])*ds
                + (gammaa/h)*dot(v[1], u_ex[1])*ds
                )

bcs =  [DirichletBC(V, u_ex, "on_boundary")]

#bcs = [DirichletBC(V.sub(0), u_ex[0], 1),
#       DirichletBC(V.sub(0), u_ex[0], 2),
#       DirichletBC(V.sub(1), u_ex[1], 3),
#       DirichletBC(V.sub(1), u_ex[1], 4)]


problem = NonlinearVariationalProblem(F, u, bcs)
params = solvers[args.solver_type]
solver = NonlinearVariationalSolver(problem, solver_parameters=params)

Res = [Re]
    
for re_ in Res:
     nu.assign(1/re_)

     message(GREEN % ("Solving for Re = %s" % (float(re_))))
     start = datetime.now()
     solver.solve()
     end = datetime.now()

     linear_its = solver.snes.getLinearSolveIterations()
     nonlinear_its = solver.snes.getIterationNumber()
     time = (end-start).total_seconds() / 60

     print(GREEN % ("Time taken: %.2f min in %d nonlinear iterations, %d linear iterations (%.2f Krylov iters per Newton step)" % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its))))
     print("%.2f @ %d @ %d @ %.2f" % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its)))

     print("||div(u)||_L^2 = %s" % sqrt(assemble(inner(div(u), div(u))*dx)))

     u_ex_ = project(u_ex, u.function_space())
     error_u = errornorm(u_ex_, u, 'L2')

     u_ex_.rename("ExactSolutionu")
     u.rename("VelocityField")
     File("output/ns.pvd").write(u, u_ex_)
     print("Error ||u_ex - u||_L^2 = %s" % error_u)

     error_u = Function(u.function_space())
     error_u.assign(abs(u_ex_ - u))
     error_u.rename("ErrorVelocity")
     File("output/errors.pvd").write(error_u)
                                                                                                             
