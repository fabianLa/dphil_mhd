# -*- coding: utf-8 -*-
from firedrake import *
from datetime import datetime
import alfi
import argparse
import numpy
from pyop2.datatypes import IntType
import ufl.algorithms
from alfi.stabilisation import *
from alfi.transfer import *
from alfi import *

distribution_parameters={"partition": True, "overlap_type": (DistributedMeshOverlapType.VERTEX, 1)}

def scross(x, y):
    return x[0]*y[1] - x[1]*y[0]

def vcross(x,y):
    return as_vector([x[1]*y,-x[0]*y])

def scurl(x):
    return x[1].dx(0) - x[0].dx(1)

def vcurl(x):
    return as_vector([x.dx(1), -x.dx(0)])

def acurl(x):
    return as_vector([
                     x[2].dx(1),
                     -x[2].dx(0),
                     x[1].dx(0) - x[0].dx(1)
                     ])

def BurmanStab(B, C, supg_wind, mesh):
    n = FacetNormal(mesh)
    h = FacetArea(mesh)
    beta = sqrt(avg(facet_avg(dot(supg_wind, n)**2)))
    gamma1 = Constant(5e-3) # as chosen in doi:10.1016/j.apnum.2007.11.001
    stabilisation_form = 0.5 * gamma1 * h**2 * beta * dot(jump(grad(B), n), jump(grad(C), n))*dS
    return stabilisation_form

class ns(AuxiliaryOperatorPC):
    def form(self, pc, V, U):
        mesh = V.function_space().mesh()
        [u, p] = split(U)
        [v, q] = split(V)
        h = CellDiameter(mesh)

        state = self.get_appctx(pc)['state']
        [u_n, p_n] = split(state)

        eps = lambda x: sym(grad(x))

        A =  (
                2 /Re * inner(eps(u), eps(v))*dx
              + advect * inner(dot(grad(u_n), u), v) * dx
              + advect * inner(dot(grad(u), u_n), v) * dx
              + gamma * inner(div(u), div(v)) * dx
              - inner(p, div(v)) * dx
              - inner(div(u), q) * dx
              + inner(vcross(B_ex, scross(u, B_ex)), v) * dx
                        )
        if stab:
            stabilisation_form = BurmanStab(u, v, z_last_u, mesh)
            A = A + advect * stabilisation_form
        
        bcs = [DirichletBC(V.function_space().sub(0), 0, "on_boundary"),
               PressureFixBC(V.function_space().sub(1), 0, 1),
        ]

        return (A, bcs)

class SchurPC2(AuxiliaryOperatorPC):
    def form(self, pc, V, U):
        mesh = V.function_space().mesh()
        [u, p] = split(U)
        [v, q] = split(V)
        h = CellDiameter(mesh)

        state = self.get_appctx(pc)['state']
        [u_n, p_n] = split(state)

        eps = lambda x: sym(grad(x))

        A =  (
                2 /Re * inner(eps(u), eps(v))*dx
              -1/(nu+gamma) * inner(p, q) * dx
            )
        bcs = [DirichletBC(V.function_space().sub(0), 0, "on_boundary"),
               PressureFixBC(V.function_space().sub(1), 0, 1),
        ]

        return (A, bcs)

class PressureFixBC(DirichletBC):
    def __init__(self, V, val, subdomain, method="topological"):
        super().__init__(V, val, subdomain, method)
        sec = V.dm.getDefaultSection()
        dm = V.mesh()._plex

        coordsSection = dm.getCoordinateSection()
        coordsDM = dm.getCoordinateDM()
        dim = dm.getCoordinateDim()
        coordsVec = dm.getCoordinatesLocal()

        (vStart, vEnd) = dm.getDepthStratum(0)
        indices = []
        for pt in range(vStart, vEnd):
            x = dm.getVecClosure(coordsSection, coordsVec, pt).reshape(-1, dim).mean(axis=0)
            if x.dot(x) == 0.0: # fix [0, 0] in original mesh coordinates (bottom left corner)
                if dm.getLabelValue("pyop2_ghost", pt) == -1:
                    indices = [pt]
                break

        nodes = []
        for i in indices:
            if sec.getDof(i) > 0:
                nodes.append(sec.getOffset(i))

        if V.mesh().comm.rank == 0:
            nodes = [0]
        else:
            nodes = []
        self.nodes = numpy.asarray(nodes, dtype=IntType)

        if len(self.nodes) > 0:
            print("Fixing nodes %s" % self.nodes)
        else:
            print("Not fixing any nodes")
        import sys; sys.stdout.flush()

lu = {
     "snes_type": "newtonls",
     "snes_monitor": None,
     "snes_atol": 1.0e-7,
     "snes_rtol": 1.0e-8,
     "snes_linesearch_type": "l2",
     #"snes_linesearch_monitor": None,
     "snes_linesearch_maxstep": 1,
     #"snes_converged_reason": None,
     #"snes_max_it": 1000,
     #"snes_convergence_test": "skip",
     "mat_type": "aij",
     "ksp_type": "preonly",
     "pc_type": "lu",
     "pc_factor_mat_solver_type": "mumps",
     "mat_mumps_icntl_14": 300,
         }
        
lu2 = {
     "snes_type": "newtonls",
     "snes_monitor": None,
     "snes_atol": 1.0e-7,
     "snes_rtol": 1.0e-8,
     "snes_linesearch_type": "l2",
     #"snes_linesearch_monitor": None,
     "snes_linesearch_maxstep": 1,
     #"snes_converged_reason": None,
     #"snes_max_it": 1000,
     #"snes_convergence_test": "skip",
     "mat_type": "aij",
     "ksp_type": "fgmres",
     "ksp_monitor_true_residual": None,
     "pc_type": "python",
     "pc_python_type": "__main__.SchurPC",
     "aux_pc_type": "lu",
     #"pc_type": "lu",
     "aux_pc_factor_mat_solver_type": "mumps",
     "aux_mat_mumps_icntl_14": 300,
         }

fslu = {
   "snes_type": "newtonls",
   "snes_max_it": 30,
   "snes_linesearch_type": "l2",
   "snes_linesearch_maxstep": 1.0,
   "snes_rtol": 1.0e-8,
   "snes_atol": 1.0e-9,
   "snes_monitor": None,
   "snes_converged_reason": None,
   "ksp_type": "fgmres",
   "ksp_atol": 1.0e-10,
   "ksp_rtol": 1.0e-8,
   "ksp_monitor_true_residual": None,
   #"ksp_monitor_true_residual": None,
   #"ksp_converged_reason": None,
   "pc_type": "python",
   "pc_python_type": "__main__.SchurPC",
   #"aux_pc_type": "lu",
   #"aux_pc_factor_mat_solver_type": "mumps",
   #"aux_mat_mumps_icntl_14": 200,
   "aux_pc_type": "fieldsplit",
   "aux_pc_fieldsplit_type": "schur",
   "aux_pc_fieldsplit_schur_factorization_type": "full",
   "aux_pc_fieldsplit_schur_precondition": "user",
   "aux_fieldsplit_0": {
           "ksp_type": "preonly",
           "ksp_max_it": 1,
           "pc_type": "lu",
           "pc_factor_mat_solver_type": "mumps",
           "mat_mumps_icntl_14": 150,
       },
   "aux_fieldsplit_1":{
           "ksp_type": "preonly",
           "pc_type": "python",
           "pc_python_type": "alfi.solver.DGMassInv"
       },
   }

fsstar = {
   "snes_type": "newtonls",
   "snes_max_it": 30,
   "snes_linesearch_type": "l2",
   "snes_linesearch_maxstep": 1.0,
   "snes_rtol": 1.0e-8,
   "snes_atol": 1.0e-9,
   "snes_monitor": None,
   "snes_converged_reason": None,
   "ksp_type": "fgmres",
   "ksp_atol": 1.0e-7,
   "ksp_rtol": 1.0e-7,
   "ksp_monitor_true_residual": None,
   #"ksp_monitor_true_residual": None,
   #"ksp_converged_reason": None,
   #"pc_type": "python",
   #"pc_python_type": "__main__.SchurPC",
   #"aux_pc_type": "lu",
   #"aux_pc_factor_mat_solver_type": "mumps",
   #"aux_mat_mumps_icntl_14": 200,
   "pc_type": "fieldsplit",
   "pc_fieldsplit_type": "schur",
   "pc_fieldsplit_schur_factorization_type": "full",
   "pc_fieldsplit_schur_precondition": "user",
   "fieldsplit_0": {
          "ksp_type": "gmres",
          "ksp_max_it": 1,
          "ksp_norm_type": "unpreconditioned",
          "pc_type": "mg",
          "pc_mg_cycle_type": "v",
          "pc_mg_type": "full",
          "mg_levels_ksp_type": "fgmres",
          "mg_levels_ksp_convergence_test": "skip",
          "mg_levels_ksp_max_it": 6,
          "mg_levels_ksp_norm_type": "unpreconditioned",
          #"mg_levels_ksp_monitor_true_residual": None,
          "mg_levels_pc_type": "python",
          "mg_levels_pc_python_type": "firedrake.PatchPC",
          "mg_levels_patch_pc_patch_save_operators": True,
          "mg_levels_patch_pc_patch_partition_of_unity": False,
          "mg_levels_patch_pc_patch_sub_mat_type": "seqaij",
          "mg_levels_patch_pc_patch_construct_dim": 0,
          "mg_levels_patch_pc_patch_construct_type": "python",
          "mg_levels_patch_pc_patch_construct_python_type": "alfi.MacroStar",
          "mg_levels_patch_sub_ksp_type": "preonly",
          "mg_levels_patch_sub_pc_type": "lu",
          "mg_levels_patch_sub_pc_factor_mat_solver_type": "petsc",
          "mg_coarse_pc_type": "python",
          "mg_coarse_pc_python_type": "firedrake.AssembledPC",
          "mg_coarse_assembled": {
                "mat_type": "aij",
                "pc_type": "telescope",
                "pc_telescope_reduction_factor": 1,
                "pc_telescope_subcomm_type": "contiguous",
                "telescope_pc_type": "lu",
                "telescope_pc_factor_mat_solver_type": "superlu_dist",
            }
       },
   "fieldsplit_1":{
           "ksp_type": "preonly",
           "pc_type": "python",
           "pc_python_type": "alfi.solver.DGMassInv"
       },
   }

fsstarschur = {
   "snes_type": "newtonls",
   "snes_max_it": 30,
   "snes_linesearch_type": "l2",
   "snes_linesearch_maxstep": 1.0,
   "snes_rtol": 1.0e-8,
   "snes_atol": 1.0e-9,
   "snes_monitor": None,
   "snes_converged_reason": None,
   "ksp_type": "fgmres",
   "ksp_atol": 1.0e-7,
   "ksp_rtol": 1.0e-7,
   "ksp_monitor_true_residual": None,
   "pc_type": "python",
   "pc_python_type": "__main__.ns",
   "aux_pc_type": "fieldsplit",
   "aux_pc_fieldsplit_type": "schur",
   "aux_pc_fieldsplit_schur_factorization_type": "full",
   "aux_pc_fieldsplit_schur_precondition": "user",
   "aux_mg_transfer_manager": __name__ + ".transfermanager",
   "aux_fieldsplit_0": {
          "ksp_type": "gmres",
          "ksp_max_it": 1,
          "ksp_norm_type": "unpreconditioned",
          "pc_type": "mg",
          "mg_transfer_manager": __name__ + ".transfermanager",
          "pc_mg_cycle_type": "v",
          "pc_mg_type": "full",
          "mg_levels_ksp_type": "fgmres",
          "mg_levels_ksp_convergence_test": "skip",
          "mg_levels_ksp_max_it": 6,
          "mg_levels_ksp_norm_type": "unpreconditioned",
          "mg_levels_ksp_monitor_true_residual": None,
          "mg_levels_pc_type": "python",
          "mg_levels_pc_python_type": "firedrake.PatchPC",
          "mg_levels_patch_pc_patch_save_operators": True,
          "mg_levels_patch_pc_patch_partition_of_unity": False,
          "mg_levels_patch_pc_patch_sub_mat_type": "seqaij",
          "mg_levels_patch_pc_patch_construct_dim": 0,
          "mg_levels_patch_pc_patch_construct_type": "python",
          "mg_levels_patch_pc_patch_construct_python_type": "alfi.MacroStar",
          "mg_levels_patch_sub_ksp_type": "preonly",
          "mg_levels_patch_sub_pc_type": "lu",
          "mg_levels_patch_sub_pc_factor_mat_solver_type": "petsc",
          "mg_coarse_pc_type": "python",
          "mg_coarse_pc_python_type": "firedrake.AssembledPC",
          "mg_coarse_assembled": {
                "mat_type": "aij",
                "pc_type": "telescope",
                "pc_telescope_reduction_factor": 1,
                "pc_telescope_subcomm_type": "contiguous",
                "telescope_pc_type": "lu",
                "telescope_pc_factor_mat_solver_type": "superlu_dist",
            }
       },
   "aux_fieldsplit_1":{
           "ksp_type": "preonly",
           "pc_type": "python",
           "pc_python_type": "alfi.solver.DGMassInv"
       },
   }

solvers = {"lu": lu, "lu2": lu2, "fslu": fslu, "fsstar": fsstar, "fsstarschur": fsstarschur}

parser = argparse.ArgumentParser(add_help=False)
parser.add_argument("--baseN", type=int, default=10)
parser.add_argument("--k", type=int, default=3)
parser.add_argument("--nref", type=int, default=1)
parser.add_argument("--Re", type=float, default=1)
parser.add_argument("--gamma", type=float, default=10000)
parser.add_argument("--advect", type=float, default=1)
parser.add_argument("--hierarchy", choices=["bary", "uniform"], default="bary")
parser.add_argument("--solver-type", choices=list(solvers.keys()), default="lu")
parser.add_argument("--testproblem", choices=["ldc", "hartmann", "Wathen", "hartmann2", "hartmann3"], default = "hartmann")
parser.add_argument("--stab", default=False, action="store_true")

args, _ = parser.parse_known_args()
baseN = args.baseN
k = args.k
nref = args.nref
Re = Constant(args.Re)
gamma = Constant(args.gamma)
hierarchy = args.hierarchy
stab = args.stab
solver_type = args.solver_type
testproblem = args.testproblem
advect = Constant(args.advect)  
nu = Constant(1.0)


base = UnitSquareMesh(baseN, baseN, diagonal="crossed", distribution_parameters=distribution_parameters)

def before(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+1)

def after(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+2)
    #rebalance(dm, i)

if hierarchy == "bary":
    mh = alfi.BaryMeshHierarchy(base, nref, callbacks=(before, after))
elif hierarchy == "uniformbary":
    bmesh = Mesh(bary(base._plex), distribution_parameters={"partition": False})
    mh = MeshHierarchy(bmesh, nref, reorder=True, callbacks=(before, after),
                       distribution_parameters=distribution_parameters)
elif hierarchy == "uniform":
    mh = MeshHierarchy(base, nref, reorder=True, callbacks=(before, after),
             distribution_parameters=distribution_parameters)
else:
    raise NotImplementedError("Only know bary, uniformbary and uniform for the hierarchy.")
for m in mh:
    m.coordinates.dat.data[:, 0] -= 0.5
    m.coordinates.dat.data[:, 1] -= 0.5
mesh = mh[-1]

area = assemble(Constant(1, domain=mh[0])*dx)

def message(msg):
    if mesh.comm.rank == 0:
        warning(msg)


V = VectorFunctionSpace(mesh, "CG", k)  #u
Q = FunctionSpace(mesh, "DG", k-1)      #p
Z = MixedFunctionSpace([V, Q])

z = Function(Z)
(u, p) = split(z)
(v, q)   = split(TestFunction(Z))
z_last_u = Function(V)


(x, y) = SpatialCoordinate(Z.mesh())
n = FacetNormal(mesh)
t = as_vector([n[1], -n[0]])

eps = lambda x: sym(grad(x))


#example taken from chapter 6.2.1 in doi.org/a10.1137/16M1098991
u_ex = as_vector([x*y*exp(x+y) + x*exp(x+y), -x*y*exp(x+y) - y*exp(x+y)])
p_ex = exp(y) * sin(x)
pintegral = assemble(p_ex*dx)
p_ex = p_ex - Constant(pintegral/area)
B_ex = as_vector([exp(x+y)*cos(x), exp(x+y)*sin(x) - exp(x+y)*cos(x)])
solution_known = True

    
u_ex_ = project(u_ex, V, solver_parameters=lu)
p_ex_ = project(p_ex, Q, solver_parameters=lu)
z.sub(0).assign(u_ex_)
z.sub(1).assign(p_ex_)


F = (
      2 /Re * inner(eps(u), eps(v))*dx
    + advect * inner(dot(grad(u), u), v) * dx
    + gamma * inner(div(u), div(v)) * dx
    - inner(p, div(v)) * dx
    - inner(div(u), q) * dx
    + inner(vcross(B_ex, scross(u, B_ex)), v) * dx
    )

f = -2 /Re * div(eps(u_ex)) + advect * dot(grad(u_ex),u_ex) - gamma * grad(div(u_ex)) + grad(p_ex) +vcross(B_ex, scross(u_ex, B_ex))

F -= inner(f, v) * dx

bcs =  [DirichletBC(Z.sub(0), u_ex, "on_boundary"),  
           PressureFixBC(Z.sub(1), 0, 1)
       ]

if stab:
    z_last_u.assign(project(u_ex, V))
    stabilisation_form_u = BurmanStab(u, v, z_last_u, mesh)
    F += (advect * stabilisation_form_u)

appctx = {"Re": Re, "gamma": gamma, "nu": 1/Re}

params = solvers[args.solver_type]

problem = NonlinearVariationalProblem(F, z, bcs)

solver = NonlinearVariationalSolver(problem, solver_parameters=params, appctx=appctx)
qtransfer = NullTransfer()
vtransfer = SVSchoeberlTransfer((1/Re, gamma), 2, hierarchy)
transfers = {
                V.ufl_element(): (vtransfer.prolong, vtransfer.restrict, inject),
                Q.ufl_element(): (prolong, restrict, qtransfer.inject),
            }
transfermanager = TransferManager(native_transfers=transfers)
solver.set_transfer_manager(transfermanager)

results = {}
res = [1, 10, 100, 2000]

fac = 0.05
dis_vec = as_vector([fac * cos(y), fac * sin(x)])
dis_scalar = fac * sin(x)
dis_vec_u_ = project(dis_vec, V)
dis_vec_p_ = project(dis_scalar, Q)

u_ex_dis_ = u_ex_ + dis_vec_u_
p_ex_dis_ = p_ex_ + dis_vec_p_


for re_ in res:
    (u, p) = z.split()

    u.assign(u_ex_dis_)
    p.assign(p_ex_dis_)

    #advect.assign(1)
    nu.assign(1 / re_)
    Re.assign(re_)

    message(GREEN % ("Solving for Re = %s, gamma = %s ,nref=%s" % (float(re_), float(gamma), int(nref))))

    start = datetime.now()
    solver.solve()
    end = datetime.now()

    linear_its = solver.snes.getLinearSolveIterations()
    nonlinear_its = solver.snes.getIterationNumber()
    time = (end-start).total_seconds() / 60

    print(GREEN % ("Time taken: %.2f min in %d nonlinear iterations, %d linear iterations (%.2f Krylov iters per Newton step)" % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its))))
    print("%.2f @ %d @ %d @ %.2f" % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its)))

    (u, p) = z.split()

    pintegral = assemble(p*dx)
    p.assign(p - Constant(pintegral/area))

    print("||div(u)||_L^2 = %s" % sqrt(assemble(inner(div(u), div(u))*dx)))


    u.rename("VelocityField")
    p.rename("Pressure")

    if solution_known:
        u_ex_ = project(u_ex, u.function_space())
        p_ex_ = project(p_ex, p.function_space())
        u_ex_.rename("ExactSolutionu")
        p_ex_.rename("ExactSolutionp")

        error_u = errornorm(u_ex_, u, 'L2')
        error_p = errornorm(p_ex_, p, 'L2')



        print("Error ||u_ex - u||_L^2 = %s" % error_u)
        print("Error ||p_ex - p||_L^2 = %s" % error_p)
        info_dict = {
            "Re": re_,
            "krylov/nonlin": linear_its/nonlinear_its,
            "error_u": error_u,
            "error_p": error_p,

        }
        results[re_] = info_dict
        
    z_last_u.assign(u)

for rem in results:
        print(results[rem])
