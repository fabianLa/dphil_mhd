# -*- coding: utf-8 -*-
from firedrake import *
import argparse
from datetime import datetime
import numpy
import alfi
from alfi.stabilisation import *
from alfi.transfer import *
from alfi import *

distribution_parameters={"partition": True, "overlap_type": (DistributedMeshOverlapType.VERTEX, 2)}

def scross(x, y):
    return x[0]*y[1] - x[1]*y[0]

def scurl(x):
    return x[1].dx(0) - x[0].dx(1)

def vcurl(x):
    return as_vector([x.dx(1), -x.dx(0)])

lu = {
   "mat_type": "aij",
   "snes_type": "newtonls",
   "snes_monitor": None,
   "ksp_type": "gmres",
   #"ksp_monitor_true_residual": None,
   "pc_type": "lu",
   "ksp_max_it": 1,
   #"pc_svd_monitor": None, 
   "pc_factor_mat_solver_type": "mumps",
   "mat_mumps_icntl_14": 200,
     }

svd = {
   "mat_type": "aij",
   "snes_type": "newtonls",
   "snes_monitor": None,
   "ksp_type": "gmres",
   "ksp_monitor_true_residual": None,
   "pc_type": "svd",
   "pc_svd_monitor": None, 
   "pc_factor_mat_solver_type": "mumps",
   "mat_mumps_icntl_14": 200,
     }

fs = {
   "mat_type": "aij",
   "snes_type": "newtonls",
   "snes_monitor": None,
   "snes_atol": 1.0e-7,
   "snes_rtol": 1.0e-10,
   "ksp_type": "fgmres",
   "ksp_norm_type": "unpreconditioned",
   "ksp_monitor_true_residual": None,
   "ksp_atol": 1.0e-7,
   "ksp_rtol": 1.0e-7,
   "pc_type": "mg",
   "pc_mg_cycle_type": "v",
   "pc_mg_type": "full",
   "mg_levels_ksp_type": "fgmres",
   #"fieldsplit_1_aux_mg_levels_ksp_richardson_scale": 1,
   "mg_levels_ksp_convergence_test": "skip",
   "mg_levels_ksp_max_it": 6,
   "mg_levels_ksp_norm_type": "unpreconditioned",
   "mg_levels_ksp_monitor_true_residual": None,
   "mg_levels_pc_type": "python",
   "mg_levels_pc_python_type": "firedrake.PatchPC",
   "mg_levels_patch_pc_patch_save_operators": True,
   "mg_levels_patch_pc_patch_partition_of_unity": False,
   "mg_levels_patch_pc_patch_sub_mat_type": "seqaij",
   "mg_levels_patch_pc_patch_construct_dim": 0,
   "mg_levels_patch_pc_patch_construct_type": "python",
   "mg_levels_patch_pc_patch_construct_python_type": "alfi.MacroStar",
   "mg_levels_patch_sub_ksp_type": "preonly",
   "mg_levels_patch_sub_pc_type": "lu",
   "mg_levels_patch_sub_pc_factor_mat_solver_type": "umfpack",
   #"mg_levels_patch_sub_mat_mumps_icntl_14": 200,
   "mg_coarse_pc_type": "python",
   "mg_coarse_pc_python_type": "firedrake.AssembledPC",
   "mg_coarse_assembled": {
         "mat_type": "aij",
         "pc_type": "telescope",
         "pc_telescope_reduction_factor": 1,
         "pc_telescope_subcomm_type": "contiguous",
         "telescope_pc_type": "lu",
         "telescope_pc_factor_mat_solver_type": "mumps",
         "telescope_pc_factor_mat_mumps_icntl_14": 200,
     }
       }

solvers = {"lu": lu, "svd": svd, "fs": fs}

parser = argparse.ArgumentParser(add_help=False)
parser.add_argument("--k", type=int, default=2)
parser.add_argument("--dim", type=int, required=True)
parser.add_argument("--nref", type=int, default=0)
parser.add_argument("--baseN", type=int, default=10)
parser.add_argument("--Rem", type=float, default=1)
parser.add_argument("--delta", type=float, default=1)
parser.add_argument("--solver-type", choices=list(solvers.keys()), default=lu)
parser.add_argument("--hierarchy", choices=["bary", "uniform"], default="bary")
args, _ = parser.parse_known_args()

Rem = Constant(args.Rem)
k = Constant(args.k)
hierarchy = args.hierarchy
Re = Constant(1)
nref = args.nref
dim = args.dim
delta = Constant(args.delta)

eta = 1/Rem

assert dim in [2, 3]

if dim == 2:
    base = UnitSquareMesh(args.baseN, args.baseN, distribution_parameters=distribution_parameters)
else:
    base = UnitCubeMesh(args.baseN, args.baseN, args.baseN, distribution_parameters=distribution_parameters)

def message(msg):
    if mesh.comm.rank == 0:
        warning(msg)

def before(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+1)

def after(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+2)
   
if hierarchy == "bary":
    mh = alfi.BaryMeshHierarchy(base, nref, callbacks=(before, after))
elif hierarchy == "uniformbary":
    bmesh = Mesh(bary(base._plex), distribution_parameters={"partition": False})
    mh = MeshHierarchy(bmesh, nref, reorder=True, 
                       distribution_parameters=distribution_parameters)
elif hierarchy == "uniform":
    mh = MeshHierarchy(base, nref, reorder=True,
             distribution_parameters=distribution_parameters)
else:
    raise NotImplementedError("Only know bary, uniformbary and uniform for the hierarchy.")
       
mesh = mh[-1]

n = FacetNormal(mesh)

if dim == 2:
    (x, y) = SpatialCoordinate(mesh)
    u_ex = as_vector([x*y*exp(x+y) + x*exp(x+y), -x*y*exp(x+y) - y*exp(x+y)])
else:
    (x, y, zz) = SpatialCoordinate(mesh)
    u_ex = as_vector([x*y*exp(x+y) + x*exp(x+y), -x*y*exp(x+y) - y*exp(x+y), Constant(0, domain=mesh)])

if dim == 2:
    W = FunctionSpace(mesh, "RT", args.k-1)    #B
    R = FunctionSpace(mesh, "CG", args.k-1)    #E
    Z = MixedFunctionSpace([R, W])
else:
    W = FunctionSpace(mesh, "N1div", args.k-1)    #B
    R = FunctionSpace(mesh, "N1curl", args.k-1)    #E
    Z = MixedFunctionSpace([R, W])


def project_hcurl(B_pot, R):
    B_proj = Function(R)
    C = TestFunction(R)
    F = (
          inner(B_proj, C)*dx
        + inner(curl(B_proj), curl(C))*dx
        - inner(B_pot, C)*dx
        - inner(curl(B_pot), curl(C))*dx
        )
    bcs = DirichletBC(R, B_pot, "on_boundary")
    nvproblem = NonlinearVariationalProblem(F, B_proj, bcs=bcs)
    solver = NonlinearVariationalSolver(nvproblem, solver_parameters=lu)
    solver.solve()
    return B_proj

if dim == 2:
   B_pot = -cos(y) + exp(y)
   B_ex = vcurl(B_pot)
   E_ex = x

   B_pot_proj = project(B_pot, R, solver_parameters=lu)
   B_ex_proj = vcurl(B_pot_proj)
   B_ex_proj = project(B_ex_proj, W, solver_parameters=lu)

   E_ex_proj = project(E_ex, R, solver_parameters=lu)
   f1 = vcurl(E_ex_proj) - eta * grad(div(B_ex))
   f1 = project(f1, W, solver_parameters=lu)
   f2 = -1/Rem * scurl(B_ex) + E_ex + delta * scross(u_ex, B_ex)
   
else:
   B_pot = as_vector([sin(y), sin(x), sin(zz)])
   B_ex = curl(B_pot)
   E_pot = x*y*zz + cos(zz)
   E_ex = grad(E_pot)

   #B_pot_proj = project_hcurl(B_pot, R)
   B_pot_proj = project(B_pot, R, solver_parameters=lu)
   B_ex_proj = curl(B_pot_proj)
   B_ex_proj = project(B_ex_proj, W, solver_parameters=lu)

   E_pot_proj = project(E_pot, FunctionSpace(mesh, "CG", args.k-1), solver_parameters=lu)
   E_ex_proj = grad(E_pot_proj)
   E_ex_proj = project(E_ex_proj, R, solver_parameters=lu)
   f1 = curl(E_ex_proj) - eta * grad(div(B_ex))
   #f1 = project(f1, W, solver_parameters=lu)
   f2 = -1/Rem * curl(B_ex) + E_ex + delta * cross(u_ex, B_ex)

z = Function(Z)
(E, B) = split(z)
(Ff, C) = split(TestFunction(Z))


if dim == 2:
    F = (
      eta * inner(div(B), div(C)) * dx
    + inner(vcurl(E), C) * dx
    - inner(f1, C) * dx
    - 1/Rem * inner(B, vcurl(Ff)) * dx
    + delta * inner(scross(u_ex, B), Ff) * dx
    + inner(E, Ff) * dx
    - inner(f2, Ff) * dx
    )
else:
    F = (
      eta * inner(div(B), div(C)) * dx
    + inner(curl(E), C) * dx
    - inner(f1, C) * dx
    - 1/Rem * inner(B, curl(Ff)) * dx
    + delta * inner(cross(u_ex, B), Ff) * dx
    + inner(E, Ff) * dx
    - inner(f2, Ff) * dx
    )

params = solvers[args.solver_type]

appctx = {"nu": 1/Rem, "gamma": 0}


bcs = [DirichletBC(Z.sub(1), B_ex_proj, "on_boundary"),
           DirichletBC(Z.sub(0), E_ex_proj, "on_boundary"),
    ]

nvproblem = NonlinearVariationalProblem(F, z, bcs=bcs)
solver = NonlinearVariationalSolver(nvproblem, solver_parameters=params)
qtransfer = NullTransfer()

dgtransfer = DGInjection()
rawtransfer = TransferManager(native_transfers={VectorElement("DG", mesh.ufl_cell(), args.k-1): (dgtransfer.prolong, restrict, dgtransfer.inject)})
class SVSchoeberlTransferDG(SVSchoeberlTransfer):
    def standard_transfer(self, source, target, mode):
        if mode == "prolong":
            rawtransfer.prolong(source, target)
        elif mode == "restrict":
            rawtransfer.restrict(source, target)
        else:
            raise NotImplementedError

vtransfer = SVSchoeberlTransferDG((1/Rem, 0), 2, hierarchy)
transfers = {
                #W.ufl_element(): (vtransfer.prolong, vtransfer.restrict, rawtransfer.inject),
                #R.ufl_element(): (prolong, restrict, qtransfer.inject),
                VectorElement("DG", mesh.ufl_cell(), args.k-1): (dgtransfer.prolong, restrict, dgtransfer.inject),
            }
transfermanager = TransferManager(native_transfers=transfers)
solver.set_transfer_manager(transfermanager)


message(GREEN % ("Solving for Rem = %s ,nref=%s" % (float(Rem), int(nref))))

start = datetime.now()
solver.solve()
end = datetime.now()

linear_its = solver.snes.getLinearSolveIterations()
nonlinear_its = solver.snes.getIterationNumber()
time = (end-start).total_seconds() / 60
print(GREEN % ("Time taken: %.2f min in %d nonlinear iterations, %d linear iterations (%.2f Krylov iters per Newton step)" % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its))))
print("%.2f @ %d @ %d @ %.2f" % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its)))

(E, B) = z.split()

if dim == 2:
   curl_E= vcurl(E)
   curl_E_ex = vcurl(E_ex)
else:
   curl_E= curl(E)
   curl_E_ex = curl(E_ex)


print("||div(B)||_L^2 = %s" % sqrt(assemble(inner(div(B), div(B))*dx)))
print("||div(B_ex)||_L^2 = %s" % sqrt(assemble(inner(div(B_ex), div(B_ex))*dx)))
print("||curl(E)||_L^2 = %s" % sqrt(assemble(inner(curl_E, curl_E)*dx)))
print("||curl(E_ex)||_L^2 = %s" % sqrt(assemble(inner(curl_E_ex, curl_E_ex)*dx)))

print("")
error_B_l2 = errornorm(B_ex, B, 'L2')
error_E_l2 = errornorm(E_ex, E, 'L2')
B_ex_ = project(B_ex, B.function_space(), solver_parameters=lu)
E_ex_ = project(E_ex, E.function_space(), solver_parameters=lu)
curl_E_proj = project(curl_E, B.function_space(), solver_parameters=lu)
B.rename("MagneticField")
B_ex_.rename("B_ex")
B_ex_proj.rename("B_ex_proj")
E_ex_proj.rename("E_ex_proj")
E.rename("E field")
E_ex_.rename("E_ex")
curl_E_proj.rename("curl_E_proj")
B_pot_proj.rename("B_pot_proj")
print("Error ||B_ex - B||_L^2 = %s" % error_B_l2)
print("Error ||E_ex - E||_L^2 = %s" % error_E_l2)
#print("Error ||B_ex - B||_hdiv = %s" % error_B_hdiv)
#print("Error ||E_ex - E||_hcurl = %s" % error_E_hcurl)
#File("output/magnetic.pvd").write(B, B_ex_, E, E_ex_, div_B_proj, B_ex_proj, E_ex_proj, curl_E_proj, B_pot_proj)
#File("output/magneticB.pvd").write(B)
   



