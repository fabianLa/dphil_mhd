import argparse
import sys

parser = argparse.ArgumentParser(add_help=False)
parser.add_argument("--Res", nargs='+', type=int)
parser.add_argument("--Rems", nargs='+', type=int)
parser.add_argument("--testproblem", type=str, required=True)

args, _ = parser.parse_known_args()
Rems = args.Rems
Res = args.Res
testproblem = args.testproblem

for linearisation in ["picard", "picardG", "newton"]:
    res_list = []
    for rem in Rems:
        for re in Res:
            try:
               with open('results/results'+str(linearisation)+str(testproblem)+'/'+str(float(re))+str(float(rem))+'.txt','r') as f:
                  res_list.append(f.read())
            except:
                res_list.append("    -   ")

    temp = sys.stdout           
    f = open('output_'+str(testproblem)+'.txt','a')
    sys.stdout = f
    print(linearisation)
    print("  Rem\Re   ", end = '')
    iterRems = iter(Rems)
    for re in Res:
            print("%8s &" % re, end = ' ')
    for i, result in enumerate(res_list):
            if (i) % len(Res) == 0:
               print(" ")
               print("%8s &" % next(iterRems) , end = ' ')
            print(result + " &" , end = ' ')
    print(" "); print("")

    sys.stdout = temp
    f.close()
    with open('output_'+str(testproblem)+'.txt','r') as f:
       print(f.read())
