import argparse
import sys
import numpy as np

parser = argparse.ArgumentParser(add_help=False)
parser.add_argument("--Res", nargs='+', type=int)
parser.add_argument("--Rems", nargs='+', type=int)

args, _ = parser.parse_known_args()
Rems = args.Rems
Res = args.Res

res_list = []
for rem in Rems:
    for re in Res:
        try:
           with open('results/divergence/'+str(float(re))+str(float(rem))+'.txt','r') as f:
              res_list.append(f.read())
        except:
            res_list.append("    -   ")

temp = sys.stdout
f = open('div_table.txt','a')
sys.stdout = f
print("  Rem\Re   ", end = '')

print(res_list)
iterRems = iter(Rems)
print("%14s" % " ", end = ' ')
for re in Res:
        print("%18s &" % re, end = ' ')
for i, result in enumerate(res_list):
        if (i) % len(Res) == 0:
           print(" ")
           print("%8s &" % next(iterRems) , end = ' ')
        result = np.fromstring(result, dtype=float, sep=', ')
        print('(%2.2E,  %2.2E) &'  % (result[0], result[1]), end = ' ')
print(" "); print("")

sys.stdout = temp
f.close()
with open('div_table.txt','r') as f:
   print(f.read())
