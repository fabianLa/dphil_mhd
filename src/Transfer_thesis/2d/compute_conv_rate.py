import numpy as np

with open("error.txt") as f:
    content = f.readlines()
    
content = [x.strip() for x in content]
print(content)
a = np.fromstring(content[0], dtype=float, sep=', ')
print("1 & %2.2E &  -   &" % (float(a[0])), end =" "),
print("%2.2E &  -   &" % (float(a[1])), end =" "),
print("%2.2E &  -   &" % (float(a[2])), end =" "),
print("%2.2E &  -   \\\\" % (float(a[3])))

for i in range(len(content)-1):
    a = np.fromstring(content[i], dtype=float, sep=', ')
    b = np.fromstring(content[i+1], dtype=float, sep=', ')
    conv_rate = np.log(a/b)/np.log(2)
    
    print("%s & %2.2E & %2.2f &" % (i+2, float(b[0]), float(conv_rate[0])), end =" "),
    print("%2.2E & %2.2f &" % (float(b[1]), float(conv_rate[1])), end =" "),
    print("%2.2E & %2.2f &" % (float(b[2]), float(conv_rate[2])), end =" "),
    print("%2.2E & %2.2f \\\\" % (float(b[3]), float(conv_rate[3])))
