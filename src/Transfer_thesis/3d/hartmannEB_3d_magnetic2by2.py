# -*- coding: utf-8 -*-
from firedrake import *
from datetime import datetime
import alfi
import argparse
import numpy
from pyop2.datatypes import IntType
import ufl.algorithms
from alfi.stabilisation import *
from alfi.transfer import *
from alfi import *
from  multiprocessing import *
import sys
import os

import petsc4py
petsc4py.PETSc.Sys.popErrorHandler()

distribution_parameters={"partition": True, "overlap_type": (DistributedMeshOverlapType.VERTEX, 2)}



lu = {
     "snes_type": "newtonls",
     "snes_monitor": None,
     "snes_atol": 1.0e-8,
     "snes_rtol": 1.0e-15,
     "snes_linesearch_type": "l2",
     #"snes_linesearch_monitor": None,
     "snes_linesearch_maxstep": 1,
     #"snes_converged_reason": None,
     #"snes_max_it": 1000,
     #"snes_convergence_test": "skip",
     "mat_type": "aij",
     "ksp_type": "preonly",
     #"ksp_monitor_true_residual": None,
     "pc_type": "lu",
     "pc_factor_mat_solver_type": "mumps",
     "mat_mumps_icntl_14": 300,
         }

mg_star = {
    "snes_type": "ksponly",
    "snes_monitor": None,
    "ksp_type": "fgmres",
    "ksp_max_it": 50,
    "ksp_atol": 1.0e-8,
    "ksp_rtol": 1.0e-8,
    "ksp_monitor_true_residual": None,#
    "ksp_converged_reason": None,
    "mat_type": "aij",
    "pc_type": "mg",
    "pc_mg_cycle_type": "v",
    "pc_mg_type": "full",
    "mg_levels_ksp_type": "fgmres",
    "mg_levels_ksp_convergence_test": "skip",
    "mg_levels_ksp_max_it": 20,
    "mg_levels_ksp_norm_type": "unpreconditioned",
    "mg_levels_ksp_monitor_true_residual": None,
    "mg_levels_pc_type": "python",
    "mg_levels_pc_python_type": "firedrake.PatchPC",
    "mg_levels_patch_pc_patch_save_operators": True,
    "mg_levels_patch_pc_patch_partition_of_unity": False,
    "mg_levels_patch_pc_patch_sub_mat_type": "seqaij",
    "mg_levels_patch_pc_patch_construct_dim": 0,
    #"mg_levels_patch_pc_patch_exclude_subspaces": "2",
    "mg_levels_patch_pc_patch_construct_type": "star",
    "mg_levels_patch_pc_patch_multiplicative": False,
    "mg_levels_patch_pc_patch_symmetrise_sweep": True,
    "mg_levels_patch_sub_ksp_type": "richardson",
    "mg_levels_patch_sub_ksp_monitor_true_residual": None,
    "mg_levels_patch_sub_pc_type": "lu",
    #"mg_levels_patch_sub_svd_monitor": None,
    "mg_levels_patch_sub_pc_factor_mat_solver_type": "mumps",
    "mg_levels_patch_sub_pc_factor_mat_mumps_icntl_14": 200,
    "mg_coarse_ksp_type": "richardson",
    "mg_coarse_ksp_max_it": 1,
    "mg_coarse_ksp_norm_type": "unpreconditioned",
    #"mg_coarse_ksp_monitor_true_residual": None,
    "mg_coarse_pc_type": "python",
    "mg_coarse_pc_python_type": "firedrake.AssembledPC",
    "mg_coarse_assembled": {
          "mat_type": "aij",
          "pc_type": "telescope",
          "pc_telescope_reduction_factor": 1,
          "pc_telescope_subcomm_type": "contiguous",
          "telescope_pc_type": "lu",
          "telescope_pc_factor_mat_solver_type": "mumps",
          "telescope_pc_factor_mat_mumps_icntl_14": 200,
      }
        }

mg_macrostar = {
    "snes_type": "ksponly",
    "snes_monitor": None,
    "ksp_type": "fgmres",
    "ksp_max_it": 50,
    "ksp_atol": 1.0e-8,
    "ksp_rtol": 1.0e-8,
    "ksp_monitor_true_residual": None,#
    "ksp_converged_reason": None,
    "mat_type": "aij",
    "pc_type": "mg",
    "pc_mg_cycle_type": "v",
    "pc_mg_type": "full",
    "mg_levels_ksp_type": "fgmres",
    "mg_levels_ksp_convergence_test": "skip",
    "mg_levels_ksp_max_it": 20,
    "mg_levels_ksp_norm_type": "unpreconditioned",
    "mg_levels_ksp_monitor_true_residual": None,
    "mg_levels_pc_type": "python",
    "mg_levels_pc_python_type": "firedrake.PatchPC",
    "mg_levels_patch_pc_patch_save_operators": True,
    #"mg_levels_patch_pc_patch_local_type": "multiplicative",
    "mg_levels_patch_pc_patch_partition_of_unity": False,
    "mg_levels_patch_pc_patch_sub_mat_type": "seqaij",
    "mg_levels_patch_pc_patch_construct_dim": 0,
    "mg_levels_patch_pc_patch_construct_type": "python",
    "mg_levels_patch_pc_patch_construct_python_type": "alfi.MacroStar",
    "mg_levels_patch_sub_ksp_type": "preonly",
    "mg_levels_patch_sub_pc_type": "lu",
    "mg_levels_patch_sub_pc_factor_mat_solver_type": "mumps",
    "mg_levels_patch_sub_pc_factor_mat_mumps_icntl_14": 200,
    "mg_coarse_ksp_type": "richardson",
    "mg_coarse_ksp_max_it": 1,
    "mg_coarse_ksp_norm_type": "unpreconditioned",
    #"mg_coarse_ksp_monitor_true_residual": None,
    "mg_coarse_pc_type": "python",
    "mg_coarse_pc_python_type": "firedrake.AssembledPC",
    "mg_coarse_assembled": {
          "mat_type": "aij",
          "pc_type": "telescope",
          "pc_telescope_reduction_factor": 1,
          "pc_telescope_subcomm_type": "contiguous",
          "telescope_pc_type": "lu",
          "telescope_pc_factor_mat_solver_type": "mumps",
          "telescope_pc_factor_mat_mumps_icntl_14": 200,
      }
        }




solvers = {"lu": lu, "mg_star": mg_star, "mg_macrostar": mg_macrostar}

parser = argparse.ArgumentParser(add_help=False)
parser.add_argument("--baseN", type=int, default=10)
parser.add_argument("--k", type=int, default=3)
parser.add_argument("--nref", type=int, default=1)
parser.add_argument("--Rem", nargs='+', type=float, default=[1])
parser.add_argument("--gamma2", type=float, default=0)
parser.add_argument("--hierarchy", choices=["bary", "uniform"], default="bary")
parser.add_argument("--solver-type", choices=list(solvers.keys()), default="lu")
parser.add_argument("--delta", type=float, default=1)
parser.add_argument("--alternative-bcs", default=False, action="store_true")

args, _ = parser.parse_known_args()
baseN = args.baseN
k = args.k
nref = args.nref
Rem = Constant(args.Rem[0])
delta = Constant(args.delta)
gamma2 = Constant(args.gamma2)
hierarchy = args.hierarchy
solver_type = args.solver_type
gamma2 = Constant(args.gamma2)
alternative_bcs = args.alternative_bcs

base = UnitCubeMesh(baseN, baseN, baseN, distribution_parameters=distribution_parameters)

def before(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+1)

def after(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+2)
    #rebalance(dm, i)

if hierarchy == "bary":
    mh = alfi.BaryMeshHierarchy(base, nref, callbacks=(before, after))
elif hierarchy == "uniformbary":
    bmesh = Mesh(bary(base._plex), distribution_parameters={"partition": False})
    mh = MeshHierarchy(bmesh, nref, reorder=True, callbacks=(before, after),
                       distribution_parameters=distribution_parameters)
elif hierarchy == "uniform":
    mh = MeshHierarchy(base, nref, reorder=True, callbacks=(before, after),
             distribution_parameters=distribution_parameters)
else:
    raise NotImplementedError("Only know bary, uniformbary and uniform for the hierarchy.")
for m in mh:
    m.coordinates.dat.data[:, 0] -= 0.5
    m.coordinates.dat.data[:, 1] -= 0.5
    m.coordinates.dat.data[:, 2] -= 0.5
mesh = mh[-1]

area = assemble(Constant(1, domain=mh[0])*dx)

def message(msg):
    if mesh.comm.rank == 0:
        warning(msg)

Rel = FiniteElement("N2curl", mesh.ufl_cell(), k, variant="integral")
R = FunctionSpace(mesh, Rel)    #E
Wel = FiniteElement("N2div", mesh.ufl_cell(), k-1, variant="integral")
W = FunctionSpace(mesh, Wel)
Z = MixedFunctionSpace([W, R])

z = Function(Z)
(B, E) = split(z)
(C, Ff)   = split(TestFunction(Z))

(x, y, zz) = SpatialCoordinate(Z.mesh())
n = FacetNormal(mesh)

#u_ex = as_vector([-exp(x+y+zz)*sin(y)+exp(x+y+zz)*sin(zz), sin(x)*exp(x+y+zz)-sin(zz)*exp(x+y+zz), -exp(x+y+zz)*sin(x)+exp(x+y+zz)*sin(y)])
pot = as_vector([sin(pi*(y+0.5))*sin(pi*(zz+0.5)), sin(pi*(x+0.5))*sin(pi*(zz+0.5)), sin(pi*(x+0.5))*sin(pi*(y+0.5))])
u_ex = 1/pi*curl(pot)
B_ex = as_vector([-exp(x+y+zz)*sin(y)+exp(x+y+zz)*sin(zz), sin(x)*exp(x+y+zz)-sin(zz)*exp(x+y+zz), -exp(x+y+zz)*sin(x)+exp(x+y+zz)*sin(y)])
E_ex = 1/Rem * curl(B_ex) - delta * cross(u_ex, B_ex)

eps = lambda x: sym(grad(x))
F = (
    + inner(E, Ff) * dx
    + delta * inner(cross(u_ex, B), Ff) * dx
    - 1/Rem * inner(B, curl(Ff)) * dx
    + inner(curl(E), C) * dx
    + 1/Rem * inner(div(B), div(C)) * dx
    + gamma2 * inner(div(B), div(C)) * dx
    )

E_ex_ = interpolate(E_ex, R)
f1 = curl(E_ex_) - 1/Rem  * grad(div(B_ex)) - gamma2 * grad(div(B_ex))
f2 = -1/Rem * curl(B_ex) + E_ex + delta * cross(u_ex, B_ex)
 
B_ex_ = interpolate(B_ex, W)

if rhs is not None:
    F -= inner(f2, Ff) * dx + inner(f1, C) * dx

if alternative_bcs:
    bcs = None
    F += +1/Rem * inner(cross(B_ex_, n), Ff) * ds - 1/Rem * inner(div(B_ex), dot(C, n)) * ds 
else:
    bcs = [DirichletBC(Z.sub(0), B_ex, "on_boundary"),
           DirichletBC(Z.sub(1), E_ex, "on_boundary")]


problem = NonlinearVariationalProblem(F, z, bcs)
 
appctx = {"Rem": Rem, "gamma2": gamma2}
params = solvers[args.solver_type]

solver = NonlinearVariationalSolver(problem, solver_parameters=params, options_prefix="", appctx=appctx)
qtransfer = NullTransfer()
Etransfer = NullTransfer()
Btransfer = SVSchoeberlTransfer((1/Rem, gamma2), 2, hierarchy)
dgtransfer = DGInjection()
def traceinject(src, dest):
    out = inject(src, dest)
    with dest.dat.vec_ro as x:
        print("Injecting %s -> %s: result has norm %s" % (src, dest, x.norm()))
    import sys; sys.stdout.flush()
    return out

transfers = {
                #R.ufl_element(): (prolong, restrict, Etransfer.inject),
                VectorElement("DG", mesh.ufl_cell(), args.k): (dgtransfer.prolong, restrict, dgtransfer.inject),
                VectorElement("DG", mesh.ufl_cell(), args.k-1): (dgtransfer.prolong, restrict, dgtransfer.inject),
            }
transfermanager = TransferManager(native_transfers=transfers)
solver.set_transfer_manager(transfermanager)

rems =  args.Rem 

def run(rem):
    (B, E) = z.split()
    Rem.assign(rem)

    message(GREEN % ("Solving for #dofs = %s, Rem = %s, gamma2 = %s, nref = %s"
                     % (Z.dim(), float(rem), float(gamma2), int(nref))))
    if not os.path.exists("results/"):
           os.mkdir("results/")

    start = datetime.now()
    solver.solve()
    end = datetime.now()

    linear_its = solver.snes.getLinearSolveIterations()
    nonlinear_its = solver.snes.getIterationNumber()
    time = (end-start).total_seconds() / 60

    if mesh.comm.rank == 0:
        if nonlinear_its == 0:
            nonlinear_its = 1
        print(GREEN % ("Time taken: %.2f min in %d nonlinear iterations, %d linear iterations (%.2f Krylov iters per Newton step)"
                       % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its))))
        print("%.2f @ %d @ %d @ %.2f" % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its)))

    (B, E) = z.split()

    B.rename("MagneticField")
    E.rename("ElectricFieldf")

    norm_div_B = sqrt(assemble(inner(div(B), div(B))*dx))
    #norm_curl_E = sqrt(assemble(inner(vcurl(E), vcurl(E))*dx))

    if mesh.comm.rank == 0:
        print("||div(B)||_L^2 = %s" % norm_div_B)
        #print("||curl(E)||_L^2 = %s" % norm_curl_E)

    B_ex_ = interpolate(B_ex, B.function_space())
    E_ex_ = interpolate(E_ex, E.function_space())
    B_ex_.rename("ExactSolutionB")
    E_ex_.rename("ExactSolutionE")

    error_B = errornorm(B_ex, B, 'L2')
    error_E = errornorm(E_ex, E, 'L2')

    error_B_bndy_n = sqrt(assemble(inner(dot((B_ex_-B),n), dot((B_ex_-B),n))*ds))
    error_B_bndy_t = sqrt(assemble(inner(cross((B_ex_-B),n), cross((B_ex_-B),n))*ds)) 
    error_E_bndy_n = sqrt(assemble(inner(dot((E_ex_-E),n), dot((E_ex_-E),n))*ds))
    error_E_bndy_t = sqrt(assemble(inner(cross((E_ex_-E),n), cross((E_ex_-E),n))*ds))

    errors=[error_B, error_E]
    if mesh.comm.rank == 0:
        print("Error ||B_ex - B||_L^2 = %s" % error_B)
        print("Error ||E_ex - E||_L^2 = %s" % error_E)

        if alternative_bcs:
            print("")
            print("Error ||(B_ex - B)*n||_L^2 = %s" % error_B_bndy_n)
            print("Error ||(B_ex - B)xn||_L^2 = %s" % error_B_bndy_t)
            print("Error ||(E_ex - E)*n||_L^2 = %s" % error_E_bndy_n)
            print("Error ||(E_ex - E)xn||_L^2 = %s" % error_E_bndy_t)


        f = open("error.txt",'a+')
        f.write("%s,%s\n" % (error_B, error_E))
        f.close()
    #File("output/mhd.pvd").write(u, u_ex_, p, p_ex_, B, B_ex_, E, E_ex_, time=float(rem))
    #print("%s,%s,%s,%s" % (error_u,error_p,error_B, error_E))

    sys.stdout.flush()
    info_dict = {
        "Rem": rem,
        "krylov/nonlin": linear_its/nonlinear_its,
        "nonlinear_iter": nonlinear_its,
        "error_B": error_B,
        "error_E": error_E,
    }
    #File("output/plots/"+str(testproblem)+"_"+str(int(float(Rem)))+"_"+str(int(float(Re)))+"u.pvd").write(u,u_ex_,  time=float(rem))
    #File("output/plots/"+str(testproblem)+"_"+str(int(float(Rem)))+"_"+str(int(float(Re)))+"B.pvd").write(B, B_ex_,  time=float(rem))

    message(BLUE % info_dict)
    if mesh.comm.rank == 0:
       dir = 'results/results'+'/'
       if not os.path.exists(dir):
           os.mkdir(dir)
       f = open(dir+str(float(Rem))+'.txt','w+')
       f.write("({0:2.0f}){1:4.1f}".format(float(info_dict["nonlinear_iter"]),float(info_dict["krylov/nonlin"])))
       f.close()
    
for rem in rems:
    try:
        run(rem)
    except Exception as e:
        message(e)
        dir = 'results/results'+'/'
        if not os.path.exists(dir):
            os.mkdir(dir)
        f = open(dir+str(float(rem))+'.txt','w+')
        f.write("({0:2.0f}){1:4.1f}".format(0,0))
        f.close()
