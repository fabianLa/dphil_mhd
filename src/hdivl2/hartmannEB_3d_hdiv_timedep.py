# -*- coding: utf-8 -*-x
from firedrake import *
from datetime import datetime
import alfi
import argparse
import numpy
from pyop2.datatypes import IntType
import ufl.algorithms
from alfi.stabilisation import *
from alfi.transfer import *
from alfi import *
from  multiprocessing import *
import sys
import os

import petsc4py
petsc4py.PETSc.Sys.popErrorHandler()

distribution_parameters={"partition": True, "overlap_type": (DistributedMeshOverlapType.VERTEX, 2)}

def BurmanStab(B, C, wind, stab_weight, mesh):
    n = FacetNormal(mesh)
    h = FacetArea(mesh)
    beta = avg(facet_avg(sqrt(inner(wind, wind)+1e-10)))
    gamma1 = stab_weight # as chosen in doi:10.1016/j.apnum.2007.11.001
    stabilisation_form = 0.5 * gamma1 * avg(h)**2 * beta * dot(jump(grad(B), n), jump(grad(C), n))*dS
    return stabilisation_form

def form_Burman2(u, v, wind, stab_weight, mesh):
    n = FacetNormal(mesh)
    h = FacetArea(mesh)
    beta = avg(facet_avg(sqrt(dot(wind, n)**2+1e-10)))
    #beta = avg(facet_avg(sqrt(inner(wind, wind)+1e-10)))
    return 0.5 * stab_weight * avg(h)**2 * beta * dot(jump(grad(u), n), jump(grad(v), n))*dS

class InnerSchurPC(AuxiliaryOperatorPC):
    def form(self, pc, C, B):
        mesh = C.function_space().mesh()
        n = FacetNormal(mesh)
        t = as_vector([n[1], -n[0]])
        h = CellDiameter(mesh)
        h_avg = (h('+') + h('-'))/2
        alpha = Constant(4)

        state = self.get_appctx(pc)['state']
        [u_n, p_n, B_n, E_n] = split(state)

        K = 1/Rem * (
              + inner(grad(B), grad(C))*dx
              + alpha/h_avg*inner(jump(C, t), jump(B, t))*dS
            )

        K = K - inner(cross(u_n, B), curl(C))*dx 

        bcs = [DirichletBC(C.function_space(), 0, "on_boundary")]

        return (K, bcs)

class SchurPC(AuxiliaryOperatorPC):
    def form(self, pc, V, U):
        mesh = V.function_space().mesh()
        [u, p] = split(U)
        [v, q] = split(V)
        Z = V.function_space()
        U_ = Function(Z)
        
        state = self.get_appctx(pc)['state']
        [u_n, p_n, B_n, E_n] = split(state)

        eps = lambda x: sym(grad(x))

        A =  (
              + 2/Re * inner(eps(u), eps(v))*dx
              + gamma * inner(div(u), div(v)) * dx
              - inner(p, div(v)) * dx
              - inner(div(u), q) * dx
              + S * inner(cross(B_n, cross(u, B_n)), v) * dx
             )

                
        if discr in ["rt", "bdm"]:
            h = CellVolume(mesh)/FacetArea(mesh)
            sigma = Constant(10) * Z.sub(0).ufl_element().degree()**2
            theta = Constant(1)
            uflux_int = 0.5*(dot(u, n) + theta*abs(dot(u, n)))*u
            uflux_ext = 0.5*(inner(u,n)+ theta*abs(inner(u,n)))*u + 0.5*(inner(u,n)- theta*abs(inner(u,n)))*u_ex
            A_DG = (
             - 1/Re * inner(avg(2*sym(grad(u))), 2*avg(outer(v, n))) * dS
             - 1/Re * inner(avg(2*sym(grad(v))), 2*avg(outer(u, n))) * dS
             + 1/Re * sigma/avg(h) * inner(2*avg(outer(u,n)),2*avg(outer(v,n))) * dS
             - inner(outer(v,n),2/Re*sym(grad(u)))*ds
             - inner(outer(u-u_ex,n),2/Re*sym(grad(v)))*ds
             + 1/Re*(sigma/h)*inner(v,u-u_ex)*ds
             - advect * dot(u,div(outer(v,u)))*dx
             + advect * dot(v('+')-v('-'), uflux_int('+')-uflux_int('-'))*dS
             + advect * dot(v,uflux_ext)*ds
               )

            A_DG = action(A_DG, U_)
            A_DG_linear = derivative(A_DG, U_, U)
            A_DG_linear = replace(A_DG_linear, {split(U_)[0]:u_n, split(U_)[1]:p_n})
            A = A + A_DG_linear
        elif discr in ["cg"]:
            A = A + advect * inner(dot(grad(u_n), u), v) * dx + advect * inner(dot(grad(u), u_n), v) * dx
            
        if stab:
            stabilisation2 = BurmanStabilisation(V.function_space().sub(0), state=z_last_u, h=FacetArea(mesh), weight=stab_weight)
            stabilisation_form_2 = stabilisation2.form(u, v)
            A = A + advect * stabilisation_form_2

        bcs = [DirichletBC(V.function_space().sub(0), 0, "on_boundary"),
               PressureFixBC(V.function_space().sub(1), 0, 1),
        ]

        A = inner(u, v) * dx + dt_factor*dt*A        
        return (A, bcs)
    

class HideousHackPC(AssembledPC):
            
    def update(self, pc):
        coeffs = [coeff for coeff in self.P.a.coefficients() if isinstance(coeff, Function)]
        assert len(coeffs) == 1

        state = self.get_appctx(pc)['state']
        #print("Assigning %s to %s %s" % (state, coeffs[0], coeffs[0].split()[0]))
        coeffs[0].split()[0].assign(state)
        AssembledPC.update(self, pc)
        import sys; sys.stdout.flush()

class Schur(AuxiliaryOperatorPC):
    def form(self, pc, V, U):
        mesh = V.function_space().mesh()
        [B, E] = split(U)
        [C, Ff] = split(V)
        h = CellDiameter(mesh)
        state = self.get_appctx(pc)['state']
        [u_n, p_n, B_n, E_n] = split(state)

        eps = lambda x: sym(grad(x))

        A =  (
             + inner(E, Ff) * dx
             + inner(cross(u_n, B), Ff) * dx
             - 1/Rem * inner(B, curl(Ff)) * dx
             + inner(curl(E), C) * dx
             + 1/Rem * inner(div(B), div(C)) * dx
             + gamma2 * inner(div(B), div(C)) * dx
                        )
    
        bcs = [DirichletBC(V.function_space().sub(0), 0, "on_boundary"),
               DirichletBC(V.function_space().sub(1), 0, "on_boundary"),
        ]

        return (A, bcs)

class PressureFixBC(DirichletBC):
    def __init__(self, V, val, subdomain, method="topological"):
        super().__init__(V, val, subdomain, method)
        sec = V.dm.getDefaultSection()
        dm = V.mesh()._topology_dm

        coordsSection = dm.getCoordinateSection()
        coordsDM = dm.getCoordinateDM()
        dim = dm.getCoordinateDim()
        coordsVec = dm.getCoordinatesLocal()

        (vStart, vEnd) = dm.getDepthStratum(0)
        indices = []
        for pt in range(vStart, vEnd):
            x = dm.getVecClosure(coordsSection, coordsVec, pt).reshape(-1, dim).mean(axis=0)
            if x.dot(x) == 0.0: # fix [0, 0] in original mesh coordinates (bottom left corner)
                if dm.getLabelValue("pyop2_ghost", pt) == -1:
                    indices = [pt]
                break

        nodes = []
        for i in indices:
            if sec.getDof(i) > 0:
                nodes.append(sec.getOffset(i))

        if V.mesh().comm.rank == 0:
            nodes = [0]
        else:
            nodes = []
        self.nodes = numpy.asarray(nodes, dtype=IntType)

        if len(self.nodes) > 0:
            print("Fixing nodes %s" % self.nodes)
        #else:
        #    print("Not fixing any nodes")
        import sys; sys.stdout.flush()

lu = {
     "snes_type": "newtonls",
     "snes_monitor": None,
     "snes_atol": 2.0e-8,
     "snes_rtol": 1.0e-13,
     "snes_linesearch_type": "l2",
     #"snes_linesearch_monitor": None,
     "snes_linesearch_maxstep": 1,
     #"snes_converged_reason": None,
     #"snes_max_it": 1000,
     #"snes_convergence_test": "skip",
     "mat_type": "aij",
     "ksp_type": "preonly",
     #"ksp_monitor_true_residual": None,
     "pc_type": "lu",
     "pc_factor_mat_solver_type": "mumps",
     "mat_mumps_icntl_14": 300,
         }

mstar = {
   "ksp_type": "fgmres",
   "ksp_max_it": 2,
   "pc_type": "mg",
   "pc_mg_cycle_type": "v",
   "pc_mg_type": "full",
   "mg_levels_ksp_type": "fgmres",
   #"fieldsplit_1_aux_mg_levels_ksp_richardson_scale": 1,
   "mg_levels_ksp_convergence_test": "skip",
   "mg_levels_ksp_max_it": 6,
   "mg_levels_ksp_norm_type": "unpreconditioned",
   #"_mg_levels_ksp_monitor_true_residual": None,
   "mg_levels_pc_type": "python",
   "mg_levels_pc_python_type": "firedrake.PatchPC",
   "mg_levels_patch_pc_patch_save_operators": True,
   "mg_levels_patch_pc_patch_partition_of_unity": False,
   "mg_levels_patch_pc_patch_sub_mat_type": "seqaij",
   "mg_levels_patch_pc_patch_construct_dim": 0,
   "mg_levels_patch_pc_patch_construct_type": "star",
   #"mg_levels_patch_pc_patch_construct_python_type": "alfi.MacroStar",
   "mg_levels_patch_sub_ksp_type": "preonly",
   "mg_levels_patch_sub_pc_type": "lu",
   "mg_levels_patch_sub_pc_factor_mat_solver_type": "umfpack",
   "mg_coarse_ksp_type": "richardson",
   "mg_coarse_ksp_max_it": 1,
   "mg_coarse_ksp_norm_type": "unpreconditioned",
   #"mg_coarse_ksp_monitor_true_residual": None,
   "mg_coarse_pc_type": "python",
   "mg_coarse_pc_python_type": "firedrake.AssembledPC",
   "mg_coarse_assembled": {
         "mat_type": "aij",
         "pc_type": "telescope",
         "pc_telescope_reduction_factor": 1,
         "pc_telescope_subcomm_type": "contiguous",
         "telescope_pc_type": "lu",
         "telescope_pc_factor_mat_solver_type": "mumps",
         "telescope_pc_factor_mat_mumps_icntl_14": 200,
     }
       }

mmacrostar = {
   "ksp_type": "fgmres",
   "ksp_max_it": 2,
   "pc_type": "mg",
   "pc_mg_cycle_type": "v",
   "pc_mg_type": "full",
   "mg_levels_ksp_type": "fgmres",
   #"fieldsplit_1_aux_mg_levels_ksp_richardson_scale": 1,                                                                               
   "mg_levels_ksp_convergence_test": "skip",
   "mg_levels_ksp_max_it": 6,
   "mg_levels_ksp_norm_type": "unpreconditioned",
   #"_mg_levels_ksp_monitor_true_residual": None,                                                                                                                         
   "mg_levels_pc_type": "python",
   "mg_levels_pc_python_type": "firedrake.PatchPC",
   "mg_levels_patch_pc_patch_save_operators": True,
   "mg_levels_patch_pc_patch_partition_of_unity": False,
   "mg_levels_patch_pc_patch_sub_mat_type": "seqaij",
   "mg_levels_patch_pc_patch_construct_dim": 0,
   "mg_levels_patch_pc_patch_construct_type": "python",
   "mg_levels_patch_pc_patch_construct_python_type": "alfi.MacroStar",                       
   "mg_levels_patch_sub_ksp_type": "preonly",
   "mg_levels_patch_sub_pc_type": "lu",
   "mg_levels_patch_sub_pc_factor_mat_solver_type": "umfpack",
   #"mg_levels_patch_sub_pc_factor_mat_solver_type": "mumps",
   #"mg_levels_patch_sub_pc_factor_mat__mumps_icntl_14": 200,
   "mg_coarse_ksp_type": "richardson",
   "mg_coarse_ksp_max_it": 1,
   "mg_coarse_ksp_norm_type": "unpreconditioned",
   #"mg_coarse_ksp_monitor_true_residual": None,
   "mg_coarse_pc_type": "python",
   "mg_coarse_pc_python_type": "firedrake.AssembledPC",
   "mg_coarse_assembled": {
         "mat_type": "aij",
         "pc_type": "telescope",
         "pc_telescope_reduction_factor": 1,
         "pc_telescope_subcomm_type": "contiguous",
         "telescope_pc_type": "lu",
         "telescope_pc_factor_mat_solver_type": "mumps",
         "telescope_pc_factor_mat_mumps_icntl_14": 200,
     }
       }

outerschurfsstar = {
   "ksp_type": "fgmres",
   "ksp_atol": 1.0e-7,
   "ksp_rtol": 1.0e-7,
   #"ksp_monitor_true_residual": None,
   #"ksp_converged_reason": None,
   "pc_type": "python",
   "pc_python_type": "__main__.SchurPC",
   "aux_mg_transfer_manager": __name__ + ".transfermanager",
   #"aux_pc_type": "lu",
   #"aux_pc_factor_mat_solver_type": "mumps",
   #"aux_mat_mumps_icntl_14": 200,
   "ksp_max_it": 2,
   "aux_mat_type": "nest",
   "aux_pc_type": "fieldsplit",
   "aux_pc_fieldsplit_type": "schur",
   "aux_pc_fieldsplit_schur_factorization_type": "full",
   "aux_pc_fieldsplit_schur_precondition": "user",
   "aux_fieldsplit_0": {
          "ksp_type": "gmres",
          "ksp_max_it": 1,
          "ksp_norm_type": "unpreconditioned",
          "pc_type": "mg",
          "pc_mg_cycle_type": "v",
          "pc_mg_type": "full",
          "mg_levels_ksp_type": "fgmres",
          "mg_levels_ksp_convergence_test": "skip",
          "mg_levels_ksp_max_it": 6,
          "mg_levels_ksp_norm_type": "unpreconditioned",
          #"mg_levels_ksp_monitor_true_residual": None,
          "mg_levels_pc_type": "python",
          "mg_levels_pc_python_type": "firedrake.ASMStarPC",
          "mg_levels_pc_star_backend": "tinyasm",
          "mg_levels_patch_pc_patch_save_operators": True,
          #"mg_levels_patch_pc_patch_partition_of_unity": False,
          #"mg_levels_patch_pc_patch_sub_mat_type": "seqaij",
          "mg_levels_patch_pc_patch_construct_dim": 0,
          #"mg_levels_patch_pc_patch_construct_type": "python",
          #"mg_levels_patch_pc_patch_construct_python_type": "alfi.MacroStar",
          #"mg_levels_patch_sub_ksp_type": "preonly",
          "mg_levels_patch_sub_pc_type": "lu",
          "mg_levels_patch_sub_pc_factor_mat_solver_type": "umfpack",
          #"mg_levels_patch_sub_mat_mumps_icntl_14": 200,
          "mg_coarse_ksp_type": "richardson",
          "mg_coarse_ksp_max_it": 1,
          "mg_coarse_ksp_norm_type": "unpreconditioned",
          #"mg_coarse_ksp_monitor_true_residual": None,
          "mg_coarse_pc_type": "python",
          #"mg_coarse_pc_factor_mat_solver_type": "mumps",
          #"mg_coarse_pc_factor_mat_mumps_icntl_14": 200,
          "mg_coarse_pc_python_type": "firedrake.AssembledPC",
          "mg_coarse_assembled": {
                "mat_type": "aij",
                "pc_type": "telescope",
                "pc_telescope_reduction_factor": 1,
                "pc_telescope_subcomm_type": "contiguous",
                "telescope_pc_type": "lu",
                "telescope_pc_factor_mat_solver_type": "mumps",
                "telescope_pc_factor_mat_mumps_icntl_14": 200,
            }
       },
   "aux_fieldsplit_1":{
           "ksp_type": "preonly",
           "pc_type": "python",
           "pc_python_type": "alfi.solver.DGMassInv"
       },
   }

outerschurfsmacrostar = {
   "ksp_type": "fgmres",
   "ksp_atol": 1.0e-7,
   "ksp_rtol": 1.0e-7,
   #"ksp_monitor_true_residual": None,
   #"ksp_converged_reason": None,
   "pc_type": "python",
   "pc_python_type": "__main__.SchurPC",
   "aux_mg_transfer_manager": __name__ + ".transfermanager",
   #"aux_pc_type": "lu",
   #"aux_pc_factor_mat_solver_type": "mumps",
   #"aux_mat_mumps_icntl_14": 200,
   "ksp_max_it": 2,
   "aux_mat_type": "nest",
   "aux_pc_type": "fieldsplit",
   "aux_pc_fieldsplit_type": "schur",
   "aux_pc_fieldsplit_schur_factorization_type": "full",
   "aux_pc_fieldsplit_schur_precondition": "user",
   "aux_fieldsplit_0": {
          "ksp_type": "gmres",
          "ksp_max_it": 1,
          "ksp_norm_type": "unpreconditioned",
          "pc_type": "mg",
          "pc_mg_cycle_type": "v",
          "pc_mg_type": "full",
          "mg_levels_ksp_type": "fgmres",
          "mg_levels_ksp_convergence_test": "skip",
          "mg_levels_ksp_max_it": 6,
          "mg_levels_ksp_norm_type": "unpreconditioned",
          #"mg_levels_ksp_monitor_true_residual": None,
          "mg_levels_pc_type": "python",
          "mg_levels_pc_python_type": "firedrake.PatchPC",
          "mg_levels_patch_pc_patch_save_operators": True,
          "mg_levels_patch_pc_patch_partition_of_unity": False,
          "mg_levels_patch_pc_patch_sub_mat_type": "seqaij",
          "mg_levels_patch_pc_patch_construct_dim": 0,
          "mg_levels_patch_pc_patch_construct_type": "python",
          "mg_levels_patch_pc_patch_construct_python_type": "alfi.MacroStar",
          "mg_levels_patch_sub_ksp_type": "preonly",
          "mg_levels_patch_sub_pc_type": "lu",
          "mg_levels_patch_sub_pc_factor_mat_solver_type": "umfpack",
          #"mg_levels_patch_sub_pc_factor_mat_solver_type": "mumps",
          #"mg_levels_patch_sub_pc_factor_mat__mumps_icntl_14": 200,
          "mg_coarse_ksp_type": "richardson",
          "mg_coarse_ksp_max_it": 1,
          "mg_coarse_ksp_norm_type": "unpreconditioned",
          #"mg_coarse_ksp_monitor_true_residual": None,
          "mg_coarse_pc_type": "python",
          #"mg_coarse_pc_factor_mat_solver_type": "mumps",
          #"mg_coarse_pc_factor_mat_mumps_icntl_14": 200,
          "mg_coarse_pc_python_type": "firedrake.AssembledPC",
          "mg_coarse_assembled": {
                "mat_type": "aij",
                "pc_type": "telescope",
                "pc_telescope_reduction_factor": 1,
                "pc_telescope_subcomm_type": "contiguous",
                "telescope_pc_type": "lu",
                "telescope_pc_factor_mat_solver_type": "mumps",
                "telescope_pc_factor_mat_mumps_icntl_14": 200,
            }
       },
   "aux_fieldsplit_1":{
           "ksp_type": "preonly",
           "pc_type": "python",
           "pc_python_type": "alfi.solver.DGMassInv"
       },
   }

nsmacro = {
   "ksp_type": "fgmres",
   "ksp_atol": 1.0e-7,
   "ksp_rtol": 1.0e-7,
   "ksp_max_it": 2,
   "mat_type": "nest",
   "pc_type": "fieldsplit",
   "pc_fieldsplit_type": "schur",
   "pc_fieldsplit_schur_factorization_type": "full",
   "pc_fieldsplit_schur_precondition": "user",
   "fieldsplit_0": {
          "ksp_type": "gmres",
          "ksp_max_it": 1,
          "ksp_norm_type": "unpreconditioned",
          "pc_type": "mg",
          "pc_mg_cycle_type": "v",
          "pc_mg_type": "full",
          "mg_levels_ksp_type": "fgmres",
          "mg_levels_ksp_convergence_test": "skip",
          "mg_levels_ksp_max_it": 6,
          "mg_levels_ksp_norm_type": "unpreconditioned",
          #"mg_levels_ksp_monitor_true_residual": None,
          "mg_levels_pc_type": "python",
          "mg_levels_pc_python_type": "firedrake.PatchPC",
          "mg_levels_patch_pc_patch_save_operators": True,
          "mg_levels_patch_pc_patch_partition_of_unity": False,
          "mg_levels_patch_pc_patch_sub_mat_type": "seqaij",
          "mg_levels_patch_pc_patch_construct_dim": 0,
          "mg_levels_patch_pc_patch_construct_type": "python",
          "mg_levels_patch_pc_patch_construct_python_type": "alfi.MacroStar",
          "mg_levels_patch_sub_ksp_type": "preonly",
          "mg_levels_patch_sub_pc_type": "lu",
          "mg_levels_patch_sub_pc_factor_mat_solver_type": "umfpack",
          #"mg_levels_patch_sub_mat_mumps_icntl_14": 200,
          "mg_coarse_ksp_type": "richardson",
          "mg_coarse_ksp_max_it": 1,
          "mg_coarse_ksp_norm_type": "unpreconditioned",
          #"mg_coarse_ksp_monitor_true_residual": None,
          "mg_coarse_pc_type": "python",
          #"mg_coarse_pc_factor_mat_solver_type": "mumps",
          #"mg_coarse_pc_factor_mat_mumps_icntl_14": 200,
          "mg_coarse_pc_python_type": "firedrake.AssembledPC",
          "mg_coarse_assembled": {
                "mat_type": "aij",
                "pc_type": "telescope",
                "pc_telescope_reduction_factor": 1,
                "pc_telescope_subcomm_type": "contiguous",
                "telescope_pc_type": "lu",
                "telescope_pc_factor_mat_solver_type": "mumps",
                "telescope_pc_factor_mat_mumps_icntl_14": 200,
            }
       },
   "fieldsplit_1":{
           "ksp_type": "preonly",
           "pc_type": "python",
           "pc_python_type": "alfi.solver.DGMassInv"
       },
   }

outerschurlu = {
   "ksp_type": "gmres",
   "ksp_max_it": 2,
   #"ksp_monitor_true_residual": None,
   #"ksp_converged_reason": None,
   "pc_type": "python",
   "pc_python_type": "__main__.SchurPC",
   "aux_pc_type": "lu",
   "aux_pc_factor_mat_solver_type": "mumps",
   "aux_mat_mumps_icntl_14": 200,
   #"aux_pc_type": "fieldsplit",
   #"aux_pc_fieldsplit_type": "schur",
   #"aux_pc_fieldsplit_schur_factorization_type": "full",
   #"aux_pc_fieldsplit_schur_precondition": "user",
   #"aux_fieldsplit_0": {
   #        "ksp_type": "preonly",
   #        "ksp_max_it": 1,
   #        "pc_type": "lu",
   #        "pc_factor_mat_solver_type": "mumps",
   #        "mat_mumps_icntl_14": 150,
   #    },
   #"aux_fieldsplit_1":{
   #         "ksp_type": "preonly",
   #        "pc_type": "python",
   #        "pc_python_type": "alfi.solver.DGMassInv"
   #    },
   }


fs2by2 = {
     "snes_type": "newtonls",
     "snes_max_it": 25,
     "snes_linesearch_type": "l2",
     "snes_linesearch_maxstep": 1.0,
     "snes_rtol": 1.0e-10,
     "snes_atol": 1.0e-6,
     #"snes_ksp_ew": None,
     #"snes_convergence_test": "skip",
     "snes_monitor": None,
     #"snes_linesearch_monitor": None,
     #"snes_converged_reason": None,
     "ksp_type": "fgmres",
     "ksp_max_it": 50,
     "ksp_atol": 1.0e-7,
     "ksp_rtol": 1.0e-5,
     "ksp_monitor_true_residual": None,
     "ksp_converged_reason": None,
     "mat_type": "aij",
     "pc_type": "fieldsplit",
     "pc_fieldsplit_type": "schur",
     "pc_fieldsplit_0_fields": "2,3",
     "pc_fieldsplit_1_fields": "0,1",
     #"fieldsplit_0": mstar,
     #"fieldsplit_1": outerschurfsstar,
    }

fs2by2mlu = {
     "snes_type": "newtonls",
     "snes_max_it": 30,
     "snes_linesearch_type": "l2",
     "snes_linesearch_maxstep": 1.0,
     "snes_rtol": 1.0e-10,
     "snes_atol": 1.0e-7,
     #"snes_convergence_test": "skip",
     "snes_monitor": None,
     #"snes_linesearch_monitor": None,
     #"snes_converged_reason": None,
     "ksp_type": "fgmres",
     "ksp_max_it": 50,
     "ksp_atol": 1.0e-8,
     "ksp_rtol": 1.0e-9,
     "ksp_monitor_true_residual": None,
     "ksp_converged_reason": None,
     "mat_type": "aij",
     "pc_type": "fieldsplit",
     "pc_fieldsplit_type": "schur",
     "pc_fieldsplit_0_fields": "2,3",
     "pc_fieldsplit_1_fields": "0,1",
     "fieldsplit_0_ksp_type": "preonly",
     "fieldsplit_0_pc_type": "lu",
     "fieldsplit_0_pc_factor_mat_solver_type": "mumps",
     "fieldsplit_0_mat_mumps_icntl_14": 200,
     #"fieldsplit_1": outerschurfsstar,
    }

fs2by2slu = {
     "snes_type": "newtonls",
     "snes_max_it": 30,
     "snes_linesearch_type": "l2",
     "snes_linesearch_maxstep": 1.0,
     "snes_rtol": 1.0e-15,
     "snes_atol": 1.0e-7,
     #"snes_convergence_test": "skip",
     "snes_monitor": None,
     #"snes_linesearch_monitor": None,
     #"snes_converged_reason": None,
     "ksp_type": "fgmres",
     "ksp_max_it": 40,
     "ksp_atol": 1.0e-7,
     "ksp_rtol": 1.0e-7,
     "ksp_monitor_true_residual": None,
     "ksp_converged_reason": None,
     "mat_type": "aij",
     "pc_type": "fieldsplit",
     "pc_fieldsplit_type": "schur",
     "pc_fieldsplit_0_fields": "2,3",
     "pc_fieldsplit_1_fields": "0,1",
     #"fieldsplit_0": mstar,
     "fieldsplit_1": outerschurlu,
    }
    
fs2by2lu = {
     "snes_type": "newtonls",
     "snes_max_it": 30,
     "snes_linesearch_type": "l2",
     "snes_linesearch_maxstep": 1.0,
     "snes_rtol": 1.0e-10,
     "snes_atol": 1.0e-7,
     #"snes_convergence_test": "skip",                      
     "snes_monitor": None,
     #"snes_linesearch_monitor": None,
     #"snes_converged_reason": None,                                     
     "ksp_type": "fgmres",
     "ksp_max_it": 50,
     "ksp_atol": 1.0e-8,
     "ksp_rtol": 1.0e-9,
     "ksp_monitor_true_residual": None,
     "ksp_converged_reason": None,
     "mat_type": "aij",
     "pc_type": "fieldsplit",
     "pc_fieldsplit_type": "schur",
     "pc_fieldsplit_0_fields": "2,3",
     "pc_fieldsplit_1_fields": "0,1",
     "fieldsplit_0_ksp_type": "preonly",
     "fieldsplit_0_pc_type": "lu",
     "fieldsplit_0_pc_factor_mat_solver_type": "mumps",
     "fieldsplit_0_mat_mumps_icntl_14": 200,
     "fieldsplit_1": outerschurlu,
    }


solvers = {"lu": lu, "fs2by2": fs2by2, "fs2by2mlu": fs2by2mlu, "fs2by2slu": fs2by2slu, "fs2by2lu": fs2by2lu}

parser = argparse.ArgumentParser(add_help=False)
parser.add_argument("--baseN", type=int, default=10)
parser.add_argument("--k", type=int, default=3)
parser.add_argument("--nref", type=int, default=1)
parser.add_argument("--Re", nargs='+', type=float, default=[1])
parser.add_argument("--Rem", nargs='+', type=float, default=[1])
parser.add_argument("--gamma", type=float, default=10000)
parser.add_argument("--gamma2", type=float, default=0)
parser.add_argument("--advect", type=float, default=1)
parser.add_argument("--dt", type=float, required=True)
parser.add_argument("--Tf", type=float, default=1)
parser.add_argument("--S", nargs='+', type=float, default=[1])
parser.add_argument("--hierarchy", choices=["bary", "uniform"], default="bary")
parser.add_argument("--discr", choices=["rt", "bdm", "cg"], required=True)
parser.add_argument("--solver-type", choices=list(solvers.keys()), default="lu")
parser.add_argument("--testproblem", choices=["ldc", "hartmann", "Wathen", "hartmann2"], default = "Wathen")
parser.add_argument("--linearisation", choices=["picard", "picardG", "newton"], required=True )
parser.add_argument("--stab", default=False, action="store_true")
parser.add_argument("--exing", default=False, action="store_true")
parser.add_argument("--checkpoint", default=False, action="store_true")
parser.add_argument("--alternative-bcs", default=False, action="store_true")

args, _ = parser.parse_known_args()
baseN = args.baseN
k = args.k
nref = args.nref
Re = Constant(args.Re[0])
Rem = Constant(args.Rem[0])
gamma = Constant(args.gamma)
gamma2 = Constant(args.gamma2)
S = Constant(args.S[0])
hierarchy = args.hierarchy
discr = args.discr
solver_type = args.solver_type
testproblem = args.testproblem
gamma2 = Constant(args.gamma2)
advect = Constant(args.advect)
dt = Constant(args.dt)
Tf = Constant(args.Tf)
linearisation = args.linearisation
stab = args.stab
exing = args.exing
checkpoint = args.checkpoint
alternative_bcs = args.alternative_bcs

S2 = Constant(1)
stab_weight = Constant(3e-3)

if exing and testproblem == "ldc":
    raise ValueError("Analytical solution for ldc is not known")

if k<3 and hierarchy == "bary":
    raise ValueError("Scott Vogelius is not stable for k<3")

if discr == "cg" and hierarchy != "bary":
    raise ValueError("SV is only stable on barycentric refined grids")

if checkpoint and exing:
    raise ValueError("It doesn't make sense to use checkpoint and exing at the same time")

if checkpoint and len(args.S) != 1 and len(args.Rem) !=1:
    raise ValueError("Only possible to have more than value for S or Rem at the same time with checkpoint")

if len(args.Rem) != 1 and testproblem == "hartmann":
    raise ValueError("Sorry, you cannot use continuation in Rem for hartmann, use a for-loop and --checkpoint instead. (The problem is that f is not 0 for Rem!=1)")

if float(Rem) !=1 and testproblem == "hartmann":
    raise ValueError("Don't use hartmann for Rem!=1, because then the rhs is not 0")

base = UnitCubeMesh(baseN, baseN, baseN, distribution_parameters=distribution_parameters)

def before(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+1)

def after(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+2)
    #rebalance(dm, i)

if hierarchy == "bary":
    mh = alfi.BaryMeshHierarchy(base, nref, callbacks=(before, after))
elif hierarchy == "uniformbary":
    bmesh = Mesh(bary(base._plex), distribution_parameters={"partition": False})
    mh = MeshHierarchy(bmesh, nref, reorder=True, callbacks=(before, after),
                       distribution_parameters=distribution_parameters)
elif hierarchy == "uniform":
    mh = MeshHierarchy(base, nref, reorder=True, callbacks=(before, after),
             distribution_parameters=distribution_parameters)
else:
    raise NotImplementedError("Only know bary, uniformbary and uniform for the hierarchy.")
for m in mh:
    m.coordinates.dat.data[:, 0] -= 0.5
    m.coordinates.dat.data[:, 1] -= 0.5
    m.coordinates.dat.data[:, 2] -= 0.5
mesh = mh[-1]

area = assemble(Constant(1, domain=mh[0])*dx)

def message(msg):
    if mesh.comm.rank == 0:
        warning(msg)

if discr == "rt":
    Vel = FiniteElement("N1div", mesh.ufl_cell(), k, variant="integral")
    V = FunctionSpace(mesh, Vel)
elif discr == "bdm":
    Vel = FiniteElement("N2div", mesh.ufl_cell(), k, variant="integral")
    V = FunctionSpace(mesh, Vel)
elif discr == "cg":
    V = VectorFunctionSpace(mesh, "CG", k)
Q = FunctionSpace(mesh, "DG", k-1)      #p
Rel = FiniteElement("N1curl", mesh.ufl_cell(), k, variant="integral")
R = FunctionSpace(mesh, Rel)    #E
Wel = FiniteElement("N1div", mesh.ufl_cell(), k, variant="integral")
W = FunctionSpace(mesh, Wel)
Z = MixedFunctionSpace([V, Q, W, R])

#z = Function(Z)
#(u, p, B, E) = split(z)
#(v, q, C, Ff)   = split(TestFunction(Z))
z_last_u = Function(V)
t = Constant(1)

if checkpoint:
    try:
        chk = DumbCheckpoint("dump/"+str(float(S*Rem))+str(linearisation)+str(testproblem), mode=FILE_READ)
        chk.load(z)
    except Exception as e:
        message(e)
    (u_, p_, B_, E_) = z.split()
    z_last_u.assign(u_)

(x, y, zz) = SpatialCoordinate(Z.mesh())
n = FacetNormal(mesh)
#tangential = as_vector([n[1], -n[0]])

def compute_rhs(u_ex, B_ex, p_ex, E_ex):
    #f1 = Function(V)
    #f2 = Function(W)
    #f3 = Function(R)
    eps = lambda x: sym(grad(x))
    E_ex_ = E_ex#interpolate(E_ex, R)
    B_ex_ = B_ex#interpolate(B_ex, W)
    f1 = (-2/Re * div(eps(u_ex)) + advect * dot(grad(u_ex),u_ex) - gamma * grad(div(u_ex))
          + grad(p_ex) + S * cross(B_ex, (E_ex + cross(u_ex, B_ex))))
    f2 = curl(E_ex_) #- 1/Rem * grad(div(B_ex)) - gamma2 * grad(div(B_ex))
    f3 = -1/Rem * curl(B_ex) + E_ex + S2 * cross(u_ex, B_ex)
    #print("||f1||_L^2 = %s" % sqrt(assemble(inner(f1, f1)*dx)))
    #print("||f2||_L^2 = %s" % sqrt(assemble(inner(f2, f2)*dx)))
    #print("||f3||_L^2 = %s" % sqrt(assemble(inner(f3, f3)*dx)))
    #exit(-1)
    #f2 = interpolate(f2, W)
    #f3 = interpolate(f3, R)
    #f1.interpolate(f1)
    #f2.interpolate(f2)
    #f3.interpolate(f3)
    return (f1, f2, f3)

if testproblem == "hartmann2":
    eps = lambda x: sym(grad(x))
    def get_mms(timelevel, mode):
        #u_ex = as_vector([exp(0)*cos(y), sin(x)])
        #B_ex = as_vector([sin(y), sin(0.1)*cos(x)]) # u_ex #as_vector([hom, hom])
        u_ex = as_vector([exp(t-dt*timelevel)*cos(y), sin(zz), cos(y)])
        B_ex = as_vector([sin(zz), sin(t-dt*timelevel)*cos(x), exp(y)]) # u_ex #as_vector([hom, hom])
        #hom = sin(0)*sin(2/pi*x)*sin(2/pi*y)
        #B_ex = curl(hom)
        p_ex = -cos(y)*x
        pintegral = assemble(p_ex*dx)
        p_ex = p_ex - Constant(pintegral/area)
        E_ex = as_vector([sin(x), cos(zz*y), sin(y*x)])
        if mode == "sol":
            return u_ex, p_ex, B_ex, E_ex
        elif mode == "f":
            E_ex_ = interpolate(E_ex, R)
            f1 = (-2/Re * div(eps(u_ex)) + advect * dot(grad(u_ex),u_ex) - gamma * grad(div(u_ex))
                  + grad(p_ex) + S * cross(B_ex, (E_ex + cross(u_ex, B_ex))) + diff(u_ex, t))
            f2 = diff(B_ex, t) + curl(E_ex_) - 1/Rem * grad(div(B_ex)) - gamma2 * grad(div(B_ex))
            f3 = -1/Rem * curl(B_ex) + E_ex + S2 * cross(u_ex, B_ex)
            #norm_div_f2 = sqrt(assemble(inner(div(f2), div(f2))*dx))
            #print("||div(f2)||_L^2 = %s" % norm_div_f2, flush=True)
            #f2 = interpolate(f2, W)
            return f1, f2, f3

    u_ex, p_ex, B_ex, E_ex = get_mms(0,"sol")
    rhs = True
    solution_known = True
    bc_varying = True

elif testproblem == "ldc":
    #example taken from chapter 5.1 in doi.org/10.1137/16M1074084
    #u_ex = as_vector([1*(x-0.5)*(x-0.5)*(x+0.5)*(x+0.5)*(0.25*(y+0.5)*(y+0.5)), 0])
    def get_mms(timelevel, mode):
        u_ex = Constant((1, 0, 0), domain=mesh)
        #B_ex = Constant((-1, 0), domain=mesh)
        B_ex = Constant((0, 1, 0), domain=mesh)
        B_ex = interpolate(B_ex, W)
        E_ex = Constant((0,0,0), domain=mesh)
        E_ex = interpolate(E_ex, R)
        p_ex = Constant(0, domain=mesh)
        if mode == "sol":
            return u_ex, p_ex, B_ex, E_ex

    u_ex, p_ex, B_ex, E_ex = get_mms(0,"sol")
    
    if alternative_bcs:
        B_ex = Constant((1, 0), domain=mesh)
        bcs = [DirichletBC(Z.sub(0), u_ex, "on_boundary"),
               #DirichletBC(Z.sub(3), E_ex , "on_boundary"),
               PressureFixBC(Z.sub(1), 0, 1)]
        F += +1/Rem * inner(cross(B_ex, n), Ff) * ds - 1/Rem * inner(div(B_ex), dot(C, n)) * ds
    else:
        bcs = [DirichletBC(Z.sub(0), u_ex, 4),  #4 == upper boundary (y==1)
               DirichletBC(Z.sub(0), 0, (1,2,3,5,6)),
               DirichletBC(Z.sub(2), B_ex, "on_boundary"),
               DirichletBC(Z.sub(3), 0, "on_boundary"),
               #DirichletBC(Z.sub(3), E_ex, "on_boundary"), 
               #PressureFixBC(Z.sub(1), 0, 1)
               ]
    rhs = None
    solution_known = False
    bc_varying = False

elif testproblem == "Wathen":
    #example taken from chapter 6.2.1 in doi.org/a10.1137/16M1098991
    u_ex = as_vector([-x*y*exp(x+y+zz)+x*zz*exp(x+y+zz), x*y*exp(x+y+zz)-y*zz*exp(x+y+zz), -x*zz*exp(x+y+zz)+y*zz*exp(x+y+zz)])
    p_ex = exp(x+y+zz)*sin(y)
    pintegral = assemble(p_ex*dx)
    p_ex = p_ex - Constant(pintegral/area)
    B_ex = s_vector([-exp(x+y+zz)*sin(y)+exp(x+y+zz)*sin(zz), sin(x)*exp(x+y+zz)-sin(zz)*exp(x+y+zz), -exp(x+y+zz)*sin(x)+exp(x+y+zz)*sin(y)])
    E_ex = 1/Rem * curl(B_ex) - cross(u_ex, B_ex)

    (f1, f2, f3) = compute_rhs(u_ex, B_ex, p_ex, E_ex)
    rhs = True
    solution_known = True
    bc_varying = False

if solution_known:  
    u_ex_ = interpolate(u_ex, V)
    B_ex_ = interpolate(B_ex, W)
    p_ex_ = interpolate(p_ex, Q)
    E_ex_ = interpolate(E_ex, R)

if solution_known:
    if alternative_bcs:
        bcs = [DirichletBC(Z.sub(0), u_ex, "on_boundary"),
               #DirichletBC(Z.sub(3), E_ex , "on_boundary"),
               PressureFixBC(Z.sub(1), 0, 1)]
        F += +1/Rem * inner(cross(B_ex_, n), Ff) * ds - 1/Rem * inner(div(B_ex), dot(C, n)) * ds 
    else:
        bcs = [DirichletBC(Z.sub(0), u_ex, "on_boundary"),
               DirichletBC(Z.sub(2), B_ex , "on_boundary"),
               DirichletBC(Z.sub(3), E_ex , "on_boundary"),
               PressureFixBC(Z.sub(1), 0, 1)
        ]

def form(z, test_z, Z, timelevel):
    if timelevel == 0.5:
        (u, p, B, E) = z
    else:
        (u, p, B, E) = split(z)
    (v, q, C, Ff) = split(test_z)
    eps = lambda x: sym(grad(x))
    F = (
        + 2/Re * inner(eps(u), eps(v))*dx
        #+ advect * inner(dot(grad(u), u), v) * dx
        + gamma * inner(div(u), div(v)) * dx
        + S * inner(cross(B, E), v) * dx
        + S * inner(cross(B, cross(u, B)), v) * dx
        - inner(p, div(v)) * dx
        - inner(div(u), q) * dx
        + inner(E, Ff) * dx
        + inner(cross(u, B), Ff) * dx
        - 1/Rem * inner(B, curl(Ff)) * dx
        + inner(curl(E), C) * dx
        + 1/Rem * inner(div(B), div(C)) * dx
        + gamma2 * inner(div(B), div(C)) * dx
        )

    if stab:
        #if solution_known:
        initial = interpolate(as_vector([sin(y), x, x]), V)
        z_last_u.assign(initial)
        #stabilisation_form_u = BurmanStab(u, v, z_last_u, stab_weight, mesh)
        #stabilisation_form_u = form_Burman2(u, v, z_last_u, stab_weight, mesh)
        stabilisation = BurmanStabilisation(Z.sub(0), state=z_last_u, h=FacetArea(mesh), weight=stab_weight)
        stabilisation_form_u = stabilisation.form(u, v)
        F += (advect * stabilisation_form_u)

    if discr in ["rt", "bdm"]:
        u_ex, p_ex, B_ex, E_ex = get_mms(timelevel, "sol")
        h = CellVolume(mesh)/FacetArea(mesh)
        sigma = Constant(10) * Z.sub(0).ufl_element().degree()**2
        theta = Constant(1)
        uflux_int = 0.5*(dot(u, n) + theta*abs(dot(u, n)))*u
        uflux_ext = 0.5*(inner(u,n)+ theta*abs(inner(u,n)))*u + 0.5*(inner(u,n)- theta*abs(inner(u,n)))*u_ex
        F_DG = (
             - 1/Re * inner(avg(2*sym(grad(u))), 2*avg(outer(v, n))) * dS
             - 1/Re * inner(avg(2*sym(grad(v))), 2*avg(outer(u, n))) * dS
             + 1/Re * sigma/avg(h) * inner(2*avg(outer(u,n)),2*avg(outer(v,n))) * dS
             - inner(outer(v,n),2/Re*sym(grad(u)))*ds
             - inner(outer(u-u_ex,n),2/Re*sym(grad(v)))*ds
             + 1/Re*(sigma/h)*inner(v,u-u_ex)*ds
             - advect * dot(u,div(outer(v,u)))*dx
             + advect * dot(v('+')-v('-'), uflux_int('+')-uflux_int('-'))*dS
             + advect * dot(v,uflux_ext)*ds
        )
        #F = F + F_DG
    elif discr == "cg":
         F +=advect * inner(dot(grad(u), u), v) * dx 

    if discr in ["rt", "bdm"]:    
        if testproblem != "ldc":
            F = F + F_DG
        else:
            uflux_ext = 0.5*(inner(u,n)+ theta*abs(inner(u,n)))*u + 0.5*(inner(u,n)-theta*abs(inner(u,n)))*u_ex
            uflux_ext_no_u_ex = 0.5*(inner(u,n)+ theta*abs(inner(u,n)))*u
            F_DG = (
                 - 1/Re * inner(avg(2*sym(grad(u))), 2*avg(outer(v, n))) * dS
                 - 1/Re * inner(avg(2*sym(grad(v))), 2*avg(outer(u, n))) * dS
                 + 1/Re * sigma/avg(h) * inner(2*avg(outer(u,n)),2*avg(outer(v,n))) * dS
                 - inner(outer(v,n),2/Re*sym(grad(u)))*ds
                 - inner(outer(u-u_ex,n),2/Re*sym(grad(v)))*ds(4)
                 - inner(outer(u,n),2/Re*sym(grad(v)))*ds((1,2,3,5,6))
                 + 1/Re*(sigma/h)*inner(v,u-u_ex)*ds(4)
                 + 1/Re*(sigma/h)*inner(v,u)*ds((1,2,3,5,6))
                 - advect * dot(u,div(outer(v,u)))*dx
                 + advect * dot(v('+')-v('-'), uflux_int('+')-uflux_int('-'))*dS
                 + advect * dot(v,uflux_ext)*ds(4)
                 + advect * dot(v,uflux_ext_no_u_ex)*ds((1,2,3,5,6))
                 )
            F = F + F_DG

    if rhs is not None:
        f1, f2, f3 = get_mms(timelevel, "f")
        F -= inner(f1, v) * dx + inner(f3, Ff) * dx + inner(f2, C) * dx

    return F

def J_form(F, z, test_z):
    (u, p, B, E) = split(z)
    (v, q, C, Ff) = split(test_z)
    w = TrialFunction(Z)
    [w_u, w_p, w_B, w_E] = split(w)

    J_newton = ufl.algorithms.expand_derivatives(derivative(F, z, w))

    if linearisation == "newton":
        J = J_newton

    elif linearisation == "picardG":
        J_picardG = (
              J_newton
            #- S * inner(cross(w_B, E), v) * dx               #J_tilde    #inner(B, E_n, v) * dx
            #- S * inner(cross(B, cross(w_u, B)), v) * dx    #D  #inner(cross(B_n, cross(u, B_n)), v) * dx
            #- S * inner(cross(w_B, cross(u, B)), v) * dx    #D_1_tilde  #inner(cross(B, cross(u_n, B_n)), v) * dx
            #- S * inner(cross(B, cross(u, w_B)), v) * dx    #D_2_tilde  #inner(cross(B_n, cross(u_n, B)), v) * dx
            #- inner(cross(u, w_B), Ff) * dx              #G_tilde    #inner(cross(u_n, B), Ff) * dx
            - dt_factor * dt * inner(cross(w_u, B), Ff) * dx              #G          #inner(cross(u, B_n), Ff) * dx
            #- S * inner(cross(B, w_E), v) * dx               #J          #inner(cross(B_n, E), v) * dx
            #- advect * inner(dot(grad(u), w_u), v) * dx   # - advect * inner(dot(grad(u_n), u), v) * dx
            #- advect * inner(dot(grad(w_u), u), v) * dx   # - advect * inner(dot(grad(u), u_n), v) * dx
        )
        J = J_picardG

    elif linearisation == "picard":
        J_picard = (
              J_newton
            - dt_factor * dt * S * inner(cross(w_B, E), v) * dx               #J_tilde    #inner(B, E_n, v) * dx
            #- S * inner(cross(B, cross(w_u, B)), v) * dx    #D  #inner(cross(B_n, cross(u, B_n)), v) * dx
            - dt_factor * dt * S * inner(cross(w_B, cross(u, B)), v) * dx    #D_1_tilde  #inner(cross(B, cross(u_n, B_n)), v) * dx
            - dt_factor * dt * S * inner(cross(B, cross(u, w_B)), v) * dx    #D_2_tilde  #inner(cross(B_n, cross(u_n, B)), v) * dx
            - dt_factor * dt * inner(cross(u, w_B), Ff) * dx              #G_tilde    #inner(cross(u_n, B), Ff) * dx
            #- inner(cross(w_u, B), Ff) * dx              #G          #inner(cross(u, B_n), Ff) * dx
            #- S * inner(cross(B, w_E), v) * dx               #J          #inner(cross(B_n, E), v) * dx
            #- advect * inner(dot(grad(u), w_u), v) * dx   # - advect * inner(dot(grad(u_n), u), v) * dx
            #- advect * inner(dot(grad(w_u), u), v) * dx   # - advect * inner(dot(grad(u), u_n), v) * dx
        )
        J = J_picard

    else:
        raise ValueError("only know newton, picardG and picard as linearisation method")

    return J

def initial_condition():
   z = Function(Z)
   t.assign(0)
   u_ex_init, p_ex_init, B_ex_init, E_ex_init = get_mms(0, "sol")
   z.sub(0).interpolate(u_ex_init)
   z.sub(1).interpolate(p_ex_init)
   z.sub(2).interpolate(B_ex_init)
   z.sub(3).interpolate(E_ex_init)
   return z

# Initial Condition
z0 = initial_condition()
z1 = initial_condition()
z = Function(z0)
z_test = TestFunction(Z)
dt_factor = Constant(1)

# Crank Nicolson form
F_cn = (
     inner(split(z)[0],split(z_test)[0])*dx
   - inner(split(z0)[0],split(z_test)[0])*dx
   + inner(split(z)[2],split(z_test)[2])*dx
   - inner(split(z0)[2],split(z_test)[2])*dx
   + 0.5*dt*(form(z,z_test,Z,0) + form(z0,z_test,Z,1))
  )

# Implicit Euler form
F_ie = (
     inner(split(z)[0],split(z_test)[0])*dx
   - inner(split(z0)[0],split(z_test)[0])*dx
   + inner(split(z)[2],split(z_test)[2])*dx
   - inner(split(z0)[2],split(z_test)[2])*dx
   + dt*(form(z,z_test,Z,0))
  )

# Implicit midpoint form
uu = 0.5*(split(z)[0]+split(z0)[0])
pp = 0.5*(split(z)[1]+split(z0)[1])
BB = 0.5*(split(z)[2]+split(z0)[2])
EE = 0.5*(split(z)[3]+split(z0)[3])

F_im = (
     inner(split(z)[0],split(z_test)[0])*dx
   - inner(split(z0)[0],split(z_test)[0])*dx
   + inner(split(z)[2],split(z_test)[2])*dx
   - inner(split(z0)[2],split(z_test)[2])*dx
   + dt*(form((uu,pp,BB,EE),z_test,Z,0.5))
  )

F_cn = F_cn

# BDF2 form
F_bdf2 = (
     inner(split(z)[0],split(z_test)[0])*dx
   - 4.0/3.0*inner(split(z0)[0],split(z_test)[0])*dx
   + 1.0/3.0*inner(split(z1)[0],split(z_test)[0])*dx
   + inner(split(z)[2],split(z_test)[2])*dx
   - 4.0/3.0*inner(split(z0)[2],split(z_test)[2])*dx
   + 1.0/3.0*inner(split(z1)[2],split(z_test)[2])*dx
   + 2.0/3.0*dt*form(z,z_test,Z, 0)
  )

J_cn = J_form(F_cn, z, z_test)
J_bdf2 = J_form(F_bdf2, z, z_test)

#problem = NonlinearVariationalProblem(F, z, bcs, J=J)
 
appctx = {"Re": Re, "gamma": gamma, "nu": 1/Re, "Rem": Rem, "gamma2": gamma2}
params = solvers[args.solver_type]

if args.solver_type in ["fs2by2", "fs2by2slu"]:
    params["fieldsplit_0"] = mstar if hierarchy == "uniform" else mmacrostar

if args.solver_type in ["fs2by2", "fs2by2mlu"]:
    params["fieldsplit_1"] = outerschurfsstar if hierarchy == "uniform" else outerschurfsmacrostar

nsp = MixedVectorSpaceBasis(Z, [Z.sub(0), VectorSpaceBasis(constant=True), Z.sub(2), Z.sub(3)])
# Set up nonlinear solver for first time step
nvproblem_cn = NonlinearVariationalProblem(F_cn, z, bcs=bcs, J=J_cn)
solver_cn = NonlinearVariationalSolver(nvproblem_cn, solver_parameters=params, options_prefix="", appctx=appctx, nullspace=nsp)

# Set up nonlinear solver for later time steps
nvproblem_bdf2 = NonlinearVariationalProblem(F_bdf2, z, bcs=bcs, J=J_bdf2)
solver_bdf2 = NonlinearVariationalSolver(nvproblem_bdf2, solver_parameters=params, options_prefix="", appctx=appctx, nullspace=nsp)

#solver = NonlinearVariationalSolver(problem, solver_parameters=params, options_prefix="", appctx=appctx)
qtransfer = NullTransfer()
Etransfer = NullTransfer()
vtransfer = SVSchoeberlTransfer((1/Re, gamma), 2, hierarchy)
Btransfer = SVSchoeberlTransfer((1/Rem, gamma2), 2, hierarchy)
dgtransfer = DGInjection()
def traceinject(src, dest):
    out = inject(src, dest)
    with dest.dat.vec_ro as x:
        print("Injecting %s -> %s: result has norm %s" % (src, dest, x.norm()))
    import sys; sys.stdout.flush()
    return out

transfers = {
                #V.ufl_element(): (vtransfer.prolong, vtransfer.restrict, inject),
                Q.ufl_element(): (prolong, restrict, qtransfer.inject),
                #R.ufl_element(): (prolong, restrict, Etransfer.inject),
                VectorElement("DG", mesh.ufl_cell(), args.k): (dgtransfer.prolong, restrict, dgtransfer.inject),
                VectorElement("DG", mesh.ufl_cell(), args.k-1): (dgtransfer.prolong, restrict, dgtransfer.inject),

            }

if hierarchy == "bary":
    transfers[V.ufl_element()] = (vtransfer.prolong, vtransfer.restrict, inject)
    
transfermanager = TransferManager(native_transfers=transfers)
solver_cn.set_transfer_manager(transfermanager)
solver_bdf2.set_transfer_manager(transfermanager)

results = {}
res =   args.Re 
rems =  args.Rem
Ss = args.S
pvd = File("output/mhd.pvd")

def run(re, rem, s):
    (u, p, B, E) = z.split()
    Re.assign(re)
    Rem.assign(rem)
    S.assign(s)
    z0.assign(initial_condition())

    # Set things up for timestepping
    T  = args.Tf  # final time
    t.assign(0.0)  # current time we are solving for
    global dt
    global bcs
    h  = args.dt # time step size
    dt.assign(h) # for use in the form
    ntimestep = 0 # number of timesteps solved

    while (float(t) < T):

        # Do short timestep for first, then regular timesteps
        if ntimestep == 0:
            dt.assign(dt/10)
            h = h/10
        if ntimestep == 1:
            dt.assign(9*dt)
            h = 9*h
        if ntimestep == 2:
            dt.assign(args.dt)
            h = args.dt
        #global p_ex
        u_ex_ = interpolate(u_ex, V)
        B_ex_ = interpolate(B_ex, W)
        p_ex_ = interpolate(p_ex, Q)
        E_ex_ = interpolate(E_ex, R)
        (u0, p0, B0, E0) = z0.split()
        File("output/mhd2.pvd").write(u0, u_ex_, p0, p_ex_, B0, B_ex_, E0, E_ex_, time=float(t))
        # Update the time we're solving for
        t.assign(t+h)
        if mesh.comm.rank == 0:
            print(BLUE % ("\nSolving for time: %f" % t), flush=True)
                
        if bc_varying:
            if not solution_known:
                raise ValueError("Sorry, don't know how to reconstruct the BCs")
            else:
                #global p_ex
                #import ipdb; ipdb.set_trace()
                u_ex_ = interpolate(u_ex, V)
                B_ex_ = interpolate(B_ex, W)
                p_ex_ = interpolate(p_ex, Q)
                E_ex_ = interpolate(E_ex, R)
                #pintegral = assemble(p_ex*dx)
                #p_ex = p_ex - Constant(pintegral/area)
                # bcs[0] is u, bcs[1] is B, bcs[2] is E
                bcs[0].function_arg = u_ex_
                if not alternative_bcs:
                    bcs[1].function_arg = B_ex_
                    bcs[2].function_arg = E_ex_
        if checkpoint:
            try:
                chk = DumbCheckpoint("dump/"+str(float(Rem*S))+str(linearisation)+str(testproblem), mode=FILE_READ)
                chk.load(z)
            except Exception as e:
                message(e)
        if mesh.comm.rank == 0:
            print(GREEN % ("Solving for #dofs = %s, Re = %s, Rem = %s, gamma = %s, gamma2 = %s, S = %s, baseN = %s, nref = %s, linearisation = %s, testproblem = %s"
                       % (Z.dim(), float(re), float(rem), float(gamma), float(gamma2), float(S), int(baseN), int(nref), linearisation, testproblem)), flush=True)
        if not os.path.exists("results/"):
               os.mkdir("results/")

        #if stab:
        #    stabilisation.update(z.split()[0])
        #    z_last_u.assign(u)

        #if float(Re) == 5000 and float(S) == 500:
        #    print("Enabling nan", flush=True)
        #    import nan; nan.enable()

        #if float(Re) == 100:
        #    if mesh.comm.rank == 0:
        #        print("Setting up new solver", flush=True)
        #    global solver
        #    problem = NonlinearVariationalProblem(F, z, bcs, J=J)
        #    solver = NonlinearVariationalSolver(problem, solver_parameters=params, options_prefix="", appctx=appctx)
        #    solver.set_transfer_manager(transfermanager)
        #bcs = [DirichletBC(Z.sub(0), u_ex, "on_boundary"),
        #       DirichletBC(Z.sub(2), B_ex , "on_boundary"),
        #       DirichletBC(Z.sub(3), E_ex , "on_boundary"),
        #       PressureFixBC(Z.sub(1), 0, 1)]
        #nvproblem_cn = NonlinearVariationalProblem(F_cn, z, bcs=bcs, J=J_cn)
        #solver_cn = NonlinearVariationalSolver(nvproblem_cn, solver_parameters=params, options_prefix="", appctx=appctx)
        
        start = datetime.now()
        if ntimestep < 4:
            dt_factor.assign(0.5)
            solver = solver_cn
            solver_cn.solve()
        else:
            dt_factor.assign(2.0/3.0)
            solver = solver_bdf2
            solver_bdf2.solve()
        end = datetime.now()

        #start = datetime.now()
        #solver.solve()
        #end = datetime.now()

        linear_its = solver.snes.getLinearSolveIterations()
        nonlinear_its = solver.snes.getIterationNumber()
        time = (end-start).total_seconds() / 60

        if mesh.comm.rank == 0:
            if nonlinear_its == 0:
                nonlinear_its = 1
            print(GREEN % ("Time taken: %.2f min in %d nonlinear iterations, %d linear iterations (%.2f Krylov iters per Newton step)"
                           % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its))), flush=True)
            print("%.2f @ %d @ %d @ %.2f" % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its)), flush=True)

        (u, p, B, E) = z.split()

        #B.assign(-dt*interpolate(curl(E), W) + z0.split()[2] + dt*interpolate(f2, W))
        B.rename("MagneticField")
        u.rename("VelocityField")
        p.rename("Pressure")
        E.rename("ElectricFieldf")

        norm_div_u = sqrt(assemble(inner(div(u), div(u))*dx))
        norm_div_B = sqrt(assemble(inner(div(B), div(B))*dx))
        #norm_curl_E = sqrt(assemble(inner(curl(E), curl(E))*dx))
        #norm_B = sqrt(assemble(inner(B, B)*dx))
        #norm_B_ex = sqrt(assemble(inner(B_ex, B_ex)*dx))
        #norm_curl_E_ex = sqrt(assemble(inner(curl(E_ex), curl(E_ex))*dx))
        #norm_B_curl_E = sqrt(assemble(inner(B-curl(E),B-curl(E))*dx))

        if mesh.comm.rank == 0:
            print("||div(u)||_L^2 = %s" % norm_div_u, flush=True)
            print("||div(B)||_L^2 = %s" % norm_div_B, flush=True)
            #print("||curl(E)||_L^2 = %s" % norm_curl_E)
            #print("||B||_L^2 = %s" % norm_B)
            #print("||B_ex||_L^2 = %s" % norm_B_ex)
            #print("||curl(E_ex)||_L^2 = %s" % norm_curl_E_ex)
            #print("||curl(E)-B||_L^2 = %s" % norm_B_curl_E)

        if solution_known:
            B_ex_ = interpolate(B_ex, B.function_space())
            u_ex_ = interpolate(u_ex, u.function_space())
            p_ex_ = interpolate(p_ex, p.function_space())
            E_ex_ = interpolate(E_ex, E.function_space())
            B_ex_.rename("ExactSolutionB")
            u_ex_.rename("ExactSolutionu")
            p_ex_.rename("ExactSolutionp")
            E_ex_.rename("ExactSolutionE")

            error_u = errornorm(u_ex, u, 'L2')
            error_B = errornorm(B_ex, B, 'L2')
            error_E = errornorm(E_ex, E, 'L2')
            error_p = errornorm(p_ex, p, 'L2')

            error_B_bndy_n = sqrt(assemble(inner(dot((B_ex_-B),n), dot((B_ex_-B),n))*ds))
            error_B_bndy_t = sqrt(assemble(inner(cross((B_ex_-B),n), cross((B_ex_-B),n))*ds)) 
            error_E_bndy_n = sqrt(assemble(inner(E_ex_-E, E_ex_-E)*ds))

            errors=[error_u,error_p,error_B, error_E]
            if mesh.comm.rank == 0:
                print("Error ||u_ex - u||_L^2 = %s" % error_u, flush=True)
                print("Error ||p_ex - p||_L^2 = %s" % error_p, flush=True)
                print("Error ||B_ex - B||_L^2 = %s" % error_B, flush=True)
                print("Error ||E_ex - E||_L^2 = %s" % error_E, flush=True)

                if alternative_bcs:
                    print("")
                    print("Error ||(B_ex - B)*n||_L^2 = %s" % error_B_bndy_n)
                    print("Error ||(B_ex - B)*t||_L^2 = %s" % error_B_bndy_t)
                    print("Error ||(E_ex - E)*n||_L^2 = %s" % error_E_bndy_n)

                f = open("error.txt",'a+')
                f.write("%s,%s,%s,%s\n" % (error_u,error_p,error_B, error_E))
                f.close()
            pvd.write(u, u_ex_, p, p_ex_, B, B_ex_, E, E_ex_, time=float(t))
            #print("%s,%s,%s,%s" % (error_u,error_p,error_B, error_E))

            sys.stdout.flush()
            info_dict = {
                "Re": re,
                "Rem": rem,
                "S": s,
                "krylov/nonlin": linear_its/nonlinear_its,
                "nonlinear_iter": nonlinear_its,
                "error_u": error_u,
                "error_p": error_p,
                "error_B": error_B,
                "error_E": error_E,
            }
            results[(rem*s,re)] = info_dict
            #File("output/plots/"+str(testproblem)+"_"+str(int(float(Rem)))+"_"+str(int(float(Re)))+"u.pvd").write(u,u_ex_,  time=float(rem))
            #File("output/plots/"+str(testproblem)+"_"+str(int(float(Rem)))+"_"+str(int(float(Re)))+"B.pvd").write(B, B_ex_,  time=float(rem))

        else:
            B_ex_ = interpolate(B_ex, B.function_space())
            error_B_bndy_n = sqrt(assemble(inner(dot((B_ex_-B),n), dot((B_ex_-B),n))*ds))
            error_B_bndy_t = sqrt(assemble(inner(cross((B_ex_-B),n), cross((B_ex_-B),n))*ds)) 
            if alternative_bcs and mesh.comm.rank == 0:
                    print("")
                    print("Error ||(B_ex - B)*n||_L^2 = %s" % error_B_bndy_n)
                    print("Error ||(B_ex - B)*t||_L^2 = %s" % error_B_bndy_t)
            info_dict = {
                "Re": re,
                "Rem": rem,
                "krylov/nonlin": linear_its/nonlinear_its,
                "nonlinear_iter": nonlinear_its,
            }
            results[(rem,re)] = info_dict
            #pvd.write(u, p, B, E, time=float(t))
            pvd.write(B, time=float(t))
            
        message(BLUE % info_dict)
        if mesh.comm.rank == 0:
           dir = 'results/results'+str(linearisation)+str(testproblem)+'/'
           if not os.path.exists(dir):
               os.mkdir(dir)
           f = open(dir+str(float(Re))+str(float(Rem*S))+'.txt','w+')
           f.write("({0:2.0f}){1:4.1f}".format(float(info_dict["nonlinear_iter"]),float(info_dict["krylov/nonlin"])))
           f.close()

           f = open("divuB.txt",'a+')
           f.write("%s,%s,%s\n" % (baseN**nref,norm_div_u, norm_div_B))
           f.close()

        z_last_u.assign(u)
        if not os.path.exists("dump/"):
               os.mkdir("dump/")
        chk = DumbCheckpoint("dump/"+str(float(Rem*S))+str(linearisation)+str(testproblem), mode=FILE_CREATE)
        chk.store(z)
        File("output/mhd3.pvd").write(u0, u_ex_, p0, p_ex_, B0, B_ex_, E0, E_ex_, time=float(t))

        # Store solution at this time point and update time step
        z1.assign(z0)
        z0.assign(z)
        ntimestep += 1

for rem in rems:
    for s in Ss:
        for re in res:
            #try:
                run(re, rem, s)
            #except Exception as e:
            #    message(e)
            #    dir = 'results/results'+str(linearisation)+str(testproblem)+'/'
            #    if not os.path.exists(dir):
            #        os.mkdir(dir)
            ##    f = open(dir+str(float(re))+str(float(rem*s))+'.txt','w+')
            #    f.write("({0:2.0f}){1:4.1f}".format(0,0))
            #    f.close()
