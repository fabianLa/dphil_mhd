from firedrake import *
from alfi import *
import argparse
from datetime import datetime
import numpy

def scross(x, y):
    return x[0]*y[1] - x[1]*y[0]

def vcross(x,y):
    return as_vector([x[1]*y,-x[0]*y])

def scurl(x):
    return x[1].dx(0) - x[0].dx(1)

def vcurl(x):
    return as_vector([x.dx(1), -x.dx(0)])

def acurl(x):
    return as_vector([
                     x[2].dx(1),
                     -x[2].dx(0),
                     x[1].dx(0) - x[0].dx(1)
                     ])

distribution_parameters = {"partition": True, "overlap_type": (DistributedMeshOverlapType.VERTEX, 2)}

parser = get_default_parser()
parser.add_argument("--mesh", type=str, default=None)
parser.add_argument("--dim", type=int, default=2)
parser.add_argument("--Rem", type=float, default=1.0)
parser.add_argument("--Re", type=float, default=1.0)
parser.add_argument("--S", type=float, default=1.0)
parser.add_argument("--gamma2", type=float, default=1.0)

args, _ = parser.parse_known_args()

N = args.baseN
nref = args.nref
k = args.k
gamma = Constant(args.gamma)
dim = args.dim
Rem = args.Rem
Re = args.Re
S = args.S
gamma2 = args.gamma2


if args.mesh is not None:
    warning("Ignoring --baseN, using unstructured Gmsh meshes")
    base = Mesh(args.mesh)
else:
    if dim == 2:
        base = UnitSquareMesh(N, N, distribution_parameters=distribution_parameters)
    elif dim == 3:
        base = UnitCubeMesh(N, N, N, distribution_parameters=distribution_parameters)

def before(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+1)

def after(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+2)

if args.mh == "bary":
    mh = BaryMeshHierarchy(base, nref, callbacks=(before, after),
                           reorder=True, distribution_parameters=distribution_parameters)
elif args.mh == "uniformbary":
    bmesh = Mesh(bary(base._plex), distribution_parameters={"partition": False})
    mh = MeshHierarchy(bmesh, nref, reorder=True, callbacks=(before, after),
                       distribution_parameters=distribution_parameters)
elif args.mh == "uniform":
    mh = MeshHierarchy(base, nref, reorder=True, callbacks=(before, after),
                       distribution_parameters=distribution_parameters)
else:
    raise NotImplementedError("Only know bary, uniformbary and uniform for the hierarchy.")
mesh = mh[-1]

#Example from MONOLITHIC MULTIGRID METHODS FOR TWO-DIMENSIONAL RESISTIVE MAGNETOHYDRODYNAMICS by Adler et al.
(x, y) = SpatialCoordinate(mesh)
u_ex = as_vector([(cosh(1.0)-cosh(y))/(sinh(1.0)), Constant(0, domain=mesh)])
p_ex = -x
sigma_ex = 0.0
B_ex = as_vector([(sinh(y)-y*sinh(1))/(cosh(1.0)),Constant(0, domain=mesh)])
f = -1/Re * div(sym(grad(u_ex))) + dot(grad(u_ex),u_ex) + grad(p_ex) + S * vcross(B_ex, scurl(B_ex))
#f = as_vector([1,1])

if args.discretisation == "sv":
   Uel = VectorElement("Lagrange", mesh.ufl_cell(), k)
   Pel = FiniteElement("Discontinuous Lagrange", mesh.ufl_cell(), k-1)
   Sigmael = FiniteElement("CG", mesh.ufl_cell(), args.k)
   Bel = FiniteElement("N1curl", mesh.ufl_cell(), args.k)
else:
   raise NotImplementedError
    
Zel = MixedElement([Uel, Pel, Sigmael, Bel])
Z = FunctionSpace(mesh, Zel)

z = Function(Z)
(u, p, sigma, B) = split(z)
(v, q, tau, C) = split(TestFunction(Z))

F = (
      inner(sym(grad(u)), grad(v)) * dx
    + inner(dot(grad(u), u), v) * dx 
    + gamma * inner(cell_avg(div(u)), div(v)) * dx  #Which term for SV? Still discrete?
    + S * inner(scross(v, B), scurl(B)) * dx
    - inner(f, v) * dx
    - p * div(v) * dx
    - div(u) * q * dx
    + S/Rem * inner(scurl(B), scurl(C)) * dx
    - S * inner(scross(u, B), scurl(C)) * dx
    + gamma2 * inner(grad(sigma), C) * dx
    - inner(sigma, tau) * dx
    + inner(B, grad(tau)) * dx
    )

bcs = [DirichletBC(Z.sub(0), u_ex, "on_boundary"),
       DirichletBC(Z.sub(2), sigma_ex , "on_boundary"),
       DirichletBC(Z.sub(3), B_ex , "on_boundary")]

lu = {
   "mat_type": "aij",
   "snes_type": "newtonls",
   "snes_monitor": None,
   "ksp_type": "preonly",
   "pc_type": "lu",
   "pc_factor_mat_solver_type": "mumps",
   "mat_mumps_icntl_14": 200,
     }

sp = lu

if args.discretisation == "sv":
    vtransfer = SVSchoeberlTransfer((1, gamma), args.dim, args.mh)
elif args.discretisation == "pkp0":
    vtransfer = PkP0SchoeberlTransfer((1, gamma), args.dim, args.mh)
    
nvproblem = NonlinearVariationalProblem(F, z, bcs=bcs)
solver = NonlinearVariationalSolver(nvproblem, solver_parameters=sp, options_prefix="")
solver.set_transfer_operators(dmhooks.transfer_operators(Z, prolong=vtransfer.prolong, restrict=vtransfer.restrict))

start = datetime.now()
solver.solve()
end = datetime.now()

linear_its = solver.snes.getLinearSolveIterations()
nonlinear_its = solver.snes.getIterationNumber()
time = (end-start).total_seconds() / 60
print(GREEN % ("Time taken: %.2f min in %d nonlinear iterations, %d linear iterations (%.2f Krylov iters per Newton step)" % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its))))
print("%.2f @ %d @ %d @ %.2f" % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its)))

(u, p, sigma, B) = z.split()
print("||div(u)||_L^2 = %s" % sqrt(assemble(inner(div(u), div(u))*dx)))
print("||div(B)||_L^2 = %s" % sqrt(assemble(inner(div(B), div(B))*dx)))

error_u = errornorm(u_ex, u, 'L2')
error_B = errornorm(B_ex, B, 'L2')
error_sigma = errornorm(sigma_ex, sigma, 'L2')
error_p = errornorm(p_ex, p, 'L2')

u_ex_ = project(u_ex, u.function_space())
B_ex_ = project(B_ex, B.function_space())
B.rename("MagneticField")
B_ex_.rename("ExactSolution B")
u.rename("VelocityField")
u_ex_.rename("ExactSolution u")
p.rename("pressure")
sigma.rename("sigma")

print("Error ||u_ex - u||_L^2 = %s" % error_u)
print("Error ||p_ex - p||_L^2 = %s" % error_p)
print("Error ||B_ex - B||_L^2 = %s" % error)
print("Error ||σ_ex - σ||_L^2 = %s" % error_sigma)
File("output/magnetic.pvd").write(B, B_ex_, sigma)
File("output/fluid.pvd").write(u, u_ex_, p)

#Stabilisation?
