\section{Formulation and Discretisation in 2d}\label{sec:FormulationandDiscretisationin2d}

\subsection{Derivation of MHD system}
 In this section, we introduce the non-dimensionalised form of the two-dimensional stationary incompressible resistive magnetohydrodynamics equations that we consider in this work.  
The Navier-Stokes and Maxwell's equations for a single, incompressible, homogeneous fluid in steady state are given by 
\begin{subequations}
\begin{align}
   - \div(2 \nu \mathbf{\varepsilon} (\uu)) + \rho \uu \cdot \nabla \uu + \nabla p + \B \cross j &= \f,  \label{eq:MHDparam1}\\
  \div \uu&=0, \label{eq:MHDparam2}\\
  \vcurl \E &= \mathbf{0},  \label{eq:MHDparam3} \\ 
  j - \scurl (\frac{1}{\mu}\B )&= \mathbf{0}, \label{eq:MHDparam4}  \\
  \div \B &=0, \label{eq:MHDparam5}\\
  \eta j - \E - \uu \cross \B &= 0, \label{eq:MHDparam6}
\end{align}
\end{subequations}
with the fluid density $\rho>0$, the kinematic viscosity $\nu>0$, the magnetic permeability $\mu>0$ and the magnetic resistivity $\eta>0$. We treat each of these parameters as constant throughout the domain. Equation \eqref{eq:MHDparam1} and \eqref{eq:MHDparam2} describe the incompressible Navier-Stokes equations where the Lorentz force $\B \times j$ acts on the fluid due to its charge. The stationary forms of the Maxwell-Faraday law and Amp\'{e}re's circuital law are given by \eqref{eq:MHDparam3} and \eqref{eq:MHDparam4}. The system is completed by the magnetic Gauss's law \eqref{eq:MHDparam5} and Ohm's law \eqref{eq:MHDparam6}. \\
To obtain the MHD system \eqref{eq:MHD} we plug the expression for $j$ from \eqref{eq:MHDparam6} into \eqref{eq:MHDparam1} and \eqref{eq:MHDparam4} and non-dimensionalise the resulting system by introducing the new unknowns
\begin{align}
    \uu^\star(\xi)&=\frac{\uu(L\xi)}{\ou},\\
    p^\star(\xi)&= \frac{p(L\xi)}{\rho \ou^2}, \\
    \B^\star(\xi)&= \frac{\B(L\xi)}{\oB},\qquad \oB = \sqrt{\frac{\rho \ou \eta}{L}},\\
    \E^\star(\xi) &= \frac{\E(L\xi)}{\oE}, \qquad \oE=\ou \oB,\\
    \f^\star(\xi)&=\frac{f(L\xi)L}{\rho \ou^2}
\end{align}
where $\ou$ and $L$ are characteristic values for the velocity of the flow and the length scale. Finally we obtain \eqref{eq:MHD} by defining the fluid Reynolds number and magnetic Reynolds number 
\begin{equation}
    \re= \frac{\rho \ou L}{\nu}, \quad \quad \rem= \frac{\mu \ou L}{\eta}.
\end{equation}
Note that many formulations include a so-called coupling number $S$ in front of the Lorentz force $\B \cross j$. In the special case of the stationary MHD system the above scaling eliminates this factor and is therefore not considered in our work.\\
The size of the Reynolds numbers has important consequences for the physical behaviour of the fluid and electromagnetic field, but also for the development of numerical methods. Low fluid Reynolds numbers describe laminar flow, which is characterised by a smooth and constant fluid motion due to the domination of viscous forces. On the other hand high fluid Reynolds number model turbulent flow driven by inertial forces that can cause eddies, vortices and other chaotic behaviour.
Small magnetic Reynolds number imply that the magnetic field is in a purely diffusive state and inhomogeneities in the field will be smoothed out. In the case of high magnetic Reynolds numbers the magnetic field lines are advected with the fluid flow. This behaviour can be nicely observed in our numerical results in Figure \ref{fig:Streamlinesldc}.\\
For liquid metal flows, $\re$ tends to be much larger than $\rem$. For example, for mercury the ratio is $10^7$ and typical values in aluminium electrolysis are $\rem=10^{-1}$ and $\re=10^5$ \cite{Gerbeau2006}. For these applications the Picard iteration that we introduce in Section \ref{sec:DiscretisationandLinearisation} can be used as a robust solver.
High magnetic Reynolds numbers typically occur on large length scales as in geophysics and astrophysics. The magnetic Reynolds number of the outer Earth's core is in the range of $10^3$ and of the sun is in the range of $10^6$ \cite{Davies2015}. Magnetic Reynolds numbers between $10^1-10^3$ have been used in several dynamo experiments that investigate the planetary magnetic fields \cite{Kawczynski2018, Molokov2007}. In astrophysical MHD flows, $\rem$ can reach values up to the range of $10
^{10}-10^{20}$ \cite{Moreau1990}. The numerical experiments in Section \ref{sec:numericalresultsin2d} show that the Newton and minimal decoupling Picard iteration provide robust solver with $\rem$ in the range of $10^4$.\\
From a mathematical point of view, high Reynolds numbers imply a strong nonlinear coupling between the fluid and magnetic field and make the equations nearly singular. This setting demands the use of appropriate linearisation schemes and globalisation techniques and the construction of highly specialised linear solver that can capture the kernel of the singular terms.


\subsection{An augmented Lagrangian Formulation}\label{sec:AnaugmentedLagrangianFormulation}
We modify system \eqref{eq:MHD} by introducing two augmented Lagrangian terms $-\gamma \grad \div \uu$ for $\gamma>0$ and $-1/\rem \grad \div \B$ which are added to \eqref{eq:MHD1} and \eqref{eq:MHD4} respectively. Note that both terms do not change the continuous solution of the problem. We use the first term to control the Schur complement of the fluid subsystem and the second term to enforce the divergence constraint $\div \B=0.$ Moreover, following \cite{douglas1976}, we add the following stabilisation term to $\eqref{eq:MHD1}$ to address the problem that the Galerkin discretisation of advection-dominated problems problems can be oscillatory \cite{brooks1982, Elman2014} 
\begin{equation}\label{eq:stabBurman}
    \mathcal{ST}(\uu,\vv)=\sum_{K\in \M_h} \frac{1}{2} \int_{\del K} \delta \, h_{\del K}^2 \llbracket \grad \uu \rrbracket : \llbracket \grad \vv \rrbracket \ \mathrm{d} s.
\end{equation}
Here, $\llbracket \grad \uu \rrbracket$ denotes the jump of the gradient, $h_{\del K}$ is a function giving the facet size, and $\delta$ is a free parameter that is chosen according to \cite{burman_edge_2006}. Note, that a robust discretisation should also include a stabilisation term for the magnetic field $\B$ in the case of dominating magnetic advection. The literature does not propose many stabilisation types for this problem. The most promising work by Wu and Xu \cite{Wu2020} uses the so-called SAFE-scheme for stabilisation which is based on an exponential fitting approach. While the original SAFE-scheme is only a first order method it can be extended to higher order as shown in \cite{wu2020unisolvence}. We aim to include this stabilisation in future work.\\
Finally, we consider the following system
\begin{subequations}
\label{eq:MHDFinal}
\begin{align}
   - \frac{2}{\mathrm{Re}} \div \mathbf{\varepsilon}( \uu)  + \uu \cdot \nabla \uu - \gamma \grad \div \uu  + \nabla p + \B \cross (\E + \uu \cross \B) &= \f, \label{eq:MHDFinal1}\\
  \div \uu&=0, \label{eq:MHDFinal2}\\
       \E + \uu \cross \B - \frac{1}{\rem} \scurl \B &= \mathbf{0}, \label{eq:MHDFinal3}\\
       -\frac{1}{\rem} \grad \div \B + \vcurl \E &= 0 \label{eq:MHDFinal4},
\end{align}
\end{subequations}
plus the stabilisation term \eqref{eq:stabBurman} subject to the boundary conditions \eqref{eq:boundarycond}.
For convenience, we consider homogeneous boundary conditions in this section but all the results extend in a straightforward manner to inhomogeneous boundary conditions. However, they cause additional difficulties for the implementation of a finite element method, which is explained in detail in Section \ref{sec:algorithmdetails}.
As we will see in the next section, normal boundary conditions are enforced strongly in the finite element space while tangential boundary conditions are enforced weakly.
%\red{Maybe add more discussion of boundary conditions. What about $\B \times \n$. Does it work in 2d? Mention 3d $\E \cross n$.}

\subsection{Discretisation and Linearisation}\label{sec:DiscretisationandLinearisation}
We mainly focus in this section on the normal boundary conditions $\B \cdot \n = 0$ on $\partial \Omega$.
For the definition of the weak formulation of \eqref{eq:MHDFinal} we look for $\mathcal{U} \coloneqq(\uu,p,\B,\E)\in \ZZ \coloneqq \V\times Q \times \W \times R$ with
\begin{equation}
    \V \coloneqq \Hozv, \quad Q\coloneqq L^2_0(\Omega),\quad \W \coloneqq \hzdiv, \quad  R \coloneqq H^1_0(\Omega).
\end{equation}
Then, the weak formulation is to find $\mathcal{U} \in \ZZ$ such that for all $\mathcal{V}\coloneqq(\vv,q,\C,\F) \in \ZZ$ and $\mathcal{F} =  (\f, 0, \mathbf{0}, 0)$ there holds 
\begin{equation} \label{eq:weakform}
    \N(\mathcal{U}, \mathcal{V})=  (\mathcal{F},\mathcal{V})
\end{equation}
with
\begin{align}
\begin{split}
    \N(\mathcal{U},\mathcal{V}) & =\frac{2}{\re}(\mathbf{\varepsilon}(\uu), \mathbf{\varepsilon}(\vv)) + (\uu\cdot \grad \uu, \vv) + \gamma (\div \uu, \div \vv) \\
    & - (p,\div \vv) + S\, (\B \cross \E, \vv) + S\, (\B \cross (\uu \cross \B), \vv)  \label{eq:NN3} \\
    &- (q,\div \uu) \\
    &+ (\E, \F) + (\uu\cross \B, \F) - \frac{1}{\rem}(\B, \vcurl \F) \\
    &+ \frac{1}{\rem} (\div \B, \div \C) + ( \vcurl \E, \C).
\end{split}
\end{align}
All boundary integrals that result from integration by parts vanish because of the choice of the boundary conditions \eqref{eq:boundarycond}.\\
Note that $\W$ and $R$ are chosen from the continuous exact De Rham complex 
\begin{equation}\label{eq:contDC}
    \R \xrightarrow[]{\text{id}} \Hoz \xrightarrow[]{\vcurl} \hzdiv \xrightarrow[]{\div} L^2_0(\Omega) \xrightarrow[]{\text{null}} 0.
\end{equation}
This ensures that formulation \eqref{eq:MHDFinal} enforces the divergence constraint $\div \B =0$ and $\vcurl \E = \mathbf{0}$. To see this, we test \eqref{eq:weakform} with $\mathcal{V} = (\mathbf{0}, 0, \vcurl \E, 0)$ and conclude that $\vcurl \E =0$. Here, $\mathcal{V}$ is a valid test function because the above exact sequence implies that $\mathrm{range}(R) = \W$. Now, testing with $\mathcal{V} = (\mathbf{0}, 0, \div \B, 0)$ results in $\div \B = 0$.\\
The Newton linearisation of \eqref{eq:weakform} for the initial guess $\mathcal{U}^0 = (\uu^0,p^0,\B^0,\E^0)$ is to find an update $\delta \mathcal{U}$ for given $\mathcal{U}^n$ such that 
\begin{align}
    \N_N(\delta\mathcal{U},\mathcal{U}^n,\mathcal{V})&=R(\mathcal{U}^n,\mathcal{V}) \quad \forall \ \mathcal{V} \in \ZZ,\\
    \mathcal{U}^{n+1} &= \mathcal{U}^n + \delta\mathcal{U}
\end{align}
with
\begin{align}
\label{eq:Newton}
\begin{split}
    \N_N(\delta\mathcal{U},\mathcal{U}^n,\mathcal{V}) &= \frac{2}{\re}(\mathbf{\varepsilon}( \delta\uu), \mathbf{\varepsilon}(\vv)) + (\uu^n\cdot \grad \delta\uu, \vv) + (\delta\uu\cdot \grad \uu^n, \vv)  \\
    &+ \gamma (\div \delta\uu, \div \vv) - (\delta p,\div \vv)\\
    & +  (\B^n\cross \delta \E, \vv)  +   (\delta\B \cross \E^n, \vv)  \\
    & +  (\B^n \cross (\delta \uu \cross \B^n), \vv) + 
     (\delta\B \cross (\uu^n \cross \B^n), \vv) \\
    &+  (\B^n \cross  (\uu^n \cross \delta\B), \vv)\\
    &- (q,\div \delta\uu) \\
    & + (\delta \E, \F) + (\uu^n\cross\delta\B, \F) + (\delta\uu \cross \B^n, \F) \\
    &- \frac{1}{\rem} (\delta \B, \vcurl \F) \\
    &+ \frac{1}{\rem} (\div \delta \B, \div \C) + (\vcurl \delta \E, \C).
\end{split}
\end{align}

Here, $R(\mathcal{U}^n, \mathcal{V})$ is the weak form of the nonlinear residual with respect to $\mathcal{U}^n$.
The nonlinear form of the Picard iteration considered in \cite{Hu2020} is given by 
\begin{align}
\label{eq:Picard}
\begin{split}
    \N_P(\delta\mathcal{U},\mathcal{U}^n,\mathcal{V}) &= \frac{2}{\re}(\mathbf{\varepsilon}( \delta\uu), \mathbf{\varepsilon}(\vv)) + (\delta\uu\cdot \grad \uu^n, \vv) + (\uu^n\cdot \grad \delta\uu, \vv) \\
    & + \gamma (\div \delta\uu, \div \vv) - (\delta p,\div \vv)\\
    & +  (\B^n\cross \delta \E, \vv) +  (\B^n \cross (\delta \uu \cross \B^n), \vv) \\
    &- (q,\div \delta\uu) \\
    & + (\delta \E, \F) + (\delta\uu \cross \B^n, \F) - \frac{1}{\rem} (\delta \B, \vcurl \F) \\
    &+ \frac{1}{\rem} (\div \delta \B, \div \C) + (\vcurl \delta \E, \C).
\end{split}
\end{align}
In comparison to the Newton linearisation $\mathcal{N}_N$ the terms $  (\delta\B \cross (\uu^n \cross \B^n), \vv)$, $ (\B^n \cross  (\uu^n \cross \delta\B), \vv)$ and $(\uu^n\cross\delta\B, \F)$  have been omitted. Note that in contrast to \cite{Hu2020}, we do not scale the term $(\vcurl \delta \E, \C)$ with $1/\rem$ and consider the full Newton linearisation of the advection term $(\uu\cdot \grad) \uu$.
Furthermore, we propose a new nonlinear scheme that we call \emph{minimal decoupling Picard} iteration, which eliminates the term $(\delta \uu \times \B^n, F)$ from the Newton form, i.e.,
\begin{equation}
\label{eq:PicardG}
    \N_{MDG}(\delta\mathcal{U},\mathcal{U}^n,\mathcal{V}) = \N_{N} (\delta\mathcal{U},\mathcal{U}^n,\mathcal{V}) - (\delta \uu \times \B^n, F).
\end{equation}
As we show later, the advantage of this scheme is that we are able to control the Schur complement of the linear system while the nonlinear scheme still converges well for high $\rem$. \\
If we consider the tangential boundary conditions $\B \cdot \mathbf{t}=0$, we don't include boundary conditions in the finite element space, i.e., we look for $\B \in \hdiv$ in \eqref{eq:weakform}. In this case, the boundary condition is weakly enforced since we omit the term
\begin{equation}
    \frac{1}{\rem} \int_{\partial \Omega} \delta \B\cdot \mathbf{t} \F \ds
\end{equation}
that results from integration by parts of the term $\frac{1}{\rem} (\scurl \B, \F)$.
\\
For a structure-preserving finite element discretisation, we look for $\mathcal{U}_h:=(\uu_h, p_h, \B_h, \E_h) \in \ZZ_h \coloneqq \mathbf{V}_h \times Q_h \times \W_h\times R_h$ such that 
\begin{equation} \label{eq:weakformdiscr}
    \N(\mathcal{U}_h, \mathcal{V}_h)=  (\mathcal{F},\mathcal{V}_h) \quad \forall\, \mathcal{V}_h \in \mathbf{Z}_h.
\end{equation}
We choose Raviart-Thomas elements $\mathbb{RT}_k$ \cite{Raviart1977} for $\W_h$ and continuous Lagrange elements $\mathbb{CG}_k$ for $E_h$. Note that these elements belong to a discrete analogue of \eqref{eq:contDC} given by 
\begin{equation}
    \mathbb{CG}_k \xrightarrow[]{\vcurl} \mathbb{RT}_k \xrightarrow[]{\div} \mathbb{DG}_{k-1} \xrightarrow[]{\text{null}} 0.
\end{equation} This implies that we enforce $\div \B_h=0$ and $\vcurl \E_h=0$ pointwise with the same proof as for the continuous case. These identities also hold for non-homogeneous boundary conditions since the interpolation operator $I^h_{\W_h}$ into the Raviart-Thomas space satisfies \cite[Prop. 2.5.2]{Boffi2013}
\begin{equation}\label{eq:InterpolationPreserveRT}
    \div(\I^h_{\W_h} \B) = 0 \quad \forall\, \B \in \mathbf{H}(0, \mathrm{div})
\end{equation}
where $\mathbf{H}(0, \mathrm{div}) = \{\C \in \hzdiv \, | \, \div \C = 0\}$. To be more precise, we consider the non-homogeneous boundary condition $\B \cdot \n = g_1$ for $g_1 \in H^{\frac{1}{2}}(\partial \Omega)$ and assume for the solvability of the problem that there exists a $\B_{g_1}  \in \mathbf{H}(0, \mathrm{div})$ such that $\B_{g_1}\cdot \n = g_1$. For a finite element approximation, one then computes $\I^h_{\W_h}\B_{g_1} = \sum_{i=1}^{N} B_{i, g_1} \mathbf{\Phi}_i  $ in terms of the $N$ basis function $\mathbf{\Phi}_i$ of $\W_h$ and sets, cmp. \cite{LectureNotesBoundary},
\begin{equation}
    \B_{h,g_1} = \sum_{i=1}^{N_D} B_{i, g_1} \mathbf{\Phi}_i.
\end{equation} Here, we have chosen the ordering that the basis functions that have non-vanishing normal components on the boundary are the first $N_D$ ones. Then, we look for a solution of the form 
\begin{equation}
    \B_h = \B_{h,{g_1}}+ \B_{h,0} \quad \text{ with }\quad \B_{h,0} = \sum_{i=N_D+1}
^N B_i \mathbf{\Phi}_i.
\end{equation} Hence, the approximation of the weak form of \eqref{eq:MHDFinal4} is given by
\begin{equation}\label{eq:weaknonhom}
    \frac{1}{\rem}(\div \B_{h,0}, \div \varphi_i) + (\vcurl \E_h, \varphi_i) = - \frac{1}{\rem} (\div \B_{h, g_1}, \div \varphi_i) \quad i=N_D + 1,...,N.
\end{equation}
As before, we can conclude that $\vcurl \E_h=0$ since $\vcurl \E_h \in \mathrm{span}\{\mathbf{\Phi}_i \, | \, i=N_D+1,...,N\}$ by the sequence \eqref{eq:contDC}.
Equation \eqref{eq:weaknonhom} implies for the homogeneous test functions $\mathbf{\Phi}=\B_{h,0}$ and $\mathbf{\Phi}=\B_{h,g_1}-\I^h_{\W_h}\B_{g_1}$ 
\begin{equation}\label{eq:divres}
    (\div \B_{h}, \div \B_{h,0}) = 0 \quad\text{ and }\quad (\div \B_{h}, \div (\B_{h,g_1}-\I^h_{\W_h}\B_{g_1})) = 0.
\end{equation}
But there holds $\div (\I^h_{\W_h}\B_{g_1})=0$  by the interpolation property \eqref{eq:InterpolationPreserveRT} and thus adding the two expressions in \eqref{eq:divres} shows that $\div \B_h=0$. \\
The same approach applied for non-homogeneous boundary conditions $\E=g_2$ on $\partial \Omega$ would add a term $ (\vcurl E_{h,g_2}, \mathbf{\Phi}_i)$ to the right-hand side of \eqref{eq:weaknonhom}. But since $\vcurl \E=0$ implies that $g_2$ is constant, this extra term vanishes and thus $\vcurl \E_h=0$ holds by the same proof as before.
\\
Moreover, we use Scott-Vogelius elements \cite{scott_conforming_1985,scott1985}, i.e.\ $(\mathbb{CG}_k)^2\times \mathbb{DG}_{k-1}$-elements, to discretise $\uu$ and $p$. This discretisation ensures that $\div \uu_h =0$ holds pointwise \cite{john2017} due to the fact that $\div \mathbf{V}_h \subset Q_h$. Additionally, it  exhibits pressure robustness, i.e. that for high Reynolds numbers the error estimates do not degrade \cite{john2017}. Note that Scott-Vogelius elements are only stable on certain type of meshes. For this reason we consider barycentric refined meshes as described in \cite{Farrell2020}, which ensure stability for polynomial order $k=d$ where $d$ denotes the spatial dimension \cite[Section 4.6]{qin1994}. We construct a multigrid hierarchy as illustrated in Figure \ref{fig:bary-hierarchy}. The idea is to refine a initial triangulation $\mathcal{M}_H$ uniformly to $\mathcal{M}_h$. We call these meshes \emph{macro meshes} and its cells \emph{macro cells}. The multigrid hierarchy is obtained by barycentrically refining $M_H$ and $M_h$. Note that this hierarchy is not nested.
\begin{figure}[htbp]
  \centering
  \includegraphics[width=0.7\textwidth]{IMG/mg_hierarchy.png}
  \caption{A three level barycentrically refined multigrid hierarchy. \cite{Farrell2020}}\label{fig:bary-hierarchy}
\end{figure}

Hu et. al.\ prove in \cite[Theorem 4]{Hu2020} that \eqref{eq:weakformdiscr} is well-posed and has at least one solution. The solution is unique for suitable source and boundary data.
While the well-posedness and convergence of the Newton and minimal decoupling Picard iteration remain open problems, Hu. et al.\ prove that the Picard iteration converges to the unique solution of \eqref{eq:weakformdiscr} if both $\re^2 \|f\|_{-1}$ and $\re\rem^\frac{3}{2} \|f\|_{-1}$ are small enough. We will see in the numerical examples that the Picard iteration already fails to converge for relative small values of $\rem$ in the range of $10
^2$, whereas the Newton and minimal decoupling Picard iteration still converge in the range of $10^4$.\\
For the Newton linearisation \eqref{eq:Newton}, we have to solve the following linear system in each step
\begin{equation}
\label{eq:matrix_upBE}
  \begin{bmatrix}
   \mathcal{F} +\D & \BB^T & \tilde{\J} + \tilde{\D}_1 + \tilde{\D}_2 & \J \\
   \BB & \mathbf{0} & \mathbf{0} & \mathbf{0} \\
   \mathbf{0} & \mathbf{0} & \frac{1}{\rem}\CC  & \A^T \\
   \G& \mathbf{0}& \tilde{\G} -\frac{1}{\rem} \A & \M_\E
  \end{bmatrix}
  \begin{bmatrix}
  x_\uu \\ x_p \\ x_\B \\ x_\E
  \end{bmatrix} = 
  \begin{bmatrix}
  R_\uu\\ R_p\\ R_\B\\ R_\E
  \end{bmatrix}
\end{equation}
where $x_\uu$, $x_p$, $x_\B$ and $x_\E$ are the discretised Newton corrections and $R_\uu$, $R_p$, $R_\B$ and $R_\E$ the corresponding nonlinear residuals.
The correspondence between the discrete and continuous operators is illustrated in Table \ref{tab:Operators}. We have chosen the notation that operators that include a '\emph{tilde}' are omitted in the Picard linearisation $\eqref{eq:Picard}$. In addition, the matrix $\G$ is dropped for the minimal decoupling Picard linearisation \eqref{eq:PicardG}.

\begin{table}[htb!]
  \begin{center}
    \label{tab:Operators}
    \renewcommand{\arraystretch}{1.2}
    \begin{tabular}{|c|c|c|} 
    \hline
      \textbf{Discrete} & \textbf{Continuous} & \textbf{Weak form}\\
      \hline
      $\mathcal{F} \uu$ & $-\frac{2}{\re} \div \varepsilon(\uu) + \uu^n\cdot \grad \uu + \uu \cdot \grad \uu^n$ & $\frac{2}{\re}(\varepsilon( \uu), \varepsilon( \vv)) + (\uu^n\cdot \grad \uu , \vv)$  \\
      & $-\gamma\grad \div \uu$ &  $+(\uu\cdot \grad \uu^n, \vv) + \gamma(\div \uu, \div \vv)  $  \\
      $\D \uu$& $\B^n\cross(\uu\cross\B^n)$ & $ (\B^n\cross (\uu \cross \B^n), \vv)$ \\
      $\J \E$ & $ \B^n\cross\E$ & $ (\B^n\cross\E, \vv)$   \\
      $\tilde{\J} \B$ & $ \B\cross\E^n$ & $ (\B\cross\E^n, \vv)$   \\
      $\tilde{\D}_1\B$ & $ \B \cross (\uu^n\cross\B^n)$ & $ (\B \cross (\uu^n\cross\B^n),\vv)$  \\
      $\tilde{\D}_2\B$ & $ \B^n \cross (\uu^n\cross\B)$ & $ (\B^n \cross (\uu^n\cross\B),\vv)$  \\
      $\M_\E \E$ & $\E$ & $(\E,\F)$  \\
      $\G \uu$ & $\uu \cross \B^n$ & $(\uu\cross\B^n, \F)$  \\
      $\tilde{\G} \B$ & $\uu^n \cross \B$ & $(\uu^n\cross\B, \F)$  \\
      $\A \B$ & $ \scurl \B$ & $ (\B, \vcurl \F)$ \\
      $\CC \B$ & $\grad \div \B$ & $(\div\B,\div\C)$  \\
      $\A^T \E$ & $\vcurl \E$ & $(\vcurl \E, \C)$ \\
      %$$ & $$ & $$ & $$ \\
      $\BB^T p$ & $\grad p$ & $-(p, \div \vv)$ \\
      $\BB \uu$ & $-\div \uu$ & $-(\div \uu, q)$  \\
      \hline
    \end{tabular}
    \caption{Overview of operators}
  \end{center}
\end{table}
