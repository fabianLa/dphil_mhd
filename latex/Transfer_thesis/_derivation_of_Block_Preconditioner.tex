\section{Derivation of a robust Block Preconditioner}\label{sec:derivationofblockpreconditioner}
For the derivation of a block preconditioner for \eqref{eq:matrix_upBE} we have found that a reordering of the unknowns to $(\E_h, \B_h, \uu_h, p_h)$ is the most convenient, which results in the linear system

\begin{equation}
\label{eq:matrix_EBup}
  \begin{bmatrix}
     \M_\E & \tilde{\G} - \frac{1}{\rem} \A & \G & \mathbf{0}\\
     \A^T & \frac{1}{\rem}\CC & \mathbf{0} & \mathbf{0} \\
     \J & \tilde{\J} + \tilde{\D}_1 + \tilde{\D}_2 & \mathcal{F} + \D & \BB^T\\
     \mathbf{0} & \mathbf{0} & \BB & \mathbf{0} 
  \end{bmatrix}
  \begin{bmatrix}
  x_\E \\ x_\B \\ x_\uu \\ x_p
  \end{bmatrix} = 
  \begin{bmatrix}
  R_\E\\ R_\B\\ R_\uu\\ R_p
  \end{bmatrix}.
\end{equation}
The outer Schur complement with respect to the hydrodynamic unknowns $(\uu_h,p_h)$ is given by 
\begin{equation}
\label{eq:outerSchurCompNewton}
    \mathcal{S}_N = 
    \begin{bmatrix}
      \F+\D & \BB^T \\
      \BB & \mathbf{0}
    \end{bmatrix}
    - 
    \begin{bmatrix}
      \J & \tilde{\J} + \tilde{\D}_1 + \tilde{\D}_2\\
      \zerov & \zerov
    \end{bmatrix}
        \begin{bmatrix}
     \M_\E& \tilde{\G} - \frac{1}{\rem} \A \\
      A^T & \frac{1}{\rem}\CC
    \end{bmatrix}^{-1}
     \begin{bmatrix}
       \G & \zerov\\
      \zerov & \zerov
    \end{bmatrix}.
\end{equation}
At this point, we immediately see that the Schur complement simplifies for the minimal decoupling Picard linearisation to
\begin{equation}
    \mathcal{S}_{MDP} = 
    \begin{bmatrix}
      \F+\D & \BB^T \\
      \BB & \mathbf{0}
    \end{bmatrix}.
\end{equation}
We simplify $\mathcal{S}_N$ with help of the formula \cite{Lu2002} for non-singular matrices 
$\A$ and $\D-\CC\A^{-1}\BB$
\begin{equation}
\label{eq:matrixinvers}
\begin{bmatrix}
    \A & \BB \\
    \CC & \D
  \end{bmatrix}^{-1} = \begin{bmatrix}
     \A^{-1} + \A^{-1} \BB (\D - \CC \A^{-1} \B)^{-1} \CC \A^{-1} &
      -\A^{-1}\BB(\D-\CC \A^{-1} \BB)^{-1} \\
    - (\D - \CC \A^{-1} \BB)^{-1} &
      (\D-\CC\A^{-1}\BB)^{-1}
  \end{bmatrix}
\end{equation}
applied to the top-left electromagnetic block 
\begin{equation}
\label{eq:M}
    \M = 
    \begin{bmatrix}
      \M_\E& \tilde{\G} - \frac{1}{\rem} \A \\
      \A^T & \frac{1}{\rem}\CC
    \end{bmatrix}.
\end{equation} 
This results in 
\begin{equation}
   \M^{-1}_{1,1} = \M_\E^{-1} + \M_\E^{-1}\left(\tilde{\G}-\frac{1}{\rem} \A\right)\left(\frac{1}{\rem}\CC - \A^T \M_\E^{-1}\left(\tilde{\G}-\frac{1}{\rem} \A\right)\right)^{-1}\A^T\M_\E^{-1}
\end{equation}
and
\begin{equation}
   \M^{-1}_{2,1} = -\left(\frac{1}{\rem}\CC - \A^T \M_\E^{-1}\left(\tilde{\G}-\frac{1}{\rem} \A\right)\right)^{-1}.
\end{equation}
Hence, 
\begin{equation}
    \mathcal{S}_N=
    \begin{bmatrix}
        \F + \D + \J\M^{-1}_{1,1} \G + (\tilde{\J} + \tilde{\D}_1 + \tilde{\D}_2)\M^{-1}_{2,1}\G& \BB^T \\
        \BB & \zerov 
    \end{bmatrix}.
\end{equation}
A further simplification for $\mathcal{S}_N$ is not straight-forward, but our numerical tests in the next section suggest that $\mathcal{S}_{MDP}$ is an acceptable preconditioner for $\mathcal{S}_N$.\\
Furthermore, we show now that the Schur complement for the Picard iteration $\mathcal{S}_{P}$ is also given by $\mathcal{S}_{MDP}$. Therefore, we have to simplify
\begin{align}
    \J\M^{-1}_{1,1} \G = J \M_E^{-1} \G -  J \M_E^{-1} \A \left(\CC + \A^T \M_\E^{-1}\A\right)^{-1}\A^T \M_E^{-1} \G.
\end{align}
The continuous relations
\begin{equation}
    \scurl \vcurl \uu - \grad \div \uu = - \Delta \uu
\end{equation}
and
%[]\red{(cmp. Phillips) this is only true in 2d, does it work in 3d?}
\begin{equation}
    \scurl \Delta^{-1} \vcurl \varphi = -\varphi
\end{equation}
imply for our structure preserving discretisation
\begin{equation}
    \A (\CC + \A^T \M_\E^{-1}\A)^{-1}\A^T = \M_E
\end{equation}
%\red{Maybe do the computation}
and hence
\begin{equation}
     \J\M^{-1}_{1,1} \G = \mathbf{0}.
\end{equation}
In summary, we have to find robust preconditioner for the electromagnetic subsystem $\M$ and the Schur complement (approximation) $\mathcal{S}_{MDP}$.

\subsection{Robust Multigrid Methods}\label{sec:robustmg}
In this section, we describe the parameter-robust multigrid methods that we apply to the electromagnetic block and the Schur complements. As we are interested in solvers for high Reynolds numbers, the equations we want to solve become nearly singular. For these kind of problems standard multigrid methods are known to perform poorly. The key components for a parameter-robust multigrid method are a parameter-robust relaxation method, that damps error modes in the kernel of the singular operators efficiently, and a kernel-preserving prolongation operator.\\
In this subsection, we follow \cite{farrell2019pcpatch, schoberl1999b}  and consider an abstract problem of the form: Find $u \in V$ for some finite element space $V$ such that
\begin{equation}\label{eq:abstractform}
    a(u,v) := \alpha a_0(u, v) + \beta b(\Lambda u, \Lambda 
    v) = (f, v) \text{ for all } v \in V,
\end{equation}
where $\Lambda: V \to Q$ for some space $Q$ is a continuous, $a_0$ is symmetric, bounded and coercive bilinearform on $V$ and $b$ is a symmetric, bounded and coercive on bilinearform on  $Q$. The kernel of $b$ is denoted by
\begin{equation}
    \mathcal{N}=\{u \in V: b(\Lambda u, q) = 0 \ \forall\, q \in Q\}.
\end{equation}
We use so-called subspace correction methods \cite{xu1992} as the relaxation method which decompose $V$ in a not necessarily direct sum
\begin{equation}\label{eq:decomp}
    V = \sum_{i} V_i.
\end{equation}
The parallel subspace correction method computes for an initial guess $u^k$ an approximation $\delta u_i$ to the error $e=u-u^k$ in each subspace $V_i$ by solving
\begin{equation}
    a(\delta u_i, v_i) = (f, v_i) - a(u^k, v_i) \text{ for all } v_i \in V_i
\end{equation}
and sets $u^{k+1} = u^k + \sum_i w_i \delta u_i$ for damping parameters $w_i$. \\
A rigorous statement which properties the decomposition \eqref{eq:decomp} has to fulfil to lead to a robust relaxation method can be in found in \cite[Theorem 4.1]{schoberl1999b}.
A key property is that the kernel $\mathcal{N}$ is decomposed over the subspaces, i.e., 
\begin{equation}\label{eq:kerneldecomp}
    \mathcal{N} = \sum_i (V_i \cap \mathcal{N}).
\end{equation}
As we will see in the next section, the definition of $V_i$ given by
\begin{equation}\label{eq:macrostarsubspace}
    V_i = \{v \in V: \mathrm{supp}(v) \subset \mathrm{macrostar}(v_i) \}
\end{equation}
ensures this property for the equations we want to solve. Here, $\mathrm{macrostar}(v_i)$ denotes the union of all macro cells touching the vertex $v_i$ in the macro mesh.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=0.7\textwidth]{IMG/macrostar.png}
  \caption{The macrostar patch for a vertex in the macro mesh. \cite{Farrell2020}}\label{fig:macrostar}
\end{figure}
A second key component for a robust multigrid method is a robust prolongation operator. The prolongation operator defined in \cite[Section 4.3]{Farrell2020} ensures that the prolongation of divergence-free velocities $\uu_H$ on the coarse grid are still divergence-free on the fine grid.  

\subsection{Solver for the electromagnetic block}\label{sec:solverformagneticblock}
At first, we consider the electromagnetic block $\M$ for the Picard iteration. In this case, $\M$ corresponds to the discretisation of a mixed formulation for the standard vector Laplace problem \cite{Arnold2006} with normal boundary conditions
\begin{align}
\begin{split}
    \frac{1}{\rem}\left(\vcurl \scurl \B - \grad \div \B\right) &= \f \text{ in } \Omega, \\
    \B \cdot \mathbf{n} &= 0 \text{ on } \partial \Omega, \\
    \scurl \B &= 0 \text{ on } \partial \Omega.
\end{split}
\end{align}
One can derive a stable formulation by giving up the $\Hozv$-conformity of $\B$ and look for $\B \in \hzdiv$. Then, one has to introduce a new unknown $\E = \frac{1}{\rem}\scurl \B$ and obtains the weak formulation of the electromagnetic subsystem for the Picard iteration: Find $(\E, \B) \in \Hoz \times \hzdiv$ such that
\begin{equation}
\begin{aligned}
    (\E, \F) - \frac{1}{\rem}(\B, \scurl \F) &= 0 &&\forall\, \F \in \Hoz,\\
    (\scurl \E, \C) + \frac{1}{\rem}(\div \B, \div \C) &= (\f, \C) &&\forall\, \C \in \hzdiv.
\end{aligned}
\end{equation}
Chen et al.\ propose in \cite{Chen2018} a Schur complement solver and Arnold et al.\ in \cite{Arnold2006} a block diagonal preconditioner for the mixed formulation. Note that the Schur complement is spectrally a vector Laplacian but on a H(div)-space. In order to avoid the approximation of the Schur complement with a discontinuous Galerkin formulation we use the norm equivalent block preconditioner described from \cite{Arnold2006}. We solve the $\hdiv$ Riesz map with the macrostar multigrid solver described in the previous section. We also found that the macrostar multigrid solver applied monolithically to $\M$ results in an efficient solver. 
%Since the top left block corresponds to a mass-matrix that is easy to solve, we expect the monolithic approach to work as well as a schur complement preconditioner, cmp. $\red{cite Berg}$. \red{Can we prove that? Does normal multigrid work for this block? No! Or kernel capturing? Yes.}
Note that both solver are $\rem$-robust, since the vector Laplace equation is just multiplied by the magnetic Reynolds number.\\
In contrast, the presence of the additional term $\tilde{\G}$, which has a non-trivial kernel, makes the problem almost singular for high $\rem$ and requires the special multigrid method described in the last section. We note that now the weak formulation of the electromagnetic block
\begin{equation}
\begin{aligned}
    (\E, \F) - (\B, \scurl \F) + (\uu^n \times \B, \F) &= 0 &&\forall\, \F \in \Hoz,\\
    (\scurl \E, \C) + (\div \B, \div \C) &= (\f, \C) &&\forall\, \C \in \hzdiv.
\end{aligned}
\end{equation}
corresponds to a mixed formulation of
\begin{align}
    \frac{1}{\rem}\left(\vcurl \scurl \B - \grad \div \B\right) + \vcurl(\uu^n\times\B) &= \f \text{ in } \Omega,\label{eq:LaplaceGtilde} \\
    \B \cdot \mathbf{n} &= 0 \text{ on } \partial \Omega, \\
    \frac{1}{\rem} \scurl \B - \uu^n\times \B &= 0 \text{ on } \partial \Omega.
\end{align}
%One can also formally compute the Schur complement of $\M$
%\begin{equation}
%    \mathcal{S}_\mathcal{M} = \frac{1}{\rem} (\CC -\A^T \M_\E^{-1}\A) - \A^T\M_\E^{-1}\tidle{G}
%\end{equation}
%which corresponds on a continuous level to 
%\begin{equation}\label{eq:SchurCont2}
%    -\frac{1}{\rem} \Delta \B + \vcurl(\uu^n \times \B).
%\end{equation}
 Although the extra term $\vcurl(\uu^n \times \B)$ does not fit in the framework of the last section, since it is not symmetric, we found
that the multigrid method with a macrostar relaxation method applied monolithically to $\M$ gives a robust preconditioner for this problem. Moreover, the kernel of $\vcurl(\uu^n \times \B)$ is not straightforward to characterise. Nevertheless, we want to mention that
with help of the vector identity
\begin{equation}
    \vcurl (\mathbf{A} \times \mathbf{B}) =  (\mathbf{B} \cdot \nabla) \mathbf{A} -  (\mathbf{A} \cdot \nabla) \mathbf{B} +
    \mathbf{A}(\nabla \cdot \mathbf{B}) - \mathbf{B}(\nabla \cdot \mathbf{A}) 
\end{equation}
 we can simplify \eqref{eq:LaplaceGtilde} to
\begin{equation}
    -\frac{1}{\rem}\Delta \B - (\B \cdot \grad) \uu^n + (\uu^n \cdot \nabla) \B  - \uu^n(\nabla \cdot \mathbf{B}) - \mathbf{B}(\nabla \cdot \uu^n).
\end{equation}
The last term $ - \mathbf{B}(\nabla \cdot  \uu^n)$ vanishes since we exactly enforce $\nabla \cdot \uu^n$ in each step. 
%We emphasise that the remaining three terms do not fit in the framework of \red{cite Schöberl} for a parameter-robust preconditioner.
A starting point for a further analysis could be the observation that the terms  $-(\B \cdot \grad) \uu^n + (\uu^n \cdot \nabla) \B $ have a similar structure to the Newton linearisation of the advection term $(\uu \cdot \grad) \uu$ of the Navier-Stokes equation for which a macrostar multigrid method is an effective solver. 
%\red{Does Schoberl apply to $-\uu
%^n(\div \B)$?}. \\
%\red{What about prolongation for $\B$?}

\subsection{Solver for the Schur complement $\mathcal{S}_{MDP}$}\label{sec:solverforschurcomp}
In the following, we describe a fluid Reynolds-robust preconditioner for the outer Schur complement $\mathcal{S}_{MDP}$. Note that this block corresponds to the standard Newton linearisation of the Navier-Stokes equations with an augmented Lagrangian term plus the linearisation of the Lorentz force $\mathcal{D}$. We use the strategy developed by Farrell et al.\ in \cite{Farrell2020} to solve this system. The first idea is to use the augmented Lagrangian term $\gamma \grad \div \uu$ to control the Schur complement of $\mathcal{S}_{MDP}$ by choosing a large $\gamma \approx 10^4$. One can show that the Schur complement of the augmented system $\tilde{\mathcal{S}}$ satisfies
\begin{equation}
    \tilde{\mathcal{S}}^{-1} = \mathcal{S}^{-1}-\gamma \M_p^{-1}
\end{equation}
where $\mathcal{S}$ denotes the Schur complement of the system without the augmented Lagrangian term and $\M_p$ denotes the pressure mass matrix. Therefore, for large $\gamma$ the scaled pressure mass matrix results in a good approximation for $\tilde{\mathcal{S}}$ for which efficient solvers are well known.\\
Since the augmented Lagrangian term has a large kernel that consists of all solenoidal vector fields a robust multigrid scheme described in Section \ref{sec:robustmg} has to be used to solve the top-left block. In the following, we briefly describe why the macrostar subspace decomposition \eqref{eq:macrostarsubspace} fulfils the property \eqref{eq:kerneldecomp} for the augmented Lagrangian term for Scott-Vogelius elements. Therefore, we consider a discrete subcomplex 
\begin{equation}\label{eq:DeRham2d}
    ...\xrightarrow[]{} \Sigma_h\xrightarrow[]{\vcurl}
    \mathbf{V}_h\xrightarrow[]{\div}
    Q_h\xrightarrow[]{\textrm{null}} 0
\end{equation}
of the Hilbert complex
\begin{equation}
    \mathbb{R}\xrightarrow[]{\mathrm{id}} H^2\xrightarrow[]{\vcurl}
    \mathbf{H}^1\xrightarrow[]{\div}
    L^2\xrightarrow[]{\textrm{null}} 0.
\end{equation}
Since the sequence \eqref{eq:DeRham2d} is exact, we can find a $\Phi_h \in \Sigma_h$ such that $\vcurl \Phi_h$ = $\uu_h$ for $\uu_h \in \mathbf{V}_h$ with $\div \uu_h=0$. Hence, we can write $\uu_h = \sum_{j} \vcurl \Phi_j$ for the basis functions $\Phi_j$ of $\Sigma_h$. Choosing $\Sigma_h$ to be the $\mathbb{HCT}$ elements \cite[§6.1]{ciarlet1978} we obtain an exact sequence together with the Scott-Vogelius elements. An illustration of the elements is given in Figure \ref{fig:DeRham2d}. We observe that for the macrostar decomposition for every $\Phi_j$ there exists a vertex $v_i$ such that $\mathrm{supp}(\Phi_j) \subset \mathrm{macrostar}(V_i)$. Thus, this composition fulfils the kernel decomposition property \eqref{eq:kerneldecomp}.\\

\begin{figure}[htbp]
  \centering
  \includegraphics[width=0.7\textwidth]{IMG/2d_stokes_complex.png}
  \caption{2d exact Stokes complex with the $\mathbb{HCT}$, $\mathbb{CG}_2^2$, and $\mathbb{DG}_1$ elements. \cite{Farrell2020}}\label{fig:DeRham2d}
\end{figure}
The construction of the special prolongation operator that is needed for a robust multigrid in presence of the augmented Lagrangian term is presented in \cite[Section 4.3]{Farrell2020}. The main idea of the robust prolongation is to ensure that divergence-free velocities on the coarse grid are mapped to divergence-free velocities on the fine grid. The problematic of the non-nested multigrid mesh hierarchy is approached by a correction of the standard prolongation operator in each macro cell.\\
The top-left block consists of further singular terms given by the convection diffusion term $(\uu\cdot \grad) \uu$, the linearisation of the Lorentz force $\B^n \times (\uu \times \B^n)$ and the stabilisation term $\mathcal{ST}(\uu, \vv)$.
Note that the first two terms do not fit in the framework of \eqref{eq:abstractform} presented in Section \ref{sec:robustmg} and its kernels are not straightforward to characterise. Nevertheless, numerical experiments in the next section and also in \cite{Farrell2020} show that these term does not degrade the performance of the preconditioner notably. The kernel of $\mathcal{ST}(\uu, \vv)$ consists of all $C^1$ vector fields. In \cite[Section 4.2]{Farrell2020} the heuristic argument is outlined that for $k\geq 3$ the macrostar captures the support of the $\mathbb{HCT}$ elements and therefore decomposes the kernel of the stabilisation term.
Since we only use second order elements to discretise $\uu$ the stabilisation term degrades the performance of the solver slightly. But the impact is not very significant since the factor $\delta h_{\partial K}^2$ is small.
