\section{Introduction}
%General things to do
%\begin{itemize}
%    \item Viscous term, eps(u) 
%    \item Get all interpolation operators right
%    \item Change magnetic subsystem to electromagnetic subsystem.
%    \item Picard with a capital P
%    \item Make components of bold vector functions not bold
%    \item Discrete fields should have a index h
%    \item Newton, Picard with capital letters
%    \item Mention that we use Newton for Navier stoeks, even for picard in total.
%    \item we use different scaling than Hu, their system is symmetric, but no numerical differences.
%    \item Exchange PicardG by minimal decoupling Picard
%    \item Figure, Table, Section all capital letters
%    \item $\gamma$ or $\gamma_1$ for fluid AL?
%    \item include scalable somewhere
%    \item two-dimensional
%\end{itemize}
The magnetohydrodynamics (MHD) equations describe the flow of electrically conducting fluids in the presence of a electromagnetic field. They have numerous important applications in astrophysics, geophysics, liquid metal industry and controlled thermonuclear fusion. The development of numerical methods is an area of active research and known to be very challenging due to the highly nonlinear character of the system and a strong coupling between the fluid and magnetic field; especially for high Reynolds numbers.\\
In this work, we consider the stationary incompressible viscoresistive MHD equations on a polyhedral domain $\Omega\subset\R^2$ of the form 
\begin{subequations}
\label{eq:MHD}
\begin{align}
   - \frac{2}{\mathrm{Re}} \div \mathbf{\varepsilon}( \uu) + \uu \cdot \nabla \uu + \nabla p + \B \cross (\E + \uu \cross \B) &= \f, \label{eq:MHD1}\\
  \div \uu&=0, \label{eq:MHD2}\\
       \E + \uu \cross \B - \frac{1}{\rem} \scurl \B &= \mathbf{0}. \label{eq:MHD3}\\
       \vcurl \E &= 0, \label{eq:MHD4}\\
       \div \B &= 0. \label{eq:MHD5}
\end{align}
\end{subequations}
Here, $\uu$ denotes the velocity, $p$ the fluid pressure, $\B$ the magnetic field, $E$ the electric field, $\re$ the fluid Reynolds number, $\rem$ the magnetic Reynolds number, $\f$ a source term and $\mathbf{\varepsilon}( \uu) = \frac{1}{2} (\grad \uu + \grad \uu^\top )$.
The system is completed with the boundary conditions
\begin{equation}\label{eq:boundarycond}
   \uu=\mathbf{0},\quad  \E = 0 \quad \text{ and either  } \quad \B \cdot \n=0 \text{ or } \B \cdot \mathbf{t} = 0 \quad \text{ on } \del \Omega,
\end{equation}
where $\n$ is the unit outer normal vector and $\mathbf{t} = (\n_2, -\n_1)$ is the unit tangential vector. Both boundary conditions can be motivated from a physical point of view \cite{Gerbeau2006, Gunzburger1991}.
The main contribution of this work is to provide a new Reynolds-robust block-preconditioner for this model. Our approach is based on a robust augmented Lagrangian preconditioner for the Navier-Stokes equations developed by Farrell et al.\ in \cite{Farrell2020} and we use similar ideas to treat the electromagnetic block. We consider three different linearisation schemes and derive expressions for the Schur complement of the arising block system.

The above formulation based on the electric and magnetic field was first rigorously analysed by Hu et al. \cite{Hu2020} for the three-dimensional case and the two-dimensional formulation considered here can be derived from this work in a straightforward manner. Other formulations include the current density $j= \E + \uu \times \B$ \cite{Hu2018} as unknown or eliminate the electric field using equation \eqref{eq:MHD3}. One of the main challenges is to enforce the Faraday law $\div \B=0$ in the weak formulation, which is achieved in most discretisations by a non-physical Lagrange multiplier $r$ \cite{Schneebeli2003, Schoetzau2004}. However, in general a Lagrange multiplier only enforces the divergence constraint in a weak sense, which can cause severe problems for numerical simulations \cite{Brackbill1980, Dai1998}. Therefore, in recent years increased attention has been paid to derive discretisations that enforce $\div \B=0$ pointwise. These approaches include the use of a magnetic vector-potential \cite{Adler2016, Adler2019, Cyr2013, Hiptmair2018}, exact penalty methods on convex domains \cite{Phillips2014},  compatible discretisations \cite{Hu20162, Hu2018}, the use of divergence-free basis functions \cite{Cai2013} and divergence-cleaning methods \cite{Brackbill1980}. For the $\E-\B$ formulation \eqref{eq:MHD} Hu et. al show that both a Lagrange multiplier and an augmented Lagrangian term lead to a point-wise preservation of the Faraday law. In this work, we consider the latter approach by modifying \eqref{eq:MHD4} to 
\[
\frac{1}{\rem}\grad \div\B + \vcurl \E = \mathbf{0}.
\]
The literature proposes numerous numerical schemes and preconditioning strategies for the numerical solution of the different formulations. The most common approach is based on block preconditioners in the stationary \cite{Li2017,  PhillipsPHD, Phillips2014, Wathen2017, Wathen2020} and time-dependent \cite{Chacn20082,Chacn2008,Cyr2013, Phillips2016} case. Here, the main challenges are to find a suitable approximation of the one or more Schur complements and robust linear solvers for the inner auxiliary problems. In \cite{Phillips2016}, Phillips et al.\ simplify the Schur complement by the use of vector identities and approximate the remaining parts based on a spectral analysis. A similar approach is used by Wathen and Greif in \cite{Wathen2020} where they construct an approximate inverse block preconditioner by sparsifying a derived formula for the exact inverse and drop low order terms. Other approaches include fully-coupled solvers for geometric \cite{adler2020monolithic, Adler2016} and algebraic \cite{Shadid2010, Shadid2016} monolithic multigrid methods.\\
However, the performance of most of these preconditioners deteriorates significantly for high Reynolds numbers and a practical Reynolds-robust preconditioner for the stationary MHD equations has not yet been proposed in the literature, to the best of our knowledge. The common problem for high magnetic Reynolds numbers for the stationary case is that the Schur complement approximation becomes less accurate for Newton-type linearisation, which causes the linear solver to fail to converge. On the other hand, Picard-type linearisations allow an exact computation of the Schur complement but fail to converge in the nonlinear iteration.\\
Ma et al.\ \cite{Ma2016} have developed Reynolds-robust preconditioners for the time-dependent MHD equations that are based on norm-equivalent and field-of-values equivalent approaches. We want to mention that their approach does not extend to the stationary case and that, in general, the time-dependent case offers crucial advantages for the development of robust solvers. Firstly, Ma et al.\ treat complicated terms like the hydrodynamic convection term $\uu \cdot \grad \uu$ explicitly in the time-stepping scheme, which does not work for convection-dominated problems or in the stationary case. Moreover, mass matrices with a scaling of $1/k$, where $k$ denotes the time step size, appear in the block matrix on the diagonal blocks. Since the preconditioner is only robust for small enough $k$, the mass matrices, for which robust solvers are well-known \cite{Elman2014}, dominate the scheme.\\
In this work, we consider three different linearisations. The first one is the Picard iteration that was proposed by Hu et. al. in \cite{Hu2020}. We compute the exact outer Schur complement of the arising block system and introduce a robust linear solver for the different blocks. This scheme works well for small magnetic Reynolds numbers but the nonlinear iteration fails to converge for higher $\rem$. Furthermore, we derive an approximation of the Schur complement of the Newton linearisation, which converges well for high Reynolds numbers, and a robust solver for electromagnetic block. The third new linearisation scheme, that we call \emph{minimal decoupling Picard} iteration, keeps the terms from the Newton scheme that are important for the convergence of the nonlinear scheme, but still allows us to compute the outer Schur complement exactly.\\
The remainder of this work is outlined as follows. In Section \ref{sec:FormulationandDiscretisationin2d}, we derive an augmented Lagrangian formulation for \eqref{eq:MHD} and describe finite element discretisations for different linearisation schemes. In Section \ref{sec:derivationofblockpreconditioner}, we introduce block preconditioners for these schemes, present a calculation of the corresponding (approximate) Schur complements and describe robust linear multigrid solvers for the different blocks. Numerical examples and a detailed description of the algorithm are presented in Section \ref{sec:numericalresultsin2d}. We discuss in Section \ref{sec:extensionto3d} the extension of the presented solver to three dimensions and discuss additional problems that arise in this case. Finally, we outline in Section \ref{sec:conclusionandoutlook} our plans for future work.
\subsection{Notation}
Throughout this paper, we assume that $\Omega\subset \R^2$ is a bounded Lipschitz polyhedron that is simply connected in order to ensure that the De Rham complexes, we consider later in this work, are exact. We use the convention that vector-valued functions and function spaces are denoted by bold letters. The standard $\mathrm{L}^2$ inner product is denoted by $(u, v)$ and the $\mathrm{L}^2$ norm by $\|u\|_0$. Both are used for scalar- and vector valued functions. Since we consider the two-dimensional case there exists two different curl-operators given by
\begin{equation}
\scurl \B = \del_x B_2 - \del_y B_1, \qquad  \vcurl \varphi = \begin{pmatrix}
    \del_y \varphi \\
    -\del_x \varphi
  \end{pmatrix}
\end{equation}
that correspond to the cross-products
\begin{equation}
\mathbf{A} \cross \B =A_1 B_2 - A_2 B_1, \qquad
    \B \cross \varphi =\begin{pmatrix}
    B_2 \varphi \\
    -B_1 \varphi
  \end{pmatrix}.
\end{equation}
We use the function-spaces
\begin{align}
    \Hozv &= \{\vv \in \Hov \, | \, \vv = \mathbf{0} \text{ on } \partial \Omega \}, \\
    \hzdiv &= \{\C \in \mathbf{L}^2(\Omega) \, | \, \div \C \in \mathrm{L}^2(\Omega), \, \C \cdot \n = 0 \text{ on } \partial \Omega\}, \\
    \mathrm{L}^2_0(\Omega) &= \{q \in L^2(\Omega) \, | \, \int_{\Omega} q \ddx = 0 \}
\end{align}
where $\n$ denotes the unit outward normal vector on the boundary of $\Omega$. \\
The interpolant of a function $u$ into a finite element space $V_h$ with a set of degrees of freedom $\{{\ell_{h,i}(\cdot)}\}$ and basis functions $\{\varphi_i\}$ is represented by
\begin{equation}
    \I^h_{V_h}(u) = \sum_i \ell_{h,i}(u) \varphi_i.
\end{equation}