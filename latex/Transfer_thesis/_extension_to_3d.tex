\section{Extension to 3d}\label{sec:extensionto3d}
In this section, we extend the augmented Lagrangian formulation that we presented in Section \ref{sec:DiscretisationandLinearisation} to three dimensions. We illustrate the major differences to the two-dimensional formulation and describe the additional problems that arise for the multigrid solver for the electromagnetic block we derived in Section \ref{sec:solverformagneticblock}. \\

\subsection{Formulation and Discretisation in 3d}
The main difference for the three-dimensional formulation is that the electric field $\Ee$ is now a vector field subject to the boundary conditions $\Ee \times \n = \mathbf{0}$. Therefore, $\Ee$ is treated as an element of $\hzcurl$ instead of $H^1_0(\Omega)$. The weak formulation is to find an $\mathcal{U} \in \ZZ := \Hozv \times L^2_0(\Omega) \times \hzdiv \times \hzcurl$ such that for all $\mathcal{V}\coloneqq(\vv,q,\C,\mathbf{F}) \in \ZZ$ and $\mathcal{F} =  (\f, 0, \mathbf{0}, \mathbf{0})$ there holds 
\begin{equation} \label{eq:weakform3d}
    \N(\mathcal{U}, \mathcal{V})=  (\mathcal{F},\mathcal{V})
\end{equation}
with
\begin{align}
    \N(\mathcal{U},\mathcal{V}) & =\frac{2}{\re}(\varepsilon( \uu), \varepsilon( \vv)) + (\uu\cdot \grad \uu, \vv) + \gamma  (\div \uu, \div \vv) \\
    & - (p,\div \vv) + (\B \cross \Ee, \vv) + (\B \cross (\uu \cross \B), \vv) \\
    &- (q,\div \uu) \\
    &+ (\Ee, \FF) + (\uu\cross \B, \FF) - \frac{1}{\rem}(\B, \vcurl \FF) \\
    &+ \frac{1}{\rem} (\div \B, \div \C) + ( \vcurl \Ee, \C).
\end{align}
Recall, that it was important for the two-dimensional formulation that the electric and magnetic field belong to the same De Rham complex. This ensured among other things the well-posedness of the formulation and that the divergence constraint $\div \B=0$ is enforced exactly. In three dimensions the same properties hold since $\Ee$ and $\B$ now belong to the continuous exact De Rham sequence  
\begin{equation}
    \R \xrightarrow[]{\text{id}} \Hoz \xrightarrow[]{\mathbf{grad}} \hzcurl
    \xrightarrow[]{\vcurl} \hzdiv
    \xrightarrow[]{\div} L^2_0(\Omega) \xrightarrow[]{\text{null}} 0.
\end{equation}
For a structure-preserving discretisation we choose $\Ee$ and $\B$ from the discrete De Rham sequence 
\begin{equation}
    \R \xrightarrow[]{\text{id}}
    \mathbb{CG}_k \xrightarrow[]{\mathbf{grad}}
    \mathbb{NED}1_{k-1} \xrightarrow[]{\vcurl}
    \mathbb{RT}_{k-1} \xrightarrow[]{\div} \mathbb{DG}_{k-1} \xrightarrow[]{\text{null}} 0
\end{equation} 
or 
\begin{equation}
    \R \xrightarrow[]{\text{id}}
    \mathbb{CG}_k \xrightarrow[]{\mathbf{grad}}
    \mathbb{NED}2_{k-1} \xrightarrow[]{\vcurl}
    \mathbb{BDM}_{k-2} \xrightarrow[]{\div} \mathbb{DG}_{k-3} \xrightarrow[]{\text{null}} 0.
\end{equation}
Here, $\mathbb{NED}1_k$, $\mathbb{NED}2_k$  denote the \Ned elements of first \cite{Nedelec1980} and second kind \cite{Ndlec1986} and $\mathbb{BDM}_k$ the Brezzi-Douglas-Marini elements \cite{Brezzi1987} of degree $k$.
For more choices of stable finite element pairs and more information about the three-dimensional formulation see \cite{Hu2020}. As before, we discretise $\uu$ and $p$ with Scott-Vogelius elements $(\mathbb{CG}_k)^3\times \mathbb{DG}_{k-1}$ on barycentric refined grids. Note that we now have to choose $k\geq 3$ to ensure an inf-sup stable discretisation for the hydrodynamic block.

\subsection{Preconditioner for the 3d formulation}
%\red{Picard in 2d used 2d identity curl Laplace invers curl, What do numerical tests show in 3d? Is it the exact invers?}
The derivation of the block preconditioner from Section \ref{sec:derivationofblockpreconditioner} extends in a straight forward manner to three dimensions. As before, the challenge is to find robust solver for the electromagnetic block $\M$ and the Schur complement $\mathcal{S}_{MDP}$. We observe as in two dimensions that the additional term $\D$ from the Lorentz force doesn't degrade that performance of the multigrid solver a with macrostar relaxation method as described in \cite{Farrell2020}. Therefore, we use the same solver for the Schur complement $\mathcal{S}_{MDP}$. \\
Unfortunately, the monolithic multigrid solver for $\M$ from Section \ref{sec:solverformagneticblock} does not straightforward extend to three dimension for the Newton and minimal decoupling Picard linearisation. Numerical tests show that this solver only works robustly for magnetic Reynolds numbers up to 400. The main reason for this behaviour might be that the singular term $\vcurl(\uu^n\times \B)$ has a much larger kernel in three dimensions. Remember that this extra term does not appear in the Picard linearisation and therefore the monolithic multigrid method still converges well independent of $\rem$ in this case. But, as in two dimensions, the nonlinear iteration only converges for very small magnetic Reynolds numbers, which makes this linearisation infeasible. In the remainder of this section, we focus on the electromagnetic subsystem for the Newton and minimal decoupling Picard linearisation. We describe the approaches we have undertaken so far to handle this problem and summarise insightful numerical observation.\\
The problem for the electromagnetic block arising from the Schur Newton and minimal decoupling Picard linearisation is given by 
\begin{align}
\begin{split}\label{eq:EM3d}
\Ee -\frac{1}{\rem} \vcurl \B + \uu^n\cross \B &= \mathbf{0} \quad \text{ in } \Omega,\\
 \vcurl \Ee  -\frac{1}{\rem} \grad \div \B  &= \mathbf{0}  \quad \text{ in } \Omega,\\
 \Ee \times \n = \mathbf{0} ,\quad  \B \cdot \n &= 0 \quad \text{on } \partial\Omega.
 \end{split}
\end{align}
The weak formulation is to find $(\Ee, \B) \in \hzcurl \times \hzdiv$ such that for all $(\FF, \C) \in \hzcurl \times \hzdiv$ there holds
\begin{align}\label{eq:EM3dmixed}
\begin{split}
    (\Ee, \FF) - \frac{1}{\rem} (\vcurl \B, \FF) + (\uu^n\times \B, \FF ) &= 0, \\ 
    (\vcurl \Ee, \C) + \frac{1}{\rem}(\div \B, \div \C) &= 0 .
\end{split}
\end{align}
Note that \eqref{eq:EM3d} is the mixed formulation of (write $\Ee = \frac{1}{\mathrm{Rem}}\vcurl \B - \uu^n \times \B$)
\begin{align}\label{eq:EM3dVectorpoisson}
   \frac{1}{\mathrm{Rem}}( \vcurl \vcurl \B - \grad \div \B )- \vcurl(\uu^n \times \B) &= \mathbf{0}  \quad \text{ in } \Omega, \\
    \left(\frac{1}{\mathrm{Rem}}\vcurl \B - \uu^n \times \B\right) \times \n = \mathbf{0}, \quad \B \cdot \n &= 0 \quad \text{on } \partial \Omega.\label{eq:bcsBinH1n}
\end{align}
As already discussed in Section \ref{sec:AnaugmentedLagrangianFormulation}, we do not apply any form of stabilisation for high magnetic Reynolds numbers so far. We plan to investigate stabilisation techniques thoroughly in future work and hope that they improve the performance of the macrostar multigrid solver.

\subsection{Numerical results in 3d}
Due to the limitations of our workstation we don't  report numerical results for the whole three-dimensional system. Instead we focus on the electromagnetic subsystem \eqref{eq:EM3dmixed} since the performance of the solver mainly hinges on the robustness of the multigrid solver for this block. Therefore, we consider $\uu^n$ as a given, divergence-free function in $\mathbb{CG}_3^3$. The following numerical results are computed on $\Omega=[-1/2, 1/2]^3$ with an absolute and relative tolerance for the linear solver of $10
^{-8}$ and 10 GMRES iterations per relaxation sweep in the macrostar multigrid method. 

\subsubsection{Monolithic multigrid for the electromagnetic block}\label{sec:Monolithicmgforemblock}
In the first example, we consider the analytical solution
\begin{align}
\B(x,y,z) & = \exp(x+y+z) \begin{pmatrix}
\sin(z) -\sin(y) \\
\sin(x) - \sin(z) \\
\sin(y) -\sin(x) 
\end{pmatrix}, \label{eq:B3d}\\
\uu^n(x,y,z) & = (y, z, x)^\top, \\
\Ee(x,y,z) &= \frac{1}{\rem} \vcurl\B - \uu^n \times \B.
\end{align}
As in two dimensions, we apply a macrostar multigrid method monolithically to the electromagnetic block \eqref{eq:EM3d}. Table \ref{tab:Monolithic3d} shows that $\mathbb{NED}2_2 \times \mathbb{BDM}_1 $-elements perform better than $\mathbb{NED}1_2 \times \mathbb{RT}_2 $-elements, but only converge for $\rem$ up to 700. Monitoring the residuals of the linear solver, we observe that for $\rem=800$ and higher the relaxation method fails to make any progress per relaxation sweep.
\begin{table}[htbp!]
\centering
\begin{tabular}{cccccccc}
\midrule
Rem &   &      1 &      100 &     400&   500 &      700 &      800 \\
\midrule
$\mathbb{NED}2_2 \times \mathbb{BDM}_1 $  & &  2 &  2 &   2 &  2 &8 &     -   \\
$\mathbb{NED}1_2 \times \mathbb{RT}_2 $ &  & 2& 2 & 16& -& -& -\\
\midrule
\end{tabular}
\caption{Number of Krylov iterations for the monolithic multigrid solver applied to the electromagnetic block.}
\label{tab:Monolithic3d}
\end{table}

\subsubsection{Approximation with $\mathbb{CG}$-elements on convex domains}
In this test, we discretise formulation \eqref{eq:EM3dVectorpoisson} with $\mathbb{CG}_3^3$ elements to investigate if the bad performance of the macrostar multigrid method results from the mixed formulation with Raviart-Thomas or BDM and \Ned elements. Note that this approach only ensures a convergent finite element method on convex domains \cite[Section 2.3.2]{Arnold2010}, which is satisfied for our domain. For a weak formulation of \eqref{eq:EM3dVectorpoisson} we want to find a $\B \in \mathbf{H}^1_{\n}(\Omega)$ where
\begin{equation}
    \mathbf{H}^1_{\n}(\Omega) \coloneqq \left \{\B \in \Hov \, | \, \B\cdot \n=0 \text{ on } \del \Omega  \right\}
\end{equation}
such that 
\begin{equation}\label{eq:3dBinH1weak}
    \frac{1}{\rem} (\vcurl \B, \vcurl\C) + \frac{1}{\rem} (\div \B, \div \C) - (\uu^n\times \B, \vcurl \C) = 0 \quad \forall \, \C \in \mathbf{H}^1_{\n}(\Omega).
\end{equation}
We use the same analytical solution for $\B$ and $\uu^n$ as in the previous example. Moreover, we consider the velocity field
\begin{equation}
    \uu^n(x,y,z) = \frac{1}{\pi} \vcurl  \begin{pmatrix}
\sin(\pi(y+0.5)) \sin(\pi(z+0.5))\\
\sin(\pi(x+0.5)) \sin(\pi(z+0.5))\\
\sin(\pi(x+0.5)) \sin(\pi(y+0.5))
\end{pmatrix}
\end{equation}
which has the property that $\uu^n \cdot \n = 0$ on $\partial \Omega.$ For a comparison we also report iteration numbers for the full Dirichlet boundary conditions $\B=0 \text{ on } \partial \Omega$, i.e., we look for $\B \in \Hozv$. We observe that formulation \eqref{eq:3dBinH1weak} leads to oscillations near the boundary for high magnetic Reynolds numbers. However, we found numerically that these oscillations can be avoided by adding an augmented Lagrangian term $\gamma_2 \grad \div \B$ to \eqref{eq:EM3dVectorpoisson} and choose $\gamma_2=0.01$. \\
The iteration numbers for the macrostar multigrid solver are shown in Table \ref{tab:CGdiscr} and include two interesting points. Firstly, we observe that for non-homogeneous normal components of $\uu
^n$ the $\mathbb{CG}$-discretisation fails to converge in the same range of $\rem$
as the mixed formulation in the previous example. This might imply that the use of Raviart-Thomas or BDM and \Ned elements is not the primary cause for the bad convergence at high $\rem$. However, if $\uu^n\cdot \n =0$ the solver converges well for high $\rem$.
This might be due to the fact, that one can rewrite the boundary term \eqref{eq:bcsBinH1n} as
\begin{equation}
    (\uu^n \cross \B) \cross \n = (\uu^n \cdot \n) \B - (\B \cdot \n) \uu^n.
\end{equation}
Hence, the  velocity $\uu^n$ is eliminated from the boundary conditions in this case.
Secondly, we see good convergence for high Reynolds numbers for the full Dirichlet boundary condition $\B=0$ on $\partial \Omega$ independent of the boundary values of $\uu^n$.
\begin{table}[htbp!]
\centering
\begin{tabular}{ccccccc}
\midrule
$\uu^n$ & \multicolumn{6}{c}{$\mathrm{Rem}$} \\
              & & 1 & 500 &  1000 & 3000 & 5000\\
\midrule
\multicolumn{7}{c}{$\B \in \mathbf{H}^1_{\n}(\Omega)$}\\
\midrule  
$\uu^n\cdot \n \neq 0$& & 1 &2 & - & - & -\\
$\uu^n\cdot \n = 0$& &  2 & 3 & 4& 10 &  14 \\
\midrule
\multicolumn{7}{c}{$\B \in \Hozv$}\\
\midrule  
$\uu^n\cdot \n \neq 0$ & &1  & 2 & 3 & 4 & 5\\
$\uu^n\cdot \n = 0$&  &  1 & 3 & 5 & 11 &  28 \\
\midrule

\end{tabular}
\caption{Number of Krylov iterations for $\mathbb{CG}$-discretisations}
\label{tab:CGdiscr}
\end{table}

An interpretation of this test can be that the bad performance of the macrostar multigrid method is mainly caused by the choice of boundary conditions and the behaviour at the boundary. Moreover, we see that for the right boundary conditions the kernel of $\vcurl(\uu^n\times \B)$ can be captured by the macrostar multigrid method. Lastly, we want to mention that the convergence of the monolithic scheme applied to the mixed formulation from Section \ref{sec:Monolithicmgforemblock} does not improve for velocities that satisfy $\uu^n\cdot \n=0$.



%\begin{itemize}
%    \item $\B \in H^1_n$, if $\uu^n\times \n=0$, %everything works,
%    \item stabilisation
%    \item 3d results for lid driven cavity and an a Wathen problem
%    \item only 2by2 magnetic results
%    \item and 2by2 solver with magnetic lu solve
%\end{itemize}