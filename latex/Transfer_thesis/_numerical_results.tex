\section{Numerical Results in 2d}\label{sec:numericalresultsin2d}
%\begin{itemize}
%    \item maybe non-convex domain
%    \item Error robustness wrt Rem, Do we %have that?
%    \item Do we want to report robustness %wrt h? Take away would bex that smaller h %is good.
%    \item Numerical test for alternative boundary conditions
%\end{itemize}

In this section, we present numerical results for the Picard, minimal decoupling Picard and Newton linearisation described in the last section. We investigate three test problems that are given by (i) a smooth solution to verify our implementation and report the expected convergence orders, (ii) the Hartmann problem and (iii) a lid-driven cavity problem. The numerical results were measured on a compute cluster that consists of 72 2.3 GHz Intel Xeon cores and 755GB of RAM. 

\subsection{Algorithm details}\label{sec:algorithmdetails}
The algorithm is implemented in Firedrake \cite{rathgeber2016} and uses the solver packages PETSC \cite{balay2019} and PCPATCH \cite{farrell2019pcpatch}. The latter includes the implementation of the multigrid relaxation method described in Section \ref{sec:derivationofblockpreconditioner}. It is well-known that the convergence of the nonlinear scheme heavily depends on the initial guess and might fail to converge for high Reynolds numbers for poor initial guesses. To circumvent this problem we perform continuation in the Reynolds numbers. At first we apply continuation to $\rem$ in the range from 1 to 10000 with a step-size of 1000 for fixed $\Re=1$. We use each of these results as the starting point for the continuation in $\re$ with a step size of 1000. \\
We use flexible GMRES \cite{saad1993} as the outermost Krylov solver since we apply GMRES in the multigrid relaxation. Moreover, we apply a full block factorisation preconditioner
\begin{equation}
    \mathcal{P}=
    \begin{pmatrix}
    \I &-\tilde{\M}^{-1}\mathcal{K} \\
    \mathbf{0} &\I
    \end{pmatrix}
    \begin{pmatrix}
    \tilde{\M}^{-1} &\mathbf{0} \\
    \mathbf{0} &\tilde{\mathcal{S}_x}^{-1}
    \end{pmatrix}
    \begin{pmatrix}
    \I &\mathbf{0} \\
    -\mathcal{L}\tilde{\M}^{-1} &\I
    \end{pmatrix}
\end{equation}
 to \eqref{eq:matrix_EBup}, here denoted as $\begin{pmatrix} \M & \mathcal{K} \\ \mathcal{L} & \mathcal{N} \end{pmatrix}$.
The matrix $\tilde{\M}$ corresponds to the monolithic multigrid method described in Section \ref{sec:solverformagneticblock} applied to $\M$ and $\tilde{S}_x$ is the block preconditioner described in Section \ref{sec:solverforschurcomp} applied to the Schur complements $\mathcal{S}_x$ for the different linearisation methods. In both multigrid methods we use 6 GMRES iterations per relaxation sweep and the direct solver MUMPS \cite{MUMPS} to solve the problem on the coarsest grid. See Figure \ref{fig:solver_diagramm} for a graphical representation of the solver.
\begin{figure}[htb!]
	\centering
    %\includegraphics[scale=0.6]{IMG/solver_diagramm_fsfs.png}
    \resizebox{\textwidth}{!}{\input{IMG/solver_1.tikz}}
    \resizebox{\textwidth}{!}{\input{IMG/solver_2.tikz}}
    \resizebox{\textwidth}{!}{\input{IMG/solver_3.tikz}}
	\caption{Graphical outline of the solver}
	\label{fig:solver_diagramm}
\end{figure}

We have chosen a relative and absolute tolerance of $10^{-10}$ and $10^{-6}$ for the nonlinear solver and $10^{-7}$ and $10^{-7}$ for the linear solver. Moreover, we use $\mathbb{CG}^2_2 \times \mathbb{DG}_1$ elements for $(u,p)$ and $\mathbb{CG}_2 \times \mathbb{RT}_2$ elements for $(\E,\B)$. We also investigated the lower order elements $\mathbb{CG}_1$ and $\mathbb{RT}_1$ for $\E$ and $\B$, but observed much better linear solver robustness for the second order elements. All problems are posed over the domain $\Omega=[-1/2,1/2]^2$. For the multigrid hierarchy we use a coarse mesh of 20x20 cells and 2 level of refinements. When we consider a manufactured solution we always substract $\int_\Omega p \,\mathrm{d}x$ from the pressure to fix the average of $p$ to be 0. \\
All the  results from the previous sections have been formulated for homogeneous boundary conditions. The generalisation is straightforward for non-homogeneous boundary conditions. However, there occurs a difficulty for the implementation if one wants to enforce the divergent constraint $\div \B_h=0$ pointwise. As outlined in Section \ref{sec:DiscretisationandLinearisation}, strong boundary conditions are enforced in a finite element code by interpolating the given function on the boundary into the corresponding finite element space. If the interpolation of the boundary values $\mathbf{g}$ was exact, identity \eqref{eq:InterpolationPreserveRT} would imply that $\div \B_h=0$ holds. However, the degrees of freedom for the interpolation are usually implemented by a quadrature rule whose quadrature degree is based on the polynomial degree of the finite element space. If $\mathbf{g}$ is a non-polynomial expression, this quadrature rule might not interpolate the boundary condition exactly and therefore one looses the property that $\div \mathbf{g}_h=0$ on $\del \Omega$ holds exactly.\\
To circumvent this problem we use high-order quadrature rules for the evaluation of the degrees of freedom to ensure that the interpolation is exact up to machine precision.
In Table \ref{tab:divBQuadDeg} we have illustrated the effect of the quadrature degree on the enforcement of the divergence constraint. We have used the method of manufactured solutions for the problem from Section \ref{sec:verificationandconvergenceorder} to compute $\|\div \B_h\|_0$ for different quadrature degrees. Moreover, we have plotted the $L^2$-norm over $\partial \Omega$ of the interpolation of the divergence-free function $\B$ into the $\mathbb{RT}_2$ space. One can clearly observe that the standard quadrature degree of 2 for $\mathbb{RT}_2$ elements is not sufficient to enforce $\div \B_h=0$ up to machine precision. A higher quadrature degree preserves the divergence of the boundary data more accurately and leads to the point-wise enforcement of $\div \B_h=0$. 

\begin{figure}[htb!]
\centering
\includegraphics[width=0.5\textwidth]{IMG/divB_quad_deg.eps}
\caption{$L^2$-norm of the divergence of the solution $\B_h$ and the interpolant of the boundary condition for different quadrature degrees in the evaluation of the degrees of freedom for the Raviart-Thomas space.}
\label{tab:divBQuadDeg}
\end{figure}

\subsection{Verification and convergence order}\label{sec:verificationandconvergenceorder}
In the first example we consider the method of manufactured solution for a smooth given solution to verify the implementation of our solver and report convergence rates.
%We compute convergence rates and confirm that the divergence constraints $\div \uu_h =0$ and $\div \B_h=0$ are enforced exactly.
The right-hand side and boundary conditions are calculated corresponding to the analytical solution
\begin{align*}
  \uu(x,y) &= \begin{pmatrix}
xy\exp(x+y) + x\exp(x+y)\\
-xy\exp(x+y) - y\exp(x+y)
\end{pmatrix},\\
p(x,y) &= \exp(y)\sin(x),\\
\B(x,y) &= \begin{pmatrix}
\exp(x+y)\cos(x)\\
\exp(x+y)\sin(x) - \exp(x+y)\cos(x)
\end{pmatrix},\\
E(x,y) &= \sin(x).
\end{align*}
We have chosen a non-constant electric field $\E$ to be able to observe a convergence order for $\E$. Note that this results in non-zero right-hand sides in \eqref{eq:MHDFinal3} and \eqref{eq:MHDFinal4}. Nevertheless, the constraint $\div \B=0$ is still enforced on a continuous level for a right-hand side $\mathbf{f}_2 \in \hzdiv$ since we can test the equation
\begin{equation}
    (\vcurl \E, \C) + \frac{1}{\rem} (\div \B, \div \C ) = (\mathbf{f}_2, \C)
\end{equation}
with $\C=\vcurl \E - \mathbf{f}_2$. On a discrete level, one has to demand that $\mathbf{f}_{h,2} \in \W_h$. For a manufactured solution this can be fulfilled by setting
$\mathbf{f}_{h,2} = \vcurl(\I^h_{R_h} \E)$.\\
Based on the standard error estimates for $\mathbb{CG}$-, $\mathbb{DG}$- and $\mathbb{RT}$-spaces one would expect third order convergence in the $L^2$-norm for $\uu$ and $\E$ and second order convergence for $p$ and $\B$. This is asymptotically verified by Table \ref{tab:convOrder}. Here, Level 1 corresponds to a mesh with 10x10 cells and the subsequent level are obtained by doubling the number of cells in each direction.
%\red{Add table of $div(B_h)$ and $div(u_h)$ for different Re and Rem, after $div(u_h)$=0 enforcement works.}
\begin{table}[!htb]
\begin{centering}
\begin{tabular}{|c |c c|c c|c c|c c|} 
\hline
level & $\|\uu-\uu_h\|_0$ & order & $\|p-p_h\|_0$ & order & $\|\B-\B_h\|_0$ & order & $\|\E-\E_h\|_0$ & order  \\
\hline
1 & 3.86E-05 &  -   & 1.24E-04 &  -   & 7.63E-04 &  -   & 1.31E-06 &  -   \\
2 & 4.69E-06 & 3.04 & 3.16E-05 & 1.97 & 1.79E-04 & 2.09 & 1.64E-07 & 3.00 \\
3 & 5.70E-07 & 3.04 & 8.48E-06 & 1.90 & 4.49E-05 & 1.99 & 2.05E-08 & 3.00 \\
4 & 6.91E-08 & 3.04 & 2.34E-06 & 1.85 & 1.14E-05 & 1.99 & 2.57E-09 & 3.00 \\
\hline
\end{tabular}
\caption{$L^2$-error and convergence order for $\rem = 100$, $\re=100$.}
\label{tab:convOrder}
\end{centering}
\end{table}

\subsection{Lid-driven cavity}
Next, we consider a lid-driven cavity problem for a magnetic field $\B=(0,1)^\top$ and zero right-hand side $\f$. We impose the boundary condition $\uu=(1,0)^\top$ at the boundary $y=0.5$ and homogeneous boundary conditions elsewhere. The problem models the flow of a conducting fluid driven by the movement of the lid at the top of the cavity. The magnetic field imposed orthogonal to the lid creates a Lorentz force that perturbes the flow of the fluid. The iteration numbers for the three linearisation methods are presented in Table \ref{tab:ldc2d}.
\begin{table}[htbp!]\label{tab:ldc2d}
\centering
\begin{tabular}{ccccccccc}
\midrule
$\mathrm{Rem}$ & \multicolumn{7}{c}{$\mathrm{Re}$} \\
               & 1 & 1000 &  5000 & 10000 &20000 & 30000 &50000\\
\midrule
\multicolumn{8}{c}{Picard}\\
\midrule             
       1 & ( 2) 3.0 & ( 3) 2.7 & ( 2) 4.0 & ( 2) 5.0 & ( 2)11.0 & ( 2)14.0 & ( 2)18.5 \\  
      50 & (19) 1.6 & ( 4) 2.5 & ( 3) 3.7 & ( 2) 5.5 & ( 2)11.0 &    ( 2)13.5 & ( 2)17.5  \\
100 & - & - & - & - & - &- &-\\
\midrule
\multicolumn{8}{c}{Minimal decoupling Picard}\\
\midrule
       1 & ( 2) 3.0 & ( 3) 2.7 & ( 3) 3.3 & ( 2) 5.0 & ( 2)11.0 & ( 2)13.0 & ( 2)17.0 \\  
    1000 & ( 6) 2.5 & ( 8) 2.2 & (10) 2.9 & ( 4) 4.8 & ( 4) 7.0 & ( 5)11.8 & ( 6)27.3 \\
    5000 & ( 6) 2.8 & ( 9) 2.7 & ( 8) 4.1 & ( 5) 4.6 & ( 8) 5.5 & ( 6) 6.3 & (22)15.4 \\  
    7000 & ( 6) 2.8 & ( 9) 2.8 & ( 9) 4.0 & ( 5) 4.8 & ( 6) 5.3 & ( 5) 6.4 & ( 3) 8.7 \\  
   10000 & ( 9) 4.2 & ( 9) 3.1 & (11) 3.8 & ( 7) 4.3 & ( 8) 4.8 & ( 5) 6.4 & ( 3) 8.7 \\ 

\midrule
\multicolumn{8}{c}{Newton}\\
\midrule
       1 & ( 2) 3.0 & ( 3) 2.7 & ( 2) 4.0 & ( 2) 5.0 & ( 2)12.0 & ( 2)13.0 & ( 2)17.0 \\  
    1000 & ( 6) 2.5 & ( 3)10.0 & ( 3)11.7 & ( 2) 9.0 & ( 2)21.0 & ( 2)14.0 & ( 2)21.5 \\ 
    5000 & ( 9) 2.9 & ( 4)12.5 & ( 2)22.0 & ( 2)15.0 & ( 2)20.0 & ( 2)19.0 & ( 2)34.5 \\  
    7000 & ( 6) 2.8 & ( 4)12.2 & ( 2)28.0 & ( 2)19.5 & ( 2)22.0 & ( 2)20.0 & ( 2)15.5 \\ 
   10000 & ( 9) 4.2 & ( 3)14.3 & ( 3)26.3 & ( 2)27.5 & ( 3)26.7 & ( 2)22.5 & ( 2)17.5\\
\midrule
\end{tabular}
\caption{(Nonlinear iterations) Average outer Krylov iterations per Newton step for the lid-driven cavity problem. \blue{@Patrick: Do you prefer this table with Re up to 50000 or the previous version with Re up to 10000? In the latter case the numbers look more robust for minimal decoupling Picard.}}
\end{table}

For the Picard iteration we see that the nonlinear scheme already fails to converge for a magnetic Reynolds number of 100. The poor nonlinear convergence of the Picard iteration for high $\rem$ even with continuation was also observed for other formulations before \cite{PhillipsPHD,Phillips2014}. For high fluid Reynolds numbers the Picard iteration converges well and is robust up to $\re=10000$. This is due to the fact that we know what the outer Schur complement is and that we have robust solver for both the electromagnetic block and the outer Schur complement. For higher $\re$ the linear iterations grow slightly.\\
For the minimal decoupling Picard iteration we observe that the linear solver is robust with respect to both Reynolds numbers up to Re=10000 and increases for higher $\re$ slightly. This results similar to the Picard iteration from the fact that we have robust solver for the $(\E, \B)$-block and the outer Schur complement. But due to the drop of the matrix $\G$ from the nonlinear scheme one can see a slight increase in the number of nonlinear iterations.\\
For the Newton iteration the linear iterations increase since the approximation of the Schur complement $\mathcal{S}_N$ by $\mathcal{S}_{MDP}$ becomes less accurate for high Reynolds numbers. Although, here the number of nonlinear iterations stays fairly constant. Interestingly, the minimal decoupling Picard and Newton scheme show almost identical iterations number when either $\rem$ or $\re$ is low. Since the work per outer Krylov iteration is roughly the same for both linearisations the product of nonlinear and linear iterations is a good estimate for the computational cost and runtime. Based on the iterations numbers above the minimal decoupling Picard iteration should be approximately twice as fast for high magnetic Reynolds numbers which coincides with our time measurements. For example, the computation for $\rem=\re=10000$ took 3.22 min for the minimal decoupling Picard iteration and 6.61 min for the Newton scheme.

\begin{figure}[htbp!]
\centering
\begin{tabular}{cc}
\includegraphics[width=7cm]{IMG/ldc_5000_5000_u.png} &
\includegraphics[width=7cm]{IMG/ldc_5000_5000_B.png} \\
$u$ & $B$
\end{tabular}
\caption{Streamlines for the lid-driven cavity at $\mathrm{Rem} = 5000$, $\mathrm{Re}=5000$}
\label{fig:Streamlinesldc}
\end{figure}

\subsection{Hartmann Flow}\label{sec:HartmannFlow}
%\red{How do we deal with fact that Hartmann flow does not have rhs zero?}
Next, we consider the Hartmann flow problem that describes the flow of a conducting fluid through a section of a channel to which a transverse magnetic field is applied perpendicular to. The analytical solution is given by $\uu=(u_1(y), 0)^T$ and $\B=(B_1(y), 0)^T$ with
\begin{align*}
    u_1(y) &= \frac{G \Re}{2 \Ha \tanh(\Ha/2)}\left(1 - \frac{\cosh(y\Ha)}{\cosh(\Ha/2)} \right),\\
    B_1(y) &= \frac{G}{2}\left( \frac{\sinh(y/\Ha)}{\sinh(\Ha/2)} -2y \right), \\
    p(x,y) &= - Gx - \frac{B_1^2(y)}{2}.
\end{align*}
Here, we used the Hartmann number $\Ha=\sqrt{\re \rem}$ and $\mathrm{G} = \frac{2 \Ha \sinh(\Ha/2)}{\re(\cosh(\Ha/2)-1)}$ . The system is completed by computing $\E$ corresponding to \eqref{eq:MHDFinal3}. Note that for high $\mathrm{Ha}$ the computation of, e.g, $\sinh(\Ha/2)$ exceeds the range that double precision floating point number can represent. Therefore, we have chosen the following approximation for the implementation of Hartmann numbers $\Ha \geq \sqrt{5000}$
\begin{align*}
    u_1(y) &= \frac{G \re}{2 \Ha} \left(1 + \exp(\Ha(-y-\frac{1}{2})) - \exp(\Ha(y-\frac{1}{2}))  \right),\\
    B_1(y) &= \frac{G}{2} \left(\exp(\Ha(-y-\frac{1}{2})) - \exp(\Ha(y-\frac{1}{2}))  - 2y\right)
\end{align*}
with $G = 2 \frac{\Ha}{\re}$. The numerical results in Table \ref{tab:hartmann2d} show similar results to the the lid-driven cavity problem. But here, the Picard iteration already fails to converge for $\rem=50$. Another difference is that the Newton linearisation performs better for high Reynolds numbers with almost identical total cost as the minimal decoupling Picard iteration.
\begin{table}[htbp!]\label{tab:hartmann2d}
\centering
\begin{tabular}{ccccccc}
\midrule
$\mathrm{Rem}$ & \multicolumn{5}{c}{$\mathrm{Re}$} \\
               & 1 & 1000 &  5000 & 7000 & 10000\\
\midrule
\multicolumn{6}{c}{Picard}\\
\midrule  
1 & ( 3) 2.3 & ( 2) 2.5 & ( 3) 5.3 & ( 3) 6.0 & ( 2) 8.0 \\
10 & ( 4) 1.8 & ( 2) 3.0 & ( 5) 4.4 & ( 3) 5.3 & ( 2) 7.5 \\
50 & - & - & - &     -    &     -   \\ 
\midrule
\multicolumn{6}{c}{minimal decoupling Picard}\\
\midrule
       1 & ( 2) 1.0 & ( 3) 3.7 & ( 3) 6.3 & ( 3) 8.3 & ( 4)10.8 \\ 
    1000 & ( 7) 3.4 & ( 7) 4.6 & ( 5) 7.0 & ( 5) 9.0 & ( 5)12.4 \\  
    5000 & ( 9) 2.9 &  ( 6) 3.7 & ( 5) 8.0 & ( 5) 9.8 & ( 6)11.5 \\
    7000 & (11) 3.4 & ( 6) 3.8 & ( 6) 7.3 & ( 6) 9.2 & ( 7)10.4 \\  
   10000 & (14) 4.9 & ( 7) 5.4 & ( 7) 7.3 & ( 8) 8.0 & ( 9) 9.0 \\

\midrule
\multicolumn{6}{c}{Newton}\\
\midrule
       1 & ( 3) 2.7 & ( 2) 3.0 & ( 2) 6.5 & ( 3) 8.3 & ( 4)10.8 \\ 
    1000 & ( 3) 4.3 & ( 2) 3.5 & ( 3) 8.0 & ( 3) 9.3 & ( 4)12.5 \\ 
    5000 & ( 4) 8.8 & ( 3) 4.0 & ( 4) 7.8 & ( 4) 9.8 & ( 5)14.2 \\ 
    7000 & ( 4)12.0 & ( 3) 4.3 & ( 3) 9.0 & ( 4)10.8 & ( 6)12.7 \\  
   10000 & ( 4)16.5 & ( 4) 5.5 & ( 4) 8.2 & ( 7)11.6 & ( 7)13.0 \\
\midrule
\end{tabular}
\caption{(Nonlinear iterations) Average outer Krylov iterations per Newton step for the Hartmann problem}
\end{table}

To summarise the previous results, the Picard iteration is unsuitable for high magnetic Reynolds number, because the nonlinear iteration fails to converge. For small $\rem$ it provides a fluid Reynolds number robust solver. One might want to prefer to use the Picard iteration in this setting, since the robustness is build upon theoretical results. These include, for example, as previously outlined that the convergence of the nonlinear scheme and the robustness of the monolithic multigrid scheme for the electromagnetic block can be proved. Both the minimal decoupling Picard and Newton scheme perform well for high $\rem$ with the difference that the minimal decoupling Picard iteration provides a robust linear solver while the Newton scheme shows better nonlinear convergence. As the total cost can be lower for the minimal decoupling Picard iteration, as seen for the lid-driven cavity problem, this method shows overall the best results. 