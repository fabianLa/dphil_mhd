\chapter{Introduction}
\section{The Magnetohydrodynamics equations}
The magnetohydrodynamics (MHD) equations describe the flow of electrically conducting fluids in the presence of electromagnetic fields. They have numerous important applications in astrophysics, geophysics, the liquid metal industry and thermonuclear fusion. Mathematically, they are described by the Navier--Stokes and Maxwell's equations which are coupled through the Lorentz force and Ohm's law. The development of numerical methods is an area of active research and known to be very challenging due to the highly nonlinear character of the system and the strong coupling between the fluid and magnetic field.

The flow of conducting fluids can be mainly characterised by three dimensionless parameters which are called the fluid Reynolds number $\Re$, the magnetic Reynolds number $\Rem$ and the coupling number $S$. The size of these numbers has important consequences for the physical behaviour of the fluid and electromagnetic field, but also for the development of numerical methods. Low fluid Reynolds numbers describe laminar flow, which is characterised by a smooth and constant fluid motion due to the domination of viscous forces. On the other hand, high fluid Reynolds numbers model turbulent flow driven by inertial forces that exhibit eddies, vortices and other chaotic behaviour.
Small magnetic Reynolds number imply that the magnetic field is in a purely diffusive state and inhomogeneities in the field will be smoothed out. For high magnetic Reynolds numbers the magnetic field lines are mainly advected by the fluid flow. The size of the coupling number $S$ quantifies the strength of the coupling between the electromagnetic and hydrodynamic unknowns.

Most applications are in the regime of high Reynolds and coupling numbers and hence it is of great interest to build robust solvers that work in these parameter regimes.
For liquid metals, the fluid Reynolds number $\Re$ tends to be much larger than $\Rem$. For example, the flow of liquid mercury is characterized by a 
ratio of $10^7$ between these two constants; typical values in aluminium electrolysis are $\Rem = 10^{-1}$ and $\Re = 10^5$ \cite{Gerbeau2006}. High 
magnetic Reynolds numbers occur on large length scales, as in geo- and astrophysics. The magnetic Reynolds number of the outer Earth's core is in the range of $10^3$ and of the sun is in the range of $10^6$ \cite{Davies2015}. Magnetic Reynolds numbers between $10^1-10^3$ have been used in several dynamo experiments that investigate planetary magnetic fields \cite{Molokov2007}. The coupling number $S$ is around $10^0$ for aluminium electrolysis \cite{Gerbeau2006} and Armero \& Simo \cite{Armero1996} define strong coupling for $S$ in the range of $10^2-10^9$.

From a mathematical point of view, high Reynolds numbers imply a strong nonlinear coupling between the fluid and magnetic field, while singular terms start to dominate the equations. This setting demands the use of appropriate discretisations, linearisation schemes and globalisation techniques and the construction of highly specialised linear solvers that can capture the kernel of the singular terms.

\section{MHD models}
The MHD equations can be derived under specific assumptions from the Vlasov-Maxwell equations which provide a general kinetic description of collisionless plasmas.
These assumptions include that the considered fluid is quasi-neutral, the considered length scales are larger than the kinetic length scales such as the gyro radius or the skin depth and the considered time scales are shorter than the cyclotron frequencies of the ions and electrons. Moreover, displacement currents, electric forces, electron inertia and relativistic effects are neglected \cite{Gerbeau2006}. 

There exist several extensions or simplifications of these standard MHD models. Ideal MHD models \cite[Section 2.4.3]{Galtier2015} further neglect the resistivity of the fluid and hence assume that the fluid is perfectly conducting (which formally corresponds to an infinite magnetic Reynolds number). This results in the so-called frozen-in condition, where the magnetic field lines are tied to the fluid flow. Electron MHD models \cite[Section 2.4.2]{Galtier2015} describe the limit of small time and space scales where the velocity of the electron is much higher than the one of the ions. In this case, the ions build a neutralising background and the electric current is only carried by the electrons. The Hall MHD equations \cite[Section 2.4]{Galtier2015} which include an additional current term in Ohm's law are described in more detail in the next section. A further extension is given by multi-fluid models that track the separate movement of ions, electrons and neutral species. 
 
%Since the MHD equations are used to model a wide range of plasmas and fluids for different applications, there exists different standard MHD formulations. 
Depending on the application, the standard MHD equations can describe compressible or incompressible fluids and isothermal or anisothermal systems with homogeneous or spatially-varying physical parameters. In this work, we focus on incompressible, anisothermal MHD models that include visco-resistive effects for homogeneous physical parameters. The most common applications for these models are liquid metals, but, e.g., also the modelling of solar winds is well approximated in the incompressible regime since the occurring density fluctuations are usually small \cite[Section 2.4.1]{Galtier2015}. 

\section{Hall MHD equations}
The Hall MHD equations extend standard MHD models by including the so-called Hall effect.
This provides a more appropriate description of fully ionized plasmas on length scales close to or smaller than the ion skin depth \cite{Galtier2015}. On these length scales the Hall MHD equations take into account the different motions of ions and electrons in a two-fluid approach. While the electron motion is frozen to the magnetic field in this regime, it remains to solve for the ion fluid velocity $\u$ \cite{Huba2003}. 
The Hall MHD equations can be used to describe many important plasma phenomena, such as magnetic reconnection processes \cite{Forbes1991, Morales2005}, the expansion of sub-Alfv\'{e}nic plasma \cite{Ripin1993} and the dynamics of Hall drift waves and Whistler waves \cite{Huba2003}.  

The essence of the Hall effect is described by adding the Hall-term $\j \times \B$, the cross product of the current density and magnetic field, in the generalized Ohm's law \cite[Section 2.2.2]{Galtier2015}.
%\begin{equation}
%	\eta \j = \E + \u \times \B - \frac{1}{n e} \j \times \B
%\end{equation}
%where $\eta$ denotes the magnetic resistivity, $n$ the charge density and $e$ the electron charge. 
Hence, the Hall-MHD equations only differ by the Hall term $\RH\,\j \times \B$ with the Hall parameter $\RH$ from the standard MHD equations. Nevertheless, the extension of existing theory and algorithms for the standard MHD equations is highly non-trivial, since the Hall term represents the highest order term in the final system of equations given by $\RH \nabla \times ((\nabla \times \B)\times \B))$. Furthermore, the current density $\j$ cannot be eliminated with the help of Ohm's law and has to be kept in the formulation.

Several analytical results for the continuous Hall MHD problem \cite{Chae2014, Danchin2020} and computational results of physical simulations \cite{Gmez2008, Chacn2003, Tth2008} are available in the literature.

\section{Structure and contribution of thesis}
This thesis is split into two chapters. Chapter \ref{chap:2} deals with robust solvers for the $\B$-$\E$ formulation of the stationary and transient standard MHD equations.
The main contribution of this chapter is to provide block preconditioners with good convergence even at high Reynolds and coupling numbers. The performance relies on the following three (novel) approaches:
\begin{itemize}
	\item[1.)] We consider a fluid-Reynolds-robust augmented Lagrangian preconditioner for an $\hdiv\times L^2$-discretisation of the Navier--Stokes equations that relies on a specialized multigrid method.
	\item[2.)]  We introduce a new monolithic multigrid method for the electromagnetic block.
	\item[3.)]  We discover that using the outer Schur complement which eliminates the $(\u,p)$ block instead of the $(\B, \E)$ block has crucial advantages for ensuring robustness for high parameters.
\end{itemize}
Furthermore, we show that our preconditioners extend in a straightforward manner to time-dependent versions. This has the substantial advantage that the choice of the time-stepping scheme is no longer restricted by the ability to solve the linear systems. In particular, it allows the use of fully implicit methods for high Reynolds numbers and coupling parameters.

In Section~\ref{sec:MHDModel}, we introduce the MHD model that we mainly focus on in this work.
In Section~\ref{sec:discretization}, we derive an augmented Lagrangian formulation for \eqref{eq:MHD3d} and describe the finite element discretisation and linearisation schemes. In Section~\ref{sec:derivationofblockpreconditioner}, we introduce block preconditioners for these schemes, present a calculation of the corresponding (approximate) Schur complements and describe robust linear multigrid solvers for the different blocks. Numerical examples and a detailed description of the algorithm are presented in Section~\ref{sec:numericalresults}. 
\newline 


In Chapter \ref{chap:3}, we introduce structure-preserving finite element discretisations for the Hall MHD equations and derive conservative algorithms that preserve energy and helicity precisely on the discrete level in the ideal limit.
Here the main contribution includes the following results:
\begin{enumerate}
	\item[1.)] We provide a variational formulation and structure-preserving discretisation for the stationary and time-dependent Hall-MHD equations and prove a well-posedness results for a Picard type linearisation
	\item[2.)] We construct numerical schemes that preserve the energy, magnetic helicity and hybrid helicity precisely in the ideal limit of $\Re=\Rem=\infty$.
	\item[3.)] We investigate parameter-robust preconditioners and report corresponding iteration numbers
\end{enumerate}

In Section \ref{sec:vatiational-formulation}, we derive a variational formulation of the stationary Hall MHD system and prove the well-posedness of a Picard type iteration. In Section \ref{sec:timedepproblems}, we derive algorithms that preserve the energy, magnetic and hybrid helicity precisely in the ideal limit. We investigate an augmented Lagrangian preconditioner for the Hall MHD system in Section \ref{sec:ALP}. Finally, we present numerical results in Section \ref{sec:numericalresultsHall}, which include iterations numbers for a lid-driven cavity problem, the simulation of magnetic reconnection for an island coalescence problem and a numerical verification of the conservation properties for our algorithms in the ideal limit.

Finally, we outline in Chapter \ref{chap:4} how we plan to complete our current work for the submission of the final DPhil thesis.

\section{Notation}
Throughout this work, we assume that $\Omega\subset \R^d$, $d \in \{2,3\}$ is a bounded Lipschitz polygon or polyhedron that is simply connected in order to ensure that the de Rham complexes, considered later in this work, are exact. We use the convention that vector-valued functions and function spaces are denoted by bold letters. We use $(\cdot,\cdot)$ and $\|\cdot\|$ to denote the $L^2(\Omega)$ inner product and norm. The dual pairing between an $H^{-1}$ (with norm $\|\cdot\|_{-1}$) and $H^1$ (with norm $\|\cdot\|_{1}$) function is denoted as $\langle \cdot , \cdot \rangle$.
We define the function spaces
\begin{align}
    \Hoz &= \{v \in H^1(\Omega) \, | \, v = 0 \text{ on } \partial \Omega \}, \\
    \hzdiv &= \{\C \in \mathbf{L}^2(\Omega) \, | \, \div \C \in \mathrm{L}^2(\Omega), \, \C \cdot \n = 0 \text{ on } \partial \Omega\}, \\
    \hzcurl &= \{\F \in \mathbf{L}^2(\Omega) \, | \, \vcurl \F \in \mathbf{L}^2(\Omega), \, \F \times \n = \mathbf{0} \text{ on } \partial \Omega\}, \\
    \mathrm{L}^2_0(\Omega) &= \{q \in L^2(\Omega) \, | \, \int_{\Omega} q\,  \mathrm{d}x = 0 \}
\end{align}
where $\n$ denotes the unit outward normal vector on the boundary of $\Omega$. In some cases, we also denote $\Hoz$ as $H_{0}(\grad, \Omega)$. Further, we drop the domain $\Omega$ in the notation of the function spaces if it is obvious which domain we consider.

In our formulations, $\u:\Omega\to\R^3$ denotes the velocity, $p:\Omega\to\R$ the fluid pressure, $\j:\Omega \to \R^3$ the current density, $\B:\Omega\to\R^3$ the magnetic field and $\E:\Omega\to\R^3$ the electric field. Furthermore, $\Re$ denotes the fluid Reynolds number, $\Rem$ the magnetic Reynolds number, $S$ the coupling number, $\RH$ the Hall parameter, $\f: \Omega \to \R^3$ a source term and $\eps \u = \frac{1}{2} (\grad \u + \grad \u^\top )$ the symmetric gradient. We often use the notation $\bm \omega = \nabla \times \u$ for the vorticity.

We often use properties of the following continuous de Rham complexes in two and three dimensions 
\begin{equation}\label{eq:contDC2d}
	\R \xrightarrow[]{\text{id}} H_0(\mathrm{curl}, \Omega) \xrightarrow[]{\vcurl} \hzdiv \xrightarrow[]{\mathrm{div}} L^2_0(\Omega) \xrightarrow[]{\text{null}} 0,
\end{equation}
\begin{equation}\label{eq:contDC3d}
	\R \xrightarrow[]{\text{id}} H_{0}(\grad, \Omega) \xrightarrow[]{\mathrm{grad}} \hzcurl\xrightarrow[]{\vcurl} \hzdiv \xrightarrow[]{\mathrm{div}} L^2_0(\Omega) \xrightarrow[]{\text{null}} 0.
\end{equation}
De~Rham complexes are called exact if the kernel of an operator in the sequence is given by the range of the preceding operator, e.g., $\mathrm{range}\ \mathrm{grad} = \mathrm{ker} \vcurl$ or $\mathrm{range} \vcurl = \mathrm{ker}\ \mathrm{div}$. Both sequences are exact for the simply connected domains we consider.

For  numerical approximations, we use the finite element de~Rham sequences
\begin{equation}
	\label{eqn:derhamfe2d}
	\R\xrightarrow[]{\text{id}}  H^{h}_{0}(\scurl, \Omega) \xrightarrow[]{\vcurl} \Hhd \xrightarrow[]{\mathrm{div}}  L^{2}_{h}(\Omega)\xrightarrow[]{\text{null}} 0, 
\end{equation}
\begin{equation}
	\label{eqn:derhamfe}
		\R \xrightarrow[]{\text{id}}  H^{h}_{0}(\grad, \Omega) \xrightarrow[]{\mathrm{grad}}  \Hhc \xrightarrow[]{\vcurl} \Hhd \xrightarrow[]{\mathrm{div}}  L^{2}_{h}(\Omega) \xrightarrow[]{\text{null}}0, 
\end{equation}
to discretize the variables from above, where $H^{h}_{0}(D, \Omega)\subset H_{0}(D, \Omega), ~D = \grad, \curl, \operatorname{div}$ are conforming finite element spaces, see e.g.~Arnold, Falk, Winther
\cite{Arnold.D;Falk.R;Winther.R.2006a,Arnold.D;Falk.R;Winther.R.2010a},
Hiptmair \cite{Hiptmair.R.2002a}, Bossavit \cite{Bossavit.A.1998a} for
more detailed discussions on discrete differential forms. A concrete example for such  finite element de~Rham sequences are given by %of \eqref{eq:contDC3d} and \eqref{eq:contDC2d} are given by 
\begin{equation}\label{eq:deRhamdiscret2d}
	\mathbb{CG}_k \xrightarrow[]{\vcurl} \mathbb{RT}_k \xrightarrow[]{\mathrm{div}} \mathbb{DG}_{k-1} \xrightarrow[]{\text{null}} 0,
\end{equation} 
\begin{equation}\label{eq:deRhamdiscret3d}
	\mathbb{CG}_k  \xrightarrow[]{\mathrm{grad}} \mathbb{NED}1_k \xrightarrow[]{\vcurl} \mathbb{RT}_k \xrightarrow[]{\mathrm{div}} \mathbb{DG}_{k-1} \xrightarrow[]{\text{null}} 0.
\end{equation}
Here, $\mathbb{RT}_k$ denotes the Raviart--Thomas elements \cite{Raviart1977} of degree $k$, $\mathbb{NED}1_k$ the  \Ned elements of first kind \cite{Nedelec1980},  $\mathbb{CG}_k$ continuous Lagrange elements and $\mathbb{DG}_k$ discontinuous Lagrange elements.

 In the schemes presented in this work, we require that $\E_h, \j_h \in \Hhc$ and $\B_h \in \Hhd$, i.e.~that they are drawn from the same sequence. We denote the finite element spaces used for the velocity $\bm{u}_h$ and pressure $p_h$
by $\bm{V}_{h}$ and $Q_{h}$ respectively,
and assume that the choice is inf-sup stable
\cite{Girault.V;Raviart.P.1986a}.

We define the weak $\curl$-operator $\tilde\nabla_{h}\times  : [L^{2}(\Omega)]^{3}\to \Hhc$ by
$$
(\tilde \nabla_{h}\times \B_h, \k_{h})=(\B_h, \nabla\times \k_{h}), \quad\forall\, \k_{h}\in \Hhc.
$$
%If the hybrid/cross helicity conservation is not part of the concern, then one may choose the velocity $\u_{h}$ and the pressure $p_{h}$ independent of other variables. \\@Fabian: does this sound reasonable? Perhaps we do not mention the last sentence at all because all the schemes we present below require the variables to be in a same complex?}
%We regularly use the notation $\bm \omega = \nabla \times \u$.
We regularly use the generalised Gaffney inequality
\begin{equation}
	\|\B_h\|_{L^{3+\delta}} \leq \|\tilde{\nabla} \times \B_h\| + \|\nabla \cdot \B_h\| \quad \forall \ \B_h \in \Hhd
\end{equation}
for  $0\leq \delta \leq3$, where $\delta$ depends on the regularity of $\Omega$. For a proof, we refer to \cite[Theorem 1]{he2019generalized} and references therein. 

In two dimensions, there exist two different curl operators \mbox{given by}
\begin{equation}\label{eq:curl2d}
	\scurl \B = \del_x B_2 - \del_y B_1, \qquad  \vcurl E = \begin{pmatrix}
		\del_y E\\
		-\del_x E
	\end{pmatrix}
\end{equation}
that correspond to the cross-products
\begin{equation}\label{eq:cross2d}
	\u \times \B =u_1 B_2 - u_2 B_1, \qquad
	\B \times E =\begin{pmatrix}
		B_2 E \\
		-B_1 E
	\end{pmatrix}.
\end{equation}

The interpolant of a function $u$ into a finite element space $V_h$ with a set of degrees of freedom $\{{\ell_{h,i}(\cdot)}\}$ and basis functions $\{\varphi_i\}$ is represented by
\begin{equation}
	\II^h_{V_h}(u) = \sum_i \ell_{h,i}(u) \varphi_i.
\end{equation}
